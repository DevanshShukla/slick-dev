/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class coupon_methodsTest {

    static testMethod void testCoupon_Methods() {
        // TO DO: implement unit test

        product2 prod = new product2(name='test');
        insert prod;
        
        contact c = new contact(lastName='test', email='test@test.com');
        insert c;
        
        Slick__coupon__c coupon = new Slick__Coupon__c(Slick__customer__c = c.id, Slick__product__c = prod.id,
         Slick__discount_amount__c = 1, name='specialdogcoupon', Slick__coupon_type__c = 'Product Group', Slick__discount_type__c = 'Dollar Off', Slick__expiration_date__c = Date.parse('1/1/2222'));
        insert coupon;
        
         Slick__coupon__c coupon2 = new Slick__Coupon__c(Slick__customer__c = c.id, Slick__product__c = prod.id,
         Slick__discount_amount__c = 1, name='specialdogcoupon2', Slick__discount_type__c = 'Percentage Off', Slick__coupon_type__c = 'Specific Product', Slick__expiration_date__c = Date.parse('1/1/2222'), Slick__apply_automatically__c=true);
        insert coupon2;

        Slick__coupon__c coupon3 = new Slick__Coupon__c(Slick__customer__c = c.id, Slick__product__c = prod.id,
        Slick__discount_amount__c = 1, name='specialdogcoupon3', Slick__coupon_type__c = 'BOGO', Slick__discount_type__c = 'BOGO', Slick__expiration_date__c = Date.parse('1/1/2222'));
        insert coupon3;

        Slick__coupon__c coupon4 = new Slick__Coupon__c(Slick__customer__c = c.id, Slick__product__c = prod.id,
        Slick__discount_amount__c = 1, name='specialdogcoupon4', Slick__coupon_type__c = 'Product Family', Slick__discount_type__c = 'Product Family', Slick__Product_Family__c = 'Biscuits', Slick__expiration_date__c = Date.parse('1/1/2222'));
        insert coupon4;

        Slick__coupon__c coupon5 = new Slick__Coupon__c(Slick__customer__c = c.id, Slick__product__c = prod.id,
        Slick__discount_amount__c = 1, name='specialdogcoupon5', Slick__coupon_type__c = 'BOGO', Slick__discount_type__c = 'BOGO', Slick__expiration_date__c = Date.parse('1/1/2222'));
        insert coupon5;

        Slick__coupon__c coupon6 = new Slick__Coupon__c(Slick__customer__c = c.id, Slick__product__c = prod.id,
        Slick__discount_amount__c = 1, name='specialdogcoupon6', Slick__coupon_type__c = 'Product Group', Slick__discount_type__c = 'Product Family', Slick__Product_Family__c = 'Biscuits', Slick__Product_Group__c = 'Select Cold River Recipe with Salmon', Slick__expiration_date__c = Date.parse('1/1/2222'));
        insert coupon6;

        coupon_Methods.couponRow cr1 = new coupon_Methods.CouponRow(coupon4);
        cr1.generateDiscountString();

        coupon_Methods.couponRow cr2 = new coupon_Methods.CouponRow(coupon5);
        cr2.generateDiscountString();

        coupon_Methods.couponRow cr3 = new coupon_Methods.CouponRow(coupon6);
        cr3.generateDiscountString();
        
        List<WrapperClasses.cartItem> items = new LIst<WrapperClasses.cartItem>();
        items.add(new WrapperClasses.cartItem(prod, '2', 2, 2));
        //coupon_methods.autoApply(items);
        //coupon_methods.calculateDiscount(coupon, items);
        //coupon_methods.calculateDiscount(coupon2, items);
        //coupon_methods.calculateDiscount(coupon3, items);
        coupon_methods.couponDiscount(coupon.name, items, c.email);
        coupon_methods.couponDiscount(coupon2.name, items, c.email);
        coupon_methods.couponDiscount(coupon3.name, items, c.email);
        
        //coupon_methods.couponApplicable(coupon, items);
        //coupon_methods.couponApplicable(coupon2, items);
        //coupon_methods.couponApplicable(coupon3, items);
       
        //coupon_methods.couponValid(Coupon, items);
        //coupon_methods.couponValid(coupon2, items);
        //coupon_methods.couponValid(coupon3, items);
        
        coupon_methods.getCouponRecordTypes();
        coupon_methods.updateCouponUsage('specialdogcoupon', c.Id);
        
    }
    
    static testMethod void test_CouponEdit_Extension() {
        test.StartTest();
        contact c = new contact(lastname='last', firstname='first');
        insert c;
        
        Pagereference p = page.coupon_Edit;

        p.getparameters().put('RecordType',[select id from recordtype where name = 'Individual Coupon' and sobjecttype ='Slick__Coupon__c'].id);
        p.getparameters().put('customer',c.id);
        test.setCurrentPage(p);
        apexpages.standardcontroller scon = new apexpages.standardcontroller(new Slick__coupon__c());
        couponEdit_Extension ext = new couponEdit_Extension(scon);
    
        p.getparameters().put('RecordType',[select id from recordtype where name = 'Credit Coupon' and sobjecttype ='Slick__Coupon__c'].id);
        p.getparameters().put('customer',c.id);
        test.setCurrentPage(p);
        
        scon = new apexpages.standardcontroller(new Slick__Coupon__c());
        ext = new couponEdit_Extension(scon);
        
        p.getparameters().put('RecordType',[select id from recordtype where name = 'Referral Reward' and sobjecttype ='Slick__Coupon__c'].id);
        p.getparameters().put('customer',c.id);
        test.setCurrentPage(p);
        
        scon = new apexpages.standardcontroller(new Slick__Coupon__c());
        ext = new couponEdit_Extension(scon);
        
        p.getparameters().put('RecordType',[select id from recordtype where name = 'New Customer' and sobjecttype ='Slick__Coupon__c'].id);
        p.getparameters().put('customer',c.id);
        test.setCurrentPage(p);
        
        scon = new apexpages.standardcontroller(new Slick__Coupon__c());
        ext = new couponEdit_Extension(scon);
        
        p.getparameters().put('ctype',[select id from recordtype where name = 'Credit Coupon' and sobjecttype ='Slick__Coupon__c'].id);
        p.getparameters().put('customer',c.id);
        test.setCurrentPage(p);
        
        scon = new apexpages.standardcontroller(new Slick__Coupon__c());
        ext = new couponEdit_Extension(scon);
        
        p.getparameters().put('ctype',[select id from recordtype where name = 'Referral Reward' and sobjecttype ='Slick__Coupon__c'].id);
        p.getparameters().put('customer',c.id);
        test.setCurrentPage(p);
        
        scon = new apexpages.standardcontroller(new Slick__Coupon__c());
        ext = new couponEdit_Extension(scon);
        
        p.getparameters().put('ctype',[select id from recordtype where name = 'New Customer' and sobjecttype ='Slick__Coupon__c'].id);
        p.getparameters().put('customer',c.id);
        test.setCurrentPage(p);
        
        scon = new apexpages.standardcontroller(new Slick__Coupon__c());
        ext = new couponEdit_Extension(scon);
        
        p.getparameters().put('ctype',[select id from recordtype where name = 'Individual Coupon' and sobjecttype ='Slick__Coupon__c'].id);
        p.getparameters().put('customer',c.id);
        test.setCurrentPage(p);
        
        scon = new apexpages.standardcontroller(new Slick__Coupon__c());
        ext = new couponEdit_Extension(scon);

        p.getparameters().put('ctype','i');
        p.getparameters().put('customer',c.id);
        test.setCurrentPage(p);
        scon = new apexpages.standardcontroller(new Slick__Coupon__c());
        ext = new couponEdit_Extension(scon);

        p.getparameters().put('ctype','c');
        p.getparameters().put('customer',c.id);
        test.setCurrentPage(p);
        scon = new apexpages.standardcontroller(new Slick__Coupon__c());
        ext = new couponEdit_Extension(scon);

        p.getparameters().put('ctype','r');
        p.getparameters().put('customer',c.id);
        test.setCurrentPage(p);
        scon = new apexpages.standardcontroller(new Slick__Coupon__c());
        ext = new couponEdit_Extension(scon);

        p.getparameters().put('ctype','n');
        p.getparameters().put('customer',c.id);
        test.setCurrentPage(p);
        scon = new apexpages.standardcontroller(new Slick__Coupon__c());
        ext = new couponEdit_Extension(scon);
        
        coupon_Methods.ApplyCouponResult acr = new coupon_Methods.ApplyCouponResult();
        acr.postProcess();
        acr.generateAppliedCouponCodes();
        acr.generateDiscountStrings();
        
        coupon_Methods cm = new coupon_Methods();
        cm.demo();

        test.stopTest();
    }
}