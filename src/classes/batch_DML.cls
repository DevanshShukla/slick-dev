global class batch_DML implements Database.Batchable<sObject>, Database.Stateful {

	global final list<sObject> itemlist;
	global final boolean deleting;
	global batch_DML(list<sObject> scope, boolean del){
		itemlist = scope;
		deleting = del;
	}

	global Iterable<sObject> start(Database.batchableContext info){ 
       return itemlist; 
   	} 

   	global void execute(Database.batchableContext info, List<sObject> scope){
   		if(deleting) delete scope;
   		else upsert scope;
   	}

   	global void finish(Database.BatchableContext BC){
		
	}
   	
   	public static Id startBatch(list<sobject> input, boolean del){
		batch_DML job = new batch_DML(input,del);
		return database.executeBatch(job);
	}

	public static void scheduleBatch(list<sobject> input, boolean del){
		batch_DML job = new batch_DML(input,del);
		system.scheduleBatch(job, 'batch_DML '+system.now()+''+math.random()+''+system.now().addseconds(integer.valueof(10*math.random())), 0);
	}
}