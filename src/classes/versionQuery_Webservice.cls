@RestResource(urlMapping='/version/')
global class versionQuery_Webservice {
	    @HttpGet
        global static String getVersionInfo(){
                System.debug('Get major version ');
                Integer major = System.requestVersion().major();
                System.debug('major ' + major);
                Integer minor = System.requestVersion().minor();
                System.debug('minor ' + minor);
                String version = String.valueOf(major) + '.' + String.valueOf(minor);
                return version;
        }
}