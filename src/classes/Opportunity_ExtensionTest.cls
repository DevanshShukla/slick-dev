/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Opportunity_ExtensionTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
      
        Contact c = new Contact(firstname='test', lastname='test',mailingstreet='test');
        insert c;
        Slick__credit_card__c card=new Slick__credit_card__c(Slick__card_number__c='123456789123456',Slick__cvv__c='123',Slick__expiration_year__c='2020',Slick__expiration_month__c='01',Slick__name_on_card__c='tester test',Slick__contact__c=c.id);
        insert card;
        product2 prod = new product2(name='test dog food', Slick__cart_category__c='dogfood', Slick__cart_subcategory__c='dryfood',isactive=true);
        insert prod;       
        pricebookEntry pbe=new pricebookentry(product2id=Prod.id,pricebook2id=Test.getStandardPriceBookId(),unitprice=1,isactive=true);
        insert pbe;
        Opportunity opp = new Opportunity(name='test',Slick__delivery_date__c=date.today(),Slick__contact__c=c.id, amount=400, closeDate=system.today(), stageName='Closed Won');
        insert opp;
        apexpages.currentPage().getParameters().put('cid', c.id);
        
        Opportunity_Extension ext = new Opportunity_Extension(new Apexpages.standardController(opp));
        ext.shipToAddress='mailing';
        ext.billToAddress='mailing';
        ext.addProductsToOrder();
        ext.getItemsOnSale();
        ext.queryContact(c.Id);
        ext.searchProducts();
        ext.searchTerm='test';
        ext.searchProducts();
        ext.getavailableCoupons();
        ext.oppty.Slick__credit_card_used__c=card.id;
        ext.justSubmit();
        ext.submitandCharge();
        ext.submitandAuthorize();
        ext.updateShipToAddress();
        ext.updateBillToAddress();
         ext.shipToAddress='other';
        ext.billToAddress='other';
         ext.updateShipToAddress();
        ext.updateBillToAddress();
        ext.updateTotals();
    }

    static testMethod void myUnitTest2() {
        // unit test for address options
      
        Contact c = new Contact(firstname='test', lastname='test', OtherStreet='other street test');
        insert c;
        Slick__credit_card__c card=new Slick__credit_card__c(Slick__card_number__c='123456789123456',Slick__cvv__c='123',Slick__expiration_year__c='2020',Slick__expiration_month__c='01',Slick__name_on_card__c='tester test',Slick__contact__c=c.id);
        insert card;
        product2 prod = new product2(name='test dog food', Slick__cart_category__c='dogfood', Slick__cart_subcategory__c='dryfood',isactive=true);
        insert prod;       
        pricebookEntry pbe=new pricebookentry(product2id=Prod.id,pricebook2id=Test.getStandardPriceBookId(),unitprice=1,isactive=true);
        insert pbe;
        Opportunity opp = new Opportunity(name='test',Slick__delivery_date__c=date.today(),Slick__contact__c=c.id, amount=400, closeDate=system.today(), stageName='Closed Won');
        insert opp;
        apexpages.currentPage().getParameters().put('cid', c.id);
        
        Opportunity_Extension ext = new Opportunity_Extension(new Apexpages.standardController(opp));
        ext.shipToAddress='mailing';
        ext.billToAddress='mailing';
        ext.addProductsToOrder();
        ext.getItemsOnSale();
        ext.queryContact(c.Id);
        ext.searchProducts();
        ext.searchTerm='test';
        ext.searchProducts();
        ext.getavailableCoupons();
        ext.oppty.Slick__credit_card_used__c=card.id;
        ext.justSubmit();
        ext.submitandCharge();
        ext.submitandAuthorize();
        ext.updateShipToAddress();
        ext.updateBillToAddress();
         ext.shipToAddress='other';
        ext.billToAddress='other';
         ext.updateShipToAddress();
        ext.updateBillToAddress();
        ext.updateTotals();
        ext.getCreditCardList();
    }

    static testMethod void myUnitTest3() {
        // unit test for address options
      
        Contact c = new Contact(firstname='test', lastname='test', OtherStreet='other street test');
        insert c;
        Slick__credit_card__c card=new Slick__credit_card__c(Slick__card_number__c='123456789123456',Slick__cvv__c='123',Slick__expiration_year__c='2020',Slick__expiration_month__c='01',Slick__name_on_card__c='tester test',Slick__contact__c=c.id);
        insert card;
        product2 prod = new product2(name='test dog food', Slick__cart_category__c='dogfood', Slick__cart_subcategory__c='dryfood',isactive=true);
        insert prod;       
        pricebookEntry pbe=new pricebookentry(product2id=Prod.id,pricebook2id=Test.getStandardPriceBookId(),unitprice=1,isactive=true);
        insert pbe;
        Opportunity opp = new Opportunity(name='test',Slick__delivery_date__c=date.today(),Slick__contact__c=c.id, amount=400, closeDate=system.today(), stageName='Closed Won');
        insert opp;
        apexpages.currentPage().getParameters().put('cid', c.id);
        
        Opportunity_Extension ext = new Opportunity_Extension(new Apexpages.standardController(opp));
        ext.shipToAddress='mailing';
        ext.billToAddress='mailing';
        ext.addProductsToOrder();
        ext.getItemsOnSale();
        ext.queryContact(c.Id);
        ext.searchProducts();
        ext.searchTerm='test';
        ext.searchProducts();
        ext.getavailableCoupons();
        ext.oppty.Slick__credit_card_used__c=card.id;
        ext.justSubmit();
        ext.submitandCharge();
        ext.submitandAuthorize();
        ext.updateShipToAddress();
        ext.updateBillToAddress();
         ext.shipToAddress='other';
        ext.billToAddress='other';
         ext.updateShipToAddress();
        ext.updateBillToAddress();
        ext.updateTotals();
        ext.getCreditCardList();
        ext.dummy();

        Opportunity_Methods.reOrder(c.id);
        
    }
}