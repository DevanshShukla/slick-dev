@RestResource(urlMapping='/shipping/*')
global with sharing class shipping_Webservice {

  @HttpPost
  global static Decimal shippingCharge(Decimal subtotal, Decimal discount){

      cart_CheckoutController cartCheckCon= new cart_CheckoutController();
      cartCheckCon.cart_subTotal = subtotal;
      cartCheckCon.cart_Coupon_Discount = discount;

      cartCheckCon.calculateShipping();

      return cartCheckCon.cart_ShippingTotal;
  }

}