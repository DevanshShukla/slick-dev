@RestResource(urlMapping='/reorder/*')
global with sharing class reorder_Webservice {

    @HttpGet
    global static void getLastOrder(){
      String email = RestContext.request.params.get('email');
      List<Contact> customer = [SELECT Id, Name, Email FROM Contact WHERE Email =:email LIMIT 1];

      List<Opportunity> lastOrder = [SELECT Id, Applied_Coupon_Codes__c,Credit_Card_Used__c FROM Opportunity WHERE Contact__c =:customer[0].Id AND This_is_a_Return__c = false ORDER BY Order_Placed__c  DESC LIMIT 1];

      List<OpportunityLineItem> olis = [SELECT ProductCode, Quantity, Name From OpportunityLineItem WHERE OpportunityId = :lastOrder[0].Id];

      List<Credit_Card__c> ccList = [SELECT CVV__c, Expiration_Month__c, Expiration_Year__c, Last_4__c, Name_on_Card__c, Card_Type__c, Active__c FROM Credit_Card__c WHERE Id =:lastOrder[0].Credit_Card_Used__c];

      List<String> coupons = new List<String>();
      coupons.add(lastOrder[0].Applied_Coupon_Codes__c);

      Map<String, List<Object> > obj = new Map<String, List<Object> >();
      obj.put('items', olis);
      obj.put('customer', customer);
      obj.put('creditcard', ccList);
      obj.put('coupon', coupons);

      Map<String,Object> response = (Map<String,Object>)JSON.deserializeUntyped( JSON.serialize(obj) );
      Map<String,Object> responseCleaned = util.removeAttributes(response);
      RestContext.response.addHeader('Content-Type', 'application/json');
      RestContext.response.responseBody = Blob.valueOf(JSON.serializePretty(responseCleaned));
    }

}