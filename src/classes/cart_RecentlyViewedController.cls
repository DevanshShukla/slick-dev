public with sharing class cart_RecentlyViewedController {



	public string currentProduct {get;set;}

	public cart_RecentlyViewedController(){
		
	}
	
	public static void addToRecentViewed(string pid){
		string cookieval = util.loadCookie('recent');
		list<string> itemsViewed = new list<String>(); 
		set<string> viewedSet = new set<string>();
		itemsViewed.add(pid);
		viewedset.add(pid);
		if (cookieval != 'Not Found'){
			Integer itemCount = 1;
			for (string s : cookieval.split(';')){
				if (viewedset.contains(s)== false){
					itemsviewed.add(s);
					viewedset.add(s);
				}
				if(itemCount == 4) {break;}
				itemCount++;
			}
		}
		cookieval = '';
		
		for (string s : itemsViewed){
			cookieVal+= s+';';
		}
		
		util.saveCookie('recent',cookieval,2592000,false); 
	}
	
	public List<Product2> getRecentViewed(){
	
		list<Product2> recentViewed;
	
		if (string.isblank(currentProduct)) currentProduct ='';
		system.debug('**current product' + currentProduct + ' **');
			
		string cookieval = util.loadcookie('recent');
		list<string> itemsViewed = new list<String>();
		recentViewed = new list<Product2>();
		if (cookieVal != 'Not found'){ itemsViewed = cookieval.split(';');
			map<id,Product2> temp = new map<id,Product2>([select id, Slick__thumbnail_image__c, Slick__full_size_image__c, name from Product2 where id IN :itemsViewed AND id != :currentProduct]);
			for (string s : itemsViewed){
				if (temp.containskey(s)) recentViewed.add(temp.get(s));
			}
		}
	
		return recentViewed;	
	}
}