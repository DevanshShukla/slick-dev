/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class cart_productDetailControllerTest {

    static testMethod void testProductDetailController() {
        // TO DO: implement unit test
        
        product2 prod = new product2(name='test',Slick__active_in_cart__c=true);
        insert prod;
        
        pricebook2 pb2 = new pricebook2();
        pb2.Name = 'On Sale';
        insert pb2;
        
        pricebookentry pbe=new pricebookentry(product2id=Prod.id,pricebook2id=Test.getStandardPriceBookId(),unitprice=1,isactive=true);
        insert pbe;
        
        
        
        
        apexPages.currentPage().getParameters().put('id', prod.id);
        Cart_ProductDetailController con =new Cart_ProductDetailController();
        con.thisProduct = prod;
        con.onload(); 
        con.getProductCategories();
        con.getProductSubcategories();
        con.getProductTab();
        Cart_ProductDetailController.test();
        
    }
}