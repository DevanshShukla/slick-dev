@RestResource(urlMapping='/status')
global with sharing class status_Webservice  {

    @HttpGet
    global static String getStatus() {
        String response = 'Success';
        RestContext.response.addHeader('Content-Type', 'application/json');
        Return response;
    }
    
}