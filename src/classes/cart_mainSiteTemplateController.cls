public with sharing class cart_mainSiteTemplateController {

	Static List<Slick__Cart_Settings__c> cartSetting = [SELECT Id, Slick__Custom_Footer_HTML__c, Slick__Custom_Head_HTML__c FROM Slick__Cart_Settings__c LIMIT 1];
	Static Slick__Instance_Setting__c websiteTitle = Slick__Instance_Setting__c.getInstance('Website Title');
	Integer cartItems;
	Decimal cartTotal;
	public String cartEmailAddress;
	public socialMedia socialMediaObject;
	public String liveChatLicense;
	public boolean hideMaps;

	public map<String,Slick__Merge_Item__c> homePageMergeItems;
	public map<String,Slick__Merge_Item__c> siteWideMergeItems;

	public cart_mainSiteTemplateController(){
	}

	public String getCustomHeadHTML(){
		String res;
		if ( cartSetting.size() > 0 && String.isNotBlank(cartSetting[0].Slick__Custom_Head_HTML__c) ){
			res = cartSetting[0].Slick__Custom_Head_HTML__c;
		}
		return res;
	}

	public String getCustomFooterHTML(){
		String res;
		if ( cartSetting.size() > 0 && String.isNotBlank(cartSetting[0].Slick__Custom_Footer_HTML__c) ){
			res = cartSetting[0].Slick__Custom_Footer_HTML__c;
		}
		return res;
	}

	public String getWebsiteTitle(){
		return (websiteTitle != null && String.isNotBlank(websiteTitle.Slick__Text_Value__c)) ? websiteTitle.Slick__Text_Value__c : 'Nature\'s Select';
	}

	public integer getCartItems(){
		cartItems = 0;
		list<wrapperClasses.cartItem> cartContents = new list<wrapperClasses.cartItem>();

		string cartCookie = util.loadCookie('cartCookie');
		if (cartCookie != 'Not Found' && String.isnotblank(cartCookie)){
			cartContents = cart_CartController.parseCartContents(cartCookie);
		}
		else cartContents = new list<wrapperClasses.cartItem>();

		for(wrapperClasses.cartItem ci : cartContents){
			cartItems++;
		}
		return cartItems;
	}

	public decimal getCartTotal(){
		cartTotal = 0;
		list<wrapperClasses.cartItem> cartContents = new list<wrapperClasses.cartItem>();

		string cartCookie = util.loadCookie('cartCookie');
		if (cartCookie != 'Not Found'){
			cartContents = cart_CartController.parseCartContents(cartCookie);
		}
		else cartContents = new list<wrapperClasses.cartItem>();
		cart_CartController.populateProductInfo(cartContents);


		for(wrapperClasses.cartItem ci : cartContents){
			cartTotal += ci.litot;
		}
		return cartTotal;
	}

	public String getCartEmailAddress(){
		if(String.isblank(cartEmailAddress)){
			list<OrgWideEmailAddress> owe=[SELECT id, address FROM orgwideemailaddress WHERE displayname='Nature\'s Select'];
			if(owe.size()>0){
				cartEmailAddress=owe.get(0).address;
			}
		}
		return cartEmailAddress;
	}

	public list<SelectShopsMenuRow> getSelectShopsMenu(){
			//limit 3 items per shop.

		list<String> shopOrder=new list<String>();
		map<String,list<cart_NavController.MenuItemWrapper>> shopsMenuMap=new map<String,list<cart_NavController.MenuItemWrapper>>();

		list<SelectShopsMenuRow> selectShopsMenu=new list<SelectShopsMenuRow>();

		List<Schema.PicklistEntry> ples =Slick__Menu_Item__c.Slick__Shop__c.getDescribe().getPicklistValues();
		for(Schema.picklistEntry ple:ples)
			shopOrder.add(ple.getvalue());
		for(Slick__Menu_Item__c mItem:[
			SELECT id,Slick__webpage_url__c,Slick__description__c,Slick__shop__c,Slick__position__c, Slick__dropdown_menu__c, Slick__label__c,Slick__is_department__c,Slick__navigation_image_url__c,
				(
					SELECT id,Slick__dropdown_menu__c, Slick__webpage_url__c,Slick__label__c,Slick__description__c,Slick__is_Department__c,Slick__position__c,Slick__navigation_image_Url__c
					FROM subMenu_Items__r
					WHERE Slick__isInactive__c = false
					ORDER BY Slick__position__c asc
				)
			FROM Slick__menu_Item__c
			WHERE  Menu__r.Slick__isActive__c=true
			AND Slick__Shop__c!=null and Slick__isInactive__c=false
			ORDER BY Slick__position__c asc
		]){
			if(shopsMenuMap.containsKey(mItem.Slick__Shop__c)){
				if(shopsMenuMap.get(mItem.Slick__shop__c).size()<3)
					shopsMenuMap.get(mItem.Slick__shop__c).add(new cart_NavController.MenuItemWrapper(mItem));
			}
			else
				shopsMenuMap.put(mItem.Slick__Shop__c,new list<cart_NavController.MenuItemWrapper>{new cart_NavController.MenuItemWrapper(mItem)});
		}
		//fill empty slots
		for(String shop:shopOrder){
			if(!shopsMenuMap.containsKey(shop)){
				list<cart_NavController.MenuItemWrapper> emptyRow=new list<cart_NavController.MenuItemWrapper>{
					new cart_NavController.MenuItemWrapper(),new cart_NavController.MenuItemWrapper(),new cart_NavController.MenuItemWrapper()
				};
				shopsMenuMap.put(shop,emptyRow);
			}
			else{
				list<cart_NavController.MenuItemWrapper> val=shopsMenuMap.get(shop);
				if(val.size()<3){
					while(val.size()<3){
						val.add(new cart_NavController.MenuItemWrapper());
					}
				}
			}
		}
		integer count=0;
		SelectShopsMenuRow row=new SelectShopsMenuRow();
		selectShopsMenu.add(row);
		//2 headers per row
		for(String shop:shopOrder){
			if(shopsMenuMap.containsKey(shop)){
				if(count==2){
					row=new SelectShopsMenuRow();
					selectShopsMenu.add(row);
					count=0;
				}
				list<cart_NavController.MenuItemWrapper> mItems=shopsMenuMap.get(shop);
				row.menuItems.addall(mItems);
				row.shopHeaders.add(shop);
				count++;
			}

		}
		System.debug(selectShopsMenu);
		System.debug(selectShopsMenu.size());
		return SelectShopsMenu;

	}
	public Slick__Merge_Item__c getHomePageLeftAd(){
		return findMergeItem('Left Advert',getHomepageMergeItems());
	}
	public Slick__Merge_Item__c getHomePageRightAd(){
		return findMergeItem('Right Advert',getHomepageMergeItems());
	}
	public Slick__Merge_Item__c getHomePageCompareRecipies(){
		return findMergeItem('Compare Recipies',getHomepageMergeItems());
	}


	public Slick__Merge_Item__c getHomePageIconLink1(){return findMergeItem('Icon Link 1',getHomepageMergeItems());}
	public Slick__Merge_Item__c getHomePageIconLink2(){return findMergeItem('Icon Link 2',getHomepageMergeItems());}
	public Slick__Merge_Item__c getHomePageIconLink3(){return findMergeItem('Icon Link 3',getHomepageMergeItems());}
	public Slick__Merge_Item__c getHomePageSlide1(){return findMergeItem('Slide1',getHomepageMergeItems());}
	public Slick__Merge_Item__c getHomePageSlide2(){return findMergeItem('Slide2',getHomepageMergeItems());}
	public Slick__Merge_Item__c getHomePageSlide3(){return findMergeItem('Slide3',getHomepageMergeItems());}
	public Slick__Merge_Item__c getMainLogo(){return findMergeItem('Main Logo',getSiteWideMergeItems());	}
	public Slick__Merge_Item__c getPromoText(){return findMergeItem('PromoText',getSiteWideMergeItems());	}

	public Slick__Merge_Item__c findMergeItem(String name,map<String,Slick__Merge_item__c> mergeItemMap){
		if(mergeItemMap.containsKey(name))
			return mergeItemMap.get(name);
		else return new Slick__Merge_item__c();
	}

	public static String getAuthorizeNetSeal(){
		String sealId='460c82b9-d158-49b7-a2b2-a9aa573c1f8a';
		Slick__Instance_Setting__c sealSetting =
			test.isRunningTest()
			? ( new Slick__Instance_Setting__c(name='Authorize.net Seal Id', Slick__text_value__c = '460c82b9-d158-49b7-a2b2-a9aa573c1f8a') )
			: ( (Slick__Instance_Setting__c.getInstance('Authorize.net Seal Id'))
		);
		if( sealSetting != null && String.isnotblank(sealSetting.Slick__text_value__c)){
			sealId=sealSetting.Slick__text_value__c;
		}
		return sealId;
	}

	public Map<String,Slick__Merge_Item__c> getHomePageMergeItems(){
		if(homePageMergeItems==null){
			HomePageMergeItems=new map<String,Slick__Merge_Item__c>();
			for(Slick__Merge_item__c mItem: [SELECT id,name, Slick__URL__c,Slick__Image_URL__c,Slick__Body__c FROM Slick__merge_Item__c WHERE Slick__type__c='Home Page Item' OR Slick__type__c='Home Page Slider']){
				HomePageMergeItems.put(mItem.name,mItem);
			}
		}
		return homePageMergeItems;
	}
	public Map<String,Slick__Merge_Item__c> getSiteWideMergeItems(){
		if(SiteWideMergeItems==null){
			SiteWideMergeItems=new map<String,Slick__Merge_Item__c>();
			for(Slick__Merge_item__c mItem: [SELECT id,name, Slick__URL__c,Slick__Image_URL__c,Slick__Body__c FROM Slick__merge_Item__c WHERE Slick__type__c='Sitewide']){
				SiteWideMergeItems.put(mItem.name,mItem);
			}
		}
		return SiteWideMergeItems;
	}



	public class SelectShopsMenuRow{
		public list<cart_NavController.MenuItemWrapper> menuItems{get;set;}
		public list<String> shopHeaders{get;set;}
		public SelectShopsMenuRow(){
			menuItems=new list<cart_NavController.MenuItemWrapper>();
			shopheaders=new list<String>();
		}
	}



	public socialMedia getSocialMediaObject(){
		if(socialMediaObject==null){
			Slick__Instance_Setting__c socialMed = (test.isRunningTest())?
											(new Slick__Instance_Setting__c(name='Social Media Links', Slick__text_value__c = '{"facebook":"www.facebook.com","twitter":"www.twitter.com","instagram":"www.instagram.com","pinterest":"www.pinterest.com"}')):
											((Slick__Instance_Setting__c.getInstance('Social Media Links')));
			try{
				socialMediaObject=(SocialMedia)JSON.deserialize(socialMed.Slick__text_value__c, SocialMedia.class);
			}
			catch(exception e){
				system.debug(e);
				//ErrorLogger.logError(e);
				//socialMediaObject=new SocialMedia();
			}
		}
		return socialMediaObject;

	}
	public String getLiveChatLicense(){
		if(String.isBlank(liveChatLicense)){
			Slick__Instance_Setting__c liveChatli = (test.isRunningTest())?
											(new Slick__Instance_Setting__c(name='LiveChat License', Slick__text_value__c = '12345')):
											((Slick__Instance_Setting__c.getInstance('LiveChat License')));
			//if(liveChatLi!=null)
				//liveChatLicense=liveChatli.Slick__text_value__c;

		}
		return liveChatLicense;
	}
	public boolean getHideMaps(){
		if(hideMaps==null){
			Slick__Instance_Setting__c hideMapSetting = (test.isRunningTest())?
											(new Slick__Instance_Setting__c(name='Hide Maps', Slick__text_value__c = 'false')):
											((Slick__Instance_Setting__c.getInstance('Hide Maps')));
			if(hideMapSetting!=null && String.isnotblank(hideMapSetting.Slick__text_value__c))
				hideMaps=boolean.valueof(hideMapSetting.Slick__text_value__c);
		}
		return hideMaps;
	}

	public class SocialMedia{
		public String twitter{get;set;}
		public String facebook{get;set;}
		public String instagram{get;set;}
		public String pinterest{get;set;}
	}
    
    public void dummy(){
        Integer i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
    }
}