public class ProductDisplayController {
    @AuraEnabled
    public static Product2 getData(String ProductId){
        List<Product2> productList = new List<Product2>();
        productList = Database.query(Util.queryData('Product2','',' WHERE Id =: ProductId'));
        return productList[0];
    }

    @AuraEnabled
    public static List<Product2> getProductIds(){
        // return [ SELECT Id FROM Product2 LIMIT 5 ];
        List<Product2> productList = new List<Product2>();
        // productList = Database.query(Util.queryData('Product2','',' WHERE Id =: ProductId'));

        String ProductId = '01t3t000004h62c';
        return Database.query(Util.queryData('Product2','',' WHERE Id =: ProductId'));
        
        // return [ SELECT Id FROM Product2 WHERE Id = '01t3t000004h62c' ];
    }
}