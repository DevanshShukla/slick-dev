/*******************************************
 * Visualforce page: Website4
 * Description: 
 * 
*******************************************/
public class WebsiteController {
    
    // BANNER 
    public Boolean showBanner {get;set;}

    // LIST OF PRODUCTS
    public List<Product2> productList {get;set;}
    public List<Product2> productSearchList {get;set;}
    public List<Product2> quickShopList {get;set;}
    public List<Product2> featuredProductList {get;set;}
    public List<Product2> AllProduct {get;set;}
    
    // INITIALIZE ALL VARIABLES
    public void init(){
        
        showBanner = true;

        productList = new List<Product2>();
        productSearchList = new List<Product2>();
        quickShopList = new List<Product2>();
        featuredProductList = new List<Product2>();
        AllProduct = new List<Product2>();
    }

    // CONSTRUCTOR
    public WebsiteController(){
        init();
        productList = database.query(util.queryData('Product2','',' ORDER BY CreatedDate DESC LIMIT 10'));
        productSearchList.addAll(productList);

        // Check wether banner should be shown or not.
        // showBanner = Database.query(util.queryData('ObjName', 'parentFields', ' LIMIT 1 '));
    }

    // METHOD TO SEARCH PRODUCT
    public List<Product2> searchProduct(String Category){
        List<Product2> filteredCategoryProductList = new List<Product2>();
        for(Product2 pr : AllProduct){
            if(pr.Slick__Cart_Subcategory__c == Category){
                filteredCategoryProductList.add(pr);
            }
        }

        return filteredCategoryProductList;
    }

    // TO DISPLAY THE PRODUCT ON QUICK SHOP
    public void displayProduct(String Category, String section){
        
        // NEED TO DEFINE WHICH PRODUCT SHOULD SHOW UP ON THE QUICK SHOP SECTION
        // TBD...
        quickShopList = database.query(util.queryData('Product2','',' ORDER BY CreatedDate DESC LIMIT 10'));

        featuredProductList = database.query(util.queryData('Product2','',' ORDER BY CreatedDate DESC LIMIT 10'));
    }

    public void getAllProduct(){
        AllProduct = database.query(util.queryData('Product2','',' ORDER BY CreatedDate DESC LIMIT 10'));
    }
}