@isTest
private class cart_NavControllerTest {
	
	@isTest static void testCart_NavController() {
	    
	    Slick__Cart_Settings__c Cs = new Slick__Cart_Settings__c();
	    insert Cs;
	    
	    Slick__Menu__c M = new Slick__Menu__c();
	    M.Slick__Menu_Type__c  = 'Top';
	    M.Slick__isActive__c   = true;
	    M.Slick__Cart_Settings__c = Cs.Id;
	    insert M;
	    
	    Slick__menu_Item__c Mi = new Slick__menu_Item__c();
	    Mi.Slick__isInactive__c = false;
	    Mi.Slick__Menu__c = M.Id;
	    insert Mi;
	    
	   // Slick__subMenu_Item__c sM = new Slick__subMenu_Item__c();
	   // sM.Slick__isInactive__c=false;
	    
	    
	    
		test.startTest();
		cart_NavController con = new cart_NavController();
		con.currentDropdownMenu = M.Id;
		con.getTopBarMenu();
		con.getTopNavigation();
		cart_NavController.MenuItemWrapper menuitemw=new cart_NavController.MenuItemWrapper(new Slick__menu_Item__c());
		test.stopTest();
	}
	
	@isTest static void testCart_NavController1() {
	    
	    Slick__Cart_Settings__c Cs = new Slick__Cart_Settings__c();
	    insert Cs;
	    
	    Slick__Menu__c M = new Slick__Menu__c();
	    M.Slick__Menu_Type__c  = 'TopBar';
	    M.Slick__isActive__c   = true;
	    M.Slick__Cart_Settings__c = Cs.Id;
	    insert M;
	    
	    Slick__menu_Item__c Mi = new Slick__menu_Item__c();
	    Mi.Slick__isInactive__c = false;
	    Mi.Slick__Menu__c = M.Id;
	    insert Mi;
	    
	   // Slick__subMenu_Item__c sM = new Slick__subMenu_Item__c();
	   // sM.Slick__isInactive__c=false;
	    
	    
	    
		test.startTest();
		cart_NavController con = new cart_NavController();
		con.currentDropdownMenu = M.Id;
		con.getTopBarMenu();
		con.getTopNavigation();
		con.generateDropDownMenu(M.Id);
		cart_NavController.MenuItemWrapper menuitemw=new cart_NavController.MenuItemWrapper(new Slick__menu_Item__c());
		test.stopTest();
	}
	
	
	
	
	
}