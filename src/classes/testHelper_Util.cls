public with sharing class testHelper_Util {

	public static Slick__Instance_Setting__c insertCutoffTimeCustomSetting(){
		Slick__Instance_Setting__c i = new Slick__Instance_Setting__c(name='Order Cutoff Time', Slick__text_value__c = '20');
		insert i;
		return i;
	}
}