public without sharing class contact_Methods {

	private static final integer defaultFrequency = retrieveDefaultFrequency();

	public static void beforeInsert(list<Contact> newlist){
		for(Contact c : newlist){
			if (c.Slick__frequency__c == null) c.Slick__frequency__c = defaultFrequency;
		} 
	}
	public static void afterInsert(list<contact> newlist){
		//default courier to route
		list<Contact> contactUpdates=new list<Contact>();
		set<string> potentialZips=new set<String>();
		for(Contact c : newlist){
			if(c.Slick__route__c==null){
				Set<String> currentPerms=opportunity_Methods.getZipPermutations(c.mailingPostalCode);
				if(currentPerms!=null)
					potentialZips.addAll(currentPerms);
				contactUpdates.add(new contact(id=c.id,mailingPostalCode=c.mailingPostalCode));	
			}
		}
	
		if(contactUpdates.size()>0){
			list<Slick__route__c> courierRoutes=[select id from Slick__Route__c where name ='Courier'];
			Id courierRouteId;
			if(courierRoutes.size()>0) courierRouteId=courierRoutes.get(0).id;
			
				map<String,id> zipMap=new map<String,id>();
				for(Slick__Zip_Code__c zipRecord: [select id, name, route__r.id from Slick__Zip_Code__c where name in :potentialZips and Slick__route__c!=null]) zipMap.put(zipRecord.name,zipRecord.route__r.id);
				
				for(contact c:contactUpdates){
					set<String> currentPerms=opportunity_Methods.getZipPermutations(c.mailingPostalCode);
					Id matchingRouteId;
					String zipPermMatch;
					if(currentPerms!=null){
						//Matching exact zipcode or longest length match to match on potential zip+4 zips(if +4 entered, and no +4 in database, OR if regular zip entered but no regular zip in database)
						for(String zipPerm:currentPerms){
							if(zipMap.containsKey(zipPerm)){
								if(matchingRouteId==null){
									zipPermMatch=zipPerm;
									matchingRouteId=zipMap.get(zipPerm);
									//exact match
									if(zipPerm==c.mailingPostalCode) break;
								}
								else if(zipPerm.length()>zipPermMatch.length()){
									zipPermMatch=zipPerm;
									matchingRouteId=zipMap.get(zipPerm);
								}						
							}
						}
					}
					if(matchingRouteId!=null)
						c.Slick__route__c=matchingRouteId;		
					else
						c.Slick__route__c=courierRouteId;			
				}					
			update contactUpdates;
		}

	}

	public static void beforeUpdate(map<id,Contact> oldmap, list<Contact> newlist){
		map<id,Contact> contactFieldMap = new map<id,Contact>([select id, Slick__First_Date_Ordered__c, Slick__Last_Date_Ordered__c, Slick__First_Delivery_Date__c, Slick__Last_Delivery_Date__c, Slick__frequency__c, Slick__next_order_Date__c from Contact where id IN :oldmap.keyset()]);
        
		for (Contact c : newlist){
			//verifies that the new last and first order dates really are more accurate
			if (oldmap.get(c.id).Slick__First_Date_Ordered__c != null && (oldmap.get(c.id).Slick__First_Date_Ordered__c != null && oldmap.get(c.id).Slick__First_Date_Ordered__c < c.Slick__First_Date_Ordered__c)) c.Slick__First_Date_Ordered__c = oldmap.get(c.id).Slick__First_Date_Ordered__c; 
			if (oldmap.get(c.id).Slick__Last_Date_Ordered__c != null && (oldmap.get(c.id).Slick__Last_Date_Ordered__c != null && oldmap.get(c.id).Slick__Last_Date_Ordered__c > c.Slick__Last_Date_Ordered__c)) c.Slick__Last_Date_Ordered__c = oldmap.get(c.id).Slick__Last_Date_Ordered__c;
			
			if (oldmap.get(c.id).Slick__First_Delivery_Date__c != null && (oldmap.get(c.id).Slick__First_Delivery_Date__c != null && oldmap.get(c.id).Slick__First_Delivery_Date__c < c.Slick__First_Delivery_Date__c)) c.Slick__First_Delivery_Date__c = oldmap.get(c.id).Slick__First_Delivery_Date__c;

			/*if (oldmap.get(c.id).Slick__Last_Delivery_Date__c != null && (oldmap.get(c.id).Slick__Last_Delivery_Date__c != null && oldmap.get(c.id).Slick__Last_Delivery_Date__c > c.Slick__Last_Delivery_Date__c)){
				c.Slick__Last_Delivery_Date__c = oldmap.get(c.id).Slick__Last_Delivery_Date__c;
			} */

			//sets next delivery date based on frequency
			//badd allow contact to frequency field
			if (contactFieldMap.get(c.id).Slick__frequency__c ==null || c.Slick__Frequency__c==null) c.Slick__frequency__c = defaultFrequency;

			//else c.Slick__frequency__c = contactFieldMap.get(c.id).Slick__frequency__c;
			if (c.Slick__Last_Delivery_Date__c != null &&  c.Slick__Last_Delivery_Date__c!=oldmap.get(c.id).Slick__Last_delivery_date__c) c.Slick__next_order_Date__c = c.Slick__Last_Delivery_Date__c.adddays(integer.valueof(7*c.Slick__frequency__c));
		}
	}

	static integer retrieveDefaultFrequency(){
		return (test.isRunningTest())?5:integer.valueof(Slick__Instance_Setting__c.getInstance('Default Frequency').Slick__text_value__c);
	}
}