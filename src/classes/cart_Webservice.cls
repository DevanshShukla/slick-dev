@RestResource(urlMapping='/cart/*')
global with sharing class cart_Webservice {

    @HttpPost
    global static void getCart(){
      String bodyStr = RestContext.request.requestBody.toString();
      System.debug(bodyStr);

      wrapperClasses.checkoutWrapper checkoutData = (wrapperClasses.checkoutWrapper)JSON.deserialize(bodyStr,wrapperClasses.checkoutWrapper.class);
      System.debug(checkoutData.authTransaction);
      System.debug(checkoutData.products);

      Contact newOrOldCustomer = new Contact();

      //search for current customer
      //List<Contact> customer = [SELECT Id, Slick__Customer_Coupon__c, AccountId FROM Contact WHERE Email =:checkoutData.email];
      List<Contact> customer = [SELECT Id, AccountId FROM Contact WHERE Email =:checkoutData.email];

      if (customer.size() > 0){
          newOrOldCustomer = customer[0];
      }
      else {
          newOrOldCustomer = opportunity_Methods.checkoutWrapperToContact(checkoutData);
          insert newOrOldCustomer;
      }

      System.debug(newOrOldCustomer);

      //pass opportunity values into wrapper
      Opportunity order_oppty=opportunity_Methods.checkOutWrapperToOpportunity(checkoutData);



      //other fields not in opportunity methods, prevent website from breaking
      order_oppty.Slick__Contact__c = newOrOldCustomer.Id;
      order_oppty.AccountId = newOrOldCustomer.AccountId;
      order_oppty.Amount=checkoutData.amount;
      order_oppty.Slick__SubTotal__c = checkoutData.amount - checkoutData.couponDiscount;
      order_oppty.Slick__Coupon_Amount__c=checkoutData.couponDiscount;
      order_oppty.Slick__Tax_Percentage__c=checkoutData.taxRate;
      order_oppty.Slick__Tax__c=checkoutData.tax;
      order_oppty.Slick__Sales_Tax_Region__c = checkoutData.taxDescription;
      order_oppty.Slick__Shipping_Cost__c=checkoutData.shippingTotal;
      order_oppty.Slick__Charge_Amount__c= checkoutData.orderTotal;
      order_oppty.CloseDate=date.today();
      order_oppty.stagename='Closed Won';
      order_oppty.name='WebOrder';
      order_oppty.Slick__Coupon_Amount__c=checkoutData.couponDiscount;
      order_oppty.Slick__Order_Placed_Method__c='NS Storefront';

      Savepoint sp;

      

      

      //Add auth.net transaction to opportunity if payment method is not by check
      if (checkoutData.PaymentType != 'Check' && checkoutData.PaymentType != 'Check (Paper)'){
          String lastFour = checkoutData.CreditCardNumber.right(4);

          Slick__Credit_Card__c ccUsed;
          List<Slick__Credit_Card__c> cards = [SELECT Id 
                                        FROM Slick__Credit_Card__c 
                                        WHERE Slick__Contact__c = : newOrOldCustomer.Id 
                                              AND Slick__Last_4__c = : lastFour 
                                              AND Slick__Expiration_Month__c = : checkoutData.CreditCardExpMonth
                                              AND Slick__Expiration_Year__c = : checkoutData.CreditCardExpYear];

          if(cards.size() > 0){
            ccUsed = cards[0];
          }else{
            ccUsed = new Slick__Credit_Card__c(
              Slick__Contact__c = newOrOldCustomer.Id,
              Slick__Card_Number__c = checkoutData.CreditCardNumber,
              Slick__Expiration_Month__c = checkoutData.CreditCardExpMonth,
              Slick__Expiration_Year__c = checkoutData.CreditCardExpYear,
              Slick__Card_Type__c = checkoutData.CreditCardType,
              Slick__Name_on_Card__c = checkoutData.NameOnCard,
              Slick__CVV__c = checkoutData.CreditCardSecurityCode,
              Slick__Billing_City__c = checkoutData.billingCity,
              Slick__Billing_Postal_Code__c = checkoutData.billingZip,
              Slick__Billing_State__c = checkoutData.billingState,
              Slick__Billing_Street__c = checkoutData.billingAddress,
              //Slick__Active__c = true,
              Slick__Last_4__c = lastFour,
              Name = checkoutData.NameOnCard + ' ' + checkoutData.CreditCardType + ' ending in ' + lastFour

            );

            insert ccUsed;
          }

          order_oppty.Slick__Credit_Card_Used__c = ccUsed.Id;

          insert order_oppty;



          Slick__Authorize_net_Transaction__c authTrans = new Slick__Authorize_net_Transaction__c(
             Slick__Order__c = order_oppty.Id,
             Slick__Amount__c = checkoutData.authtransaction.Amount,
             Slick__Authorization_Cod__c = checkoutData.authtransaction.AuthorizationCode,
             Slick__Transaction_Id__c = checkoutData.authtransaction.TransactionId,
             Slick__Transaction_Status__c = checkoutData.authtransaction.TransactionStatus,
             Slick__Transaction_Time__c = Datetime.parse(checkoutData.authtransaction.TransactionTime),
             Slick__Transaction_Type__c = checkoutData.authtransaction.TransactionType,
             Slick__CC_Num__c = checkoutData.authtransaction.CreditCardNumber,
             Slick__CC_CVV__c = checkoutData.authtransaction.CreditCardSecurityCode,
             Slick__CC_Expiration__c = checkoutData.authtransaction.CreditCardExp,
             Slick__First_Name_on_Card__c = checkoutData.authtransaction.FirstNameOnCard,
             Slick__Last_Name_on_Card__c = checkoutData.authtransaction.LastNameOnCard
          );

          if(authTrans.Slick__Transaction_Type__c == 'authCaptureTransaction'){
            authTrans.Slick__Transaction_Type__c = 'auth_capture';
            authTrans.Slick__Payment_Number__c = 1.1;
          }else if(authTrans.Slick__Transaction_Type__c == 'authOnlyTransaction'){
            authTrans.Slick__Payment_Number__c = 1.2;
            authTrans.Slick__Transaction_Type__c = 'auth_only';
          }

          


          authTrans.Slick__Credit_Card__c = ccUsed.Id;
          
          //update order_oppty;

          insert authTrans;

          



      }else{
        insert order_oppty;
      }

      String orderNumber = [select id, Slick__order_number__c from Opportunity where id = :order_oppty.id].Slick__order_number__c;

      //Add product line items to opportunity
      Map<String, Object> productCodesQuantity = new Map<String, Object>();
      List<String> productCodes = new List<String>();

      for (wrapperClasses.Item obj: checkoutData.products){
          productCodes.add(obj.productcode);
          productCodesQuantity.put(obj.productcode, obj.qty);
      }
      List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
      List<PriceBook2> pb = [SELECT Id, Name from PriceBook2 WHERE Name = 'Standard Price Book'];

      for (PriceBookEntry pbe : [SELECT Id, UnitPrice, Product2Id, ProductCode FROM PriceBookEntry WHERE ProductCode IN :productCodes AND PriceBook2Id = :pb[0].Id] ){
          OpportunityLineItem oli = new OpportunityLineItem(
                Product2Id = pbe.Product2Id,
                OpportunityId = order_oppty.Id,
                PricebookEntryId = pbe.Id,
                Quantity = (Decimal)productCodesQuantity.get(pbe.ProductCode),
                UnitPrice = pbe.UnitPrice
          );
          oliList.add(oli);
      }
      insert oliList;
      System.debug(oliList);

      //increment coupon use
      coupon_Methods.updateCouponUsage(order_oppty.Slick__Applied_Coupon_Codes__c, order_oppty.Slick__Contact__c);

      Opportunity confirmationOppty=new Opportunity(id=order_Oppty.id,Slick__confirmation_sent__c=dateTime.now());
      update confirmationOppty;
      
      Map<String,String> responseObj = new Map<String,String>();
      responseObj.put('orderNumber', orderNumber);
      responseObj.put('orderOpptyId', order_oppty.id);

      String response = JSON.serialize(responseObj);
      RestContext.response.addHeader('Content-Type', 'application/json');
      RestContext.response.responseBody = Blob.valueOf(response);

    }

}