/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Opportunity_MethodsTest {

    static testMethod void Opportunity_methods_test() {
        // TO DO: implement unit test
        Slick__sales_tax__c sTax= new Slick__Sales_Tax__c(Slick__tax_state__c = 'CA',Slick__tax_percentage__c=20);
        insert sTax;
        Slick__route__c rt=new Slick__route__c(name='test route',Slick__Day__c='Two Business Days');
        insert rt;
       	Slick__zip_Code__c ziprec= new Slick__Zip_Code__c(name='90019',Slick__sales_tax__c=sTax.id,Slick__route__c=rt.id);
        insert ziprec;


        
        Opportunity_Methods.calculateDeliveryDates('90019');
        Opportunity_methods.calculateShippingDate('Next Business Day', system.today());
        Opportunity_methods.calculateShippingDate('Two Business Days', system.today());
        Opportunity_methods.calculateShippingDate('Three Business Days', system.today());
        Opportunity_methods.calculateTax('90019',50);
        Opportunity_Methods.getIncrementalDateOptions(system.today(), 1);
        Opportunity_Methods.getNext7DayOfWeek(system.today(), 'Tuesday', 5);
        Opportunity_Methods.getNextAvailableDate(system.today(), 2, 3);
        Opportunity_Methods.getTaxState('CA');
        Opportunity_Methods.getTaxById(sTax.id);


        Opportunity_Methods.calculateDeliveryDates(rt);
        rt.Slick__Day__c='Next Business Day';
        update rt;
        Opportunity_Methods.calculateDeliveryDates(rt);
        rt.Slick__Day__c='Two Business Days';
        update rt;
        Opportunity_Methods.calculateDeliveryDates(rt);
        rt.Slick__Day__c='Three Business Days';
        update rt;
        Opportunity_Methods.calculateDeliveryDates(rt);
        rt.Slick__Day__c='Same Business Day';
        update rt;
        Opportunity_Methods.calculateDeliveryDates(rt);
        Opportunity_Methods.getRoute('90019');
        Opportunity_Methods.getRoute('90019-1234');
        Opportunity_Methods.getnextNdayOfWeek(dateTime.now(),'monday',1,3);

        wrapperClasses.checkoutWrapper checkW=new wrapperClasses.checkoutWrapper();
        Opportunity_Methods.checkoutWrapperToContact(checkW);
        checkW.useShipToAddress=true;
        Opportunity_Methods.checkoutWrapperToContact(checkW);



        product2 prod = new product2(name='test dog food', Slick__cart_category__c='dogfood', Slick__cart_subcategory__c='dryfood');
        insert prod;
        
        pricebookEntry pbe=new pricebookentry(product2id=Prod.id,pricebook2id=Test.getStandardPriceBookId(),unitprice=1,isactive=true);
        insert pbe;
        Slick__Route__c r=new Slick__route__c(name='Courier',Slick__day__c='Next Business Day');
        insert r;
       contact c=new contact(lastname='test'); 
       insert c;
        opportunity opp=new opportunity(Slick__contact__c=c.id,closeDate=date.today(),stagename='closed',name='test');
        
        insert opp;
        OpportunitylineItem oli=new OpportunityLineItem(opportunityid=opp.id, pricebookEntryid=pbe.id,quantity=1,totalprice=1);
        insert oli;
       
 oli=[select id,totalprice,quantity,pricebookentryid,pricebookentry.name,pricebookentry.product2.name, pricebookentry.product2id, pricebookentry.unitprice from opportunitylineitem where id=:oli.id limit 1];
    //oli=[Select UnitPrice, Slick__Tax__c, SortOrder, ServiceDate, Quantity, PricebookEntryId, OpportunityId, ListPrice, TotalPrice,  Id, Description, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Description, PriceBookEntry.Product2.Family, PriceBookEntry.Product2.Slick__Group__c, PriceBookEntry.Product2.Name, PriceBookEntry.ProductCode, PricebookEntry.Name, PricebookEntry.IsActive, PriceBookEntry.UnitPrice from OpportunityLineItem   where id=:oli.id limit 1];
        
        wrapperClasses.productWrapper pw=Opportunity_Methods.toProductWrapper(oli);      
        opportunity_Methods.cloneProductWrapper(pw);
        Opportunity_Methods.productWrapperToCartItem(pw);

    

        
    }
}