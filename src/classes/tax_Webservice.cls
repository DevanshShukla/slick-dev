@RestResource(urlMapping='/tax/*')
global with sharing class tax_Webservice {

    @HttpGet
    global static void calculateTax(){
        String email = RestContext.request.params.get('email');
        String zip = RestContext.request.params.get('zip');
        String state = RestContext.request.params.get('state');

        List<Contact> customer = [SELECT Id, Slick__Sales_Tax__c, Slick__Tax_Exempt__c FROM Contact WHERE Email =: email];


        Slick__Sales_Tax__c tax;
        if (customer.size() > 0){
            Contact contact = customer[0];
            
            if(Contact.Slick__Tax_Exempt__c!=true){
                if(Contact.Slick__Sales_tax__c!=null){
                    tax=opportunity_Methods.getTaxById(Contact.Slick__Sales_Tax__c);
                }
                else{
                    tax=opportunity_Methods.getTaxByZipThenState( zip , state);
                }
            }else{
                tax = opportunity_Methods.cleanTax(tax);
            }

        }else{
            tax = opportunity_Methods.getTaxByZipThenState(zip, state);
        }

        

        

        //Slick__Sales_Tax__c tax = opportunity_Methods.getTaxByZipThenState(zip, state);

        Map<String,Object> response = (Map<String,Object>)JSON.deserializeUntyped( JSON.serialize(tax) );
        system.debug('--response--');
        system.debug(response);
        Map<String,Object> responseCleaned = util.removeAttributes(response);
        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(JSON.serializePretty(responseCleaned));

    }

}