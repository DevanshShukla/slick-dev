public with sharing class ContactActions_Extension {
	public Contact contact{get;set;}
	public String action{get;set;}
	public ContactActions_Extension(ApexPages.standardController con) {
		contact=(contact)con.getRecord();
		list<Contact> conts=[select id,mailingpostalcode,otherpostalcode,Slick__route__c from contact where id=:contact.id];
		if(conts.size()>0)
			contact=conts.get(0);
		if(ApexPages.CurrentPage().getParameters().containsKey('action'))
			 action=ApexPages.CurrentPage().getParameters().get('action');
	}

	public pageReference doAction(){
		if(contact!=null){
			if(action=='recalculateroute'){
				String zip;
				id routeId;
				if(String.isnotblank(contact.mailingPostalcode))
					zip=contact.mailingPostalCode;				
				else if(String.isnotblank(contact.otherPostalcode))
					zip=contact.otherPostalCode;
				if(String.isnotblank(zip))
					routeId=opportunity_Methods.getRoute(zip);
				if(routeId==null){
					list<Slick__Route__c> routes=[select id from Slick__route__c where name='Courier'];
					if(routes.size()>0)
						routeId=routes.get(0).id;
				}
				if(routeId!=null){
					contact.Slick__route__c=routeId;
					update contact;
				}
			}
			return new ApexPages.StandardController(contact).view();
		}
		return null;
	}
}