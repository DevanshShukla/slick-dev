@RestResource(urlMapping='/checkout/')
global with sharing class checkout_Webservice {

    @HttpPost
    global static void saveOrder(){
        String bodyStr = RestContext.request.requestBody.toString();
        System.debug(bodyStr);
        String response = (bodyStr);
        System.debug(response);
        RestContext.response.addHeader('Content-Type', 'text/plain');
        RestContext.response.responseBody = Blob.valueOf(response);
    }
}