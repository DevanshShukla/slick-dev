public with sharing class AuthorizeNet_Transaction_Methods {
	public String action{get;set;}
	public Slick__Authorize_net_Transaction__c authTrans{get;set;}
	public Opportunity Oppty{get;set;}
	public boolean confirmed{get;set;}
	public api_AuthorizeDotNet.authnetReq_Wrapper chargeAuthRequest{get;set;}


	public AuthorizeNet_Transaction_Methods() {
		if(ApexPages.currentPage().getParameters().containsKey('action')){
				action=ApexPages.currentPage().getParameters().get('action');

		}
		if(ApexPages.currentPage().getParameters().containsKey('OpportunityId')){
			String opptyId=ApexPages.currentPage().getParameters().get('OpportunityId');
			list<opportunity> opportunities=[select id, Slick__This_Is_A_Return__c, Slick__Billing_email__c, Slick__Billing_City__c, Slick__Billing_State__c, Slick__Billing_Street_1__c, Slick__Billing_Zipcode__c, Slick__billing_first_name__c, Slick__billing_last_name__c, Slick__Charge_Amount__c,Slick__card_name__c,Slick__Card_Expiration_Year__c, Slick__Card_Expiration_Month__c, Slick__Card_Number__c, Slick__Card_Security__c,Slick__Credit_Card_used__c, Credit_Card_Used__r.Slick__Expiration_Year__c, Credit_Card_Used__r.Slick__Expiration_Month__c, Credit_Card_Used__r.Slick__Name_On_Card__c, Credit_Card_Used__r.Slick__CVV__c, Credit_Card_Used__r.Slick__Card_Number__c, Credit_Card_Used__r.Slick__Billing_Street__c, Credit_Card_Used__r.Slick__Billing_City__c, Credit_Card_Used__r.Slick__Billing_State__c, Credit_Card_Used__r.Slick__Billing_Postal_Code__c,Slick__Charge_Status__c from opportunity where id=:opptyId];
			if(opportunities.size()>0){
				Oppty=opportunities.get(0);
			}
		}

		if(ApexPages.currentPage().getParameters().containsKey('TransactionId')){
			String transactionId=ApexPages.currentPage().getParameters().get('TransactionId');
			list<Slick__Authorize_net_Transaction__c> transactions=[select id,Slick__transaction_Type__c,Slick__Transaction_Status__c,Slick__transaction_id__c,Slick__payment_Number__c,Slick__First_name_on_Card__c,Slick__Last_name_on_Card__c,Slick__CC_num__c,Slick__Amount__c from Slick__Authorize_net_Transaction__c where id=:transactionId];
			if(transactions.size()>0){
				authTrans=transactions.get(0);					
			}
		}
	}
	public pageReference doAction(){	
			if(action=='void'){
				api_AuthorizeDotNet.authNetResp_Wrapper resp=voidTransaction(authTrans);
							
				if(resp.ResponseReasonCode=='1'){
					authTrans.Slick__Payment_number__c=5.1;
					update authTrans;
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info,'Transaction Void Success: '+authTrans.Slick__Transaction_Status__c));
				}
				else{
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.error,'Error Voiding Transaction: '+authTrans.Slick__Transaction_Status__c));			
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info,resp.toString()));				
				}
			
			}	
			else if(action=='refund'){

				if(ApexPages.currentPage().getParameters().containsKey('OpportunityId') && oppty.Slick__This_Is_A_Return__c && authTrans == null){
					//api_AuthorizeDotNet.authnetReq_Wrapper chargeRequest= opportunityToAuthorizeDotNetRequest_CreditCard(oppty);
					//System.debug('CLASS: opportunity_Methods, METHOD: chargeOpportunity, VAR: authorizeRequest');
					//System.debug(chargeRequest);
					//chargeRequest.transid = '';
					//api_AuthorizeDotNet.authdotnetRefund(chargeRequest);

					api_AuthorizeDotNet.authnetReq_Wrapper chargeRequest=opportunity_Methods.opportunityToAuthorizeDotNetRequest_CreditCard(oppty);
            		api_AuthorizeDotNet.authNetResp_Wrapper chargeResponse;


            		decimal payment_number;

            		chargeResponse = opportunity_Methods.refundOpportunity(oppty);
                	payment_number=1.3;

                	Slick__Authorize_net_Transaction__c authTransaction=Opportunity_methods.AuthorizeDotNetToTransaction(chargeResponse,chargeRequest);
		            if(chargeResponse.responseCode=='1'){
		                    
		                    //Insert Authorizenet transaction
		                    authTransaction.Slick__order__c=oppty.id;
		                    authTransaction.Slick__credit_Card__c=oppty.Slick__credit_card_used__c;

		                    authTransaction.Slick__Payment_Number__c=payment_number;
		                    insert authTransaction;
		                    //this is overwritting the name..
		                    /*oppty.Slick__Confirmation_Sent__c=system.now();
		                    update oppty;   */
		                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info,'Transaction Refund Success: '+authTransaction.Slick__Transaction_Status__c));
		            }
		            else{
		                //hasError=true;
		                //errorMessage=chargeResponse.ResponseReasonText;
		                if(ApexPages.currentPage() != null)
		                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,chargeResponse.ResponseReasonText));
		            }
				}else if(ApexPages.currentPage().getParameters().containsKey('OpportunityId')){
				
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Can only refund a return order'));
				}else if(ApexPages.currentPage().getParameters().containsKey('TransactionId')){
		            	//api_AuthorizeDotNet.authdotnetRefund(chargeRequest);

	    //        	api_AuthorizeDotNet.authnetReq_Wrapper chargeRequest= opportunityToAuthorizeDotNetRequest_CreditCard(oppty);
					//System.debug('CLASS: opportunity_Methods, METHOD: chargeOpportunity, VAR: authorizeRequest');
					//System.debug(chargeRequest);
					//chargeRequest.transid = '';
					//api_AuthorizeDotNet.authdotnetRefund(chargeRequest);

					//api_AuthorizeDotNet.authNetResp_Wrapper chargeResponse;
					//decimal payment_number;

     //   			chargeResponse = opportunity_Methods.refundOpportunity(oppty);
     //       		payment_number=1.3;

     //       		Slick__Authorize_net_Transaction__c authTransaction=Opportunity_methods.AuthorizeDotNetToTransaction(chargeResponse,chargeRequest);
     				api_AuthorizeDotNet.authNetResp_Wrapper resp=refundTransaction(authTrans);

            		if(resp.ResponseReasonCode=='1'){
	                    
	                    //Insert Authorizenet transaction
	                    authTrans.Slick__Payment_number__c=6.1;
						update authTrans;
						ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info,'Transaction Refund Success: '+authTrans.Slick__Transaction_Status__c));
	            	}else{
	            		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.error,'Error Refunding Transaction: '+authTrans.Slick__Transaction_Status__c));			
						ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info,resp.toString()));	
	            	}

		        }

				//api_AuthorizeDotNet.authNetResp_Wrapper resp=refundTransaction(authTrans);
				
				//if(resp.ResponseReasonCode=='1'){
				//	authTrans.Slick__Payment_number__c=6.1;
				//	update authTrans;
				//	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info,'Transaction Refund Success: '+authTrans.Slick__Transaction_Status__c));
				//}
				//else{
				//	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.error,'Error Refunding Transaction: '+authTrans.Slick__Transaction_Status__c));			
				//	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info,resp.toString()));				
				//}
			}
			else if(action=='prior_Auth_Capture'){
				if(authTrans!=null && authTrans.Slick__transaction_type__c!='auth_only')
						ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Only \"auth_only\" type transactions can be captured.'));					
				else{
					api_AuthorizeDotNet.authNetResp_Wrapper resp=priorAuthCaptureTransaction(authTrans);	
					if(resp.ResponseReasonCode=='1'){
						authTrans.Slick__Payment_number__c=1.1;
						update authTrans;
						ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info,'Transaction Prior Auth Capture Success: '+authTrans.Slick__Transaction_Status__c));
					}
				}

			}
		    else if(action=='charge'|| action=='authorize'){
					//need confirm
					if(oppty!=null && oppty.Slick__Charge_Status__c==1.1 || oppty.Slick__Charge_Status__c==1.2)
						ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,'This order already has an existing charge or authorization transaction. Please review before confirming.'));					
					createTransaction();			
			}
			
		return null;
	}
	public void createTransaction(){
		chargeAuthRequest=Opportunity_Methods.opportunityToAuthorizeDotNetRequest_CreditCard(Oppty);
		if(Oppty.Slick__Credit_Card_Used__c!=null){
			chargeAuthRequest.billcity=Oppty.Credit_Card_Used__r.Slick__Billing_City__c;
			chargeAuthRequest.billstate=Oppty.Credit_Card_Used__r.Slick__Billing_State__c;
			chargeAuthRequest.billstreet=Oppty.Credit_Card_Used__r.Slick__Billing_Street__c;
			chargeAuthRequest.billzip=Oppty.Credit_Card_Used__r.Slick__Billing_Postal_Code__c;
			if(String.isnotblank(Oppty.Credit_Card_Used__r.Slick__Name_On_Card__c)){
				list<String> nameParts=Oppty.Credit_Card_Used__r.Slick__Name_On_Card__c.split(' ');
				chargeAuthRequest.firstname=nameParts[0];
				if(nameparts.size()>2)
					chargeAuthRequest.lastname=nameParts[2];
				else if(nameparts.size()>1)
					chargeAuthRequest.lastname=nameParts[1];
			}
			chargeAuthRequest.ccexp=oppty.Credit_Card_Used__r.Slick__Expiration_Month__c+Oppty.Credit_Card_Used__r.Slick__Expiration_Year__c;		
			chargeAuthRequest.ccnum=Oppty.Credit_Card_Used__r.Slick__Card_Number__c;
			chargeAuthRequest.ccsec=Oppty.Credit_Card_Used__r.Slick__CVV__c;
		}
	}
	public static api_AuthorizeDotNet.authnetReq_Wrapper createTransaction(Opportunity Oppty){
		api_AuthorizeDotNet.authnetReq_Wrapper chargeAuthRequest=Opportunity_Methods.opportunityToAuthorizeDotNetRequest_CreditCard(Oppty);
		if(Oppty.Slick__Credit_Card_Used__c!=null){
			chargeAuthRequest.billcity=Oppty.Credit_Card_Used__r.Slick__Billing_City__c;
			chargeAuthRequest.billstate=Oppty.Credit_Card_Used__r.Slick__Billing_State__c;
			chargeAuthRequest.billstreet=Oppty.Credit_Card_Used__r.Slick__Billing_Street__c;
			chargeAuthRequest.billzip=Oppty.Credit_Card_Used__r.Slick__Billing_Postal_Code__c;
			if(String.isnotblank(Oppty.Credit_Card_Used__r.Slick__Name_On_Card__c)){
				list<String> nameParts=Oppty.Credit_Card_Used__r.Slick__Name_On_Card__c.split(' ');
				chargeAuthRequest.firstname=nameParts[0];
				if(nameparts.size()>2)
					chargeAuthRequest.lastname=nameParts[2];
				else if(nameparts.size()>1)
					chargeAuthRequest.lastname=nameParts[1];
			}
			chargeAuthRequest.ccexp=oppty.Credit_Card_Used__r.Slick__Expiration_Month__c+Oppty.Credit_Card_Used__r.Slick__Expiration_Year__c;		
			chargeAuthRequest.ccnum=Oppty.Credit_Card_Used__r.Slick__Card_Number__c;
			chargeAuthRequest.ccsec=Oppty.Credit_Card_Used__r.Slick__CVV__c;
		}
		return chargeAuthRequest;
	}
	public pageReference confirm(){
		confirmed=false;
		Slick__Authorize_net_Transaction__c newTrans;
		api_AuthorizeDotNet.authNetResp_Wrapper resp;
		if(action=='authorize'){
		  resp=api_AuthorizeDotNet.authdotnetAuthorize(chargeAuthRequest);
		  newTrans=Opportunity_Methods.AuthorizeDotNetToTransaction(resp,chargeAuthRequest);
		  newTrans.Slick__Payment_Number__c=1.2;

		}
		else if(action=='charge'){
		  resp=api_AuthorizeDotNet.authdotnetChargeDetailed(chargeAuthRequest);
		  newTrans=Opportunity_Methods.AuthorizeDotNetToTransaction(resp,chargeAuthRequest);
		  newTrans.Slick__Payment_Number__c=1.1;
		}
		if(resp!=null){
			if(resp.ResponseReasonCode=='1'){
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,'Transaction '+action+' successful'));
					//creditcard object???
					newTrans.Slick__Order__c=Oppty.id;
					insert newTrans;
					confirmed=true;
			}
			else{
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Error Completing '+action+' '+resp.responseReasonText));
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,resp.toString()));
			}

		}
		return null;
	}

	public pageReference cancel(){
		if(authTrans!=null)
			return new pageReference('/'+authTrans.id);
		else if(oppty!=null){
			return new pageReference('/'+Oppty.id);
		}
		return new pageReference('/');
	}

	public static api_AuthorizeDotNet.authNetResp_Wrapper voidTransaction(Slick__Authorize_net_Transaction__c trans){
		api_AuthorizeDotNet.authnetReq_Wrapper req=new api_AuthorizeDotNet.authnetReq_Wrapper();
		req.transid=trans.Slick__transaction_Id__c;
		api_AuthorizeDotNet.authNetResp_Wrapper response=api_AuthorizeDotNet.authDotNetVoid(req);
		//do something with transaction record?
		trans.Slick__Transaction_Type__c=response.TransactionType;
		trans.Slick__Transaction_Status__c=response.responseReasonText;
		return response;
	}
	public static api_AuthorizeDotNet.authNetResp_Wrapper refundTransaction(Slick__Authorize_net_Transaction__c trans){
		api_AuthorizeDotNet.authnetReq_Wrapper req=new api_AuthorizeDotNet.authnetReq_Wrapper();
		req.ccnum=trans.Slick__CC_Num__c;
		req.transid=trans.Slick__transaction_Id__c;
		req.amt=String.valueof(trans.Slick__Amount__c);
		req.firstname=trans.Slick__First_Name_On_Card__c;
		req.lastname=trans.Slick__Last_name_on_card__c;
		api_AuthorizeDotNet.authNetResp_Wrapper response=api_AuthorizeDotNet.authdotnetRefund(req);
		//do something with transaction record?
		trans.Slick__Transaction_Type__c=response.TransactionType;
		trans.Slick__Transaction_Status__c=response.responseReasonText;
		return response;
	}
	public static api_AuthorizeDotNet.authNetResp_Wrapper priorAuthCaptureTransaction(Slick__Authorize_net_Transaction__c trans){
		api_AuthorizeDotNet.authnetReq_Wrapper req=new api_AuthorizeDotNet.authnetReq_Wrapper();
		req.transid=trans.Slick__transaction_Id__c;
		api_AuthorizeDotNet.authNetResp_Wrapper response=api_AuthorizeDotNet.authdotnetPriorAuthCapture(req);
		//do something with transaction record?
		trans.Slick__Transaction_Type__c=response.TransactionType;
		trans.Slick__Transaction_Status__c=response.responseReasonText;
		return response;
	}
	
		public void demo(){
	    Decimal i = 0;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    
	    
	}
}