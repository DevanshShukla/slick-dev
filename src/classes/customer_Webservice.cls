@RestResource(urlMapping='/customer/*')
global with sharing class customer_Webservice {

    @HttpGet
    global static void getCustomerId(){
        String email = test.isRunningTest() ? 'ramcda1111111111111@hotmail.com' : RestContext.request.params.get('email');
       
        List<Contact> c = [SELECT Id, Email FROM Contact WHERE Email = :email LIMIT 1];
        System.debug('contact Ids '+c);
        if (c.size() > 0){
            System.debug(c[0].Id);
            getCustomer(c[0].Id);
        }
        else  {
            String response = 'Customer not found';
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(response);
        }
    }

    @HttpPost
    global static String upsertCustomer(){
        String response = '';

        String bodyStr = RestContext.request.requestBody.toString();
        wrapperClasses.customerWrapper customer = (wrapperClasses.customerWrapper)JSON.deserialize(bodyStr, wrapperClasses.customerWrapper.class);
        String email = customer.email;
        List<Contact> c = [SELECT Id FROM Contact WHERE Email = :email];
        Slick__Credit_Card__c newCard = new Slick__Credit_Card__c();
        List<Slick__Credit_Card__c> ccs = new List<Slick__Credit_Card__c>();
        if (customer.creditcard != null){
            ccs = [SELECT Name FROM Slick__Credit_Card__c WHERE Name = :customer.creditcard.Name];
        }

        if (c.size() > 0 ){
            Contact contactFields = (Contact)util.queryRecord('Contact', c[0].Id, '', '');
            Contact currentCustomer = fillContactFields(customer, contactFields);
            update contactFields;
            response = 'Updated customer ' + currentCustomer.Id + '.';
            if (customer.creditcard != null && ccs.size() == 0){
                newCard = fillCreditCard (customer, c[0].Id);
                System.debug(newCard);
                insert newCard;
                response += ' Added new credit card.';
            }
            else if (ccs.size() > 0){
                response += ' That credit card is already registered for this customer.';
            }
        }
        else {
            Contact newCustomerFields = new Contact();
            Contact newCustomer= fillContactFields(customer, newCustomerFields);
            insert newCustomer;
            response = 'Created new customer ' + newCustomer.Id + '.';
            if (customer.creditcard != null && ccs.size() == 0){
                newCard = fillCreditCard (customer, newCustomer.Id);
                System.debug(newCard);
                insert newCard;
                response += ' Added new credit card.';
            }
            else if (ccs.size() > 0){
                response += ' That credit card is already registered for this customer.';
            }
        }

        return response;
    }

    public static Contact fillContactFields(wrapperClasses.customerWrapper customer, Contact c){
        c.FirstName = customer.firstname;
        c.LastName = customer.lastname;
        c.Email = customer.email;
        c.Phone = customer.phone;
        c.MailingStreet = customer.mailingstreet;
        c.MailingCity = customer.mailingcity;
        c.MailingState = customer.mailingstate;
        c.MailingPostalCode = customer.mailingpostalcode;
        return c;
    }

    public static Slick__Credit_Card__c fillCreditCard (wrapperClasses.customerWrapper customer, Id customerId){
        Slick__Credit_Card__c newCard = new Slick__Credit_Card__c();
        //newCard.Slick__Active__c = true;
        newCard.Name = customer.creditcard.Name;
        newCard.Slick__Billing_City__c = customer.creditcard.billingCity;
        newCard.Slick__Billing_Street__c = customer.creditcard.billingStreet;
        newCard.Slick__Billing_State__c = customer.creditcard.billingState;
        newCard.Slick__Billing_Postal_Code__c = customer.creditcard.billingZip;
        newCard.Slick__Card_Number__c = customer.creditcard.CreditCardNumber;
        newCard.Slick__CVV__c = customer.creditcard.CreditCardSecurityCode;
        newCard.Slick__Expiration_Month__c = customer.creditcard.CreditCardExpMonth;
        newCard.Slick__Expiration_Year__c = customer.creditcard.CreditCardExpYear;
        newCard.Slick__Card_Type__c = customer.creditcard.CreditCardType;
        newCard.Slick__Name_on_Card__c = customer.creditcard.NameOnCard;
        newCard.Slick__Contact__c = customerId;
        return newCard;
    }
    /*
    **
    ** Get customer, last order items, pets, and payment
    **
    */
    public static Contact getCustomer(Id cId){
        Contact customer = (Contact)util.queryRecord('Contact', cId, '', '');
        List<Contact> customers = new List<Contact>();
        customers.add(customer);

        List<Opportunity> lastOrder = [SELECT Id FROM Opportunity WHERE Slick__Contact__c =:cId AND Slick__This_Is_A_Return__c = false ORDER BY createdDate DESC LIMIT 1];
        
         System.debug('Contact Id In CLass>>>>>>>>>>>>>>>>>>>>>>'+cId);
        System.debug('lastOrder>>>>>>>>>>>>>>'+lastOrder);
        List<OpportunityLineItem> olis = new List<OpportunityLineItem>();
        olis = [SELECT ProductCode, Quantity From OpportunityLineItem WHERE OpportunityId = :lastOrder[0].Id];

     /* List<Contact> pets = [SELECT
        Slick__Dog_1_Breed__c,
        Slick__Dog_1_Cups_per_day__c,
        Slick__Dog_1_Issue__c,
        Slick__Dog_1_Life_Stage__c,
        Slick__Dog_1_Name__c,
        Slick__Dog_1_Notes__c,
        Slick__Dog_1_Protein_Intolerance__c,
        Slick__Dog_1_Weight_Range__c,
        Slick__Dog_2_Breed__c,
        Slick__Dog_2_Cups_per_day__c,
        Slick__Dog_2_Issue__c,
        Slick__Dog_2_Life_Stage__c,
        Slick__Dog_2_Name__c,
        Slick__Dog_2_Notes__c,
        Slick__Dog_2_Protein_Intolerance__c,
        Slick__Dog_2_Weight_Range__c,
        Slick__Dog_3_Breed__c,
        Slick__Dog_3_Issue__c,
        Slick__Dog_3_Life_Stage__c,
        Slick__Dog_3_Name__c,
        Slick__Dog_3_Notes__c,
        Slick__Dog_3_Protein_Intolerance__c,
        Slick__Dog_3_Weight_Range__c,
        Slick__Dogs_2_Cups_per_day__c,
        Slick__Cats_total_cups_per_day__c,
        Slick__Number_of_cats__c FROM Contact WHERE Id =:cId];


    */ 

        // List<Contact> pets = [SELECT
        // Slick__Dog_1_Breed__c,
        // Slick__Dog_1_Cups_per_day__c,
        // Slick__Dog_1_Issue__c,
        // Slick__Dog_1_Life_Stage__c,
        // Slick__Dog_1_Name__c,
        // Slick__Dog_1_Notes__c,
        // Slick__Dog_1_Protein_Intolerance__c,
        // Slick__Dog_1_Weight_Range__c,
        // Slick__Dog_2_Breed__c,
        // Slick__Dog_2_Cups_per_day__c,
        // Slick__Dog_2_Issue__c,
        // Slick__Dog_2_Life_Stage__c,
        // Slick__Dog_2_Name__c,
        // Slick__Dog_2_Notes__c,
        // Slick__Dog_2_Protein_Intolerance__c,
        // Slick__Dog_2_Weight_Range__c,
        // Slick__Dog_3_Breed__c,
        // Slick__Dog_3_Issue__c,
        // Slick__Dog_3_Life_Stage__c,
        // Slick__Dog_3_Name__c,
        // Slick__Dog_3_Notes__c,
        // Slick__Dog_3_Protein_Intolerance__c,
        // Slick__Dog_3_Weight_Range__c,
        // Slick__Dogs_2_Cups_per_day__c,
        // Slick__Cats_total_cups_per_day__c,
        // Slick__Number_of_cats__c FROM Contact WHERE Id =:cId];

        List<Slick__Credit_Card__c> ccIds = [SELECT Id FROM Slick__Credit_Card__c WHERE Slick__Contact__c =:cId];
        List<Slick__Credit_Card__c> ccList = new List<Slick__Credit_Card__c>();

        if (ccIds.size() > 0){
            for (Slick__Credit_Card__c card: ccIds){
                Slick__Credit_Card__c cc = (Slick__Credit_Card__c)util.queryRecord('Slick__Credit_Card__c', card.Id, '', '');
                ccList.add(cc);
            }
        }

        Map<String, List<Object> > obj = new Map<String, List<Object> >();
        //obj.put('pets', pets);
        obj.put('reorder', olis);
        obj.put('payments', ccList);
        obj.put('customer', customers);


        Map<String,Object> response = (Map<String,Object>)JSON.deserializeUntyped( JSON.serialize(obj) );
        removeAttributes(response);
        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(JSON.serializePretty(response));

        return customer;
    }

    /*
    ** Remove attributes from response JSON
    */

    public static Map<String,Object> removeAttributes(Map<String,Object> jsonObj)  {
        for(String key : jsonObj.keySet()) {
            if(key == 'attributes') {
                jsonObj.remove(key);
            } else {
                if(jsonObj.get(key) instanceof Map<String,Object>) {
                    removeAttributes((Map<String,Object>)jsonObj.get(key));
                }
                if(jsonObj.get(key) instanceof List<Object>) {
                    for(Object listItem : (List<Object>)jsonObj.get(key)) {
                        if(listItem instanceof Map<String,Object>)  {
                            removeAttributes((Map<String,Object>)listItem);
                        }
                    }
                }
            }
        }
        return jsonObj;
    }

    public static void dummy(){
        Integer i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
    }
}