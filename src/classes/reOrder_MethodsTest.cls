/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class reOrder_MethodsTest {

    static testMethod void myUnitTest() {
    	product2 prod = new product2(name='test', Slick__mobile_featured_upsell__c = true);
    	insert prod;
    	Slick__Route__c route = new Slick__Route__c(name='test');
    	insert route;
    	Slick__Zip_Code__c zippy = new Slick__Zip_Code__c(name='90019', Slick__route__c = route.id);
    	insert zippy;
    	contact c=new contact(lastName='test');
      insert c;
   		opportunity opp = new opportunity(name='test',Slick__contact__c=c.id, closeDate=system.today(), stageName='Closed Won', Slick__Shipping_Zipcode__c = '90019');
   		insert opp;
   		
   		reOrder_Methods.reconstructOrder(opp.id, true);
   		reOrder_Methods.query3UpsellProducts();
   	
   		
   		
    
        
    }
}