public class ProductSearchController {
    public list <Product2> p {get;set;}
    public String searchKey {get;set;}

    // Constructor
    public ProductSearchController() {
    }

    // Product Search method
    public void search(){
        string searchquery='select id,Name,ProductCode from product2 where name like \'%'+searchKey+'%\' OR ProductCode like \'%'+searchKey+'%\'';
        p= Database.query(searchquery);
    }

    // FIND PRODUCT BASED ON CATEGORY
    public static List<Product2> searchProductByCategory( List<String> ProductCategoryList ){

        try{
            List<Product2> productList = Database.query(util.queryData('Product2', '', ' WHERE ( Slick__Product_Category__c =: ProductCategoryList OR '+
                                                                                    ' Slick__Cart_Category__c =: ProductCategoryList OR '+
                                                                                    ' Slick__Cart_Subcategory__c =: ProductCategoryList OR '+
                                                                                    ' Slick__Category__c =: ProductCategoryList '));

            return productList;
        }catch(exception ex){
            system.debug('Error:' + ex.getMessage() + ':' + ex.getLineNumber());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Error:' + ex.getMessage() + ':' + ex.getLineNumber()));
            return new List<Product2>();
        }
    }

}