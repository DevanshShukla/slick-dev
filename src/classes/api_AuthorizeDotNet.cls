public with sharing class api_AuthorizeDotNet {
	public static boolean TESTMODE = isTestMode();
	public static Slick__Instance_Setting__c CREDS = getAuthNetCreds();
	public static string APILOGIN;
	public static string APITRANSKEY;


	public class testWrapper {
		public String test;
	}


	public static authnetresp_wrapper authdotnetPriorAuthCapture (authnetreq_wrapper input) {
		HttpRequest req = new HttpRequest();
     	req.setEndpoint('https://secure2.authorize.net/gateway/transact.dll'); // https://secure.authorize.net/gateway/transact.dll //test  https://test.authorize.net/gateway/transact.dll
     	if (testMode) {
     		req.setEndpoint('https://test.authorize.net/gateway/transact.dll');
     	}
     	req.setMethod('POST');

		/***build message**/
		Map<string,string> messagestring = new map<String,String>();

		/**Default Fields**/
		if (input.apiname != null && input.apitrans != null){
			messagestring.put('x_login',input.apiname);
			messagestring.put('x_tran_key',input.apitrans);
		}
		else {
			messagestring.put('x_login',APILOGIN);
			messagestring.put('x_tran_key',APITRANSKEY);
		}
		messagestring.put('x_version', '3.1');
		messagestring.put('x_delim_data', 'TRUE');
		messagestring.put('x_delim_char', ';');
		messagestring.put('x_relay_response', 'FALSE');
		messagestring.put('x_type', 'PRIOR_AUTH_CAPTURE');
		messagestring.put('x_duplicate_window', '0');
		/***************/

		/*Order Specific Information*/
		messagestring.put('x_trans_id', input.transid);


		String encodedmsg = '';
		for (string s : messagestring.keySet()){
        	encodedmsg += s+'='+EncodingUtil.urlEncode(util.fixnull(messagestring.get(s)), 'UTF-8')+'&';
        	system.debug(s+' added');
		}
		encodedmsg += 'endofdata';
		system.debug('TRACE: Encoded Message: \n\n' + encodedmsg);
		req.setBody(encodedmsg);

		string resp ='';
		Http http = new Http();
		HTTPResponse res;
		authnetresp_wrapper parsedResponse=new authnetresp_wrapper();
		try{
		list<string> responses = ('1;1;1;This transaction has been approved.;DMPWBZ;Y;2196294662;;Transaction: test;10.00;CC;auth_capture;;first;last;;street;city;state;billzip;;;;;;;;;;;;;;;;;;449B10456AF4D3B815F828D3E82185F5;P;2;;;;;;;;;;;XXXX0002;American Express;;;;;;;;;;;;;;;;;').split(';');
    	if (!test.isrunningTest()) {
     		resp = string.valueof(http.send(req).getbody());
     		system.debug(resp);
     		responses = resp.split(';');
    	}

		parsedResponse = parseIntoResponseWrapper(responses);
		system.debug(parsedResponse);
		}
		catch(exception e){
			//Slick__Logger__c log=new Slick__Logger__c(Slick__error__c='Authorize.net Authorize Debug',Slick__Body__c=resp+' '+e+' '+e.getStackTraceString());
     		//insert log;
     		ErrorLogger.logError ('Authorize.net Authorize Debug',resp);
		}
		//http://developer.authorize.net/guides/AIM/wwhelp/wwhimpl/js/html/wwhelp.htm   response/response reason codes
		return parsedResponse;
		//return (parsedResponse.responseCode == '1' && parsedResponse.ResponseReasonText == 'This transaction has been approved.')?true:false;
	}



	public static authnetresp_wrapper authdotnetVoid (authnetreq_wrapper input) {
		HttpRequest req = new HttpRequest();
     	string endpointString = (testMode)?'https://test.authorize.net/gateway/transact.dll':'https://secure2.authorize.net/gateway/transact.dll';
     	req.setEndpoint(endpointString);
     	req.setMethod('POST');

		/***build message**/
		Map<string,string> messagestring = new map<String,String>();

		/**Default Fields**/
		if (input.apiname != null && input.apitrans != null){
			messagestring.put('x_login',input.apiname);
			messagestring.put('x_tran_key',input.apitrans);
		}
		else {
			messagestring.put('x_login',APILOGIN);
			messagestring.put('x_tran_key',APITRANSKEY);
		}
		messagestring.put('x_version', '3.1');
		messagestring.put('x_delim_data', 'TRUE');
		messagestring.put('x_delim_char', ';');
		messagestring.put('x_relay_response', 'FALSE');
		messagestring.put('x_type', 'VOID');
		messagestring.put('x_duplicate_window', '0');
		/***************/

		/*Order Specific Information*/
		messagestring.put('x_trans_id', input.transid);

		String encodedmsg = '';
		for (string s : messagestring.keySet()){
        	encodedmsg += s+'='+EncodingUtil.urlEncode(util.fixnull(messagestring.get(s)), 'UTF-8')+'&';
        	system.debug(s+' added');
		}
		encodedmsg += 'endofdata';
		system.debug('TRACE: Encoded Message: \n\n' + encodedmsg);
		req.setBody(encodedmsg);

		string resp ='';
		Http http = new Http();
		HTTPResponse res;
		list<string> responses = ('1;1;1;This transaction has been approved.;DMPWBZ;Y;2196294662;;Your Transaction: test;10.00;CC;auth_capture;;first;last;;street;city;state;billzip;;;;;;;;;;;;;;;;;;449B10456AF4D3B815F828D3E82185F5;P;2;;;;;;;;;;;XXXX0002;American Express;;;;;;;;;;;;;;;;;').split(';');
    	if (!test.isrunningTest()) {
     		resp = string.valueof(http.send(req).getbody());
     		system.debug(resp);
     		responses = resp.split(';');
     		//Slick__Logger__c log=new Slick__Logger__c(Slick__error__c='Authorize.net Void Debug',Slick__Body__c=resp);
     		//insert log;
     		ErrorLogger.logError ('Authorize.net Void Debug',resp);

    	}

		authnetresp_wrapper parsedResponse = parseIntoResponseWrapper(responses);
		system.debug(parsedResponse);

		return parsedResponse;
	}

	public static authnetresp_wrapper authdotnetRefund (authnetreq_wrapper input) {
		HttpRequest req = new HttpRequest();
     	req.setEndpoint('https://secure2.authorize.net/gateway/transact.dll'); //Live https://secure.authorize.net/gateway/transact.dll //test  https://test.authorize.net/gateway/transact.dll
     	if (testMode) {
     		req.setEndpoint('https://test.authorize.net/gateway/transact.dll');
     	}
     	req.setMethod('POST');

		/***build message**/
		Map<string,string> messagestring = new map<String,String>();

		/**Default Fields**/
		if (input.apiname != null && input.apitrans != null){
			messagestring.put('x_login',input.apiname);
			messagestring.put('x_tran_key',input.apitrans);
		}
		else {
			messagestring.put('x_login',APILOGIN);
			messagestring.put('x_tran_key',APITRANSKEY);
		}
		messagestring.put('x_version', '3.1');
		messagestring.put('x_delim_data', 'TRUE');
		messagestring.put('x_delim_char', ';');
		messagestring.put('x_relay_response', 'FALSE');
		messagestring.put('x_type', 'CREDIT');
		messagestring.put('x_duplicate_window', '0');
		/***************/

		/*Order Specific Information*/
		messagestring.put('x_card_num', input.ccnum);
		messagestring.put('x_exp_date', input.ccexp);
		messagestring.put('x_trans_id', input.transid);
		messagestring.put('x_amount', input.amt);
		messagestring.put('x_first_name', input.firstname);
		messagestring.put('x_last_name', input.lastname);

		String encodedmsg = '';
		for (string s : messagestring.keySet()){
        	encodedmsg += s+'='+EncodingUtil.urlEncode(util.fixnull(messagestring.get(s)), 'UTF-8')+'&';
        	system.debug(s+' added');
		}
		encodedmsg += 'endofdata';
		system.debug('TRACE: Encoded Message: \n\n' + encodedmsg);
		req.setBody(encodedmsg);

		string resp ='';
		Http http = new Http();
		HTTPResponse res;
		list<string> responses = ('1;1;1;This transaction has been approved.;DMPWBZ;Y;2196294662;;Your Transaction: test;10.00;CC;auth_capture;;first;last;;street;city;state;billzip;;;;;;;;;;;;;;;;;;449B10456AF4D3B815F828D3E82185F5;P;2;;;;;;;;;;;XXXX0002;American Express;;;;;;;;;;;;;;;;;').split(';');
    	if (!test.isrunningTest()) {
     		resp = string.valueof(http.send(req).getbody());
     		system.debug(resp);
     		responses = resp.split(';');
     		//	Slick__Logger__c log=new Slick__Logger__c(Slick__error__c='Authorize.net Authorize Refund',Slick__Body__c=resp);
     		//insert log;
     		ErrorLogger.logError ('Authorize.net Refund Debug',resp);
    	}

		authnetresp_wrapper parsedResponse = parseIntoResponseWrapper(responses);
		system.debug(parsedResponse);

		return parsedResponse;
	}

	public static boolean authdotnetChargeSimple (authnetreq_wrapper input) {
		HttpRequest req = new HttpRequest();
     	req.setEndpoint('https://secure2.authorize.net/gateway/transact.dll'); // https://secure.authorize.net/gateway/transact.dll //test  https://test.authorize.net/gateway/transact.dll
     	if (testMode) {
     		req.setEndpoint('https://test.authorize.net/gateway/transact.dll');
     	}
     	req.setMethod('POST');

		/***build message**/
		Map<string,string> messagestring = new map<String,String>();

		/**Default Fields**/
		if (input.apiname != null && input.apitrans != null){
			messagestring.put('x_login',input.apiname);
			messagestring.put('x_tran_key',input.apitrans);
		}
		else {
			messagestring.put('x_login',APILOGIN);
			messagestring.put('x_tran_key',APITRANSKEY);
		}
		messagestring.put('x_version', '3.1');
		messagestring.put('x_delim_data', 'TRUE');
		messagestring.put('x_delim_char', ';');
		messagestring.put('x_relay_response', 'FALSE');
		messagestring.put('x_type', 'AUTH_CAPTURE');
		messagestring.put('x_method', 'CC');
		messagestring.put('x_duplicate_window', '0');
		/***************/

		/*Order Specific Information*/
		messagestring.put('x_card_num', input.ccnum);
		messagestring.put('x_exp_date', input.ccexp);
		messagestring.put('x_card_code', input.ccsec);  ///CREDIT CARD SECURITY CODE

		messagestring.put('x_amount', input.amt);
		messagestring.put('x_description', 'Your Transaction: '+input.ordername);

		messagestring.put('x_first_name', input.firstname);
		messagestring.put('x_last_name', input.lastname);
		messagestring.put('x_address', input.billstreet);
		messagestring.put('x_city', input.billcity);
		messagestring.put('x_state', input.billstate);
		messagestring.put('x_zip', input.billzip);
		messagestring.put('x_email', input.emailaddress);

		String encodedmsg = '';
		for (string s : messagestring.keySet()){
        	encodedmsg += s+'='+EncodingUtil.urlEncode(util.fixnull(messagestring.get(s)), 'UTF-8')+'&';
        	system.debug(s+' added');
		}
		encodedmsg += 'endofdata';
		system.debug('TRACE: Encoded Message: \n\n' + encodedmsg);
		req.setBody(encodedmsg);

		string resp ='';
		Http http = new Http();
		HTTPResponse res;
		list<string> responses = ('1;1;1;This transaction has been approved.;DMPWBZ;Y;2196294662;;Transaction: test;10.00;CC;auth_capture;;first;last;;street;city;state;billzip;;;;;;;;;;;;;;;;;;449B10456AF4D3B815F828D3E82185F5;P;2;;;;;;;;;;;XXXX0002;American Express;;;;;;;;;;;;;;;;;').split(';');
    	if (!test.isrunningTest()) {
     		resp = string.valueof(http.send(req).getbody());
     		system.debug(resp);
     		responses = resp.split(';');
     			//Slick__Logger__c log=new Slick__Logger__c(Slick__error__c='Authorize.net charge simple Debug',Slick__Body__c=resp);
     		//insert log;
     		ErrorLogger.logError ('Authorize.net Simple Debug',resp);
    	}

		authnetresp_wrapper parsedResponse = parseIntoResponseWrapper(responses);
		system.debug(parsedResponse);
		//http://developer.authorize.net/guides/AIM/wwhelp/wwhimpl/js/html/wwhelp.htm   response/response reason codes

		return (parsedResponse.responseCode == '1' && parsedResponse.ResponseReasonText == 'This transaction has been approved.')?true:false;
	}

	public static authnetresp_wrapper authdotnetChargeDetailed (authnetreq_wrapper input) {
		HttpRequest req = new HttpRequest();
     	req.setEndpoint('https://secure2.authorize.net/gateway/transact.dll'); // https://secure.authorize.net/gateway/transact.dll //test  https://test.authorize.net/gateway/transact.dll
     	if (testMode) {
     		req.setEndpoint('https://test.authorize.net/gateway/transact.dll');
     	}
     	req.setMethod('POST');

		/***build message**/
		Map<string,string> messagestring = new map<String,String>();
		System.debug(input.apiname);
		/**Default Fields**/
		if (input.apiname != null && input.apitrans != null){
			messagestring.put('x_login',input.apiname);
			messagestring.put('x_tran_key',input.apitrans);
		}
		else {
			messagestring.put('x_login',APILOGIN);
			messagestring.put('x_tran_key',APITRANSKEY);
		}
		messagestring.put('x_version', '3.1');
		messagestring.put('x_delim_data', 'TRUE');
		messagestring.put('x_delim_char', ';');
		messagestring.put('x_relay_response', 'FALSE');
		messagestring.put('x_type', 'AUTH_CAPTURE');
		messagestring.put('x_method', 'CC');
		messagestring.put('x_duplicate_window', '0');
		/***************/

		/*Order Specific Information*/
		messagestring.put('x_card_num', input.ccnum);
		messagestring.put('x_exp_date', input.ccexp);
		messagestring.put('x_card_code', input.ccsec);  ///CREDIT CARD SECURITY CODE

		messagestring.put('x_amount', input.amt);
		messagestring.put('x_description', 'Your Transaction: '+input.ordername);

		messagestring.put('x_first_name', input.firstname);
		messagestring.put('x_last_name', input.lastname);
		messagestring.put('x_address', input.billstreet);
		messagestring.put('x_city', input.billcity);
		messagestring.put('x_state', input.billstate);
		messagestring.put('x_zip', input.billzip);
		messagestring.put('x_email', input.emailaddress);

		String encodedmsg = '';
		for (string s : messagestring.keySet()){
        	encodedmsg += s+'='+EncodingUtil.urlEncode(util.fixnull(messagestring.get(s)), 'UTF-8')+'&';
        	system.debug(s+' added');
		}
		encodedmsg += 'endofdata';
		system.debug('TRACE: Encoded Message: \n\n' + encodedmsg);
		req.setBody(encodedmsg);

		string resp ='';
		Http http = new Http();
		HTTPResponse res;
		list<string> responses = ('1;1;1;This transaction has been approved.;DMPWBZ;Y;2196294662;;Transaction: test;10.00;CC;auth_capture;;first;last;;street;city;state;billzip;;;;;;;;;;;;;;;;;;449B10456AF4D3B815F828D3E82185F5;P;2;;;;;;;;;;;XXXX0002;American Express;;;;;;;;;;;;;;;;;').split(';');
    	if (!test.isrunningTest()) {
     		resp = string.valueof(http.send(req).getbody());
     		system.debug(resp);
     		responses = resp.split(';');
     		//Slick__Logger__c log=new Slick__Logger__c(Slick__error__c='Authorize.net Authorize charge detailed',Slick__Body__c=resp);
     		//insert log;
     		ErrorLogger.logError ('Authorize.net charge detailed Debug',resp+' Request: '+ encodedMsg);

    	}

		authnetresp_wrapper parsedResponse = parseIntoResponseWrapper(responses);
		system.debug(parsedResponse);
		//http://developer.authorize.net/guides/AIM/wwhelp/wwhimpl/js/html/wwhelp.htm   response/response reason codes
		return parsedResponse;
		//return (parsedResponse.responseCode == '1' && parsedResponse.ResponseReasonText == 'This transaction has been approved.')?true:false;
	}
	public static authnetresp_wrapper authdotnetAuthorize (authnetreq_wrapper input) {
		HttpRequest req = new HttpRequest();
     	req.setEndpoint('https://secure2.authorize.net/gateway/transact.dll'); // https://secure.authorize.net/gateway/transact.dll //test  https://test.authorize.net/gateway/transact.dll
     	if (testMode) {
     		req.setEndpoint('https://test.authorize.net/gateway/transact.dll');
     	}
     	req.setMethod('POST');
		System.debug('CLASS: api_AuthorizeDotNet, METHOD: authdotnetAuthorize, VAR: endpoint');
		System.debug(req);
		/***build message**/
		Map<string,string> messagestring = new map<String,String>();

		/**Default Fields**/
		if (input.apiname != null && input.apitrans != null){
			messagestring.put('x_login',input.apiname);
			messagestring.put('x_tran_key',input.apitrans);
		}
		else {
			messagestring.put('x_login',APILOGIN);
			messagestring.put('x_tran_key',APITRANSKEY);
		}
		messagestring.put('x_version', '3.1');
		messagestring.put('x_delim_data', 'TRUE');
		messagestring.put('x_delim_char', ';');
		messagestring.put('x_relay_response', 'FALSE');
		messagestring.put('x_type', 'AUTH_ONLY');
		messagestring.put('x_method', 'CC');
		messagestring.put('x_duplicate_window', '0');
		/***************/

		/*Order Specific Information*/
		messagestring.put('x_card_num', input.ccnum);
		messagestring.put('x_exp_date', input.ccexp);
		messagestring.put('x_card_code', input.ccsec);  ///CREDIT CARD SECURITY CODE

		messagestring.put('x_amount', input.amt);
		messagestring.put('x_description', 'Your Transaction: '+input.ordername);

		messagestring.put('x_first_name', input.firstname);
		messagestring.put('x_last_name', input.lastname);
		messagestring.put('x_address', input.billstreet);
		messagestring.put('x_city', input.billcity);
		messagestring.put('x_state', input.billstate);
		messagestring.put('x_zip', input.billzip);
		messagestring.put('x_email', input.emailaddress);

		String encodedmsg = '';
		for (string s : messagestring.keySet()){
        	encodedmsg += s+'='+EncodingUtil.urlEncode(util.fixnull(messagestring.get(s)), 'UTF-8')+'&';
        	system.debug(s+' added');
		}
		encodedmsg += 'endofdata';
		system.debug('TRACE: Encoded Message: \n\n' + encodedmsg);
		req.setBody(encodedmsg);

		string resp ='';
		Http http = new Http();
		HTTPResponse res;
		authnetresp_wrapper parsedResponse=new authnetresp_wrapper();
		try{
		list<string> responses = ('1;1;1;This transaction has been approved.;DMPWBZ;Y;2196294662;;Transaction: test;10.00;CC;auth_capture;;first;last;;street;city;state;billzip;;;;;;;;;;;;;;;;;;449B10456AF4D3B815F828D3E82185F5;P;2;;;;;;;;;;;XXXX0002;American Express;;;;;;;;;;;;;;;;;').split(';');
	    	if (!test.isrunningTest()) {
	     		resp = string.valueof(http.send(req).getbody());
	     		system.debug(resp);
	     		responses = resp.split(';');
	    	}

			parsedResponse = parseIntoResponseWrapper(responses);
			System.debug('CLASS: api_AuthorizeDotNet, METHOD: authdotnetAuthorize, VAR: response');
			system.debug(parsedResponse);
		}
		catch(exception e){
			//Slick__Logger__c log=new Slick__Logger__c(Slick__error__c='Authorize.net Authorize Debug',Slick__Body__c=resp+' '+e+' '+e.getStackTraceString());
     		//insert log;
     		ErrorLogger.logError ('Authorize.net Authorize Debug',resp);
			System.debug('inside error catch');
			System.debug(e);
		}
		//http://developer.authorize.net/guides/AIM/wwhelp/wwhimpl/js/html/wwhelp.htm   response/response reason codes
		return parsedResponse;
		//return (parsedResponse.responseCode == '1' && parsedResponse.ResponseReasonText == 'This transaction has been approved.')?true:false;
	}






	public static boolean isTestMode(){
		if (test.isRunningTest()) insert new Slick__Instance_Setting__c(name='Authorize.net Test Mode', Slick__text_value__c = 'true');
		Slick__Instance_Setting__c testModeSetting = (Slick__Instance_Setting__c.getInstance('Authorize.net Test Mode'));
		Boolean b = (testModeSetting.Slick__text_value__c == 'true' || test.isRunningTest())?true:false;
		System.debug('CLASS: api_AuthorizeDotNet, METHOD: isTestMode, VAR: boolean');
		System.debug(b);
		return (testModeSetting.Slick__text_value__c == 'true' || test.isRunningTest())?true:false;
	}

	public static Slick__Instance_Setting__c getAuthNetCreds(){
		Slick__Instance_Setting__c theseCreds = 	(test.isRunningTest())?
												(new Slick__Instance_Setting__c(name='Authorize.net Credentials - Test', Slick__text_value__c = '{"tk":"test","api":"test"}')):
											((Slick__Instance_Setting__c.getInstance('Authorize.net Credentials'+((testMode)?' - Test':''))));

		wrapperClasses.AuthNetCredsSetting credWrap = (wrapperClasses.AuthNetCredsSetting)JSON.deserialize(theseCreds.Slick__text_value__c, wrapperClasses.AuthNetCredsSetting.class);
		APILOGIN = credWrap.api;
		APITRANSKEY = credWrap.tk;
		return theseCreds;
	}

	public static authNetResp_Wrapper parseIntoResponseWrapper(list<string> input){
		authNetResp_Wrapper temp = new authNetResp_Wrapper();
		temp.responseCode = input[0];
		temp.ResponseSubcode = input[1];
		temp.ResponseReasonCode =input[2];
		temp.ResponseReasonText=input[3];
		temp.AuthorizationCode=input[4];
		temp.AVSResponse= input[5];//5
		temp.TransactionID=input[6];
		temp.InvoiceNumber= input[7];//7
		temp.Description= input[8];//8
		temp.Amount= input[9];//9
		temp.Method= input[10];//10
		temp.TransactionType= input[11];//11
		temp.CustomerID= input[12];//12
		temp.FirstName= input[13];//13
		temp.LastName= input[14];//14
		temp.Company= input[15];//15
		temp.Address= input[16];//16
		temp.City= input[17];//17
		temp.State= input[18];//18
		temp.ZIPCode= input[19];//19
		temp.Country= input[20];//20
		temp.Phone= input[21];//21
		temp.Fax= input[22];//22
		temp.EmailAddress= input[23];//23
		temp.ShipToFirstName= input[24];//24
		temp.ShipToLastName= input[25];//25
		temp.ShipToCompany= input[26];//26
		temp.ShipToAddress= input[27];//27
		temp.ShipToCity= input[28];//28
		temp.ShipToState= input[29];//29
		temp.ShipToZIPCode= input[30];//30
		temp.ShipToCountry= input[31];//31
		temp.Tax= input[32];//32
		temp.Duty= input[33];//33
		temp.Freight= input[34];//34
		temp.TaxExempt= input[35];//35
		temp.PurchaseOrderNumber= input[36];//36
		temp.MD5Hash= input[37];//37
		temp.CardCodeResponse= input[38];//38
		temp.CardholderAuthenticationVerificationResponse= input[39];//39
		temp.AccountNumber= input[40];//40
		temp.CardType= input[41];//41
		temp.SplitTenderID= input[42];//42
		temp.RequestedAmount= input[43];//43
		temp.BalanceOnCard= input[44];//44
		return temp;
	}

	public class authnetReq_Wrapper {
		public string apiname {get;set;}
		public string apitrans {get;set;}
		public string ordername {get;set;}
		public string ccnum {get;set;}
		public string ccexp {get;set;}
		public string ccsec {get;set;}
		public string amt {get;set;}
		public string firstname {get;set;}
		public string lastname {get;set;}
		public string billstreet {get;set;}
		public string billcity {get;set;}
		public string billstate {get;set;}
		public string billzip {get;set;}
		public string transid {get; set;}
		public string routingnumber {get; set;}
		public string accountnumber {get; set;}
		public string bankaccounttype {get; set;}
		public string bankname {get; set;}
		public string bankaccountname {get; set;}
		public string emailaddress{get; set;}

		public authnetreq_wrapper(){}
	}

	public class authNetResp_Wrapper{
		// value, index in split string list
		public string responseCode {get;set;} //0
		public string ResponseSubcode{get;set;} //1
		public string ResponseReasonCode{get;set;} //2
		public string ResponseReasonText{get;set;} //3
		public string AuthorizationCode{get;set;} //4
		public string AVSResponse{get;set;} //5
		public string TransactionID{get;set;} //6
		public string InvoiceNumber{get;set;} //7
		public string Description{get;set;} //8
		public string Amount{get;set;} //9
		public string Method{get;set;} //10
		public string TransactionType{get;set;} //11
		public string CustomerID{get;set;} //12
		public string FirstName{get;set;} //13
		public string LastName{get;set;} //14
		public string Company{get;set;} //15
		public string Address{get;set;} //16
		public string City{get;set;} //17
		public string State{get;set;} //18
		public string ZIPCode{get;set;} //19
		public string Country{get;set;} //20
		public string Phone{get;set;} //21
		public string Fax{get;set;} //22
		public string EmailAddress{get;set;} //23
		public string ShipToFirstName{get;set;} //24
		public string ShipToLastName{get;set;} //25
		public string ShipToCompany{get;set;} //26
		public string ShipToAddress{get;set;} //27
		public string ShipToCity{get;set;} //28
		public string ShipToState{get;set;} //29
		public string ShipToZIPCode{get;set;} //30
		public string ShipToCountry{get;set;} //31
		public string Tax{get;set;} //32
		public string Duty{get;set;} //33
		public string Freight{get;set;} //34
		public string TaxExempt{get;set;} //35
		public string PurchaseOrderNumber{get;set;} //36
		public string MD5Hash{get;set;} //37
		public string CardCodeResponse{get;set;} //38
		public string CardholderAuthenticationVerificationResponse{get;set;} //39
		public string AccountNumber{get;set;} //40
		public string CardType{get;set;} //41
		public string SplitTenderID{get;set;} //42
		public string RequestedAmount{get;set;} //43
		public string BalanceOnCard{get;set;} //44
		public authnetresp_wrapper(){}
	}
}