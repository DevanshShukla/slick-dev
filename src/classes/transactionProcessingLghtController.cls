public with sharing class transactionProcessingLghtController {
    @AuraEnabled  
    public static List<Opportunity> fetchPostns(String OS, Date DD, Date OD, String PM, String PS) {  
        System.debug(OS);
        System.debug(DD);
        System.debug(OD);
        System.debug(PM);
        System.debug(PS);
        
        List<Opportunity>OpportunityList = new List<Opportunity>();
        Boolean flag = false;
        String QueryString = ' SELECT Id, Name, Slick__Contact__c, Slick__Charge_Amount__c, Slick__TotalDiscountTaxOther__c, Slick__Delivery_Date__c, Slick__Order_Date__c, Slick__OrderSource__c, Slick__Payment_Method__c, Slick__Payment_Status__c, Slick__Credit_Card_Used__c, Slick__Has_Critical_Note__c, Slick__Route__c ';
        QueryString += 'FROM Opportunity ';
        if(OS !=null || DD != null || OD != null || PM != null || PS != null){
            QueryString += ' WHERE ';
        }
        if(OS != null){
            if(flag) QueryString += ' AND ';
            QueryString +=' Slick__OrderSource__c =: OS ';
            // QueryString += flag ? ' AND ': ' ' + ' Slick__OrderSource__c =: OS ';
            flag = true;
        }
        if( DD != null ){
            if(flag) QueryString += ' AND ';
            QueryString +=' Slick__Delivery_Date__c =: DD ';
            //QueryString += flag ? ' AND ': ' ' + ' Slick__Delivery_Date__c =: DD ';
            flag = true;
        }
        if(OD != null){
            if(flag) QueryString += ' AND ';
            QueryString += ' Slick__Order_Date__c =: OD ';
            // QueryString += flag ? ' AND ': ' ' + ' Slick__Order_Date__c =: OD ';
            flag = true;
        }
        if(PM != null && PM != '' && PM != '0'){
            if(flag) QueryString += ' AND ';
            QueryString += ' Slick__Payment_Method__c =: PM ';
            // QueryString += flag ? ' AND ': ' ' + ' Slick__Payment_Method__c =: PM ';
            flag = true;
        }
        if(PS != null && PS != '' && PS != '0' ){
            if(flag) QueryString += ' AND ';
            QueryString += ' Slick__Payment_Status__c =: PS ';
            // QueryString += flag ? ' AND ': ' ' + ' Slick__Payment_Status__c =: PS ';
            flag = true;
        }
        if((OS == '0' || OS == null ) && DD == null && OD == null && (PM ==  '0' || PM == null) && (PS == '0' || PS == null)){
            QueryString = ' SELECT Id, Name, Slick__Contact__c, Slick__Charge_Amount__c, Slick__TotalDiscountTaxOther__c, Slick__Delivery_Date__c, Slick__Order_Date__c, Slick__OrderSource__c, Slick__Payment_Method__c, Slick__Payment_Status__c, Slick__Credit_Card_Used__c, Slick__Has_Critical_Note__c, Slick__Route__c FROM Opportunity ';
        }
        System.debug(QueryString);     
        OpportunityList = Database.query(QueryString);
        
        
        //return  [ SELECT Id, Job_Description__c, Job_level__c, Name, Max_Pay__c, Min_Pay__c, Close_Date__c, Open__c, Status__c, Hiring_Manager__c, Travel_Required__c, Type__c, Skills_Required__c FROM Position__c WHERE Job_Level__c =: OS AND Status__c =: PM AND Type__c =: PS AND Close_Date__c =: DD AND Open__c =:OD ];
        
        return OpportunityList;  
        
    }
    @AuraEnabled
    public static List<String> getPickListValues(String ObjName,String FieldName){
        String[] values = new String[]{};
            String[] types = new String[]{ObjName};
                Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
        for(Schema.DescribeSobjectResult res : results) {
            for (Schema.PicklistEntry entry : res.fields.getMap().get(FieldName).getDescribe().getPicklistValues()) {
                if (entry.isActive()) {values.add(entry.getValue());}
            }
        }
        return values;
    }



    

	
}