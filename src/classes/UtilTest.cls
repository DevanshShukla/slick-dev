@isTest
private class UtilTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Util.DayofWeek(system.now());
        Util.dayOfWeekAbbr(system.now());
        Util.DayofWeekGMT(system.now());
        Util.fixNull('test');
        Util.getCountries();
        Util.getCreditCards();
        Util.getExpirationMonths();
        Util.getExpirationYears();
        Util.getUSStates();
        Util.loadCookie('cooooookie');
        util.pageGet('testpage');
        contact c=new contact(lastname='test');
        insert c;
        opportunity o = new Opportunity(name='test',Slick__contact__c=c.id, closeDate=system.today(), stageName='test');
        insert o;
        util.queryOpportunityWithLines_AllFields(o.id);
        util.saveCookie('test', 'test', 1, true);

        util.pagemsg('error','error');
        util.pagemsg('confirm','confirm');
    }

    static TestMethod void contactAction_UnitTest(){
        Contact testContact = new Contact();
        testContact.FirstName = 'Test';
        testContact.LastName = 'Test';
        testContact.mailingPostalcode = '00011';
        testContact.otherPostalcode = '0001';
        insert testContact;

        test.startTest();
        
             Slick__Route__c r = new Slick__Route__c();
             r.name='Courier';
             insert r;
        
            PageReference myVfPage = Page.ContactActions;
            Test.setCurrentPage(myVfPage);
            ApexPages.currentPage().getParameters().put('action','recalculateroute');
            ContactActions_Extension ext = new ContactActions_Extension(new apexPages.standardController(testContact));
            ext.doAction();
        test.stopTest();
    }
    
    static TestMethod void contactAction_UnitTest1(){
        
        Contact testContact = new Contact();
        testContact.FirstName = 'Test';
        testContact.LastName = 'Test';
        testContact.otherPostalcode = '0001';
        insert testContact;

        test.startTest();
            PageReference myVfPage = Page.ContactActions;
            Test.setCurrentPage(myVfPage);
            ApexPages.currentPage().getParameters().put('action','recalculateroute');
            ContactActions_Extension ext = new ContactActions_Extension(new apexPages.standardController(testContact));
            ext.doAction();
        test.stopTest();
    }
    

}