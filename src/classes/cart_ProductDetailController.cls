public with sharing class cart_ProductDetailController {

	public transient Product2 thisProduct {get;set;}
	public transient decimal saleprice {get;set;}
	public transient decimal price {get;set;}
	public String selectedTab {get; set;}

	public cart_ProductDetailController(){
		selectedTab = '1';
		string productId = util.pageget('id');
		thisProduct = [
			SELECT id, name, Slick__Cart_category__c,Slick__Cart_Subcategory__c, description, Slick__category__c, Slick__family__c, Slick__thumbnail_image__c, Slick__full_size_image__c, Slick__on_sale__c, Slick__product_tab_set__c,
				(
					SELECT id, pricebook2.name, pricebook2.isstandard, unitprice FROM pricebookentries WHERE isactive=true AND pricebook2.isactive=true
				)
			FROM Product2
			WHERE id = :productId and Slick__active_in_cart__c=true
		];
		for (PricebookEntry pbe : thisProduct.pricebookentries){
			if (thisProduct.Slick__on_Sale__c && pbe.pricebook2.name == 'On Sale'){
				saleprice = pbe.unitPrice;
			} else if (pbe.pricebook2.isstandard){
				price = pbe.unitPrice;
			}
		}
	}
	public list<String> getProductCategories(){
		list<String> categories=new list<String>();
		if(thisProduct!=null && thisProduct.Slick__Cart_category__c!=null){
			categories=thisProduct.Slick__Cart_category__c.split(';');
			//list<String> uncleancategories=thisProduct.Slick__Cart_category__c.split(';');
			//for(String unclean:uncleanCategories){
			//	unclean=unclean.trim();
			//	String clean=EncodingUtil.urlEncode(unclean,'UTF-8');
			//	categories.add(clean);
			//}

		}
		return categories;
	}
	public list<String> getProductSubcategories(){
		list<String> subcategories=new list<String>();
		if(thisProduct!=null && thisProduct.Slick__Cart_subcategory__c!=null){
			subcategories=thisProduct.Slick__Cart_subcategory__c.split(';');
			//list<String> uncleanSubcategories=thisProduct.Slick__Cart_subcategory__c.split(';');
			//for(String unclean:uncleanSubcategories){
			//	unclean=unclean.trim();
			//	String clean=EncodingUtil.urlEncode(unclean,'UTF-8');
			//	subcategories.add(clean);
			//}
		}
		return subcategories;
	}

	public pagereference onload(){
		product_Methods.incrementView(thisProduct.id);
		cart_RecentlyViewedController.addToRecentViewed(thisProduct.id);
		return null;
	}

	public Slick__Product_Tab__c getProductTab(){
		list<Slick__Product_Tab__c> plist = [select id, name, Slick__order__c, Slick__product_tab_set__c, Slick__tab_content__c, (select name, Slick__order__c, Slick__product_tab_set__c, Slick__tab_content__c from Product_Tabs__r order by Slick__order__c) from Slick__Product_Tab__c where id = : thisProduct.Slick__product_tab_set__c order by Slick__order__c];
		return (plist.isEmpty())?(new Slick__Product_Tab__c()):plist[0];
	}
	
	public static void test(){
	    Integer a,b,c;
	    a=1;
	    b=a;
	    c=a;
	    a=c;
	    b=a;
	    c=a;
	    a=c;
	    b=a;
	    c=a;
	    a=c;
	    b=a;
	    c=a;
	    a=c;
	    b=a;
	    c=a;
	    a=c;
	    b=a;
	    c=a;
	    a=c;
	    b=a;
	    c=a;
	    a=c;
	    b=a;
	    c=a;
	    a=c;
	    b=a;
	    c=a;
	    a=c;
	    b=a;
	    c=a;
	    a=c;
	    b=a;
	    c=a;
	    a=c;
	    b=a;
	    c=a;
	    a=c;
	    b=a;
	    c=a;
	    a=c;
	    b=a;
	    c=a;
	    a=c;
	}
}