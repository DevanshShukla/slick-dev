/**
 * taskCleanup_Batch_Scheduler Class
 * @author: M. Wicherski, MK Partners 11/2012
 * @Description: Control method for scheduling of taskCleanup_Batch job(s)
 */

/*
	To Run:
		taskCleanup_Batch_Scheduler m = new taskCleanup_Batch_Scheduler();
		String sch  =  '0 0 * * * ?';
		String jobId = system.schedule('taskCleanup_Batch', sch, m);

	To Check Status:
		CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
 
*/

global  class  taskCleanup_Batch_Scheduler  implements  Schedulable{
	global  void  execute(SchedulableContext  sc)  {
		taskCleanup_Batch b = new taskCleanup_Batch();
		database.executebatch(b,1);
	}

	

}