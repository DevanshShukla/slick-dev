public with sharing class cart_CatalogController {

	Static Slick__Instance_Setting__c websiteTitle = Slick__Instance_Setting__c.getInstance('Website Title');
	public transient integer resultCount {get;set;}
	public transient list<Product2> results {get;set;}
	public transient list<WrapperClasses.cartItem> resultItems{get;set;}

	public transient string family {get;set;}
	public transient string category {get;set;}

	public transient Slick__Cart_Department__c department{get;set;}
	public transient list<String> subcategories{get;set;}
	public transient map<String,categoryProductWrapper> ProductsBySubCategoryMap;

	public transient list<departmentWrapper> allDepartments;

	public static map<String,subcategoryPositionWrapper> subcategoryPositionMap{get;set;}
	public cart_CatalogController(){
		//string family = util.pageget('family');
		//string category = util.pageget('category');
		family = util.pageget('subcategory');
		category = util.pageget('category');
		if(!string.isblank(category)){
			list<Slick__Cart_Department__c> cDept=[
				SELECT id,Slick__Label__c,Slick__category__c,Slick__subcategories__c,Slick__Banner_Image_url__c
				FROM Slick__Cart_Department__c
				WHERE Slick__category__c=:category
			];
			if(cDept.size()>0){
				department=cDept.get(0);
				initSubcategories();
			}
		}
		results = new list<Product2>();
		resultItems=new list<wrapperClasses.cartItem>();
		if (!string.isblank(family) && !string.isblank(category)){
			list<String> subcatq=new list<String>{family};
			results = [
				SELECT Id, Name, Slick__Thumbnail_image__c, Slick__full_size_image__c, Slick__category__c,Slick__cart_Category__c,Slick__cart_SubCategory__c,Slick__on_sale__c,
				(
					SELECT id, pricebook2.name, pricebook2.isstandard, unitprice
					FROM pricebookentries
					WHERE isactive=true
					AND pricebook2.isactive=true
				)
				FROM Product2
				WHERE isactive=true
				AND Slick__active_in_cart__c=true
				AND Slick__cart_category__c includes (:category)
				AND Slick__cart_subCategory__c includes (:family)
				AND Slick__Hide_from_Search_Results__c = false
				ORDER BY Slick__Ordering_Position__c,name
			];
		}
		else if (!string.isblank(family) && string.isblank(category)){
			list<String> subcatq=new list<String>{family};
			results = [
				SELECT Id, Name, Slick__Thumbnail_image__c, Slick__full_size_image__c, Slick__category__c,Slick__cart_Category__c,Slick__cart_SubCategory__c,Slick__on_sale__c,
				(
					SELECT id, pricebook2.name, pricebook2.isstandard, unitprice
					FROM pricebookentries
					WHERE isactive=true
					AND pricebook2.isactive=true
				)
				FROM Product2 WHERE isactive=true
				AND Slick__active_in_cart__c=true
				AND Slick__cart_subCategory__c includes (:family)
				AND Slick__Hide_from_Search_Results__c = false
				ORDER BY Slick__Ordering_Position__c,name
			];
		}
		else if (!string.isblank(category)){
			results = [
				SELECT Id, Name, Slick__Thumbnail_image__c, Slick__full_size_image__c, Slick__category__c,Slick__cart_Category__c,Slick__cart_SubCategory__c,Slick__on_sale__c,
				(
					SELECT id, pricebook2.name, pricebook2.isstandard, unitprice
					FROM pricebookentries
					WHERE isactive = true
					AND pricebook2.isactive = true
				)
				FROM Product2
				WHERE isactive = true
				AND Slick__active_in_cart__c = true
				AND Slick__cart_category__c includes (:category)
				AND Slick__Hide_from_Search_Results__c = false
				ORDER BY Slick__Ordering_Position__c,name
			];
		}
		else {
			results = [
				SELECT Id, Name, Slick__Thumbnail_image__c, Slick__full_size_image__c, Slick__category__c,Slick__cart_Category__c,Slick__cart_SubCategory__c,Slick__on_sale__c,
				(
					SELECT id, pricebook2.name, pricebook2.isstandard, unitprice
					FROM pricebookentries
					WHERE isactive=true
					AND pricebook2.isactive=true
				)
				FROM Product2
				WHERE isactive = true
				AND Slick__active_in_cart__c = true
				AND Slick__Hide_from_Search_Results__c = false
				ORDER BY name
			];
		}
		resultCount = results.size();
		for( product2 pResult:results ){
			decimal saleprice;
			decimal price=0;
			for( priceBookEntry pbe:pResult.pricebookEntries ){
				if (pResult.Slick__on_Sale__c && pbe.pricebook2.name == 'On Sale')
					saleprice = pbe.unitPrice;
				else if (pbe.pricebook2.isstandard)
					price = pbe.unitPrice;
			}
			resultItems.add(new wrapperClasses.cartItem(pResult,null,price,saleprice));
		}
	}

	public String getWebsiteTitle(){
		return (websiteTitle != null && String.isNotBlank(websiteTitle.Slick__Text_Value__c)) ? websiteTitle.Slick__Text_Value__c : 'Nature\'s Select';
	}

	public void initSubCategories(){
		subcategories=new list<String>();
		if(subcategoryPositionMap==null){
			subcategoryPositionMap=new map<String,subcategoryPositionWrapper>();
			//Get describe because multipicklist default order bug.
			List<Schema.PicklistEntry> ples =Slick__Cart_Department__c.Slick__Subcategories__c.getDescribe().getPicklistValues();
			integer count=0;
			for(Schema.picklistEntry ple:ples){
				//system.debug(pp);
				subcategoryPositionMap.put(ple.getvalue(),new subcategoryPositionWrapper(ple.getValue(),count));
				count++;
			}


		}
		if(department.Slick__subCategories__c!=null){
			//retain user order

			subcategories=department.Slick__subCategories__c.split(';');


			list<SubcategoryPositionWrapper> spws=new list<SubcategoryPositionWrapper>();
			for(string subcatStr: subcategories){
				if(subcategoryPositionMap.containskey(subcatStr))
					spws.add(subcategoryPositionMap.get(subcatStr));
			}
			spws.sort();
			subcategories=new list<String>();
			for(SubcategoryPositionWrapper spw:spws){
				subcategories.add(spw.subcategory);
			}
			System.debug('after sub cats: '+ subcategories);
		}
	}
	public map<String,categoryProductWrapper> getProductsBySubCategoryMap(){
		if(ProductsBySubCategoryMap==null){
			ProductsBySubCategoryMap=new map<String,categoryProductWrapper>();
			if(department!=null){
				if(department.Slick__subCategories__c!=null){
					for(String subcat:subCategories)
						ProductsBySubCategoryMap.put(subCat,new categoryProductWrapper());
				}
				if(resultItems!=null){
					for(wrapperClasses.cartItem resultItem:resultItems){
						list<String> resultSubCats=new list<String>();
						if(resultItem.subcategories!=null)
							resultSubCats=resultItem.subcategories.split(';');
						for(String subCat:resultSubCats){
							//only map subcategories pertaining to this category
							if(ProductsBySubCategoryMap.containskey(subCat)){
								ProductsBySubCategoryMap.get(subCat).productItems.add(resultItem);
								ProductsBySubCategoryMap.get(subCat).hasProduct=true;

							}
						}
					}
				}

			}
		}
		System.debug('get subcategory map '+productsbySubcategoryMap);
		return ProductsBySubCategoryMap;
	}

	public list<departmentWrapper> getAllDepartments(){
		if(allDepartments==null){
			list<Slick__cart_Department__c> departmentQuery=[select id,Slick__category__c,Slick__label__c,Slick__subcategories__c from Slick__cart_department__c where Slick__isinactive__c = false order by Slick__order__c];
			allDepartments=new list<departmentWrapper>();
			for(Slick__cart_department__c dept: departmentQuery){
				departmentWrapper dw=new departmentWrapper(dept.Slick__label__c);
				if(dept.Slick__subcategories__c!=null){
					dw.subcategories=dept.Slick__subcategories__c.split(';');
				}
				allDepartments.add(dw);
			}
		}
		return allDepartments;
	}

	public class departmentWrapper{
		public String name{get;set;}
		public list<String> subcategories{get;set;}
		public departmentWrapper(String name){
			this.name=name;
		}

	}
	public class categoryProductWrapper{
		public list<product2> products{get;set;}
		public list<wrapperClasses.cartItem> productItems{get;set;}
		public boolean  hasProduct{get;set;}
		public categoryProductWrapper(){
			this.products=new list<product2>();
			this.productItems=new list<wrapperClasses.cartItem>();
			this.hasProduct=false;
		}
	}

	public class subcategoryPositionWrapper implements Comparable{
		public string subcategory{get;set;}
		public integer position{get;set;}
		public subcategoryPositionWrapper(String subcategory,Integer position){
			this.subcategory=subcategory;
			this.position=position;
		}
		public integer compareTo(object other){
			subcategoryPositionWrapper compareVal=(subcategoryPositionWrapper)other;
			if(this.position==compareVal.position)
				return 0;
			else if(this.position>compareVal.position)
				return 1;
			else
				return -1;
		}

	}

}