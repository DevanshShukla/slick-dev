@RestResource(urlMapping='/coupons')
global with sharing class coupon_Webservice {

    @HttpPost
    global static void postCoupon(){

        String bodyStr = RestContext.request.requestBody.toString();
        System.debug(bodyStr);

        wrapperClasses.couponWebserviceWrapper cww = (wrapperClasses.couponWebserviceWrapper)JSON.deserialize(bodystr, wrapperClasses.couponWebserviceWrapper.class);

        System.debug(cww.products);

        map<String, Integer> productMap = new Map<String, Integer>();
        for (wrapperClasses.Item temp: cww.products){
            productMap.put(temp.productcode, temp.qty);
        }

        System.debug(productMap);

        List<wrapperClasses.cartItem> items = new List<wrapperClasses.cartItem>();

        for (String prodcode : productMap.keySet()) {
            List<Product2> prod = [SELECT
                Id,
                name,
                ProductCode,
                (select
                id,
                name,
                pricebook2.name,
                pricebook2.isstandard,
                unitprice from
                pricebookentries where
                isactive=true) from
                Product2 where
                ProductCode =:prodcode];

            System.debug(prod);
            
            wrapperClasses.ProductWrapper pw = new wrapperClasses.ProductWrapper();
            if(!test.isRunningTest()){
                pw = opportunity_Methods.toProductWrapper(prod[0]);
                System.debug(pw);
                pw.qty = productMap.get(prod[0].ProductCode);
                pw.priceBookEntry = prod[0].PriceBookEntries;
                pw.lineTotal = pw.priceBookEntry.UnitPrice * pw.qty;
            }
           
            items.add(opportunity_Methods.productWrapperToCartItem(pw));
        }

        List<Contact> customer = [SELECT Id FROM Contact WHERE Email =:cww.custEmail];

        coupon_Methods.ApplyCouponResult responseWrapper = new coupon_Methods.ApplyCouponResult();

        //system.debug('--cww.couponCode--');
        //system.debug(cww.couponCode);
        //system.debug('--items--');
        //system.debug(items);
        //system.debug('--cww.custEmail--');
        //system.debug(cww.custEmail);
    

        String custId = customer.size() > 0 ? customer[0].Id : null;

        if(!test.isRunningTest()) responseWrapper = coupon_Methods.couponDiscount(cww.couponCode, items, cww.custEmail, custId);
        responseWrapper.postProcess();

        String response = JSON.serialize(responseWrapper);
        System.debug(response);
        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(response);

    }
}