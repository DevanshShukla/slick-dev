public with sharing class couponEdit_Extension {
	
	public RecordType r {get;set;}

	public couponEdit_Extension (Apexpages.standardcontroller con){
		r = new RecordType();	
		system.debug(con.getRecord());
		//override defaults
		if(con.getRecord().get('id')==null)
			con.getrecord().put('Slick__coupon_type__c','');

		if (apexpages.currentpage().getparameters().get('RecordType') != null) {
			r = [select id, name from RecordType where id = :apexpages.currentpage().getparameters().get('RecordType') AND sobjectType='Slick__Coupon__c'];	
		}
		contact customer;
		if (String.isnotblank(apexpages.currentpage().getparameters().get('customer'))) {
			customer = [select id,name,lastname, firstname from contact where id = :apexpages.currentpage().getparameters().get('customer')];
			con.getRecord().put('Slick__customer__c',customer.id);
		}
		
		if (String.isnotblank(apexpages.currentpage().getparameters().get('ctype'))) {
			string ctype = apexpages.currentpage().getparameters().get('ctype');
			System.debug('ctype:'+ctype);
			con.getRecord().put('Slick__start_date__c',system.today());
			string rtype = (ctype=='i')?'Individual Coupon':(ctype=='c')?'Credit Coupon':(ctype=='r')?'Referral Reward':(ctype=='n')?'New Customer':'';
			if (rtype.trim().length()>0){
				r = [select id, name from RecordType where name=:rtype AND sobjectType='Slick__Coupon__c'];
				con.getRecord().put('recordtypeid',r.id);
			}
			if (ctype == 'c'){
				con.getRecord().put('Slick__coupon_type__c','Entire Order');
				con.getRecord().put('name','Credit'+customer.lastname+'{auto#}');
				con.getRecord().put('Slick__discount_type__c','Dollar Off');
				con.getRecord().put('Slick__Max_Uses__c',1);
				con.getRecord().put('Slick__Max_Individual_Uses__c',1);
				con.getRecord().put('Slick__expiration_date__c',system.today().adddays(365));
			}
			
			if (ctype=='n'){
				con.getRecord().put('Slick__Max_Individual_Uses__c',1);
			}
			
			if (ctype=='r') {
				if(customer==null){
					ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error,'No Referred Customer Found'));

				}
				else{
					con.getRecord().put('name','Thanks'+customer.firstname+'{auto#}');
					con.getRecord().put('Slick__expiration_date__c',system.today().adddays(365));
				}
			}
			System.debug(con.getRecord());
		}
		
		if (apexpages.currentpage().getparameters().get('amt')!= null) {
			con.getRecord().put('Slick__discount_amount__c',apexpages.currentpage().getparameters().get('amt'));
		}
		
		if (apexpages.currentpage().getparameters().get('maxiuse')!= null) {
			con.getRecord().put('',apexpages.currentpage().getparameters().get('maxiuse'));
		}
		
	} 

	
	
	/*@isTest //(seealldata=true)
	static void couponEdit_test(){
		contact c = new contact(lastname='last', firstname='first');
		insert c;
		
		Pagereference p = page.coupon_Edit;
		p.getparameters().put('RecordType',[select id from recordtype where name = 'Individual Coupon' and sobjecttype ='Slick__Coupon__c'].id);
		p.getparameters().put('customer',c.id);
		test.setCurrentPage(p);
		apexpages.standardcontroller scon = new apexpages.standardcontroller(new Slick__coupon__c());
		couponEdit_Extension ext = new couponEdit_Extension(scon);
	
		p.getparameters().put('RecordType',[select id from recordtype where name = 'Credit Coupon' and sobjecttype ='Slick__Coupon__c'].id);
		p.getparameters().put('customer',c.id);
		test.setCurrentPage(p);
		
		scon = new apexpages.standardcontroller(new Slick__coupon__c());
		ext = new couponEdit_Extension(scon);
		
		p.getparameters().put('RecordType',[select id from recordtype where name = 'Referral Reward' and sobjecttype ='Slick__Coupon__c'].id);
		p.getparameters().put('customer',c.id);
		test.setCurrentPage(p);
		
		scon = new apexpages.standardcontroller(new Slick__coupon__c());
		ext = new couponEdit_Extension(scon);
		
		p.getparameters().put('RecordType',[select id from recordtype where name = 'New Customer' and sobjecttype ='Slick__Coupon__c'].id);
		p.getparameters().put('customer',c.id);
		test.setCurrentPage(p);
		
		scon = new apexpages.standardcontroller(new Slick__coupon__c());
		ext = new couponEdit_Extension(scon);
		
		p.getparameters().put('ctype',[select id from recordtype where name = 'Credit Coupon' and sobjecttype ='Slick__Coupon__c'].id);
		p.getparameters().put('customer',c.id);
		test.setCurrentPage(p);
		
		scon = new apexpages.standardcontroller(new Slick__coupon__c());
		ext = new couponEdit_Extension(scon);
		
		p.getparameters().put('ctype',[select id from recordtype where name = 'Referral Reward' and sobjecttype ='Slick__Coupon__c'].id);
		p.getparameters().put('customer',c.id);
		test.setCurrentPage(p);
		
		scon = new apexpages.standardcontroller(new Slick__coupon__c());
		ext = new couponEdit_Extension(scon);
		
		p.getparameters().put('ctype',[select id from recordtype where name = 'New Customer' and sobjecttype ='Slick__Coupon__c'].id);
		p.getparameters().put('customer',c.id);
		test.setCurrentPage(p);
		
		scon = new apexpages.standardcontroller(new Slick__coupon__c());
		ext = new couponEdit_Extension(scon);
		
		p.getparameters().put('ctype',[select id from recordtype where name = 'Individual Coupon' and sobjecttype ='Slick__Coupon__c'].id);
		p.getparameters().put('customer',c.id);
		test.setCurrentPage(p);
		
		scon = new apexpages.standardcontroller(new Slick__coupon__c());
		ext = new couponEdit_Extension(scon);
		
	}*/
}