@isTest
private class Util_Test_Class {
    static testMethod void getTests(){
        test.startTest();
             
            OrderRecalculation_Extension.dummy();
            
            List<SObject> contactList = new List<SObject>();
            for(Integer i =0 ; i<100;i++){
                Contact c = new Contact(firstname='test', lastname='test', OtherStreet='other street test', email='ramcda1111111111111@hotmail.com');
                contactList.add(c);
            }
            
            insert contactList;
            
            batch_DML batch = new batch_DML(contactList, true);
            batch_DML.startBatch(contactList, true);
            
            email_Methods em = new email_Methods();
            
            cart_SearchController cs = new cart_SearchController();
            cart_SearchController.dummy();
            
            ropBatch_Scheduler sh1 = new ropBatch_Scheduler();
            
            urlRewriter.dummy();

            String sch = '0 0 23 * * ?'; 
            system.schedule('Test Territory Check', sch, sh1); 

        test.stopTest();
    }
    
    static testMethod void getTests2(){
        test.startTest();
             
            OrderRecalculation_Extension.dummy();
            
            List<SObject> contactList = new List<SObject>();
            for(Integer i =0 ; i<100;i++){
                Contact c = new Contact(firstname='test', lastname='test', OtherStreet='other street test', email='ramcda1111111111111@hotmail.com');
                contactList.add(c);
            }
            
            insert contactList;
            
            batch_DML batch = new batch_DML(contactList, true);
            batch_DML.scheduleBatch(contactList, true);
        test.stopTest();
    }
	
	@isTest
	static void authDotNet_Test() {
		api_AuthorizeDotNet.authnetReq_Wrapper testin = new api_AuthorizeDotNet.authnetReq_Wrapper();
		testin.ordername = 'test';
		testin.ccnum = '370000000000002';
		testin.ccexp = '02/2015';
		testin.ccsec = '5643';
		testin.amt = '10';
		testin.firstname = 'first';
		testin.lastname = 'last';
		testin.billstreet = 'street';
		testin.billcity = 'city';
		testin.billstate = 'state';
		testin.billzip = 'billzip';
		testin.transid = '0';
		testin.accountnumber = '00001234123412';
		testin.bankaccountname = 'test test';
		testin.bankaccounttype = 'CHECKING';
		testin.bankname = 'OBFA';
		testin.routingnumber = '121000123';

		boolean testResponse = api_AuthorizeDotNet.authdotnetChargeSimple(testin);
		api_AuthorizeDotNet.authdotnetChargeDetailed (testin);
		api_AuthorizeDotNet.authdotnetRefund (testin);
		api_AuthorizeDotNet.authdotnetVoid (testin);
	}
	
	@isTest 
	static void taskCleanup_Batch_Scheduler_Test() {
		taskCleanup_Batch job = new taskCleanup_Batch();
		Id processId = Database.executeBatch(job);
		
		contact c1 = new Contact();
		c1.lastname = 'test1';
		c1.Slick__Last_Delivery_Date__c = system.today();
		
		insert c1; 
		
		Task t = new Task();
		t.Subject = 'DEBUG';
		t.Description = '';
		t.Status = 'Debug';
		t.WhoId = c1.id;
		
		insert t;
		
	
		
		taskCleanup_Batch_Scheduler m = new taskCleanup_Batch_Scheduler();
		String sch  =  '0 0 * * * ?';
		String jobId = system.schedule('test_taskCleanup_Batch', sch, m);  
	}
	
	@isTest
	static void ropBatch_Test() {
		batch_ROP_Batch job = new batch_ROP_Batch();
		Id processId = Database.executeBatch(job);
		
		contact c1 = new Contact();
		c1.lastname = 'test1';
		c1.Slick__Next_Order_Date__c = system.today().adddays(-7);
		
		contact c2 = new Contact();
		c2.lastname = 'test2';
		c2.Slick__Next_Order_Date__c = system.today().adddays(-5);
		c2.Slick__Auto_Reorder__c = false;
		
		insert new list<Contact>{c1,c2};
		
		//string x = batch_ROP_Batch.getQueryString();
		list<Contact> customers = new list<Contact>{c1,c2};
		///ropBatch.exec(customers);
	}
}