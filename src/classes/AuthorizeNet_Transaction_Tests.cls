@isTest
private class AuthorizeNet_Transaction_Tests {

	@isTest static void testAuthorizeNet_Transactions() {
		test.startTest();
			Contact c=new contact(lastname='tester',email='test@test.com');
			insert c;
			Slick__credit_card__c card=new Slick__credit_card__c(Slick__contact__c=c.id,Slick__name_on_card__c='john doe');
			insert card;
			Opportunity opp = new Opportunity(name='test',Slick__credit_card_used__c=card.id,Slick__delivery_date__c=date.today(), amount=400, closeDate=system.today(), stageName='Closed Won');
	        insert opp;
	        PageReference currentpage=new pageReference('/?OpportunityId='+opp.id);
	        currentPage.getParameters().put('action','charge');
	        Test.setCurrentPage(currentPage);
	        AuthorizeNet_Transaction_Methods authMethods=new AuthorizeNet_Transaction_Methods();
	        authMethods.doAction();
	        authMethods.confirm();
	        authMethods.Cancel();
	        authMethods.action='authorize';
	        authMethods.doAction();
	        authMethods.confirm();
	        list<Slick__Authorize_net_Transaction__c> trans=[select id,Slick__transaction_Type__c from Slick__Authorize_net_Transaction__c];
	        if(trans.size()>0){
	        	Slick__Authorize_net_transaction__c tranRecord=trans.get(0);
	        	 currentPage.getParameters().put('action','void');
	        	 currentPage.getParameters().put('TransactionId',trans.get(0).id);
	        	 authMethods=new AuthorizeNet_Transaction_Methods();
	        	 authMethods.doAction();
	        	 authMethods.action='refund';
	        	 authMethods.doAction();
	        	 authMethods.Cancel();
	        	

	        	 tranRecord.Slick__transaction_Type__c='auth_only';
	        	 update tranRecord;
	        	 authMethods=new AuthorizeNet_Transaction_Methods();
	        	 authMethods.action='prior_Auth_Capture';
	        	 authMethods.doAction();

	        }
	       AuthorizeNet_Transaction_Methods.createTransaction(authMethods.oppty);
	       // Slick__Authorize_net_Transaction__c=authMethods.Slick__Authorize_net_Transaction__c;


		test.stopTest();	
	}
	@isTest static void testTransactionProcessing_Controller() {
		test.startTest();

		Account a = new account(name='testtransactionproc');
		insert a;
		Slick__Route__c r = new Slick__Route__c(Slick__zip__c='91019', Slick__time__c='10:10' );
		insert r;
		Contact c = new Contact(lastname='testlast', accountid = a.id, Slick__route__c = r.id, Slick__Last_Date_Ordered__c = system.today());
		insert c;
		Opportunity o = new Opportunity();
		o.AccountId = a.Id;
		o.Amount = 0;
		o.CloseDate = system.today();
		o.Slick__Contact__c = c.Id;
		o.Slick__Delivery_Date__c = system.today();
		o.Slick__Discount__c = '1%';
		o.Slick__Applied_Coupon_Codes__c = 'test';
		o.Name = string.valueOf(system.now());
		o.StageName = 'Closed Won';
		o.Slick__payment_method__c = 'Credit Card';
		insert o;
		
		transactionProcessing_Controller con = new transactionProcessing_Controller();
		con.getOrderSourceOptions();
		con.getPaymentStatusOptions();
		con.selectAll();
		con.updateUseFileInfo();
		
		con.transactionslist[0].selected=true;
		con.chargeSelected();
		con.authorizeSelected();
		
		con.ordersource='Back-End';
		con.deliveryDateHolder.activitydate = system.today();
		con.orderDateHolder.activitydate = system.today();
		con.paymentstatusfilter = 'RedDot';
		con.filter();
	
		test.stopTest();
	}
	
	@isTest static void testCreditCard_Extension() {
		test.startTest();
		  contact c=new contact(lastname='test',email='testNS@test.com');
		  insert c;
		  Slick__credit_card__c card=new Slick__credit_card__c(Slick__contact__c=c.id);
		  card.Slick__contact__c=c.id;
		  ApexPages.standardController std=new ApexPages.standardController(card);
		  Credit_Card_Extension cce=new Credit_Card_Extension(std);
		  cce.card.Slick__card_number__c='12345';
		  cce.card.Slick__name_on_card__c='tester t tester t tester t tester t tester t tester';
		  cce.card.Slick__card_type__c='Visa';
		  cce.setCreditCardName();
		 c.otherStreet='test';
		 c.otherCity='test';
		 c.otherState='test';
		 c.otherPostalCode='test';
		 update c;
		  cce=new Credit_Card_Extension(std);
    
        AuthorizeNet_Transaction_Methods am = new AuthorizeNet_Transaction_Methods();
        am.demo();
        cce.customSave();
        cce.checkCardType();
		test.stopTest();

	}


}