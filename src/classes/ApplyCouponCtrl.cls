public class ApplyCouponCtrl {

    public String CouponCode {get; set;}
    public String Msg {get; set;}
    public Decimal TotalAmount {get; set;}
    public Decimal Discount {get; set;} // Percentage or Amount
    public Decimal DiscountedAmount {get; set;}
    public String CouponId {get;set;}

    public Decimal countDiscount(){
        Date today = System.today();
        Slick__Coupon__c coupon = new Slick__Coupon__c();
        coupon = Database.query(util.queryData('Slick__Coupon__c', '',' WHERE Name =: CouponCode AND Slick__Max_Uses__c > 0 AND Slick__Start_Date__c < today AND Slick__Expiration_Date__c > today LIMIT 1'));
        CouponId = coupon.Id;
        if(coupon != new Slick__Coupon__c() ){
            Discount = coupon.Slick__Discount_Amount__c == null ? 0 : coupon.Slick__Discount_Amount__c;
            if(coupon.Slick__Discount_Type__c == 'Dollar Off'){
                DiscountedAmount = TotalAmount - Discount;
            }else{
                DiscountedAmount = TotalAmount - ( ( 100 * Discount ) / TotalAmount );
            }

            if(TotalAmount > coupon.Slick__Min_Order_Amnt_for_Target__c){
                Msg = ' Coupon code Applied! ';
            }else{
                Msg = ' Minimum cart value should be '+coupon.Slick__Min_Order_Amnt_for_Target__c;
            }
        }else{
            Msg = ' No coupon code found. Or Code expired. ';
        }
        return DiscountedAmount;
    }
    
    public Slick__Coupon__c orderPlaced(){
        Slick__Coupon__c coupon = new Slick__Coupon__c();
        coupon.Id = CouponId;
        coupon.Slick__Max_Uses__c = coupon.Slick__Max_Uses__c - 1;
        update coupon; 
        return coupon;
    }

    // METHOD RETURNING DISCOUNTED AMOUNT
    public static Decimal countDiscount(String CouponCode, Decimal TotalAmount ){
        Date today = System.today();
        Decimal DiscountedAmount = 0;
        Slick__Coupon__c coupon = new Slick__Coupon__c();
        coupon = Database.query(util.queryData('Slick__Coupon__c', '',' WHERE Name =: CouponCode AND Slick__Max_Uses__c > 0 AND Slick__Start_Date__c < today AND Slick__Expiration_Date__c > today LIMIT 1'));
        
        if(coupon != new Slick__Coupon__c() ){
            Decimal Discount = coupon.Slick__Discount_Amount__c == null ? 0 : coupon.Slick__Discount_Amount__c;
            if(coupon.Slick__Discount_Type__c == 'Dollar Off'){
                DiscountedAmount = TotalAmount - Discount;
            }else{
                DiscountedAmount = TotalAmount - ( ( 100 * Discount ) / TotalAmount );
            }
        }else{
            DiscountedAmount = TotalAmount;
        }
        return DiscountedAmount;
    }

    // CALL THIS METHOD TO UPDATE THE COUPON RECORD, WHEN ORDER IS PLACED.
    public Slick__Coupon__c orderPlaced(String CouponId){
        Slick__Coupon__c coupon = new Slick__Coupon__c();
        coupon.Id = CouponId;
        coupon.Slick__Max_Uses__c = coupon.Slick__Max_Uses__c - 1;
        update coupon; 
        return coupon;
    }
}