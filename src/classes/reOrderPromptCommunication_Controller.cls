global with sharing class reOrderPromptCommunication_Controller {

	global Id thisId {get;set;}
	global emailWrapper eWrapper; 
	global String thisEmail{get;set;}

	global String ROPType{get;set;}
	global String ROPSuccessBody;
	
	//Legacy wrapper, other lists may be used in future.
	global class emailWrapper{
		public String lastDeliveryDate {get;set;}
		public String routeDeliveryDay {get;set;}
		public String neverFeedSuggestion {get;set;}
		public Date nextAvailableDeliveryDate {get;set;}
		public List<String> lastOrderProducts {get;set;}
		public List<String> last12MonthProducts {get;set;}
		public List<String> suggested2Products {get;set;}
		public String SiteURL{get;set;}
		public String Name {get;set;} 
		public boolean AutoReorder {get;set;}
		public Date NextOrderDate {get;set;}
		public String ContactRouteDay{get;set;}

	}
	//The products purchased in the last 12 months, only active products, only listed once.
	global emailWrapper getEWrapper(){
		if(eWrapper==null){
			eWrapper=new emailWrapper();
			eWrapper.lastOrderProducts=new list<String>();
			eWrapper.last12MonthProducts=new list<String>();
			if ( thisId == null ){
				thisId = apexPages.currentPage().getParameters().get('thisId');
			}
			
			System.debug('this Id in Class>>>>>>>>>>>>>>>>'+thisId);
			if(thisId!=null){
				Date last12Months = system.today().addDays(-365);		
				List<Opportunity> opptyQuery = [
				Select Id, contact__r.Slick__Next_Order_Date__c,Slick__Contact__c,Contact__r.Slick__route__c,Contact__r.route__r.Slick__day__c, Contact__r.Name, Contact__r.Route__r.Slick__Next_Delivery_Date__c, CloseDate, Slick__Delivery_Date__c,Slick__Delivery_Day_Of_Week__c,
					(Select UnitPrice, Slick__Tax__c, SortOrder, ServiceDate, Quantity, PricebookEntryId, OpportunityId, ListPrice, TotalPrice,  Id, Description, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Description, PriceBookEntry.Product2.Family, PriceBookEntry.Product2.Slick__Group__c, PriceBookEntry.Product2.Name, PriceBookEntry.ProductCode, PricebookEntry.Name, PricebookEntry.IsActive, PriceBookEntry.UnitPrice,product2.isActive,product2.Slick__active_in_cart__c from OpportunityLineItems)
				from Opportunity where Slick__Delivery_Date__c >= :last12Months and IsWon = true and Slick__Contact__c = :thisId and Slick__this_is_a_return__c = false order by CloseDate desc
				];
				set<String> past12ProductSet=new set<String>();
				set<id> productSet=new set<id>();
				
				if ( opptyQuery.size() > 0 ){
					//current
					Opportunity mostRecentOppty = opptyQuery.get(0);
					for(OpportunityLineItem oli: mostRecentOppty.OpportunityLineItems){
						productSet.add(oli.pricebookentry.product2id);
						eWrapper.lastOrderProducts.add(oli.pricebookentry.product2.name);
					}
					eWrapper.LastDeliveryDate = mostRecentOppty.Slick__Delivery_Date__c.format();
					eWrapper.routeDeliveryDay = mostRecentOppty.Slick__Delivery_Day_Of_Week__c;
					eWrapper.Name = mostRecentOppty.Contact__r.Name; 
					//eWrapper.AutoReorder = mostRecentOppty.Contact__r.Slick__Auto_Reorder__c;
					eWrapper.NextOrderDate = mostRecentOppty.Contact__r.Slick__Next_Order_Date__c; 
					if ( mostRecentOppty.Contact__r.Route__r.Slick__Next_Delivery_Date__c != null){
						eWrapper.nextAvailableDeliveryDate = mostRecentOppty.Contact__r.Route__r.Slick__Next_Delivery_Date__c;	//need to update this
					}
					//3/19/2015
					if(mostRecentOppty.Contact__r.Slick__route__c!=null){
						eWrapper.ContactRouteDay= mostRecentOppty.Contact__r.route__r.Slick__day__c;
					}
					//last 12 months
					for ( Integer i=1;i<opptyQuery.size();i++ ){
						for ( OpportunityLineItem oli : opptyQuery[i].OpportunityLineItems ){				
							if(!productset.contains(oli.pricebookentry.product2id) && !past12productset.contains(oli.pricebookentry.product2.name) && oli.pricebookEntry.isActive==true && oli.product2.isactive==true && oli.product2.Slick__active_in_cart__c==true){
								productSet.add(oli.pricebookentry.product2id);
								past12ProductSet.add(oli.pricebookentry.product2.name);
							}
						}
					}					
					for(String prod:past12ProductSet){
						eWrapper.last12MonthProducts.add(prod);
					}
				}			
			}
			//populate site
			Slick__Instance_Setting__c siteURL = (test.isRunningTest())?
									(new Slick__Instance_Setting__c(name='Site URL', Slick__text_value__c = 'http://www.test.com')):
									((Slick__Instance_Setting__c.getInstance('Site URL')));
			if(siteURL!=null)
				eWrapper.siteURL=siteURL.Slick__text_Value__c;
		}
			return eWrapper;
	}
	global String getROPSuccessBody(){
		if(ROPSuccessBody==null){
			if(ROPType=='OneClick'){
				list<Slick__Merge_item__c> mItem=[select id, Slick__body__c from Slick__Merge_item__c where name='ROP One Click Success'];
				if(mItem.size()>0){
					ROPSuccessBody= mItem.get(0).Slick__body__c;
				}
				else
					ROPSuccessBody='null';
				
			}
			else if(ROPType=='YourRequest'){
				list<Slick__Merge_item__c> mItem=[select id, Slick__body__c from Slick__Merge_item__c where name='ROP Your Request Success'];
				if(mItem.size()>0){
					ROPSuccessBody= mItem.get(0).Slick__body__c;
				}
				else
				   ROPSuccessBody='null';			
			}
		}
		return ROPSuccessBody;
	}


}