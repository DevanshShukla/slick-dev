public without sharing class reOrder_Methods {

	static map<id, PricebookEntry> updatedPrices;
	static map<id, PricebookEntry> updatedOnSalePrices;

	//copy relevant information from Opportunity
	public static wrapperClasses.opptyCloneWrapper reconstructOrder(id orderid, boolean save){
	    
		Opportunity oldOrder = util.queryOpportunityWithLines_AllFields(orderid);
		Opportunity newOrder = new Opportunity(closedate=system.today());
		list<OpportunityLineItem> lines = new list<OpportunityLineItem>();

		system.debug(oldorder);
		newOrder = copyOpportunityFields(oldOrder,newOrder, opportunityFieldsToClone());
		system.debug(newOrder);
		if(!test.isRunningTest()) newOrder.Slick__Delivery_Date__c = date.parse(opportunity_Methods.calculateDeliveryDates(newOrder.Slick__Shipping_Zipcode__c)[1].getValue());
		newOrder.stagename = 'Closed Won';
		newOrder.name = 'nam+e?';

		updatedPricesLineItems(oldOrder.OpportunityLineItems);

		for (OpportunityLineItem oli : oldOrder.OpportunityLineItems){
			lines.add(copyOpportunityLineItemFields(oli, new OpportunityLineItem()));
		}

		wrapperClasses.opptyCloneWrapper wrap = calculateTotals(new wrapperClasses.opptyCloneWrapper(newOrder, lines));
		if (save) insertReconstructedOrder(wrap);

		return wrap;
		//Sales tax max change/
		//Tax exempt items may change
		//default route may change
		//shipping cost etc.

	}

	//insert record if save = true
	public static void insertReconstructedOrder(wrapperClasses.opptyCloneWrapper input){
		insert input.theOrder;
		for (OpportunityLineItem oli : input.lineItems) oli.opportunityid = input.theOrder.id;
		insert input.lineItems;

		cart_CheckoutController.updateInventory(input.lineItems);
		//update coupon usage
		if(input.theOrder.Slick__Applied_Coupon_Codes__c!=null && input.theOrder.Slick__Coupon_Amount__c!=null && input.theOrder.Slick__Coupon_Amount__c>0){
			Coupon_Methods.updateCouponUsage(input.theOrder.Slick__Applied_Coupon_Codes__c, input.theOrder.Slick__Contact__c);
		}

	}

	//copy opportunity fields
	public static Opportunity copyOpportunityFields(Opportunity oldRecord, Opportunity newRecord, set<string> fieldset){
		map <String, Schema.SObjectField> fieldMap = schema.getGlobalDescribe().get('opportunity').getDescribe().fields.getMap();
		for (string field : fieldMap.keySet()) {
			Schema.DescribeFieldResult fieldDescribe = fieldmap.get(field).getDescribe();
			if (fieldDescribe.isCreateable() && fieldDescribe.isAutoNumber() == false && fieldDescribe.isCalculated() == false && fieldset.contains(field.toLowerCase())) {
				newRecord.put(field,oldRecord.get(field));
			}
		}
		return newRecord;
	}

	//copy opportunityLineItem fields
	public static OpportunityLineItem copyOpportunityLineItemFields(OpportunityLineItem oldRecord, OpportunityLineItem newRecord){
		map <String, Schema.SObjectField> fieldMap = schema.getGlobalDescribe().get('opportunitylineitem').getDescribe().fields.getMap();
		for (string field : fieldMap.keySet()) {
			Schema.DescribeFieldResult fieldDescribe = fieldmap.get(field).getDescribe();
			if (fieldDescribe.isCreateable() && fieldDescribe.isAutoNumber() == false && fieldDescribe.isCalculated() == false && field.toLowerCase() != 'opportunityid' && field.toLowerCase() != 'totalprice') {
				newRecord.put(field,oldRecord.get(field));
			}
			PricebookEntry pbe = updatedPrices.get(oldRecord.pricebookentry.product2id);
			newRecord.PricebookEntryId = pbe.id;
			if(updatedOnSalePrices.containsKey(oldRecord.pricebookEntry.product2id))
				newRecord.unitprice=updatedOnSalePrices.get(oldRecord.pricebookEntry.product2id).unitprice;
			else
				newRecord.unitprice = pbe.unitprice;
		}
		return newRecord;
	}

	//update prices
	public static void updatedPricesLineItems(list<OpportunityLineItem> inlist){
		updatedPrices = new map<id,PricebookEntry>();
		updatedOnSalePrices=new map<id,PricebookEntry>();

		map<id,Product2> productMap = new map<id,Product2>();
		for (OpportunityLineItem oli : inlist) productMap.put(oli.pricebookentry.product2id, null);

		productMap = new map<id,Product2>([select id, name, Slick__thumbnail_image__c, Slick__on_sale__c, (select id, pricebook2.name, pricebook2.isstandard, unitprice from pricebookentries where isactive=true AND pricebook2.isactive=true order by pricebook2.isstandard) from Product2 where id IN :productmap.keyset()]);
		for (OpportunityLineItem oli : inlist){
			Product2 tempProd = productMap.get(oli.pricebookentry.product2id);
			for (PricebookEntry pbe : tempProd.pricebookentries){
				if (pbe.pricebook2.isstandard) updatedPrices.put(tempProd.id,pbe);
				//cant mix and match onsale pricebook
				else if (tempProd.Slick__on_Sale__c && pbe.pricebook2.name == 'On Sale') updatedOnSalePrices.put(tempProd.id,pbe);
			}
		}
	}

	//calculate totals
	public static wrapperClasses.opptyCloneWrapper calculateTotals(wrapperClasses.opptyCloneWrapper input){
		input.theOrder.amount = 0;
		for (OpportunityLineItem oli : input.lineItems) input.theOrder.amount += (oli.unitprice*oli.quantity);
		input.theOrder.Slick__Tax__c = opportunity_Methods.calculateTax(input.theOrder.Slick__shipping_ZipCode__c,input.theOrder.Amount);
		input.theOrder.Slick__SubTotal__c = input.theOrder.amount;
		input.theOrder.Slick__SubTotal_With_Tax__c = input.theOrder.Amount + input.theOrder.Slick__Tax__c;
		input.theOrder.Slick__Charge_Amount__c = input.theOrder.Slick__SubTotal_With_Tax__c;
		if(input.theOrder.Slick__Other_Charges__c != null){
			input.theOrder.Slick__Charge_Amount__c += input.theOrder.Slick__Other_Charges__c;
		}
		return input;
	}

	/*public static wrapperClasses.opptyCloneWrapper calculateTotals(wrapperClasses.opptyCloneWrapper cloneWrapper){
		system.debug(cloneWrapper.theOrder.Slick__contact__c);
		Contact contact = [SELECT Id, Email, Slick__Sales_Tax__c, Slick__Tax_Exempt__c FROM Contact WHERE Id = : cloneWrapper.theOrder.Slick__contact__c LIMIT 1];
		cloneWrapper.theOrder.amount = 0;
		Decimal TaxableItemTotal = 0;

		List<wrapperClasses.productWrapper> opptyProducts = new List<wrapperClasses.productWrapper>();
		for( OpportunityLineItem oli : cloneWrapper.lineItems){
			wrapperClasses.productWrapper pw = opportunity_Methods.toProductWrapper(oli);

			pw.qty = (integer)oli.quantity;
			opptyProducts.add(pw);
		}

		List<wrapperClasses.productWrapper> tempProducts = new List<wrapperClasses.productWrapper>();

		for( wrapperClasses.productWrapper pw : opptyProducts){
			if(pw.qty != null && pw.oli != null ){
				pw.oli.Quantity = pw.qty;
			}

			if ( pw.oli != null && pw.oli.Quantity != null && pw.oli.Quantity > 0 && pw.oli.UnitPrice != null){
				pw.lineTotal = ((pw.oli.Quantity) * (pw.oli.UnitPrice));
				tempProducts.add(pw);
				cloneWrapper.theOrder.Amount += pw.lineTotal;
				if( pw.product != null && pw.product.Slick__Tax_Exempt__c != null && pw.product.Slick__Tax_Exempt__c != true){
					TaxableItemTotal += pw.lineTotal;
				}
			}
		}



		opptyProducts = tempProducts;
        //TODO: COUPONS
 		cloneWrapper.theOrder.Slick__Coupon_Amount__c = 0;


 		list<wrapperClasses.cartItem> cartItems=new list<WrapperClasses.cartItem>();
		system.debug(opptyProducts.size());
		system.debug('oppty size');
 		for(wrapperClasses.ProductWrapper prod:opptyProducts){
 			cartItems.add(opportunity_Methods.productWrapperToCartItem(prod));
 		}
 		Coupon_Methods.ApplyCouponResult couponResult=new Coupon_Methods.ApplyCouponResult();
 		//if(cartItems.size()>0)
 		//	oppty.Slick__Coupon_Amount__c = coupon_Methods.couponDiscount('autoProcess', cartItems, contact.email);
 		//if (oppty.Slick__Applied_Coupon_Codes__c != null && oppty.Slick__Applied_Coupon_Codes__c.trim().length() > 0 && cartItems.size()>0) {
			//oppty.Slick__Coupon_Amount__c = coupon_Methods.couponDiscount(oppty.Slick__Applied_Coupon_Codes__c, cartItems, contact.email);
 		//}

 	// 	if(String.isblank(this.CouponCode))
 			couponResult = coupon_Methods.couponDiscount('autoProcess', cartItems, contact.email, cloneWrapper.theOrder.Slick__contact__c);
 	// 	if (String.isnotblank(this.CouponCode) && cartItems.size()>0) {
			// couponResult = coupon_Methods.couponDiscount(this.CouponCode, cartItems, null, cloneWrapper.theOrder.Slick__contact__c.Id);
 	// 	}

 		couponResult.postProcess();
 		if(couponResult.overallDiscount>0 && couponResult.coupons.size()>0){
 			couponResult.overallDiscount.setScale(2);
 			cloneWrapper.theOrder.Slick__Applied_Coupon_Codes__c=couponResult.appliedCouponCodes;
 		}
 		cloneWrapper.theOrder.Slick__Coupon_Amount__c=couponResult.overallDiscount;
		cloneWrapper.theOrder.amount -= cloneWrapper.theOrder.Slick__Coupon_Amount__c;
		TaxableItemTotal-=cloneWrapper.theOrder.Slick__Coupon_Amount__c;

		string discounttemp=cloneWrapper.theOrder.Slick__discount__c;
		decimal discount;
		//discount can either be % or  number
		if(String.isnotblank(discounttemp)){
			if(discounttemp.contains('%')){
				discounttemp=discounttemp.substringbefore('%');
				discount= decimal.valueof(discounttemp)/100;
				discount= (discount*cloneWrapper.theOrder.amount).setScale(2);
			}
			else
				discount=decimal.valueof(discountTemp);
		}
		if(discount!=null){
			cloneWrapper.theOrder.amount-=discount;
			TaxableItemTotal-=discount;
		}

		/*if(oppty.amount<opportunity_methods.shippingMinimum)
			oppty.Slick__Shipping_Cost__c=opportunity_methods.shippingCost;
		else
			cloneWrapper.theOrder.Slick__Shipping_Cost__c=0;



		cloneWrapper.theOrder.Slick__Tax_percentage__c=0;
		Slick__Sales_Tax__c salesTaxRecord;
		if(Contact.Slick__Tax_Exempt__c!=true){
			if(Contact.Slick__Sales_tax__c!=null){
				salesTaxRecord=opportunity_Methods.getTaxById(Contact.Slick__Sales_Tax__c);
			}
			else{
				//oppty.Slick__Tax_percentage__c=opportunity_Methods.getTaxRateZip(oppty.Slick__Shipping_Zipcode__c);
				salesTaxRecord=opportunity_Methods.getTaxByZipThenState(cloneWrapper.theOrder.Slick__Shipping_Zipcode__c,cloneWrapper.theOrder.Slick__Shipping_State__c);
				//oppty.Slick__Tax__c=opportunity_Methods.calculateTax(oppty.Slick__shipping_ZipCode__c,TaxableItemTotal);
			}
		}
		//else{
		//	//Do this if Contact is tax exempt
		//	if(Contact.Slick__Sales_tax__c!=null){
		//		salesTaxRecord.id = Contact.Slick__Sales_Tax__c;
		//		salesTaxRecord.Slick__Tax_Description__c = 'Tax Exempt';
		//	}
		//}
		system.debug('salestaxrecord');
		system.debug(salestaxrecord);
		if(salesTaxRecord !=null){
			cloneWrapper.theOrder.Slick__Tax_percentage__c=salesTaxRecord.Slick__Tax_percentage__c;
			cloneWrapper.theOrder.Slick__Sales_Tax_Region__c=salesTaxRecord.Slick__Tax_Description__c;
		}
		system.debug(taxableItemTotal);
		cloneWrapper.theOrder.Slick__Tax__c=(cloneWrapper.theOrder.Slick__Tax_percentage__c/100)*TaxableItemTotal;
		if(cloneWrapper.theOrder.Slick__Tax__c<0) cloneWrapper.theOrder.Slick__Tax__c=0;

		cloneWrapper.theOrder.Slick__SubTotal__c= cloneWrapper.theOrder.Amount;
		system.debug(cloneWrapper.theOrder.Amount);
		system.debug(cloneWrapper.theOrder.Slick__SubTotal__c);
		cloneWrapper.theOrder.Slick__SubTotal_With_Tax__c = cloneWrapper.theOrder.Amount + cloneWrapper.theOrder.Slick__Tax__c;
		cloneWrapper.theOrder.Slick__Charge_Amount__c=cloneWrapper.theOrder.Slick__SubTotal_With_Tax__c;
		if(cloneWrapper.theOrder.Slick__Other_Charges__c != null){
			cloneWrapper.theOrder.Slick__Charge_Amount__c+=cloneWrapper.theOrder.Slick__Other_Charges__c;
		}
		if(cloneWrapper.theOrder.Slick__shipping_Cost__c!=null)
			cloneWrapper.theOrder.Slick__Charge_Amount__c+=cloneWrapper.theOrder.Slick__shipping_cost__c;

		cloneWrapper.lineItems = new List<OpportunityLineItem>();
		for ( wrapperClasses.productWrapper pw : opptyProducts ){
			pw.oli.UnitPrice = pw.pricebookEntry.unitPrice;
			pw.oli.PricebookEntryId = pw.PricebookEntry.Id;
			//pw.oli.OpportunityId = oppty.Id;
			cloneWrapper.lineItems.add(pw.oli);
		}

		return cloneWrapper;


	}*/



	//which opportunity fields are being cloned
	public static set<string> opportunityFieldsToClone(){
		set<string> temp = new set<string>();
		temp.add('Slick__contact__c'.toLowerCase());
		temp.add('AccountId'.toLowerCase());
		temp.add('Slick__Shipping_Address_Line_1__c'.toLowerCase());
		temp.add('Slick__Shipping_Address_Line_2__c'.toLowerCase());
		temp.add('Slick__Shipping_City__c'.toLowerCase());
		temp.add('Slick__Shipping_State__c'.toLowerCase());
		temp.add('Slick__Shipping_Zipcode__c'.toLowerCase());
		temp.add('Slick__Billing_Street_1__c'.toLowerCase());
		temp.add('Slick__Billing_Street_2__c'.toLowerCase());
		temp.add('Slick__Billing_City__c'.toLowerCase());
		temp.add('Slick__Billing_State__c'.toLowerCase());
		temp.add('Slick__Billing_Zipcode__c'.toLowerCase());
		temp.add('Slick__Route__c'.toLowerCase());
		temp.add('Description'.toLowerCase());
		temp.add('Slick__Other_Charges__c'.toLowerCase());
		temp.add('Slick__Billing_First_Name__c'.toLowerCase());
		temp.add('Slick__Billing_Last_name__c'.toLowerCase());
		temp.add('Slick__Billing_Company_Name__c'.toLowerCase());
		temp.add('Slick__Billing_Email__c'.toLowerCase());
		temp.add('Slick__Billing_Phone__c'.toLowerCase());
		temp.add('Slick__Shipping_First_Name__c'.toLowerCase());
		temp.add('Slick__Shipping_Last_Name__c'.toLowerCase());
		temp.add('Slick__Shipping_Email__c'.toLowerCase());
		temp.add('Slick__Shipping_Phone__c'.toLowerCase());
		temp.add('Slick__Card_name__c'.toLowerCase());
		temp.add('Slick__Card_Type__c'.toLowerCase());
		temp.add('Slick__Card_Number__c'.toLowerCase());
		temp.add('Slick__Card_Security__c'.toLowerCase());
		temp.add('Slick__Card_Expiration_Month__c'.toLowerCase());
		temp.add('Slick__Card_Expiration_year__c'.toLowerCase());
		temp.add('Credit_Card_Used'.toLowerCase());

		temp.add('Slick__contact__c'.toLowerCase());
		temp.add('Slick__Shipping_Address_Line_1__c'.toLowerCase());
		temp.add('Slick__Shipping_Address_Line_2__c'.toLowerCase());
		temp.add('Slick__Shipping_City__c'.toLowerCase());
		temp.add('Slick__Shipping_State__c'.toLowerCase());
		temp.add('Slick__Zipcode__c'.toLowerCase());
		temp.add('Slick__Billing_Street_1__c'.toLowerCase());
		temp.add('Slick__Billing_Street_2__c'.toLowerCase());
		temp.add('Slick__Billing_City__c'.toLowerCase());
		temp.add('Slick__Billing_State__c'.toLowerCase());
		temp.add('Slick__Billing_Zipcode__c'.toLowerCase());
		temp.add('Slick__Route__c'.toLowerCase());
		temp.add('Description'.toLowerCase());
		temp.add('Slick__Other_Charges__c'.toLowerCase());
		temp.add('Slick__Billing_First_Name__c'.toLowerCase());
		temp.add('Slick__Billing_Last_name__c'.toLowerCase());
		temp.add('Slick__Billing_Company_Name__c'.toLowerCase());
		temp.add('Slick__Billing_Email__c'.toLowerCase());
		temp.add('Slick__Billing_Phone__c'.toLowerCase());
		temp.add('Slick__Shipping_First_Name__c'.toLowerCase());
		temp.add('Slick__Shipping_Last_Name__c'.toLowerCase());
		temp.add('Slick__Shipping_Email__c'.toLowerCase());
		temp.add('Slick__Shipping_Phone__c'.toLowerCase());
		temp.add('Slick__Card_name__c'.toLowerCase());
		temp.add('Slick__Card_Type__c'.toLowerCase());
		temp.add('Slick__Card_Number__c'.toLowerCase());
		temp.add('Slick__Card_Security__c'.toLowerCase());
		temp.add('Slick__Card_Expiration_Month__c'.toLowerCase());
		temp.add('Slick__Card_Expiration_year__c'.toLowerCase());
		temp.add('Credit_Card_Used'.toLowerCase());

		for (string s : temp) s = s.toLowerCase();
		return temp;
	}

	public static list<Product2> query3UpsellProducts(){
		list<Product2> plist = [
			Select id, name, Slick__taxable__c,Slick__tax_exempt__c, Slick__Full_Size_Image__c,
				(
					SELECT Id, Name, Pricebook2Id, Product2Id, UnitPrice, IsActive, UseStandardPrice, ProductCode, Product2.Name, Product2.IsActive, Product2.Description, Product2.Family, Product2.Slick__Family_Code__c, Product2.Slick__Family_Code2__c, Product2.Slick__Group__c, Product2.Slick__Tax_Exempt__c
					From PricebookEntries
				)
			FROM Product2 where Slick__Mobile_Featured_Upsell__c = true limit 3];
		list<Product2> returnlist = new list<Product2>();
		returnlist.addall(plist);
		//for (integer i = returnlist.size(); i<3; i++){
		//	returnlist.add(plist[i]);
		//}
		return returnlist;
	}
}