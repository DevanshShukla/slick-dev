@RestResource(urlMapping='/product/*')
global with sharing class product_Webservice {

    @HttpGet
    global static void getProductInfo(){
        String itemId = RestContext.request.params.get('productcode');
        String response = '';
        System.debug(itemId);
        List<Product2> thisProduct = new List<Product2>();

        if (itemId == null){
            response = getProducts();
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(response);
        }
        else {
            thisProduct = getProduct('ProductCode',itemId);
            if (thisProduct != null){
                response = JSON.serialize(thisProduct[0]);
            }
            else {
                response = JSON.serialize('There is no product with this product code.');
            }
            Map<String,Object> responseMap = (Map<String,Object>)JSON.deserializeUntyped( response );
            Map<String,Object> responseCleaned = util.removeAttributes(responseMap);
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(JSON.serializePretty(responseCleaned));
        }
    }


    @HttpPost
    global static void upsertProduct(){
        String bodyStr = RestContext.request.requestBody.toString();
        List<Product2> prod = new List<Product2>();

        wrapperClasses.createProductWrapper product = (wrapperClasses.createProductWrapper)JSON.deserialize(bodyStr, wrapperClasses.createProductWrapper.class);

        // Retrieve by ID if provided otherwise fall back to product code
        if ( product.id != null ) {
            prod = getProduct('Id',product.id);
        } else {
            prod = getProduct('ProductCode',product.productCode);
        }

        //If product code is found in Salesforce instance
        if ( prod != null ){
            //List<Product2> prod = getProduct('ProductCode',product.productCode);
            prod[0].Name = product.fields.name;
            prod[0].IsActive = product.fields.active;
            prod[0].Slick__Active_in_Cart__c = product.fields.activeInCart;

            //prod[0].ProductCode = product.productCode;
            prod[0].Slick__Tax_Exempt__c = product.fields.taxExempt;
            prod[0].Slick__On_Sale__c = product.fields.onSale;
            prod[0].Slick__Featured__c = product.fields.featured;

            List<PricebookEntry> pbe = [SELECT UnitPrice FROM PricebookEntry WHERE Product2Id =:prod[0].Id AND PriceBook2.Name = 'Standard Price Book'];

            if(pbe.size() > 0){
                pbe[0].UnitPrice = product.fields.price;
                pbe[0].IsActive = true;
                update pbe;
            }else{
                Pricebook2 pricebook = [SELECT Id FROM Pricebook2 WHERE Name =: 'Standard Price Book'];

                insert new PricebookEntry(UnitPrice = product.fields.price, Product2Id = prod[0].Id, Pricebook2Id = pricebook.Id, IsActive = true);

            }

            update prod;

            Map<String,String> productJSON = new Map<String,String>();
            productJSON.put('isSuccess','true');
            productJSON.put('id', prod[0].Id);
            productJSON.put('productCode',product.productCode);

            String response = JSON.serialize(productJSON);
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(response);
        }

        //If product code can't be found, create new Product2
        else {

            Product2 newProd = new Product2 (
                    Name = product.fields.name,
                    IsActive = product.fields.active,
                    Slick__Active_in_Cart__c = product.fields.activeInCart,
                    ProductCode = product.productCode,
                    Slick__Tax_Exempt__c = product.fields.taxExempt,
                    Slick__On_Sale__c = product.fields.onSale,
                    Slick__Featured__c = product.fields.featured
            );

            insert newProd;

            List<PriceBook2> pb = [
                    SELECT Id,
                    Name from
            PriceBook2 WHERE
                    Name = 'Standard Price Book'];

            PricebookEntry newPrice = new PricebookEntry (
                    Product2Id = newProd.Id,
                    PriceBook2Id = pb[0].Id,
                    UnitPrice = product.fields.price,
                    IsActive = true
            );

            insert newPrice;

            Map<String,String> productJSON = new Map<String,String>();
            productJSON.put('isSuccess','true');
            productJSON.put('id',newProd.Id);
            productJSON.put('productCode',product.productCode);

            String response = JSON.serialize(productJSON);
            System.debug(response);
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(response);

        }

    }

    public static List<Product2> getProduct(String field, String id){
        String queryString = 'SELECT IsActive, Slick__Active_in_Cart__c, ProductCode, Name, Slick__Taxable__c, Slick__On_Sale__c, Slick__Featured__c, Slick__Exclude_from_Sync__c, ';
        queryString += '(SELECT UnitPrice FROM PriceBookEntries WHERE IsActive = true AND PriceBook2.IsActive = true AND Pricebook2.Name = \'Standard Price Book\')';
        queryString += ' FROM Product2 WHERE '+field+' = :id';
        system.debug(queryString);

        List<Product2> thisProduct = (List<Product2>)database.query(queryString);

        if (thisProduct.size() > 0 ){
            return thisProduct;
        }
        else return null;
    }


    public static String getProducts(){
        String queryString = 'SELECT IsActive, Slick__Active_in_Cart__c, ProductCode, Name, Slick__Taxable__c, Slick__Tax_Exempt__c, Slick__On_Sale__c, Slick__Featured__c, Slick__Exclude_from_Sync__c, ';
        queryString += '(SELECT UnitPrice FROM PriceBookEntries WHERE IsActive = true AND PriceBook2.IsActive = true AND PriceBook2.Name = \'Standard Price Book\')';
        queryString += ' FROM Product2 WHERE Slick__Active_In_Cart__c = true OR IsActive = true';
        system.debug(queryString);
        List<Product2> thisProduct = (List<Product2>)database.query(queryString);


        System.debug(thisProduct);


        List<Map<String,Object>> removedAttributes = new List<Map<String,Object>>();
        for (Product2 prod: thisProduct){
            String response = JSON.serialize(prod);
            Map<String,Object> responseMap = (Map<String,Object>)JSON.deserializeUntyped( response );
            Map<String,Object> responseCleaned = util.removeAttributes(responseMap);
            removedAttributes.add(responseCleaned);
        }
        return JSON.serialize(removedAttributes);
    }
    
    public static void dummy(){
        Integer i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
    }

}