/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class cart_CheckoutControllerTest {

    static testMethod void testCartCheckoutController() {
       
        
        
        product2 prod = new product2(name='test');
        insert prod;

        account acc=new account(name='Individual Customer');
        insert acc;
        
        //contact c = new contact(lastName='test', email='test@test.com', Slick__Checks_Enabled__c = true);
        contact c = new contact(lastName='test', email='test@test.com');
        insert c;
        
        Slick__coupon__c coupon = new Slick__Coupon__c(Slick__customer__c = c.id, Slick__product__c = prod.id,
         Slick__discount_amount__c = 1, name='specialdogcoupon', Slick__coupon_type__c = 'Product Group', Slick__discount_type__c = 'Dollar Off', Slick__expiration_date__c = Date.parse('1/1/2222'));
        insert coupon;
        
         Slick__coupon__c coupon2 = new Slick__Coupon__c(Slick__customer__c = c.id, Slick__product__c = prod.id,
         Slick__discount_amount__c = 1, name='specialdogcoupon2', Slick__discount_type__c = 'Percentage Off', Slick__coupon_type__c = 'Specific Product', Slick__expiration_date__c = Date.parse('1/1/2222'), Slick__apply_automatically__c=true);
        insert coupon2;
        
        Slick__coupon__c coupon11 = new Slick__Coupon__c(Slick__customer__c = c.id, Slick__product__c = prod.id,
         Slick__discount_amount__c = 1, name='specialdogcoupon', Slick__coupon_type__c = 'Product Group', Slick__discount_type__c = 'Dollar Off', Slick__expiration_date__c = Date.parse('1/1/2222'));
        insert coupon11;
        
         Slick__coupon__c coupon12 = new Slick__Coupon__c(Slick__customer__c = c.id, Slick__product__c = prod.id,
         Slick__discount_amount__c = 1, name='specialdogcoupon2', Slick__discount_type__c = 'Percentage Off', Slick__coupon_type__c = 'Specific Product', Slick__expiration_date__c = Date.parse('1/1/2222'), Slick__apply_automatically__c=true);
        insert coupon12;

        Slick__coupon__c coupon3 = new Slick__Coupon__c(Slick__customer__c = c.id, Slick__product__c = prod.id,
        Slick__discount_amount__c = 1, name='specialdogcoupon3', Slick__coupon_type__c = 'BOGO', Slick__discount_type__c = 'BOGO', Slick__expiration_date__c = Date.parse('1/1/2222'));
        insert coupon3;

        Slick__coupon__c coupon4 = new Slick__Coupon__c(Slick__customer__c = c.id, Slick__product__c = prod.id,
        Slick__discount_amount__c = 1, name='specialdogcoupon4', Slick__coupon_type__c = 'Product Family', Slick__discount_type__c = 'Product Family', Slick__Product_Family__c = 'Biscuits', Slick__expiration_date__c = Date.parse('1/1/2222'));
        insert coupon4;

        Slick__coupon__c coupon5 = new Slick__Coupon__c(Slick__customer__c = c.id, Slick__product__c = prod.id,
        Slick__discount_amount__c = 1, name='specialdogcoupon5', Slick__coupon_type__c = 'BOGO', Slick__discount_type__c = 'BOGO', Slick__expiration_date__c = Date.parse('1/1/2222'));
        insert coupon5;

        Slick__coupon__c coupon6 = new Slick__Coupon__c(Slick__customer__c = c.id, Slick__product__c = prod.id,
        Slick__discount_amount__c = 1, name='specialdogcoupon6', Slick__coupon_type__c = 'Product Group', Slick__discount_type__c = 'Product Family', Slick__Product_Family__c = 'Biscuits', Slick__Product_Group__c = 'Select Cold River Recipe with Salmon', Slick__expiration_date__c = Date.parse('1/1/2222'));
        insert coupon6;
        
        Cart_CheckoutController controller = new Cart_CheckoutController();
        
        wrapperClasses.checkoutWrapper checkout = new WrapperClasses.checkoutWrapper();
         //controller.applyCoupon();
        checkout.couponCode = 'specialdogcoupon';
        controller.checkoutData = checkout;
        
         List<WrapperClasses.cartItem> items = new LIst<WrapperClasses.cartItem>();
        items.add(new WrapperClasses.cartItem(prod, '2', 2, 2));
         controller.loadCartContents();
        controller.cartContents = items;
        String shippingOrDelivery = controller.shippingOrDelivery;
        String shippingDeliveryMergeField = controller.shippingDeliveryMergeField;
        Decimal shippingCost = controller.shippingCost;
        
        controller.validateCheckOption();
        controller.checkSecCode();
        controller.validateCheckOption();
        controller.currentContact = c;
        Cart_CheckoutController.maskCheckoutData(new wrapperClasses.checkoutWrapper());
        Cart_CheckoutController.maskString('test',2);
        Cart_CheckoutController.isValidSecCode('Code01');
        
        controller.applyCoupon();
        //controller.attachOpportunityLineItems(oppty, cItems)
        controller.calculateShipping();
        controller.calculateTax();
        controller.calculateTotals();
        controller.clearErrors();
        controller.getCountryOptions();
        controller.getCreditCardOptions();
        controller.getMonthOptions();
        controller.getShippingDateOptions();
        controller.getStateOptions();
        controller.getYearOptions();
        controller.loadCartContents();
        
       	controller.checkoutData.billingAddress = 'test';
       	controller.checkoutData.billingAddress2 = 'test';
       	controller.checkoutData.billingCity='test';
       	controller.checkoutData.billingCompany='test';
       	controller.checkoutData.billingCountry='test';
       	controller.checkoutData.billingFirstName='test';
       	controller.checkoutData.billingLastName='test';
       	controller.checkoutData.billingState='CA';
      
       	controller.checkoutData.CreditCardExpMonth='05';
       	controller.checkoutData.CreditCardExpYear='5005';
        controller.checkoutData.CreditCardNumber='4111111111111';
        controller.checkCCType();
        controller.checkoutData.CreditCardNumber='51';
        controller.checkCCType();
        controller.checkoutData.CreditCardNumber='6011';
        controller.checkCCType();
        controller.checkoutData.CreditCardNumber='34';
        controller.checkCCType();
       	controller.checkoutData.CreditCardNumber='12345';
       	controller.checkoutData.CreditCardSecurityCode='111';
       	controller.checkoutData.CreditCardType='visa';
       	controller.checkoutData.DeliveryDate='01/01/3005';

       
        controller.checkoutData.email='invalidEmail@test.com';
       	controller.checkoutData.NameOnCard='test';
       	controller.checkoutData.ordernotes='notes';
       	controller.checkoutData.PaymentType='test';
       	controller.checkoutData.phone='5555555555';
       	controller.checkoutData.shippingAddress='test';
       	controller.checkoutData.shippingAddress2 = 'test';
       	controller.checkoutData.shippingCity='test';
       	controller.checkoutData.shippingCompany='test';
       	controller.checkoutData.shippingCountry='test';
       	controller.checkoutData.shippingFirstName='test';
       	controller.checkoutData.shippingState='ca';
        controller.checkReturningCustomer();
      
  
        
        controller.placeOrder();
        controller.checkoutData.shippingZip='99999';
        controller.checkoutData.billingZip='CA';
        controller.placeOrder();
        controller.verifiedEmail='wrong';
        controller.validateForm();
        controller.verifiedEmail=controller.checkoutData.email;
        controller.queryContactFromEmail();
        controller.placeOrder();
        controller.checkoutData.email='test@test.com';
        controller.checkoutData.couponCode=null;
        controller.checkoutData.billingZip='99999';
        controller.placeOrder();
        System.debug('Error message' +controller.errorMessage);


        controller.checkoutData.isReturningCustomer=true;
        controller.checkoutData.returningEmail='test@test.com';
        controller.placeOrder();
        controller.checkoutData.returningBillingZip='99999';
        controller.checkoutData.returningSecurityCode='123';
        controller.getShippingDateOptions();
        controller.placeOrder();
        System.debug('Error message' +controller.errorMessage);
        controller.checkoutData.returningEmail='test@test.com';
        controller.checkoutData.returningBillingZip='99999';
        controller.checkoutData.returningSecurityCode='111';
        controller.checkReturningCustomer();
        list<selectOption> cardops=controller.getReturningCardOptions();
        if(cardOps.size()>0)
          controller.selectedReturningCCid=cardOps.get(0).getvalue();
        controller.placeOrder();
        controller.clearReturningCustomerData();
        System.debug('Error message' +controller.errorMessage);
        String seal=controller.authorizeNetSealId;
        controller.getPaymentOptions();
        cart_CheckoutController.voidCheckoutTransaction('123');
        
        cart_CheckoutController.cleanCartContents(controller.cartContents);
        
        
    }
}