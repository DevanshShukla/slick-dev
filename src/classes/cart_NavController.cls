public class cart_NavController {
	public list<MenuItemWrapper> topNavigation;
	public list<MenuItemWrapper> topBarMenu;
	public Id currentDropdownMenu{get;set;}

	public cart_NavController(){
	}	 
	public list<MenuItemWrapper> getTopNavigation(){
		Id menuId = currentDropdownMenu;
		system.debug('printing the menuID before: ' + menuId);
		//if(topNavigation==null){	
			topNavigation=new list<MenuItemWrapper>();	 
        
			for( Slick__Menu_Item__c mItem:[
				SELECT id,Slick__description__c, Slick__menu__c, Slick__webpage_url__c,Slick__position__c, Slick__dropdown_menu__c,Slick__label__c,Slick__is_department__c,Slick__navigation_image_url__c,
					(select id,Slick__webpage_url__c,Slick__label__c,Slick__description__c,Slick__is_Department__c,Slick__position__c, Slick__dropdown_menu__c, Slick__navigation_image_Url__c from subMenu_Items__r where Slick__isInactive__c=false order by Slick__position__c asc) 
				FROM Slick__Menu_Item__c 
				WHERE Menu__r.Slick__Menu_Type__c = 'Top' 
				AND Menu__r.Id = :menuId 
				AND Menu__r.Slick__isActive__c = true 
				AND Slick__is_Department__c = true 
				AND Slick__isInactive__c = false 
				ORDER BY Slick__position__c asc
			]){
				system.debug('printing the menuID after: ' + menuId);
				topNavigation.add(new MenuItemWrapper(mItem));
				system.debug('print mItem: ' + mItem);			
			}		
		//}
		system.debug('print top menu: ' + topNavigation);
		return topNavigation;
	}
	public list<MenuItemWrapper> getTopBarMenu(){
		if(topBarMenu==null){	
			topBarMenu=new list<MenuItemWrapper>();	
			for(Slick__Menu_Item__c mItem:[select id,Slick__webpage_url__c,Slick__description__c,Slick__position__c, Slick__dropdown_menu__c, Slick__label__c,Slick__is_department__c,Slick__navigation_image_url__c,(select id,Slick__label__c,Slick__description__c,Slick__is_Department__c,Slick__position__c,Slick__navigation_image_Url__c from subMenu_Items__r where Slick__isInactive__c=false order by Slick__position__c asc) from Slick__menu_Item__c where Menu__r.Slick__Menu_Type__c='TopBar' and Menu__r.Slick__isActive__c=true and Slick__isInactive__c=false order by Slick__position__c asc]){
				MenuItemWrapper miw = new MenuItemWrapper(mItem);
				miw.dropDownMenuItems = generateDropDownMenu(miw.dropDownMenuId);
				topBarMenu.add(miw);			
			}		
		}
		system.debug('Menu: ' + topBarMenu);
		return topBarMenu;
	}

	public List<MenuItemWrapper> generateDropDownMenu(Id menu){
		currentDropdownMenu = menu;
		if(currentDropdownMenu != null){
			return getTopNavigation();
		}
		return null;
	}
	
	
	public class MenuItemWrapper{
		public String label{get;set;}
		public String description{get;set;}
		public boolean is_Department{get;set;}
		public String id{get;set;}
		public integer position{get;set;}
		public Integer numSubItems{get;set;}
		
		public String urlencodeLabel{get;set;}
		public String navImageUrl{get;set;}
		public String webpageURL{get;set;}
		public Id dropDownMenuId{get;set;}
		public List<MenuItemWrapper> dropDownMenuItems{get;set;}
		
		public list<MenuItemWrapper> subItems{get;set;}
		
		public MenuItemWrapper(Slick__Menu_Item__c mItem){
			this.label=mItem.Slick__label__c;
			this.description=mItem.Slick__description__c;
			this.is_Department=mItem.Slick__is_department__c;
			this.id=mItem.id;
			this.position=(integer)mItem.Slick__position__c;
			this.dropDownMenuItems = new List<MenuItemWrapper>();

			this.WebpageUrl=mItem.Slick__webpage_url__c;
			this.dropDownMenuId=mItem.Slick__dropdown_menu__c;

			subItems=new list<menuItemWrapper>();
			if(mItem.subMenu_Items__r!=null && mItem.subMenu_Items__r.Size()>0){				
				for(Slick__Menu_Item__c subItem:mItem.subMenu_Items__r){
					subItems.add(new MenuItemWrapper(subItem));
				
				}
			}
			this.navImageUrl=mItem.Slick__navigation_image_url__c;
			if(subItems.size() > 0){
				this.numSubItems = subItems.size();
			}
			else{
				this.numSubItems = 0;
			}
			cleanLabel();			
		}
		public MenuItemWrapper(){ 
			//blank
		}
		public void cleanLabel(){
			if(String.isnotblank(this.label)){
				label=label.trim();
				urlEncodeLabel=EncodingUtil.urlEncode(label,'UTF-8');		
			}		
		}
	
	}

}