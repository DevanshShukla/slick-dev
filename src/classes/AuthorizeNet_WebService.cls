@RestResource(urlMapping='/transaction')
global with sharing class AuthorizeNet_WebService {

    @HttpPost
    global static void postMethods()
    {
      String bodyStr = RestContext.request.requestBody.toString();
      System.debug(bodyStr);

      //pass correct values into wrapper
      api_AuthorizeDotNet.authnetReq_Wrapper m = (api_AuthorizeDotNet.authnetReq_Wrapper)JSON.deserialize(bodyStr, api_AuthorizeDotNet.authnetReq_Wrapper.class);
      api_AuthorizeDotNet.authnetResp_Wrapper responseWrapper = api_AuthorizeDotNet.authdotnetChargeDetailed(m);

      String response = JSON.serialize(responseWrapper);
      System.debug(response);
      // return response;
      RestContext.response.addHeader('Content-Type', 'application/json');
      RestContext.response.responseBody = Blob.valueOf(response);
    }

}