public with sharing class Credit_Card_Extension {
	//Update name dynamically
	//pull in contact if one exists, update addresses.
	//update last4 dynamically.
	public Slick__Credit_Card__c card{get;set;}
	public Contact contact{get;set;}

	public Credit_Card_Extension(ApexPages.standardController con) {
		card=(Slick__credit_card__c)con.getRecord();
		Contact contact;
		if(card.Slick__contact__c!=null){
			list<Contact> contacts=[select id,firstname,lastname,mailingstreet,mailingcity,mailingpostalcode,mailingstate,otherstreet,othercity,otherpostalcode,otherstate from contact where id=:card.Slick__contact__c];
			if(contacts.size()>0){
				contact=contacts.get(0);
				if(String.isblank(contact.otherStreet) && String.isblank(contact.otherCity) && String.isblank(contact.otherState) && String.isblank(contact.otherPostalCode)) {
					card.Slick__billing_Street__c=contact.mailingstreet;
					card.Slick__billing_City__c=contact.mailingcity;
					card.Slick__billing_state__c=contact.mailingstate;
					card.Slick__billing_postal_code__c=contact.mailingpostalcode;
				}
				else{
					card.Slick__billing_Street__c=contact.otherstreet;
					card.Slick__billing_City__c=contact.othercity;
					card.Slick__billing_state__c=contact.otherstate;
					card.Slick__billing_postal_code__c=contact.otherpostalcode;
				}
			}
		}
	}

	public pageReference customSave(){
		if(String.isblank(card.name)){
			card.name.addError('You must enter a value');
			return null;
		}
		upsert card;
		return new PageReference('/'+card.id);	
	}

	public static String getCreditCardName(Slick__Credit_Card__c card){
		String cardName='';
		String cardType='';
		String last4='';
		if(String.isnotblank(card.Slick__name_on_card__c)){
			//48 chars available for firstname lastname
			//lastName=card.Slick__name_on_card__c.subStringAfterLast(' ');
			//if(lastname=='')
			//	lastName=card.Slick__name_on_card__c;
			//if(lastname.length()>48)
			//	lastName=lastname.subString(0,48);
			cardName=card.Slick__name_on_card__c;	
			if(cardName.length()>48)
				cardName=cardName.subString(0,48);		
		}
		if(String.isnotblank(card.Slick__card_type__c))
			cardType=card.Slick__card_Type__c;
		if(String.isnotblank(card.Slick__last_4__c))
			last4=card.Slick__last_4__c;
		return cardName+' '+cardType+' ending in '+last4;	
	}
	public pageReference setCreditCardName(){
		if(card.Slick__card_number__c!=null && card.Slick__Card_number__c.length()>=4)
			card.Slick__last_4__c = card.Slick__card_number__c.substring(card.Slick__card_number__c.length()-4,card.Slick__card_number__c.length());
		card.name=getCreditCardName(card);
		return null;
	}

	public pageReference checkCardType(){
		if(String.isnotblank(card.Slick__card_number__c)){
			String cardType=cart_CheckoutController.determineCCtype(card.Slick__card_number__c);
			if(String.isnotblank(cardType)){
				card.Slick__card_type__c=cardType;
			}
			setCreditCardName();
		}
		return null;
	}

}