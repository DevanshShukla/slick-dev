@isTest
private class Test_ErrorLogger {
    static testMethod void getTests(){
        test.startTest();
             try{
                CalloutException e = new CalloutException();
                e.setMessage('This is a constructed exception!');
                throw e;
            }
            catch(Exception e){
                
                ErrorLogger.LogError(e);
                ErrorLogger.LogError(e, 'test');
            }
            
        test.stopTest();
    }
}