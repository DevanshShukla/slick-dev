/**
 * ropBatch_Scheduler Class
 * @author: M. Wicherski, MK Partners 11/2012
 * @Description: Control method for scheduling of ropBatch job(s)
 */

/*
	To Run:
		NSCart.ropBatch_Scheduler m = new NSCart.ropBatch_Scheduler();
		String sch  =  '0 0 8 * * ?';
		String jobId = system.schedule('ropBatch', sch, m);

	To Check Status:
		CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
 
*/

global  class  ropBatch_Scheduler  implements  Schedulable{
	global  void  execute(SchedulableContext  sc)  {
		batch_ROP_Batch b = new batch_ROP_Batch();
		database.executebatch(b,1);
	}

}