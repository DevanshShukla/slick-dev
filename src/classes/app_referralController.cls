public with sharing class app_referralController {
	/*Referral process

		create a lead.
		have referer's email attach to lead.
		insert lead

		Back end:
		Upon conversion-> also convert refer's email.
		after insert trigger -> link Existing Customer who referred field.
	*/

	public lead lead{get;set;}
	public boolean hasError{get;set;}
	public boolean isComplete{get;set;}
	public String errorMessage{get;set;} 


	public app_referralController(){
		lead=new lead(Company='Individual Customer',leadSource='Referral Page');
	}
	public pageReference insertReferee(){
		hasError=false;
		isComplete=false;
		validate();
		if(hasError==false){
			insert lead;
			isComplete=true;	
		}
		return null;
	}
	public void validate(){
		if(String.isblank(lead.firstName) || String.isblank(lead.lastName) || String.isblank(lead.email) || String.isblank(lead.Slick__referrer_Email__c) ){
			hasError=true;
			errorMessage='Please fill in all required fields.';
		}
	}

		

}