@RestResource(urlMapping='/lead')
global class lead_Webservice {
    @HttpPost
    global static String createLead(){
        String bodyStr = RestContext.request.requestBody.toString();

        wrapperClasses.leadWrapper leadFields = (wrapperClasses.leadWrapper)JSON.deserialize(bodyStr, wrapperClasses.leadWrapper.class);

        Lead newLead = new Lead(
            FirstName = leadFields.firstname,
            LastName = leadFields.lastname,
            Email = leadFields.email,
            Referrer_Email__c = leadFields.refemail,
            Cell_Phone__c = leadFields.refphone,
            Company = 'N/A',
            Status = 'Open - Not Contacted'
        );

        try {
            insert newLead;
            return 'Lead created';
        } catch(DMLException e) {
            return 'There was an error creating lead: ' + e;
        }
    }
}