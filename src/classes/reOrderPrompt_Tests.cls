@isTest
private class reOrderPrompt_Tests {
	
	@isTest static void testReOrderPrompt() {
		test.startTest();
    		 product2 prod = new product2(name='test dog food', Slick__cart_category__c='dogfood', Slick__cart_subcategory__c='dryfood');
      		 insert prod;       
		    pricebookEntry pbe=new pricebookentry(product2id=Prod.id,pricebook2id=Test.getStandardPriceBookId(),unitprice=1,isactive=true);
	        insert pbe;
	        contact c=new contact(lastname='test',email='tester@test.com'); 
	        insert c;
	        opportunity opp=new opportunity(Slick__contact__c=c.id,closeDate=date.today(),stagename='Closed Won',name='test',Slick__Delivery_Date__c=date.today());      
	        insert opp;
	        
	        opp.StageName = 'Closed Won';
            update opp;
            
            Test.setCurrentPageReference(new PageReference('Page.reOrder')); 
            System.currentPageReference().getParameters().put('thisId', c.Id);
            
            System.debug('this Id in tests>>>>>>>>>>>>>>>>'+c.Id);
    
	        OpportunitylineItem oli=new OpportunityLineItem(opportunityid=opp.id, pricebookEntryid=pbe.id,quantity=1,totalprice=1);
	        insert oli;
			reOrderPromptCommunication_Controller reorderPrompt=new reOrderPromptCommunication_Controller();
			//reOrderPrompt.thisId=c.id;
			
			reOrderPromptCommunication_Controller.emailWrapper ts = new reOrderPromptCommunication_Controller.emailWrapper();
			ts.neverFeedSuggestion = 'test';
			ts.nextAvailableDeliveryDate = system.today();
			ts.suggested2Products = new List<String>();
			ts.AutoReorder = true;
			ts.ContactRouteDay = 'test';
			reOrderPrompt.thisEmail=c.email;
			reOrderPrompt.getEwrapper();
			reorderPrompt.getROPSuccessbody();
			reorderPrompt.ROPType='OneClick';
			reorderPrompt.getROPSuccessbody();
			reorderPrompt.ROPSuccessbody=null;
			reorderPrompt.ROPType='YourRequest';
			reorderPrompt.getROPSuccessbody();

		test.StopTest();
	}	
}