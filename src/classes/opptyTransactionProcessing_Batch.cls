/*
	To Run:
			opptyTransactionProcessing_Batch job = new opptyTransactionProcessing_Batch(set<id> transactionids);
			Id processId = Database.executeBatch(job);
	
	To Check Status:
			AsyncApexJob j = [select ApexClassId, CompletedDate, CreatedById, CreatedDate, 
								Id, JobItemsProcessed, JobType, MethodName, NumberOfErrors, Status, 
								TotalJobItems from AsyncApexJob where Id =:processId];
	
	Info about this Class
			ApexClass ac = [select ApiVersion, Body, BodyCrc, CreatedById, CreatedDate, Id, 
								IsValid, LastModifiedById, LastModifiedDate, LengthWithoutComments, Name, 
								NamespacePrefix, Status, SystemModstamp from ApexClass where Id = :j.ApexClassId];
*/
global class opptyTransactionProcessing_Batch implements Database.Batchable<sObject>, Database.AllowsCallouts, database.stateful{

	global final String queryString;
	global final set<id> opptyIds;
	global list<EmailResult> batchResults;
	global final string method;

	global opptyTransactionProcessing_Batch(set<id> oppids, string meth){
		opptyids = oppids;
		method = meth;
		batchresults = new list<emailResult>();
		if ( queryString == null ){
			String x ='select id, Slick__Billing_City__c, Slick__Billing_Email__c, Slick__Billing_State__c, Slick__Billing_Street_1__c, Slick__Billing_Zipcode__c, Slick__billing_first_name__c, Slick__billing_last_name__c, Slick__Charge_Amount__c,Slick__Card_Name__c, Slick__Card_Expiration_Year__c, Slick__Card_Expiration_Month__c, Slick__Card_Number__c, Slick__Card_Security__c, Slick__Credit_Card_used__c, Credit_Card_Used__r.Slick__Expiration_Year__c, Credit_Card_Used__r.Slick__Expiration_Month__c, Credit_Card_Used__r.Slick__Name_On_Card__c, Credit_Card_Used__r.Slick__CVV__c, Credit_Card_Used__r.Slick__Card_Number__c, Credit_Card_Used__r.Slick__Billing_Street__c, Credit_Card_Used__r.Slick__Billing_City__c, Credit_Card_Used__r.Slick__Billing_State__c, Credit_Card_Used__r.Slick__Billing_Postal_Code__c, Slick__Charge_Status__c,(select id,Slick__transaction_Type__c,Slick__Transaction_Status__c,Slick__transaction_id__c,Slick__payment_Number__c,Slick__First_name_on_Card__c,Slick__Last_name_on_Card__c,Slick__CC_num__c,Slick__Amount__c from Authorize_net_Transactions__r order by createddate desc) from opportunity where id in:opptyids';
			queryString = x;
			system.debug(queryString);
		}
	}

	global Database.QueryLocator start(Database.BatchableContext bc){
		system.debug(queryString);
		return Database.getQueryLocator(queryString);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope){
		
		for ( Opportunity o : (List<Opportunity>)scope ){
			if (method=='Authorize Only') batchResults.add(authorizeOpportunity(o));
			else if (method=='Charge') batchResults.add(chargeOpportunity(o));
		}
	}

	global static emailResult authorizeOpportunity(Opportunity o){
		emailresult er = new emailresult(o.id, 'Authorizing Opportunity');
		//ChargentSFA.TChargentOperations.TChargentResult chargeResult = ChargentSFA.TChargentOperations.AuthorizeOpportunity_Click(o.Id);
		api_AuthorizeDotNet.authnetReq_Wrapper authRequest= AuthorizeNet_Transaction_Methods.createTransaction(o);
		api_AuthorizeDotNet.authnetresp_wrapper chargeResult=Api_AuthorizeDotNet.authdotnetAuthorize(authRequest);
		if(chargeResult.ResponseReasonCode=='1'){
			Slick__Authorize_Net_Transaction__c newTrans=Opportunity_Methods.AuthorizeDotNetToTransaction(chargeResult,authRequest);
			newTrans.Slick__Payment_Number__c=1.2;
			newTrans.Slick__Order__c=o.id;
			insert newTrans;			
		}
		
		er.status = chargeResult.responseCode;
		er.message = 'Response Reason Code:'+ chargeresult.responseReasonCode+' Response Reason Text:'+ chargeresult.responseReasonText+' TransactionId: '+chargeResult.TransactionId;
		
		return er;
	}

	global static emailResult chargeOpportunity(Opportunity o){
		emailresult er = new emailresult(o.id, 'Charging Opportunity');
		//ChargentSFA.TChargentOperations.TChargentResult chargeResult = ChargentSFA.TChargentOperations.ChargeOpportunity_Click(o.id);
		api_AuthorizeDotNet.authnetReq_Wrapper authRequest;
		api_AuthorizeDotNet.authnetresp_wrapper chargeResult;
		Slick__Authorize_Net_Transaction__c existingTransaction=new Slick__Authorize_Net_Transaction__c();
		//Capture existing authorizations.
		for(Slick__Authorize_Net_Transaction__c AuthTrans: o.Authorize_Net_Transactions__r){
			if(authTrans.Slick__transaction_Type__c=='auth_only'){
				existingTransaction=authTrans;
				break;
			}
		}
		if(existingTransaction!=null && existingTransaction.id!=null){
			chargeResult=AuthorizeNet_Transaction_Methods.priorAuthCaptureTransaction(existingTransaction);
		}
		else{
			authRequest= AuthorizeNet_Transaction_Methods.createTransaction(o);
			chargeResult=Api_AuthorizeDotNet.authdotnetChargeDetailed(authRequest);
		}
		if(chargeResult.ResponseReasonCode=='1'){
			if(existingTransaction==null || existingTransaction.id==null){
				Slick__Authorize_Net_Transaction__c newTrans=Opportunity_Methods.AuthorizeDotNetToTransaction(chargeResult,authRequest);
				newTrans.Slick__Payment_Number__c=1.1;
				newTrans.Slick__Order__c=o.id;
				insert newTrans;	
			}
			else{
				existingTransaction.Slick__Payment_number__c=1.1;
				update existingTransaction;
			}	
		}
		er.status = chargeResult.responseCode;
		er.message = 'Response Reason Code:'+ chargeresult.responseReasonCode+' Response Reason Text:'+ chargeresult.responseReasonText+' TransactionId: '+chargeResult.TransactionId;
	
		return er;
	}

	global void finish(Database.BatchableContext BC){
		//Send Email notification	
		sendEmail();
	}
	
	global void sendEmail(){
		Messaging.singleemailmessage msg = new messaging.singleemailmessage();
		msg.setToAddresses(new list<String>{'annakpayne@gmail.com'});
		msg.setSubject('Opportunity Processing Results');
		
		string messageBody ='These transactions were attempted with the following results:\n\n';
		
		for (emailresult er : batchresults){
			messageBody += 'ID: '+er.tid+' - Type: '+er.typex;
			messageBody += '\n\n'+er.message;
			messageBody += '\nSTATUS: '+er.status;
			messageBody += '\n-----------------------------------------------\n';
		}
		
		messageBody += '\n\n **END RESULTS**';
		msg.setPlainTextBody(messageBody);
		
		if (test.isrunningtest() == false) messaging.sendemail(new list<messaging.singleemailmessage>{msg});
	}

	global class emailResult{
		string tid;
		string typex;
		string status;
		string message;
		emailResult(string txid, string typexin){
			tid = txid;
			typex = typexin;
		}
	}

 

}