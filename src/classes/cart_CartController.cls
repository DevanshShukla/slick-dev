public without sharing class cart_CartController {

	Static Slick__Instance_Setting__c websiteTitle = Slick__Instance_Setting__c.getInstance('Website Title');
	public list<wrapperClasses.cartItem> cartContents {get;set;}
	public string paramstring {get;set;}
	public string errorMsg {get;set;}
	public string infoMsg{get;set;}
	public boolean isSuccess{get;set;}
	public string authorizeNetSealId{
		get{
			if(authorizeNetSealId==null){
				authorizeNetSealId=cart_mainSiteTemplateController.getauthorizeNetSeal();
			}
			return authorizeNetSealId;
		}
		set;
	}
	public decimal shippingMinimum{
		get{
			if(shippingMinimum==null)
				shippingMinimum=opportunity_methods.shippingMinimum;
			return shippingMinimum;
		}
		set;
	}
	public cart_CartController(){
		loadCart();
		if (util.pageget('em')!=null){
			paramstring = util.pageget('em');
			loadPastOrder();
		}
		if (util.pageget('add')!=null && util.pageget('id')!=null){
			addProduct();
		}
		populateProductInfo(cartContents);
		saveCart();
		paramstring = '';
	}

	public String getWebsiteTitle(){
		return (websiteTitle != null && String.isNotBlank(websiteTitle.Slick__Text_Value__c)) ? websiteTitle.Slick__Text_Value__c : 'Nature\'s Select';
	}

	public void addProduct(){
		 Product2 thisProduct;
		if(Test.isRunningTest()){
		     thisProduct = [select id, name, Slick__thumbnail_image__c,Slick__full_size_image__c, Slick__on_sale__c,Slick__Cart_Category__c,Slick__Cart_Subcategory__c, (select id, pricebook2.name, pricebook2.isstandard, unitprice from pricebookentries where isactive=true) from Product2 where id = :util.pageget('id') and Slick__active_in_cart__c=true];

		}
		else
			thisProduct = [select id, name, Slick__thumbnail_image__c,Slick__full_size_image__c, Slick__on_sale__c,Slick__Cart_Category__c,Slick__Cart_Subcategory__c, (select id, pricebook2.name, pricebook2.isstandard, unitprice from pricebookentries where isactive=true AND pricebook2.isactive=true) from Product2 where id = :util.pageget('id') and Slick__active_in_cart__c=true];
		decimal price;
		decimal saleprice;
		for (PricebookEntry pbe : thisProduct.pricebookentries){
			if (pbe.pricebook2.isstandard || test.isRunningTest()) price = pbe.unitprice;
			else if (thisProduct.Slick__on_Sale__c && pbe.pricebook2.name == 'On Sale') saleprice = pbe.unitprice;

		}
		boolean cartHas = false;
		for (wrapperClasses.cartItem ci : cartContents){
			if (ci.pid == thisProduct.id){
				cartHas = true;
				ci.qty += (util.pageget('quantity')!=null)?(integer.valueof(util.pageget('quantity'))):1;
			}
		}
		if (!cartHas) cartContents.add(new wrapperClasses.cartItem(thisProduct, util.pageget('quantity'), price, saleprice));
	}

	public pageReference reOrderAction(){
		System.debug('at least init reorderAction');
		//Is email alert from contact or opportunity.
		String email=util.pageget('em');
		// String orderId=util.pageget('orderId');
		String contactId=util.pageget('contactId');
		String recontact=util.pageget('recontact');
		isSuccess=false;
		if(String.isnotblank(contactId) && String.isnotblank(recontact) && String.isnotblank(email)){
			//list<Contact> conts=[select id,Slick__call_requested__c,Slick__One_Click_Done_Order_Request__c,Slick__next_order_date__c from contact where id=:contactId and (email=:email or Slick__alternate_Email__c=:email or Slick__alternate_Email_2__c=:email or Slick__alternate_Email_3__c=:email) limit 1];
			list<Contact> conts=[select id,Slick__call_requested__c,Slick__One_Click_Done_Order_Request__c,Slick__next_order_date__c from contact where id=:contactId and (email=:email or Slick__alternate_Email__c=:email or Slick__alternate_Email_2__c=:email) limit 1];
			//list<Contact> conts=[select id,Slick__One_Click_Done_Order_Request__c,Slick__next_order_date__c from contact where id=:contactId  limit 1];
			System.debug('LOOK AT ME: cart_CartController');
			System.debug(conts);
			if(conts.size()>0){
				System.debug('LOOK AT ME: cart_CartController');
				System.debug('inside conts size greater than 0');
				Contact reOrderContact= conts.get(0);
				System.debug('LOOK AT ME: cart_CartController, this is reOrderContact');
				System.debug(reOrderContact);

				Date recontactDate=System.today();
				//Date nextOrderDate=reOrderContact.Slick__next_order_date__c;
				//if(nextOrderDate==null)
				//	nextOrderDate=System.today();
				//One Click Done
				if(recontact=='repeat'){
					System.debug('LOOK AT ME: cart_CartController');
					System.debug('inside recontact=repeat');
					reOrderContact.Slick__One_Click_Done_Order_Request__c=system.now();
					update reOrderContact;

					System.debug('this is the result of test.isRunningTest');
					System.debug(test.isRunningTest());

					Slick__Instance_Setting__c autoReOrder = (test.isRunningTest())?(new Slick__Instance_Setting__c(name='One Click Done Reorder', Slick__text_value__c = 'false')):
					((Slick__Instance_Setting__c.getInstance('One Click Done Reorder')));

					Boolean doReorder=false;

					System.debug('LOOK AT ME: cart_CartController, this is autoReorder:');
					System.debug(autoReOrder);
					System.debug(autoReOrder.Slick__text_value__c);


					if( autoReOrder!=null && String.isnotblank(autoReOrder.Slick__text_value__c) ){
						System.debug('LOOK AT ME: cart_CartController');
						System.debug('inside if, this is doReorder bool value');
						doReorder=boolean.valueof(autoReOrder.Slick__text_value__c);
						System.debug(doReorder);
					}

					if(doReorder){
						System.debug('LOOK AT ME: cart_CartController');
						System.debug('doing the doReorder');
						opportunity_Methods.reOrder(contactId);
					}

					PageReference reorderRedirect = Page.Voila;
					return reorderRedirect;

				}
				else if(recontact=='1'){
					recontactDate=recontactDate.addDays(14);
				}
				else if(recontact=='2'){
					recontactDate=recontactDate.addDays(21);
				}
				else if(recontact=='3'){
					recontactDate=recontactDate.addDays(28);
				}
				else if(recontact=='call'){
					reOrderContact.Slick__Call_Requested__c=system.now();
				}
				if(recontact=='1' ||  recontact=='2' || recontact=='3'){
					reorderContact.Slick__next_order_date__c=recontactDate;
				}
				update reOrderContact;
				isSuccess=true;
				return null;
			}

		}
		return page.Exception;
		//return null;
	}



	public pagereference loadPastOrder(){
		paramstring = (util.pageGet('paramstring')!=null)?util.pageget('paramstring'):paramstring;
		// list<Opportunity> pastOrders = [select id, (select id, quantity, pricebookentry.product2id,pricebookentry.product2.name,pricebookentry.product2.Slick__thumbnail_image__c, pricebookentry.product2.Slick__active_in_cart__c, pricebookentry.isactive, pricebookentry.pricebook2.isactive  from OpportunityLineItems where pricebookentry.product2.Slick__active_in_cart__c=true) from Opportunity where Slick__This_Is_A_Return__c = false AND (contact__r.email = :paramstring OR contact__r.Slick__alternate_email__c = :paramstring OR contact__r.Slick__alternate_email_2__c = :paramstring OR contact__r.Slick__alternate_email_3__c = :paramstring)  order by closedate  desc limit 1];
		list<Opportunity> pastOrders = [select id, (select id, quantity, pricebookentry.product2id,pricebookentry.product2.name,pricebookentry.product2.Slick__thumbnail_image__c, pricebookentry.product2.Slick__active_in_cart__c, pricebookentry.isactive, pricebookentry.pricebook2.isactive  from OpportunityLineItems where pricebookentry.product2.Slick__active_in_cart__c=true) from Opportunity where Slick__This_Is_A_Return__c = false AND (contact__r.email = :paramstring OR contact__r.Slick__alternate_email__c = :paramstring OR contact__r.Slick__alternate_email_2__c = :paramstring )  order by closedate  desc limit 1];
		if (pastOrders.size()==0){
			errorMsg = 'Apologies! We cannot find a prior order for email, '+paramstring+'<br/> Try another email to recall your order or just resume shopping.';
			return null;
		}
		else if (pastOrders[0].opportunitylineitems.size()==0){
			errorMsg = 'The last order for '+paramstring+' had no products.';
			return null;
		}
		else {
			//check inactive items
			boolean hasInactiveItems=false;
			for(OpportunityLineItem oli: pastOrders[0].OpportunityLineItems){
				if(oli.pricebookentry.isactive==false || oli.pricebookentry.pricebook2.isactive==false ){
					hasInactiveItems=true;
				}
			}
			if(hasInactiveItems==true){
				errorMsg='Cannot load order. The last order for '+paramstring+' has discountinued products';
				return null;
			}
			
			System.debug('pastOrders[0].OpportunityLineItems>>>>>>>>>>>>>>>>>>>>>'+pastOrders[0].OpportunityLineItems);
			
		cartContents.clear();
		
			for (OpportunityLineItem oli : pastOrders[0].OpportunityLineItems){
				cartContents.add(new wrapperClasses.cartItem(
															new Product2(
																		id=oli.pricebookentry.product2id,
																		name=oli.pricebookentry.product2.name,
																		Slick__thumbnail_image__c = oli.pricebookentry.product2.Slick__thumbnail_image__c
																		),
															string.valueof(oli.quantity), 0, 0));
			}

			populateProductInfo(cartContents);
			saveCart();
			return null;
		}
	}

	public static void populateProductInfo(list<wrapperClasses.cartItem> tCartContents){
		map<id,Product2> productMap = new map<id,Product2>();
		for (wrapperClasses.cartItem ci : tCartContents){
			productmap.put(ci.pid, new Product2());
		}
		if(test.isrunningTest()){
			productMap = new map<id,Product2>([select id, name, Slick__thumbnail_image__c, Slick__on_sale__c,Slick__Tax_Exempt__c,Slick__full_size_image__c,Slick__Cart_Category__c ,Slick__Cart_Subcategory__c, (select id, pricebook2.name, pricebook2.isstandard, unitprice from pricebookentries where isactive=true ) from Product2 where id IN :productmap.keyset() and Slick__active_in_cart__c=true]);
		}
		else
			productMap = new map<id,Product2>([select id, name, Slick__thumbnail_image__c, Slick__on_sale__c,Slick__Tax_Exempt__c,Slick__full_size_image__c,Slick__Cart_Category__c,Slick__Cart_Subcategory__c,  (select id, pricebook2.name, pricebook2.isstandard, unitprice from pricebookentries where isactive=true AND pricebook2.isactive=true) from Product2 where id IN :productmap.keyset() and Slick__active_in_cart__c=true]);
		for (wrapperClasses.cartItem ci : tCartContents){
			ci.price = null;
			ci.saleprice = null;
			Product2 tempProd = productMap.get(ci.pid);
			for (PricebookEntry pbe : tempProd.pricebookentries){
				if (pbe.pricebook2.isstandard || Test.isRunningTest()) ci.price = pbe.unitprice;
				else if (tempProd.Slick__on_Sale__c && pbe.pricebook2.name == 'On Sale') ci.saleprice = pbe.unitprice;
			}
			ci.litot=ci.qty*((ci.saleprice!=null)?ci.saleprice:ci.price);

			ci.taxExempt=tempProd.Slick__tax_Exempt__c;
			ci.thumbnail=tempProd.Slick__thumbnail_image__c;
			ci.fullSizeImage=tempProd.Slick__full_size_image__c;
			ci.pname=tempProd.name;
			ci.subcategories=tempProd.Slick__Cart_SubCategory__c;
			ci.categories=tempProd.Slick__Cart_Category__c;

		}
	}

	public void loadCart(){
		string cartCookie = util.loadCookie('cartCookie');
		System.debug('cartCookie'+ cartCookie);
		if (cartCookie != 'Not Found' && String.isnotblank(cartCookie)){
				cartContents = parseCartContents(cartCookie);
		}
		else cartContents = new list<wrapperClasses.cartItem>();
	}


	public void saveCart(){
		list<wrapperClasses.cartItem> tempContents=new list<wrapperClasses.cartItem>();
		for (wrapperClasses.cartItem ci : cartContents){
			if(ci.qty==null || ci.qty<0){
				ci.qty=0;
			}
			System.debug(ci);
			ci.litot = ci.litot=ci.qty*((ci.saleprice!=null)?ci.saleprice:ci.price);
			//remove 0 qty items
			if(ci.qty>0){
				tempContents.add(ci);
			}
		}
		cartContents=tempContents;
		//Clear Name, URL, and category information to save cookie space.
		list<wrapperClasses.Item> cookieCartContents=new list<wrapperClasses.Item>();
		for(wrapperClasses.cartItem cItem:cartContents)
			cookieCartContents.add(new WrapperClasses.Item(cItem));
		util.saveCookie('cartCookie',JSON.serialize(cookieCartContents),2592000,false);
	}


	public pagereference updateCart(){
		saveCart();
		loadCart();
		populateproductInfo(cartContents);

		return null;
	}

	public pagereference removeItem(){
		paramstring = (util.pageGet('paramstring')!=null)?util.pageget('paramstring'):paramstring;
		for (integer i=0; i<cartContents.size(); i++){
			if (cartContents[i].pid == paramstring) {
				cartContents.remove(i);
				break;
			}
		}
		updateCart();
		paramstring=null;
		return null;
	}
	public pagereference checkout(){

		return Page.checkout;
	}


	public decimal getCartTotal(){
		decimal temp = 0;
		for (wrapperClasses.cartItem ci : cartContents){
			temp += ci.litot;
		}
		return temp;
	}
	public decimal getTaxableCartTotal(){
		decimal temp = 0;
		for (wrapperClasses.cartItem ci : cartContents){
			if(ci.taxExempt!=true)
				temp += ci.litot;
		}
		return temp;
	}

	public static list<wrapperClasses.cartItem> parseCartContents(string val){
		return (list<wrapperClasses.cartItem>)JSON.deserialize(val,list<wrapperClasses.cartItem>.class);
	}
}