@isTest
private class cart_CartControllerTest {
	
	 static testMethod void test_CartCartController() {
		// Implement test code
		
		
		Product2 prod = new Product2(name='test dog food', Slick__cart_category__c='dogfood', Slick__cart_subcategory__c='dryfood',Slick__active_in_cart__c=true);
		insert prod;
		
		PricebookEntry pbe=new PricebookEntry(unitprice=1);
		pbe.Pricebook2id=Test.getStandardPricebookId();
		pbe.product2Id=prod.id;
		pbe.isActive=true;
		insert pbe;
		
		list<product2> pce=[select id, name, Slick__thumbnail_image__c, Slick__on_sale__c, (select Id, pricebook2.name, pricebook2.isstandard, unitprice from pricebookentries where (isactive = true AND pricebook2.isactive=true )) from Product2 where (id = :prod.id and Slick__active_in_cart__c = true)];
		System.debug('wat ');
		
		contact c=new contact(lastname='test',email='tester@test.com'); 
        insert c;
	        
        opportunity opp=new opportunity(Slick__contact__c=c.id,closeDate=date.today(),stagename='Closed Won',name='test',Slick__Delivery_Date__c=date.today(), Slick__This_Is_A_Return__c = false);      
        insert opp;
        
        OpportunitylineItem oli=new OpportunityLineItem(opportunityid=opp.id, pricebookEntryid=pbe.id,quantity=1,totalprice=1);
        insert oli;
	        
		System.debug(pce.get(0).pricebookentries+' '+pce);
		System.debug([select id, description, isactive,isstandard, name from pricebook2 where id=:Test.getStandardPricebookId()]+' '+ test.getStandardPricebookId());
		PageReference p= new pageReference('/test');
		p.getParameters().put('id',prod.id);
		p.getParameters().put('add','true');
		Test.setCurrentPage(p);
		
		cart_CartController cart=new cart_CartController();
		cart.updateCart();
		cart.removeItem();
		cart.checkout();
		cart.getCartTotal();
		cart.getTaxableCartTotal();
		cart.loadPastOrder();

		
		cart.cartContents=new list<wrapperClasses.cartItem>();
		wrapperClasses.cartItem newItem=new wrapperClasses.cartItem(prod,'1.0', 9.0, 1.0);
		cart.cartContents.add(newItem);
		cart.addProduct();
			p.getParameters().put('paramString',prod.id);
		cart.removeItem();
		//reOrder
		cart.reorderAction();

		p.getParameters().put('em',c.email);
		p.getParameters().put('contactId',c.id);
		p.getParameters().put('recontact','1');
		cart.reorderAction();
		p.getParameters().put('recontact','2');
		cart.reorderAction();
		p.getParameters().put('recontact','3');
		cart.reorderAction();
		p.getParameters().put('recontact','call');
		cart.reorderAction();
		p.getParameters().put('recontact','repeat');
		cart.reorderAction();


	}

	
	
	
}