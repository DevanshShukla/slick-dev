@RestResource(urlMapping='/delivery')
global with sharing class delivery_Webservice {

        @HttpGet
        global static void getSchedule(){
            String zip = RestContext.request.params.get('zip');
            String email = RestContext.request.params.get('email');
            String state = RestContext.request.params.get('state');
            List<SelectOption> dates = new List<SelectOption>();


            List<Contact> con = [SELECT id, Slick__Route__c from Contact where Email=:email LIMIT 1];
            if (con.size() > 0){
                dates = (!test.isRunningTest()) ? opportunity_Methods.calculateDeliveryDates(new Slick__Route__c(Id =con[0].Slick__Route__c)) : new List<SelectOption>();
            }
            else {
                dates = opportunity_Methods.calculateDeliveryDates(zip);
            }

            List<String> dateValue = new List<String>();
            for(SelectOption so: dates){
                if (so.getValue() != ''){
                    dateValue.add(so.getValue());
                }
            }

            String response = JSON.serialize(dateValue);

            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(response);
        }


}