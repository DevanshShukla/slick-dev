public with sharing class transactionProcessing_Controller {

	public list<transactionWrapper> transactionslist {get;set;}

	//control variables
	public boolean selectAllFlag {get;set;}
	public string orderSource {get;set;}
	public task deliveryDateHolder {get;set;}
	public task orderDateHolder {get;set;}
	public string paymentstatusfilter {get;set;}
	public string paymentMethodFilter{get;set;}

	public string opptyFileId {get;set;}

	public transactionProcessing_Controller(){
		selectAllFlag = false;
		deliverydateholder = new task();
		orderdateholder = new task();
		paymentMethodFilter = 'Credit Card';
		filter();
		//transactionslist = buildTransactionWrapperList(null, null);
		
		
	}
	
	public class transactionWrapper{
		public Slick__Authorize_net_Transaction__c t {get;set;}
		public Opportunity opp {get;set;}
		public string kind {get;set;}
		public boolean selected {get;set;}
		
		public transactionWrapper(Slick__Authorize_net_Transaction__c input){
			//t = input;
			//kind = 'Transaction';
		}
		
		public transactionWrapper(Opportunity input){
			opp = input;
			kind = 'Opportunity';
		}
	}
	
	public static list<transactionWrapper> buildTransactionWrapperList(string filter, string oppfilter){
		list<transactionWrapper> twlist = new list<transactionwrapper>();
		for (Slick__Authorize_net_Transaction__c ct: getTransactions(filter)){
			twlist.add(new transactionWrapper(ct));
		}
		for (Opportunity o : getOpportunities(oppfilter)) {
			/*if (o.ChargentSFA__Transactions__r.size()==0)*/ twlist.add(new transactionWrapper(o));
		}
		return twlist;
	}
	
	public static list<Slick__Authorize_net_Transaction__c> getTransactions(string filter){
	/*	string queryString = 'Select c.ChargentSFA__Opportunity__r.Slick__Route__c, c.ChargentSFA__Opportunity__r.Slick__ordersource__c, c.ChargentSFA__Opportunity__r.Slick__Contact__c, c.ChargentSFA__Opportunity__r.Slick__ChargentSFA__Payment_Method__c, c.ChargentSFA__Opportunity__r.Slick__order_Date__c, c.ChargentSFA__Opportunity__r.Slick__Delivery_Date__c, c.ChargentSFA__Opportunity__r.Slick__ChargentSFA__Payment_Status__c, c.ChargentSFA__Opportunity__r.name, c.SystemModstamp, c.Slick__Payment_Number__c, c.Name, c.LastModifiedDate, c.LastModifiedById, c.LastActivityDate, c.IsDeleted, c.Id, c.CreatedDate, c.CreatedById, c.Slick__ChargentSFA__Type__c, c.Slick__ChargentSFA__Response__c, c.Slick__ChargentSFA__Response_Status__c, c.Slick__ChargentSFA__Response_Message__c, c.Slick__ChargentSFA__Response_Code__c, c.Slick__ChargentSFA__Recurring__c, c.Slick__ChargentSFA__Reason_Text__c, c.Slick__ChargentSFA__Reason_Code__c, c.Slick__ChargentSFA__Payment_Method__c, c.Slick__ChargentSFA__Opportunity__c, c.Slick__ChargentSFA__Gateway__c, c.Slick__ChargentSFA__Gateway_ID__c, c.Slick__ChargentSFA__Gateway_Date__c, c.Slick__ChargentSFA__Details__c, c.Slick__ChargentSFA__Details_Payflow__c, c.Slick__ChargentSFA__Description__c, c.Slick__ChargentSFA__Currency__c, c.Slick__ChargentSFA__Credit_Card_Type__c, c.Slick__ChargentSFA__Credit_Card_Number__c, c.Slick__ChargentSFA__Credit_Card_Name__c, c.Slick__ChargentSFA__Card_Code_Response__c, c.Slick__ChargentSFA__Billing_State__c, c.Slick__ChargentSFA__Billing_Province__c, c.Slick__ChargentSFA__Billing_Postal_Code__c, c.Slick__ChargentSFA__Billing_Phone__c, c.Slick__ChargentSFA__Billing_Last__c, c.Slick__ChargentSFA__Billing_First__c, c.Slick__ChargentSFA__Billing_Fax__c, c.Slick__ChargentSFA__Billing_Email__c, c.Slick__ChargentSFA__Billing_Country__c, c.Slick__ChargentSFA__Billing_Company__c, c.Slick__ChargentSFA__Billing_City__c, c.Slick__ChargentSFA__Billing_Address__c, c.Slick__ChargentSFA__Bank_Routing_Number__c, c.Slick__ChargentSFA__Bank_Name__c, c.Slick__ChargentSFA__Bank_Account_Type__c, c.Slick__ChargentSFA__Bank_Account_Number__c, c.Slick__ChargentSFA__Bank_Account_Name__c, c.Slick__ChargentSFA__Authorization__c, c.Slick__ChargentSFA__Amount__c, c.Slick__ChargentSFA__AVS_Zip__c, c.Slick__ChargentSFA__AVS_Response_Code__c, c.Slick__ChargentSFA__AVS_International__c From Slick__ChargentSFA__Transaction__c c where ChargentSFA__Opportunity__r.Slick__ChargentSFA__Payment_Status__c != \'Complete\'';
		if (filter != null && filter.trim().length()>0) {
			querystring += filter;
		}	
//		apexpages.addmessage(new apexpages.message(apexpages.severity.info,querystring));	
		return database.query(querystring);*/
		return new list<Slick__Authorize_net_Transaction__c>();
	}
	
	public static list<Opportunity> getOpportunities(string filter){
		//string queryString = 'Select (select id from Authorize_net_Transactions__r), Slick__Charge_Amount__c, Slick__TotalDiscountTaxOther__c, Slick__Use_Information_On_File__c, Slick__Charge_Status__c,Slick__Charge_Status_Final__c, amount, Slick__Route__c, Slick__OrderSource__c, Slick__Contact__c, Slick__Payment_Method__c, Slick__Order_Date__c, Slick__Delivery_Date__c, Slick__Payment_Status__c, SystemModstamp, Name, LastModifiedDate, LastModifiedById, Id, CreatedDate, CreatedById, Slick__Payment_Dot__c,Slick__Has_Critical_Note__c,Slick__Critical_Note_This_Order__c,Slick__Credit_Card_Used__c  From Opportunity where (NOT Slick__Payment_Dot__c LIKE \'%GreenDot%\') ';
		string queryString = 'Select (select id from Authorize_net_Transactions__r), Slick__Charge_Amount__c, Slick__TotalDiscountTaxOther__c, Slick__Use_Information_On_File__c, Slick__Charge_Status__c,Slick__Charge_Status_Final__c, amount, Slick__Route__c, Slick__OrderSource__c, Slick__Contact__c, Slick__Payment_Method__c, Slick__Order_Date__c, Slick__Delivery_Date__c, Slick__Payment_Status__c, SystemModstamp, Name, LastModifiedDate, LastModifiedById, Id, CreatedDate, CreatedById, Slick__Payment_Dot__c,Slick__Has_Critical_Note__c,Slick__Credit_Card_Used__c  From Opportunity where (NOT Slick__Payment_Dot__c LIKE \'%GreenDot%\') ';
		if (filter != null && filter.trim().length()>0) {
			querystring += filter;
		}	
		//queryString+=' order by createdDate desc limit 500';
		queryString+=' order by Slick__Delivery_Date__c desc limit 500';
//		apexpages.addmessage(new apexpages.message(apexpages.severity.info,querystring));	
		return database.query(querystring);
	}
	
	public pagereference chargeSelected(){
		set<id> selectedtxIds = new set<id>();
		set<id> selectedoppids = new set<id>();
		
		for (transactionwrapper tw : transactionslist) {
			if (tw.selected==true && tw.t != null){ 
				selectedtxids.add(tw.t.id);
			}
			else if (tw.selected==true && tw.t == null && tw.opp != null){
				selectedOppIds.add(tw.opp.id);
			}
		}
		if (selectedtxids.size() > 0) {
//			transactionProcessing_Batch job = new transactionProcessing_Batch(selectedtxids);
//			Id processId = Database.executeBatch(job,10);
//			apexpages.addmessage(new apexpages.message(apexpages.severity.confirm, 'Authorized transactions sent to bulk charge processing queue.'));
		}
		if (selectedoppids.size() > 0) {
			opptyTransactionProcessing_Batch job = new opptyTransactionProcessing_Batch(selectedoppids, 'Charge');
			Id processId = Database.executeBatch(job,1);
			apexpages.addmessage(new apexpages.message(apexpages.severity.confirm, 'Opportunities sent to bulk authorization and charge processing queue.'));
		}
		
		
		return null;
	}
	
	public pagereference authorizeSelected(){
		//must be opportunity
		set<id> selectedIds = new set<id>();
		for (transactionwrapper tw : transactionslist) {
			if (tw.selected==true){
				selectedids.add(tw.opp.id);
			}
		}
		
		if (selectedids.size() > 0) {
			opptyTransactionProcessing_Batch job = new opptyTransactionProcessing_Batch(selectedids, 'Authorize Only');
			Id processId = Database.executeBatch(job,1);
			apexpages.addmessage(new apexpages.message(apexpages.severity.confirm, 'Opportunities sent to bulk authorization processing queue.'));
		}
		
		return null;		
	}
	
	public list<SelectOption> getOrderSourceOptions(){
		list<SelectOption> thislist = new list<SelectOption>();
		thislist.add(new selectOption('', 'Not Filtered'));
		thislist.add(new selectOption('Back-End', 'Back-End'));
		thislist.add(new selectOption('Front-End', 'Front-End'));
		return thislist;
	}
	
	public list<SelectOption> getPaymentStatusOptions(){
		list<SelectOption> thislist = new list<SelectOption>();
		thislist.add(new selectOption('', 'Not Filtered'));
		/*for (Schema.Picklistentry ple : Opportunity.Slick__ChargentSFA__Payment_Status__c.getDescribe().getPickListValues()) {
			thislist.add(new selectoption(ple.getvalue(), ple.getlabel()));
		}*/
		thislist.add(new selectoption('GreenDot', 'Green'));
		thislist.add(new selectoption('YellowDot','Yellow'));
		thislist.add(new selectoption('RedDot','Red'));
		return thislist;
	}
	public list<SelectOPtion> getPaymentMethodOptions(){

		list<SelectOption> thislist = new list<SelectOption>();
		thislist.add(new selectOption('', 'Not Filtered'));
		list<Schema.PicklistEntry> ples=Opportunity.Slick__Payment_Method__c.getDescribe().getPickListValues();
		for(Schema.PicklistEntry ple:ples){
			thislist.add(new selectOption(ple.getValue(),ple.getValue()));
		}		
		//thislist.add(new selectOption('Credit Card','Credit Card'));
		//thislist.add(new selectOption('Check (Paper)','Check (Paper)'));
		//thislist.add(new selectOption('E-Check','E-Check'));
		//thislist.add(new selectOption('QBCC','QBCC'));
		//thislist.add(new selectOption('Store POS','Store POS'));
		return thislist;
	}
	
	public pagereference filter(){
		string filters = '';
		string oppfilters = '';
		selectallFlag= false;
		if (orderSource != null && orderSource.trim().length()>0) {
			filters += ' AND c.Order__r.Slick__ordersource__c = \''+orderSource+'\'';
			oppfilters += ' AND Slick__ordersource__c = \''+orderSource+'\'';
		}
		
		if (deliveryDateHolder.activitydate != null) {
			filters += ' AND c.Order__r.Slick__Delivery_Date__c ='+string.valueof(deliveryDateHolder.activitydate);
			oppfilters += ' AND Slick__Delivery_Date__c ='+string.valueof(deliveryDateHolder.activitydate);
		}
		
		if (orderDateHolder.activitydate != null) {
			filters += ' AND c.Order__r.Slick__order_Date__c ='+string.valueof(orderDateHolder.activitydate);
			oppfilters += ' AND Slick__order_Date__c ='+string.valueof(orderDateHolder.activitydate);
		}
		
		if (paymentstatusfilter != null && paymentstatusfilter.trim().length()>0) {
			filters += ' AND c.Order__r.Slick__Payment_Status__c = \''+paymentstatusfilter+'\'';
			oppfilters += ' AND Slick__payment_dot__c LIKE \'%'+paymentstatusfilter+'%\'';
		}
		if (paymentMethodFilter != null && paymentMethodFilter.trim().length()>0) {
			filters += ' AND c.Order__r.Slick__Payment_Method__c = \''+paymentMethodFilter+'\'';
			oppfilters += ' AND Slick__Payment_Method__c = \''+paymentMethodFilter+'\'';
		}
		
		transactionslist = buildTransactionWrapperList(filters, oppfilters);
		
		return null;
	}
	
	public pagereference selectAll(){
		for (TransactionWrapper tw : transactionslist){
			tw.selected= (selectallFlag==true)?true:false;
		}
		return null;
	}
	
	public pagereference updateUseFileInfo(){
		opptyFileId = (apexpages.currentpage().getParameters().get('opptyFileId')==null)?opptyFileId:apexpages.currentpage().getParameters().get('opptyFileId');
		for (transactionwrapper tw : transactionslist){
			if (tw.opp.id == opptyFileId) update tw.opp;
		}
		return null;
	}
	
	/*@isTest
	static void testing(){
		Account a = new account(name='testtransactionproc');
		insert a;
		Slick__Route__c r = new Slick__Route__c(Slick__zip__c='91019', Slick__time__c='10:10' );
		insert r;
		Contact c = new Contact(lastname='testlast', accountid = a.id, Slick__route__c = r.id, Slick__Last_Date_Ordered__c = system.today());
		insert c;
		Opportunity o = new Opportunity();
		o.AccountId = a.Id;
		o.Amount = 0;
		o.CloseDate = system.today();
		o.Slick__Contact__c = c.Id;
		o.Slick__Delivery_Date__c = system.today();
		o.Slick__Discount__c = '1%';
		o.Slick__Ida_Applied_Coupon__c = 'test';
		o.Name = string.valueOf(system.now());
		o.StageName = 'Closed Won';
		insert o;
		
		transactionProcessing_Controller con = new transactionProcessing_Controller();
		con.getOrderSourceOptions();
		con.getPaymentStatusOptions();
		con.selectAll();
		con.updateUseFileInfo();
		
		con.transactionslist[0].selected=true;
		con.chargeSelected();
		con.authorizeSelected();
		
		con.ordersource='Back-End';
		con.deliveryDateHolder.activitydate = system.today();
		con.orderDateHolder.activitydate = system.today();
		con.paymentstatusfilter = 'RedDot';
		con.filter();
	}*/
}