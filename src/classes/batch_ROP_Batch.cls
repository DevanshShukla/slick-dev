/*
	To Run:
			batch_ROP_Batch job = new batch_ROP_Batch();
			Id processId = Database.executeBatch(job);
*/
global class batch_ROP_Batch implements Database.Batchable<sObject>, Database.AllowsCallouts{

	global final String queryString;
	
	global batch_ROP_Batch(){
		if(Test.isRunningTest()){
			Slick__Instance_setting__c tset=new Slick__instance_setting__c();
			tset.name='ROP 5 Day';
			tset.Slick__text_value__c='2';
			insert tset;
		}
		map<string, Slick__Instance_Setting__c> settingsMap = Slick__Instance_Setting__c.getAll();
		set<integer> ropDaysSet = new set<integer>();

		for (String s : settingsMap.keySet()){
			if (s.startsWith('ROP')) ropDaysSet.add(integer.valueof(settingsMap.get(s).Slick__text_value__c));
		}
		
		string ropDaysSetString = '(';
		for (integer i : ropDaysSet) ropDaysSetString += i+',';
		ropDaysSetString = ropDaysSetString.substring(0,ropDaysSetString.length()-1) + ')';
		if ( queryString == null ){
			String x = 'select id,';
			x+= ' c.Slick__Days_till_Next_Food__c,';
			x+= ' Slick__ROP_Trigger_Field__c';
			x+= ' from Contact c';
			x+= ' where c.Slick__active__c = true';
			x+= ' AND c.HasOptedOutOfEmail = false';
			x+= ' AND c.Slick__Auto_Reorder__c = false'; 
			x+= ' AND c.Slick__Days_till_Next_Food__c IN '+ropDaysSetString;
			
			queryString = x;
			system.debug(queryString);
		}
	}

	global Database.QueryLocator start(Database.BatchableContext bc){
		system.debug(queryString);
		return Database.getQueryLocator(queryString);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope){
		//process batch method
		processThis((list<Contact>)scope);
	}

	global static void processThis(list<Contact> input){
		//Will update the contact ROP trigger field, which will send out the ROP Email
		try {
			for (Contact c : input){
				c.Slick__ROP_Trigger_Field__c = system.now();
			}
			database.update(input,false);
		}
		catch (Exception e) {
			throw e; 
		}
	}

	global static void startBatch(){
		batch_ROP_Batch job = new batch_ROP_Batch();
		Database.executeBatch(job);
	}

	global static void startBatch(integer batchSize){
		batch_ROP_Batch job = new batch_ROP_Batch();
		Database.executeBatch(job,batchSize);
	}
	
	global static void startBatchDelay(integer minuteDelay){
		batch_ROP_Batch job = new batch_ROP_Batch();
		system.scheduleBatch(job, 'ROP Batch', minuteDelay);
	}
	
	global static void startBatchDelay(integer minuteDelay, integer batchSize){
		batch_ROP_Batch job = new batch_ROP_Batch();
		system.scheduleBatch(job, 'ROP Batch', minuteDelay, batchsize);
	}

	global void finish(Database.BatchableContext BC){
		//Send Email notification	
	}
}