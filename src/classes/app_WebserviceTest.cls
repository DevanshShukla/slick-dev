@isTest
private class app_WebserviceTest {

    static testMethod void app_Webservice_UnitTest() {
        // TO DO: implement unit test
        //contact con = new Contact(lastName = 'test', Slick__Nature_s_Mobile_App_Registration_Key__c = 'test');
        contact con = new Contact(lastName = 'test');
        insert con;
        
        System.debug('con Id is >>>>>>>>>>>>>>>>>>>>>>'+con);
        
        Slick__Credit_Card__c cc = new Slick__Credit_Card__c(Slick__Contact__c = con.Id);
        insert cc;
        
        Opportunity opp = new Opportunity(name='test', closeDate=system.today(), stageName='Closed Won', Slick__contact__c = con.id, Slick__Order_Placed__c = system.today(), Slick__credit_card_used__c = cc.Id, Slick__Shipping_Zipcode__c = 'zipcode',Slick__delivery_date__c = System.today(), Slick__charge_Amount__c = 10); //, Slick__credit_card_used__c = 'yes'
        insert opp;
        
        product2 prod = new product2(name='test dog food', Slick__cart_category__c='dogfood', Slick__cart_subcategory__c='dryfood',isactive=true, Slick__Mobile_Featured_Upsell__c = true);
        insert prod;
        
        pricebookEntry pbe=new pricebookentry(product2id=Prod.id,pricebook2id=Test.getStandardPriceBookId(),unitprice=1,isactive=true);
        insert pbe;
        
        List<OpportunityLineItem> OppLineList = new List<OpportunityLineItem>();
        
        for(Integer i=0; i<10; i++){
            OpportunityLineItem oli = new OpportunityLineItem(
                 OpportunityId = opp.Id,
                 Quantity = 5,
                 PricebookEntryId = pbe.Id,
                 TotalPrice = 10 * pbe.UnitPrice
            );
            OppLineList.add(oli);
        }
        
        insert OppLineList;
        
        System.debug('Opp Id is >>>>>>>>>>>>>>>>>>>>>>'+opp.Id);

        
        app_WebService.fetchInstanceList();
        app_webservice.fetchRegistrationResponse(null);
        app_webservice.fetchRegistrationResponse('123');
        app_webservice.fetchRegistrationResponse('test');
        app_webservice.fetchLatestOrderDetail(null);
        app_webservice.fetchLatestOrderDetail('test');
        app_webservice.fetchReorderPrepResponse(null);
        app_webservice.fetchReorderPrepResponse('test');
        
        app_webservice.addUpsellProduct(new wrapperClasses.opptyCloneWrapper(opp, OppLineList), prod.Id, '10');

        



     //   app_webservice.fetchReorderResponse();


    	//app_webservice.fetchResponseValue('NSInstances', '123'); 
    	//app_webservice.fetchResponseValue('reg', '123'); 
    	//app_webservice.fetchResponseValue('od', '123'); 
    	//app_webservice.fetchResponseValue('reprep', '123'); 
    	////app_webservice.fetchResponseValue('reorder', '123'); 
    	//app_webservice.fetchResponseValue('reprep', '123'); 
        
    }

    static testmethod void app_webservice_unittest1() {
        Contact c = new Contact(firstname='test', lastname='test',mailingstreet='test', email='fakeemail@example.com');
        insert c;
        Slick__credit_card__c card=new Slick__credit_card__c(Slick__card_number__c='123456789123456',Slick__cvv__c='123',Slick__expiration_year__c='2020',Slick__expiration_month__c='01',Slick__name_on_card__c='tester test',Slick__contact__c=c.id);
        insert card;
        product2 prod = new product2(name='test dog food', Slick__cart_category__c='dogfood', Slick__cart_subcategory__c='dryfood',isactive=true);
        insert prod;
        pricebookEntry pbe =new pricebookentry(product2id=Prod.id,pricebook2id=Test.getStandardPriceBookId(),unitprice=1,isactive=true);
        insert pbe;
        Opportunity opp = new Opportunity(name='test',Slick__delivery_date__c=date.today(),Slick__contact__c=c.id, amount=400, closeDate=system.today(), stageName='Closed Won');
        insert opp;
        apexpages.currentPage().getParameters().put('cid', c.id);

        RestRequest restReq = new RestRequest();
        RestResponse restRes = new RestResponse();
        restReq.requestURI = '/services/apexrest/app_Webservice';
        restReq.addParameter('act', 'reorder');
        restReq.addParameter('oid', opp.Id);
        restReq.httpMethod = 'GET';

        //String body = '{"oid": "' + opp.Id + '", "act": "reorder"}'; 
        //restReq.requestBody = Blob.valueOf(body);

        RestContext.request = restReq;
        RestContext.response = restRes;
        app_Webservice.response();
        
        app_Webservice.AppWebService_Response app = new app_Webservice.AppWebService_Response();
        //app_Webservice.fetchReOrderResponse();
    }
}