public with sharing class product_Methods {
	
	@future
	public static void incrementView(string pid){
		try{
			Product2 p = [select id, Slick__times_viewed__c from Product2 where id = :pid];
			p.Slick__times_viewed__c = (p.Slick__times_viewed__c != null)?p.Slick__times_viewed__c+1:1;
			update p;
		}
		catch(exception e){}
	}
}