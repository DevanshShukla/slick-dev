@istest
public with sharing class cart_CatalogControllerTest {

	public static testmethod void testCatalogController(){
		Cart_CatalogController controller = new Cart_catalogController();	
		apexpages.currentPage().getParameters().put('category', 'dogfood');
		Slick__cart_Settings__c setting = new Slick__cart_Settings__c();
		insert setting;
		Slick__cart_Department__c dep = new Slick__Cart_Department__c(Slick__category__c = 'dogfood',Slick__subCategories__c='dryfood;wetfood');
		dep.Slick__Cart_Settings__c = setting.id;
		insert dep;
		product2 prod = new product2(name='test dog food', Slick__cart_category__c='dogfood', Slick__cart_subcategory__c='dryfood',isactive=true,Slick__active_in_cart__c=true);
		insert prod;
		controller = new Cart_catalogController();
		controller.getAllDepartments();
		controller.getProductsBySubCategoryMap();
		controller.initSubCategories();
		new cart_catalogController.categoryProductWrapper();
		apexpages.currentPage().getParameters().put('subcategory','dryfood');
		controller = new Cart_catalogController();
		controller.getProductsBySubCategoryMap();
	
	}

}