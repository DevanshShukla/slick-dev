global class PostInstallScript implements InstallHandler {
    global void onInstall(InstallContext context) {
        if( context.previousVersion() == null ){
            InsertInitialCustomSettings();
            SetupCart();
        }
        if( context.isUpgrade() ){
            postUpdateProcess();
            list<Slick__Cart_Settings__c> cartset = [select id from Slick__Cart_Settings__c limit 1];
            if(cartset.size() == 0){
                SetupCart();
            }else setupShopByDeptMenu();
        }
    }

    /**
    *   @description    Runs the very first time the package is installed in an org
    **/
    global void InsertInitialCustomSettings(){
        list<Slick__Instance_Setting__c> settingsList=new list<Slick__Instance_Setting__c>{
            new Slick__Instance_Setting__c(name='Authorize.net Credentials', Slick__text_value__c = '{"tk":"REPLACE_TRANSACTIONKEY","api":"REPLACE_APILOGINID"}'),
            new Slick__Instance_Setting__c(name='Authorize.net Credentials - Test', Slick__text_value__c = '{"tk":"REPLACE_TRANSACTIONKEY","api":"REPLACE_APILOGINID"}'),
            new Slick__Instance_Setting__c(name='Authorize.net Test Mode', Slick__text_value__c = 'false'),
            new Slick__Instance_Setting__c(name='Default Frequency', Slick__text_value__c = '5'),
            new Slick__Instance_Setting__c(name='LiveChat License', Slick__text_value__c = null),
            new Slick__Instance_Setting__c(name='Order Cutoff Time', Slick__text_value__c = '20'),
            new Slick__Instance_Setting__c(name='Order Cutoff Time Day', Slick__text_value__c = '1'),
            new Slick__Instance_Setting__c(name='Order Number', Slick__text_value__c = '20000'),
            new Slick__Instance_Setting__c(name='ROP 5 Day', Slick__text_value__c = '5'),
            new Slick__Instance_Setting__c(name='ROP 7 Day', Slick__text_value__c = '7'),
            new Slick__Instance_Setting__c(name='ROP 9 Day', Slick__text_value__c = '9'),
            new Slick__Instance_Setting__c(name='Site URL', Slick__text_value__c = 'http://www.REPLACE.com'),
            new Slick__Instance_Setting__c(name='Social Media Links', Slick__text_value__c = '{"facebook":"http://www.facebook.com","twitter":"http://www.twitter.com","instagram":"http://www.instagram.com","pinterest":"http://www.pinterest.com"}'),
            new Slick__Instance_Setting__c(name='Disable Triggers', Slick__text_value__c = 'false'),
            new Slick__Instance_Setting__c(name='Enable Check Payment', Slick__text_value__c = 'false'),
            new Slick__Instance_Setting__c(name='Authorize.net Seal Id', Slick__text_value__c = ''),
            new Slick__Instance_Setting__c(name='Authorize Only Front End', Slick__text_value__c = 'false'),
            new Slick__Instance_Setting__c(name='Authorize Only Mobile App', Slick__text_value__c = 'false'),
            new Slick__Instance_Setting__c(name='Delivery Days To Skip', Slick__text_value__c = 'Saturday, Sunday'),
            new Slick__Instance_Setting__c(name='Hide Maps', Slick__text_value__c = 'false'),
            new Slick__Instance_Setting__c(name='Shipping Cost', Slick__text_value__c = ''),
            new Slick__Instance_Setting__c(name='Shipping Minimum', Slick__text_value__c = ''),
            new Slick__Instance_Setting__c(name='Shipping or Delivery', Slick__text_value__c = 'Delivery'),
            new Slick__Instance_Setting__c(name='One Click Done Submission Type', Slick__text_value__c='false'),
            new Slick__Instance_Setting__c(Name='One Click Done Reorder', Slick__text_value__c='false')

        };
        insert settingsList;
    }

    global void postUpdateProcess(){
        //1/23/2015 new custom settings
        List<Slick__Instance_Setting__c> newSettings = new List<Slick__Instance_Setting__c>();
        if( Slick__Instance_Setting__c.getInstance('Authorize.net Seal Id')==null){
            newSettings.add(new Slick__Instance_Setting__c(name='Authorize.net Seal Id',Slick__text_value__c=''));
        }
        if(Slick__Instance_Setting__c.getInstance('Authorize Only Front End')==null){
            newSettings.add(new Slick__Instance_Setting__c(name='Authorize Only Front End',Slick__text_value__c='false'));
        }
        if(Slick__Instance_Setting__c.getInstance('Hide Maps')==null){
            newSettings.add(new Slick__Instance_Setting__c(name='Hide Maps',Slick__text_value__c='false'));
        }
        if(Slick__Instance_Setting__c.getInstance('Delivery Days To Skip')==null){
            newSettings.add(new Slick__Instance_Setting__c(name='Delivery Days To Skip',Slick__text_value__c='["Saturday","Sunday"]'));
        }
        if(Slick__Instance_Setting__c.getInstance('Authorize Only Mobile App')==null){
            newSettings.add(new Slick__Instance_Setting__c(name='Authorize Only Mobile App',Slick__text_value__c='null'));
        }
        if(Slick__Instance_Setting__c.getInstance('Shipping or Delivery')==null){
            newSettings.add(new Slick__Instance_Setting__c(name='Shipping or Delivery',Slick__text_value__c='Delivery'));
        }
        if(Slick__Instance_Setting__c.getInstance('Shipping Minimum')==null){
            newSettings.add(new Slick__Instance_Setting__c(name='Shipping Minimum',Slick__text_value__c='0'));
        }
        if(Slick__Instance_Setting__c.getInstance('Shipping Cost')==null){
            newSettings.add(new Slick__Instance_Setting__c(name='Shipping Cost',Slick__text_value__c='0'));
        }
        if(Slick__Instance_Setting__c.getInstance('One Click Done Submission Type')==null){
            newSettings.add(new Slick__Instance_Setting__c(name='One Click Done Submission Type', Slick__text_value__c='false'));
        }
        if(Slick__Instance_Setting__c.getInstance('One Click Done Reorder')==null){
            newSettings.add(new Slick__Instance_Setting__c(Name='One Click Done Reorder', Slick__text_value__c='false'));
        }
        insert newSettings;
    }


    global void setupShopByDeptMenu(){
        Slick__Menu_Item__c shopByDepartmentMenuItem;
        Slick__Menu__c topBarMenuItem;
        Slick__Menu__c topMenuItem;
        for ( Slick__Menu__c m : [
            SELECT Id, Slick__Menu_Type__c
            FROM Slick__Menu__c
        ]){
            if ( m.Slick__Menu_Type__c == 'TopBar' ){
                topBarMenuItem = m;
            }
            if ( m.Slick__Menu_Type__c == 'Top' ){
                topMenuItem = m;
            }
        }
        List<Slick__Menu_Item__c> allMenuItems = [
            SELECT Id, Slick__Dropdown_Menu__c, Slick__Is_Department__c, Slick__Label__c, Slick__Menu__c, Slick__Webpage_URL__c, Slick__Parent_Menu_item__c,
                Menu__r.Slick__Menu_Type__c,
                Parent_Menu_item__r.Slick__Label__c,
                Dropdown_Menu__r.Slick__Menu_Type__c
            FROM Slick__Menu_Item__c
        ];
        for ( Slick__Menu_Item__c m : allMenuItems ){
            if ( m.Slick__Label__c == 'Shop by Department' ){
                shopByDepartmentMenuItem = m;
            }
            if ( String.isBlank(m.Slick__Webpage_URL__c) && m.Menu__r.Slick__Menu_Type__c == 'Top' && m.Slick__Parent_Menu_item__c != null && String.isNotBlank(m.Parent_Menu_Item__r.Slick__Label__c) ){
                m.Slick__Webpage_URL__c = '/ShopByDepartment?category='+EncodingUtil.urlEncode(m.Parent_Menu_Item__r.Slick__Label__c,'UTF-8')+'&subcategory='+EncodingUtil.urlEncode(m.Slick__Label__c,'UTF-8');
            } else
            if ( String.isBlank(m.Slick__Webpage_URL__c) && m.Menu__r.Slick__Menu_Type__c == 'Top' ){
                m.Slick__Webpage_URL__c = '/ShopByDepartment?category='+EncodingUtil.urlEncode(m.Slick__Label__c,'UTF-8');
            }
        }
        if ( shopByDepartmentMenuItem == null ){
            shopByDepartmentMenuItem = new Slick__Menu_Item__c();
            shopByDepartmentMenuItem.Slick__Label__c = 'Shop by Department';
            shopByDepartmentMenuItem.Slick__Menu__c = topBarMenuItem.Id;
            shopByDepartmentMenuItem.Slick__Position__c = 0;
            shopByDepartmentMenuItem.Slick__Webpage_URL__c = '/ShopByDepartment';
            shopByDepartmentMenuItem.Slick__Dropdown_Menu__c = topMenuItem.Id;
            allMenuItems.add(shopByDepartmentMenuItem);
        }
        upsert allMenuItems;
    }

    global void SetupCart(){
        Slick__Cart_Settings__c cartSetting=new Slick__Cart_Settings__c();
        insert cartSetting;


        //create default categories.
        list<Slick__Cart_Department__c> departments=new list<Slick__Cart_Department__c>{
        new Slick__Cart_department__c(Slick__cart_Settings__c=cartSetting.id, Slick__Banner_image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectimages/corp/products/GroupBanner/SC_BiscuitsGroupBanner_Extended.jpg',
                Slick__Category__c='Biscuits',
                Slick__Label__c='Biscuits',
                Slick__Subcategories__c='Classic Flavors;Specialty Flavors;Grain Free & Organic'),
        new Slick__Cart_department__c(Slick__cart_Settings__c=cartSetting.id, Slick__Banner_image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectimages/corp/products/GroupBanner/seasonal/SC_GroupBanner_FlavorsofFall.jpg',
                Slick__Category__c='Flavors of Fall',
                Slick__Label__c='Flavors of Fall',
                Slick__Subcategories__c='Treats;Biscuits;Food;Gifts'),
        new Slick__Cart_department__c(Slick__cart_Settings__c=cartSetting.id, Slick__Banner_image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectimages/corp/products/GroupBanner/SC_DogFoodDry_2PaneBanner.jpg',
                Slick__Category__c='Dog Food: Dry',
                Slick__Label__c='Dog Food: Dry',
                Slick__Subcategories__c='Select Classic Nutrition;Select Cold River Recipe with Salmon;Select Grain Free Chicken;Select New Zealand Lamb;Select Plus with Glucosamine;Select Multi Protein;Select High Protein & Puppy;Select Feline Premium'),
        new Slick__Cart_department__c(Slick__cart_Settings__c=cartSetting.id, Slick__Banner_image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectimages/corp/products/GroupBanner/SC_BonesChewsGroupBanner_Extended.jpg',
                Slick__Category__c='Bones & Chews',
                Slick__Label__c='Bones & Chews',
                Slick__Subcategories__c='Antlers;Pig Ears & Bulli Sticks;Specialty Chews;Bones'),
        new Slick__Cart_department__c(Slick__cart_Settings__c=cartSetting.id, Slick__Banner_image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectimages/corp/products/GroupBanner/SC_TreatsTrainGroupBanner_Extended.jpg',
                Slick__Category__c='Treats & Training',
                Slick__Label__c='Treats & Training',
                Slick__Subcategories__c='Meaty Treats;Sam\'s Yams;Treats for Training'),
        new Slick__Cart_department__c(Slick__cart_Settings__c=cartSetting.id, Slick__Banner_image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectimages/corp/products/GroupBanner/SC_DonateDaycareGroupBanner_Extended.jpg',
                Slick__Category__c='Rescue Donations & Doggy Daycare',
                Slick__Label__c='Rescue Donations & Doggy Daycare',
                Slick__Subcategories__c='Rescue Donations;Doggy Daycare'),
        new Slick__Cart_department__c(Slick__cart_Settings__c=cartSetting.id, Slick__Banner_image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectimages/corp/products/GroupBanner/SC_FleaTickOdorGroupBanner_Extended.jpg',
                Slick__Category__c='Flea, Tick, & Odor',
                Slick__Label__c='Flea, Tick, & Odor',
                Slick__Subcategories__c='Flea & Tick;Waste bags & Lawn Care;Air & Fabric Fresheners'),
        new Slick__Cart_department__c(Slick__cart_Settings__c=cartSetting.id, Slick__Banner_image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectimages/corp/products/GroupBanner/SC_CannedFoodGroupBanner_Extended.jpg',
                Slick__Category__c='Dog Food: Canned',
                Slick__Label__c='Dog Food: Canned',
                Slick__Subcategories__c='Entrees;Whole Chicken, Beef, & Salmon;Loaf Style;Grain Free;Game Meats;95% Meat'  ),
        new Slick__Cart_department__c(Slick__cart_Settings__c=cartSetting.id, Slick__Banner_image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectimages/corp/products/GroupBanner/SC_SupplementsGroupBanner_Extended.jpg ',
                Slick__Category__c='Supplements',
                Slick__Label__c='Supplements',
                Slick__Subcategories__c='Hip & Joint;Digestive Aids;Multi Vitamins;Skin & Coat'),
        new Slick__Cart_department__c(Slick__cart_Settings__c=cartSetting.id, Slick__Banner_image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectimages/corp/products/GroupBanner/SC_CollarLeashBowlGroupBanner_Extended.jpg',
                Slick__Category__c='Collars, Leashes, & Bowls',
                Slick__Label__c='Collars, Leashes, & Bowls',
                Slick__Subcategories__c='Collars;Leashes;Bowls'),
        new Slick__Cart_department__c(Slick__cart_Settings__c=cartSetting.id, Slick__Banner_image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectimages/corp/products/GroupBanner/SC_IDTagsGroupBanner_Extended.jpg',
                Slick__Category__c='ID Tags',
                Slick__Label__c='ID Tags',
                Slick__Subcategories__c='Dog ID Tags;Cat ID Tags'),
        new Slick__Cart_department__c(Slick__cart_Settings__c=cartSetting.id, Slick__Banner_image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectimages/corp/products/GroupBanner/SC_GroomingGroupBanner_Extended.jpg',
                Slick__Category__c='Grooming',
                Slick__Label__c='Grooming',
                Slick__Subcategories__c='Shampoo;Oral Care;Ear & Eye Care'),
        new Slick__Cart_department__c(Slick__cart_Settings__c=cartSetting.id, Slick__Banner_image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectimages/corp/products/GroupBanner/SC_GiftsGroupBanner_Extended.jpg',
                Slick__Category__c='Gifts',
                Slick__Label__c='Gifts',
                Slick__Subcategories__c='Thundershirts;Key Chains;Gift Certificates'),
        new Slick__Cart_department__c(Slick__cart_Settings__c=cartSetting.id, Slick__Banner_image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectimages/corp/products/GroupBanner/SC_CatsOnlyGroupBanner_Extended.jpg',
                Slick__Category__c='For Cats Only',
                Slick__Label__c='For Cats Only',
                Slick__Subcategories__c='Dry Food;Canned Food;Treats;ID Tags;Cat Litter'),
        new Slick__Cart_department__c(Slick__cart_Settings__c=cartSetting.id, Slick__Banner_image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectimages/corp/products/GroupBanner/SC_ToysGroupBanner_Extended.jpg',
                Slick__Category__c='Toys',
                Slick__Label__c='Toys',
                Slick__Subcategories__c='Toys to Chew;Toys to Chase')
        };

        insert Departments;
        //build menus and submenus..
        Slick__menu__c topMenu=new Slick__Menu__c(Slick__cart_Settings__c=cartSetting.id,Slick__Menu_Type__c='Top',Slick__isActive__c=true);
        Slick__menu__c topBar=new Slick__Menu__c(Slick__cart_Settings__c=cartSetting.id,Slick__Menu_Type__c='TopBar',Slick__isActive__c=true);
        list<Slick__menu__c> menus=new list<Slick__Menu__c>{topBar,topMenu};
        insert menus;


//Menu Item Parents
    list<Slick__Menu_Item__c> topItems=new list<Slick__Menu_Item__c>{
        new Slick__Menu_Item__c(Slick__Label__c='Dog Food: Dry',Slick__Menu__c=topMenu.id,Slick__Position__c=0,Slick__Navigation_Image_Url__c='https://c.na15.content.force.com/servlet/servlet.ImageServer?id=015i0000003cPug&oid=00Di0000000jpse&lastMod=1405707312000',Slick__Shop__c= 'The Food Bowl',Slick__Webpage_URL__c= '/ShopByDepartment?category=Dog+Food%3A+Dry' ),
        new Slick__Menu_Item__c(Slick__Label__c='Dog Food: Canned',Slick__Menu__c=topMenu.id,Slick__Position__c=1,Slick__Navigation_Image_Url__c='https://c.na15.content.force.com/servlet/servlet.ImageServer?id=015i0000003cPuQ&oid=00Di0000000jpse&lastMod=1405707252000',Slick__Shop__c='The Food Bowl',Slick__Webpage_URL__c= '/ShopByDepartment?category=Dog+Food%3A+Canned'),
        new Slick__Menu_Item__c(Slick__Label__c='For Cats Only',Slick__Menu__c=topMenu.id,Slick__Position__c=2,Slick__Navigation_Image_Url__c='https://c.na15.content.force.com/servlet/servlet.ImageServer?id=015i0000003cPuV&oid=00Di0000000jpse&lastMod=1405707273000',Slick__Shop__c='The Food Bowl',Slick__Webpage_URL__c= '/ShopByDepartment?category=For+Cats+Only'),
        new Slick__Menu_Item__c(Slick__Label__c='Supplements',Slick__Menu__c=topMenu.id,Slick__Position__c=3,Slick__Navigation_Image_Url__c='https://c.na15.content.force.com/servlet/servlet.ImageServer?id=015i0000003cPvd&oid=00Di0000000jpse&lastMod=1405707368000',Slick__Shop__c='Health & Wellness',Slick__Webpage_URL__c= '/ShopByDepartment?category=Supplements'),
        new Slick__Menu_Item__c(Slick__Label__c='Flea, Tick, & Odor',Slick__Menu__c=topMenu.id,Slick__Position__c=4,Slick__Navigation_Image_Url__c='https://c.na15.content.force.com/servlet/servlet.ImageServer?id=015i0000003cPts&oid=00Di0000000jpse&lastMod=1405707323000  ',Slick__Shop__c='Health & Wellness',Slick__Webpage_URL__c= '/ShopByDepartment?category=Flea%2C+Tick%2C+%26+Odor'),
        new Slick__Menu_Item__c(Slick__Label__c='Grooming',Slick__Menu__c=topMenu.id,Slick__Position__c=5,Slick__Navigation_Image_Url__c='https://c.na15.content.force.com/servlet/servlet.ImageServer?id=015i0000003cPvO&oid=00Di0000000jpse&lastMod=1405707345000',Slick__Shop__c='Health & Wellness',Slick__Webpage_URL__c= '/ShopByDepartment?category=Grooming'),
        new Slick__Menu_Item__c(Slick__Label__c='Biscuits',Slick__Menu__c=topMenu.id,Slick__Position__c=6,Slick__Navigation_Image_Url__c='https://c.na15.content.force.com/servlet/servlet.ImageServer?id=015i0000003cPuB&oid=00Di0000000jpse&lastMod=1405707216000',Slick__Shop__c='Biscuits, Chews, & Treats',Slick__Webpage_URL__c= '/ShopByDepartment?category=Biscuits'),
        new Slick__Menu_Item__c(Slick__Label__c='Bones & Chews',Slick__Menu__c=topMenu.id,Slick__Position__c=7,Slick__Navigation_Image_Url__c='https://c.na15.content.force.com/servlet/servlet.ImageServer?id=015i0000003cPuG&oid=00Di0000000jpse&lastMod=1405707231000',Slick__Shop__c='Biscuits, Chews, & Treats',Slick__Webpage_URL__c= '/ShopByDepartment?category=Bones+%26+Chews'),
        new Slick__Menu_Item__c(Slick__Label__c='Treats & Training',Slick__Menu__c=topMenu.id,Slick__Position__c=8,Slick__Navigation_Image_Url__c='https://c.na15.content.force.com/servlet/servlet.ImageServer?id=015i0000003cPvn&oid=00Di0000000jpse&lastMod=1405707393000',Slick__Shop__c='Biscuits, Chews, & Treats',Slick__Webpage_URL__c= '/ShopByDepartment?category=Treats+%26+Training'),
        new Slick__Menu_Item__c(Slick__Label__c='Toys',Slick__Menu__c=topMenu.id,Slick__Position__c=9,Slick__Navigation_Image_Url__c='https://c.na15.content.force.com/servlet/servlet.ImageServer?id=015i0000003cPvi&oid=00Di0000000jpse&lastMod=1405707381000',Slick__Shop__c='Pet Accessories',Slick__Webpage_URL__c= '/ShopByDepartment?category=Toys'),
        new Slick__Menu_Item__c(Slick__Label__c='Collars, Leashes, & Bowls',Slick__Menu__c=topMenu.id,Slick__Position__c=10,Slick__Navigation_Image_Url__c='https://c.na15.content.force.com/servlet/servlet.ImageServer?id=015i0000003cPua&oid=00Di0000000jpse&lastMod=1405707287000',Slick__Shop__c='Pet Accessories',Slick__Webpage_URL__c= '/ShopByDepartment?category=Collars%2C+Leashes%2C+%26+Bowls'),
        new Slick__Menu_Item__c(Slick__Label__c='ID Tags',Slick__Menu__c=topMenu.id,Slick__Position__c=11,Slick__Navigation_Image_Url__c='https://c.na15.content.force.com/servlet/servlet.ImageServer?id=015i0000003cPvY&oid=00Di0000000jpse&lastMod=1405707356000',Slick__Shop__c='Pet Accessories',Slick__Webpage_URL__c= '/ShopByDepartment?category=ID+Tags'),
        new Slick__Menu_Item__c(Slick__Label__c='Gifts',Slick__Menu__c=topMenu.id,Slick__Position__c=12,Slick__Navigation_Image_Url__c='https://c.na15.content.force.com/servlet/servlet.ImageServer?id=015i0000003cPvE&oid=00Di0000000jpse&lastMod=1405707333000',Slick__Webpage_URL__c= '/ShopByDepartment?category=Gifts'),
        new Slick__Menu_Item__c(Slick__Label__c='Rescue Donations & Doggy Daycare',Slick__Menu__c=topMenu.id,Slick__Position__c=13,Slick__Navigation_Image_Url__c='https://c.na15.content.force.com/servlet/servlet.ImageServer?id=015i0000003cPuf&oid=00Di0000000jpse&lastMod=1405707301000',Slick__Webpage_URL__c= '/ShopByDepartment?category=Rescue+Donations+%26+Doggy+Daycare')
    };
    insert topItems;
        //child
    list<Slick__Menu_Item__c> topChildItems=new list<Slick__Menu_Item__c>{
            new Slick__Menu_Item__c(Slick__Label__c='Select Classic Nutrition',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(0).id, Slick__Position__c=0,Slick__Webpage_URL__c= '/ShopByDepartment?category=Dog+Food%3A+Dry&subcategory=Select+Classic+Nutrition'),
            new Slick__Menu_Item__c(Slick__Label__c='Select Cold River Recipe with Salmon',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(0).id, Slick__Position__c=1,Slick__Webpage_URL__c= '/ShopByDepartment?category=Dog+Food%3A+Dry&subcategory=Select+Cold+River+Recipe+with+Salmon'),
            new Slick__Menu_Item__c(Slick__Label__c='Select Grain Free Chicken',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(0).id,    Slick__Position__c=2,Slick__Webpage_URL__c= '/ShopByDepartment?category=Dog+Food%3A+Dry&subcategory=Select+Grain+Free+Chicken'),
            new Slick__Menu_Item__c(Slick__Label__c='Select New Zealand Lamb',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(0).id,  Slick__Position__c=3,Slick__Webpage_URL__c= '/ShopByDepartment?category=Dog+Food%3A+Dry&subcategory=Select+New+Zealand+Lamb'),
            new Slick__Menu_Item__c(Slick__Label__c='Select Plus with Glucosamine',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(0).id, Slick__Position__c=4,Slick__Webpage_URL__c= '/ShopByDepartment?category=Dog+Food%3A+Dry&subcategory=Select+Plus+with+Glucosamine'),
            new Slick__Menu_Item__c(Slick__Label__c='Select Multi Protein',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(0).id, Slick__Position__c=5,Slick__Webpage_URL__c= '/ShopByDepartment?category=Dog+Food%3A+Dry&subcategory=Select+Multi+Protein'),
            new Slick__Menu_Item__c(Slick__Label__c='Select High Protein & Puppy',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(0).id,  Slick__Position__c=5,Slick__Webpage_URL__c= '/ShopByDepartment?category=Dog+Food%3A+Dry&subcategory=Select+High+Protein+%26+Puppy'),
            new Slick__Menu_Item__c(Slick__Label__c='Select Feline Premium',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(0).id,Slick__Position__c=6,Slick__Webpage_URL__c= '/ShopByDepartment?category=Dog+Food%3A+Dry&subcategory=Select+Feline+Premium'),

            new Slick__Menu_Item__c(Slick__Label__c='Entrees',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(1).id,  Slick__Position__c=0,Slick__Webpage_URL__c='/ShopByDepartment?category=Dog+Food%3A+Canned&subcategory=Entrees'),
            new Slick__Menu_Item__c(Slick__Label__c='Whole Chicken, Beef, & Salmon',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(1).id,    Slick__Position__c=1,Slick__Webpage_URL__c='/ShopByDepartment?category=Dog+Food%3A+Canned&subcategory=Whole+Chicken%2C+Beef%2C+%26+Salmon'),
            new Slick__Menu_Item__c(Slick__Label__c='Loaf Style',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(1).id,   Slick__Position__c=5,Slick__Webpage_URL__c='/ShopByDepartment?category=Dog+Food%3A+Canned&subcategory=Loaf+Style'),
            new Slick__Menu_Item__c(Slick__Label__c='Grain Free',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(1).id,   Slick__Position__c=6,Slick__Webpage_URL__c='/ShopByDepartment?category=Dog+Food%3A+Canned&subcategory=Grain+Free'),
            new Slick__Menu_Item__c(Slick__Label__c='95% Meat',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(1).id, Slick__Position__c=3,Slick__Webpage_URL__c='/ShopByDepartment?category=Dog+Food%3A+Canned&subcategory=95%25%20Meat'),
            new Slick__Menu_Item__c(Slick__Label__c='Game Meats',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(1).id,   Slick__Position__c=4,Slick__Webpage_URL__c='/ShopByDepartment?category=Dog+Food%3A+Canned&subcategory=Game%20Meats'),


            new Slick__Menu_Item__c(Slick__Label__c='Dry Food',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(2).id, Slick__Position__c=0,Slick__Webpage_URL__c='/ShopByDepartment?category=For+Cats+Only&subcategory=Dry+Food'),
            new Slick__Menu_Item__c(Slick__Label__c='Canned Food',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(2).id,  Slick__Position__c=1,Slick__Webpage_URL__c='/ShopByDepartment?category=For+Cats+Only&subcategory=Canned+Food'),
            new Slick__Menu_Item__c(Slick__Label__c='Treats',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(2).id,   Slick__Position__c=2,Slick__Webpage_URL__c='/ShopByDepartment?category=For+Cats+Only&subcategory=Treats'),
            new Slick__Menu_Item__c(Slick__Label__c='ID Tags',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(2).id,  Slick__Position__c=3,Slick__Webpage_URL__c='/ShopByDepartment?category=For+Cats+Only&subcategory=ID+Tags'),
            new Slick__Menu_Item__c(Slick__Label__c='Cat Litter',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(2).id,   Slick__Position__c=4,Slick__Webpage_URL__c='/ShopByDepartment?category=For+Cats+Only&subcategory=Cat%20Litter'),

            new Slick__Menu_Item__c(Slick__Label__c='Hip & Joint',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(3).id,  Slick__Position__c=0,Slick__Webpage_URL__c='/ShopByDepartment?category=Supplements&subcategory=Hip+%26+Joint'),
            new Slick__Menu_Item__c(Slick__Label__c='Digestive Aids',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(3).id,   Slick__Position__c=1,Slick__Webpage_URL__c='/ShopByDepartment?category=Supplements&subcategory=Digestive+Aids'),
            new Slick__Menu_Item__c(Slick__Label__c='Multi Vitamins',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(3).id,   Slick__Position__c=2,Slick__Webpage_URL__c='/ShopByDepartment?category=Supplements&subcategory=Multi+Vitamins'),
            new Slick__Menu_Item__c(Slick__Label__c='Skin & Coat',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(3).id,  Slick__Position__c=3,Slick__Webpage_URL__c='/ShopByDepartment?category=Supplements&subcategory=Skin+%26+Coat'),

            new Slick__Menu_Item__c(Slick__Label__c='Flea & Tick',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(4).id,  Slick__Position__c=0,Slick__Webpage_URL__c='/ShopByDepartment?category=Flea%2C+Tick%2C+%26+Odor&subcategory=Flea+%26+Tick'),
            new Slick__Menu_Item__c(Slick__Label__c='Waste bags & Lawn Care',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(4).id,   Slick__Position__c=1,Slick__Webpage_URL__c='/ShopByDepartment?category=Flea%2C+Tick%2C+%26+Odor&subcategory=Waste+bags+%26+Lawn+Care'),
            new Slick__Menu_Item__c(Slick__Label__c='Air & Fabric Fresheners',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(4).id,  Slick__Position__c=2,Slick__Webpage_URL__c='/ShopByDepartment?category=Flea%2C+Tick%2C+%26+Odor&subcategory=Air+%26+Fabric+Fresheners'),

            new Slick__Menu_Item__c(Slick__Label__c='Shampoo',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(5).id,  Slick__Position__c=0,Slick__Webpage_URL__c='/ShopByDepartment?category=Grooming&subcategory=Shampoo'),
            new Slick__Menu_Item__c(Slick__Label__c='Oral Care',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(5).id,    Slick__Position__c=1,Slick__Webpage_URL__c='/ShopByDepartment?category=Grooming&subcategory=Oral+Care'),
            new Slick__Menu_Item__c(Slick__Label__c='Ear & Eye Care',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(5).id,   Slick__Position__c=2,Slick__Webpage_URL__c='/ShopByDepartment?category=Grooming&subcategory=Ear+%26+Eye+Care'),

            new Slick__Menu_Item__c(Slick__Label__c='Classic Flavors',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(6).id,  Slick__Position__c=0,Slick__Webpage_URL__c='/ShopByDepartment?category=Biscuits&subcategory=Classic+Flavors'),
            new Slick__Menu_Item__c(Slick__Label__c='Specialty Flavors',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(6).id,    Slick__Position__c=1,Slick__Webpage_URL__c='/ShopByDepartment?category=Biscuits&subcategory=Specialty+Flavors'),
            new Slick__Menu_Item__c(Slick__Label__c='Grain Free & Organic',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(6).id, Slick__Position__c=2,Slick__Webpage_URL__c='/ShopByDepartment?category=Biscuits&subcategory=Grain%20Free%20%26%20Organic'),

            new Slick__Menu_Item__c(Slick__Label__c='Antlers',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(7).id,  Slick__Position__c=0,Slick__Webpage_URL__c='/ShopByDepartment?category=Bones+%26+Chews&subcategory=Antlers'),
            new Slick__Menu_Item__c(Slick__Label__c='Pig Ears & Bulli Sticks',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(7).id,  Slick__Position__c=2,Slick__Webpage_URL__c='/ShopByDepartment?category=Bones+%26+Chews&subcategory=Pig+Ears+%26+Bulli+Sticks'),
            new Slick__Menu_Item__c(Slick__Label__c='Specialty Chews',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(7).id,  Slick__Position__c=3,Slick__Webpage_URL__c='/ShopByDepartment?category=Bones+%26+Chews&subcategory=Specialty+Chews'),
            new Slick__Menu_Item__c(Slick__Label__c='Bones',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(7).id,    Slick__Position__c=1,Slick__Webpage_URL__c='/ShopByDepartment?category=Bones+%26+Chews&subcategory=Bones'),

            new Slick__Menu_Item__c(Slick__Label__c='Meaty Treats',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(8).id, Slick__Position__c=0,Slick__Webpage_URL__c='/ShopByDepartment?category=Treats+%26+Training&subcategory=Meaty+Treats'),
            new Slick__Menu_Item__c(Slick__Label__c='Sam\'s Yams',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(8).id,  Slick__Position__c=1,Slick__Webpage_URL__c='/ShopByDepartment?category=Treats+%26+Training&subcategory=Sam%27s%20Yams'),
            new Slick__Menu_Item__c(Slick__Label__c='Treats for Training',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(8).id,  Slick__Position__c=2,Slick__Webpage_URL__c='/ShopByDepartment?category=Treats+%26+Training&subcategory=Treats%20for%20Training'),

            new Slick__Menu_Item__c(Slick__Label__c='Toys to Chew',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(9).id, Slick__Position__c=0,Slick__Webpage_URL__c='/ShopByDepartment?category=Toys&subcategory=Toys%20to%20Chew'),
            new Slick__Menu_Item__c(Slick__Label__c='Toys to Chase',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(9).id,    Slick__Position__c=1,Slick__Webpage_URL__c='/ShopByDepartment?category=Toys&subcategory=Toys%20to%20Chase'),

            new Slick__Menu_Item__c(Slick__Label__c='Collars',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(10).id, Slick__Position__c=0,Slick__Webpage_URL__c='/ShopByDepartment?category=Collars%2C+Leashes%2C+%26+Bowls&subcategory=Collars'),
            new Slick__Menu_Item__c(Slick__Label__c='Leashes',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(10).id, Slick__Position__c=1,Slick__Webpage_URL__c='/ShopByDepartment?category=Collars%2C+Leashes%2C+%26+Bowls&subcategory=Leashes'),
            new Slick__Menu_Item__c(Slick__Label__c='Bowls',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(10).id,   Slick__Position__c=2,Slick__Webpage_URL__c='/ShopByDepartment?category=Collars%2C+Leashes%2C+%26+Bowls&subcategory=Bowls'),

            new Slick__Menu_Item__c(Slick__Label__c='Dog ID Tags',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(11).id, Slick__Position__c=0,Slick__Webpage_URL__c='/ShopByDepartment?category=ID+Tags&subcategory=Dog+ID+Tags'),
            new Slick__Menu_Item__c(Slick__Label__c='Cat ID Tags',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(11).id, Slick__Position__c=1,Slick__Webpage_URL__c='/ShopByDepartment?category=ID+Tags&subcategory=Cat+ID+Tags'),

            new Slick__Menu_Item__c(Slick__Label__c='Thundershirts',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(12).id,   Slick__Position__c=1,Slick__Webpage_URL__c='/ShopByDepartment?category=Gifts&subcategory=Thundershirts'),
            new Slick__Menu_Item__c(Slick__Label__c='Key Chains',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(12).id,  Slick__Position__c=2,Slick__Webpage_URL__c='/ShopByDepartment?category=Gifts&subcategory=Key+Chains'),
            new Slick__Menu_Item__c(Slick__Label__c='Gift Certificates',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(12).id,   Slick__Position__c=0,Slick__Webpage_URL__c='/ShopByDepartment?category=Gifts&subcategory=Gift+Certificates'),

            new Slick__Menu_Item__c(Slick__Label__c='Rescue Donations',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(13).id,    Slick__Position__c=0,Slick__Webpage_URL__c='/ShopByDepartment?category=Rescue+Donations+%26+Doggy+Daycare&subcategory=Rescue+Donations'),
            new Slick__Menu_Item__c(Slick__Label__c='Doggy Daycare',Slick__Menu__c=topMenu.id,Slick__Parent_Menu_item__c=topItems.get(13).id,   Slick__Position__c=1,Slick__Webpage_URL__c='/ShopByDepartment?category=Rescue+Donations+%26+Doggy+Daycare&subcategory=Doggy+Daycare')
        };

        //topbar
        list<Slick__Menu_Item__c> topbarChildItems=new list<Slick__Menu_item__c>{
            new Slick__Menu_Item__c(Slick__Label__c='Pet Food', Slick__Menu__c=topBar.id,Slick__position__c=0,Slick__Webpage_URL__c='https://naturesselectpetfood.com/products'),
            new Slick__Menu_Item__c(Slick__Label__c='Compare Recipes', Slick__Menu__c=topBar.id,Slick__position__c=1,Slick__Webpage_URL__c='https://naturesselectpetfood.com/products'),
            new Slick__Menu_Item__c(Slick__Label__c='Our Services', Slick__Menu__c=topBar.id,Slick__position__c=2,Slick__Webpage_URL__c='https://naturesselectpetfood.com/services'),
            new Slick__Menu_Item__c(Slick__Label__c='Our Story', Slick__Menu__c=topBar.id,Slick__position__c=3,Slick__Webpage_URL__c='https://naturesselectpetfood.com/our-story'),
            new Slick__Menu_Item__c(Slick__Label__c='Contact Us', Slick__Menu__c=topBar.id,Slick__position__c=4,Slick__Webpage_URL__c='https://naturesselectpetfood.com/contact'),
            new Slick__Menu_Item__c(Slick__Label__c='Referrals', Slick__Menu__c=topBar.id,Slick__position__c=5,Slick__Webpage_URL__c='https://naturesselectpetfood.com/contact')
        };

        list<Slick__Menu_Item__c> allchildrenItems=new list<Slick__Menu_Item__c>();
        allChildrenItems.addAll(topChildItems);
        allChildrenItems.addAll(topbarChildItems);
        insert allChildrenItems;

        //Merge Items
        list<Slick__Merge_Item__c> mergeItems=new list<Slick__Merge_Item__c>{
            new Slick__Merge_item__c(name='Left Advert',Slick__cart_Settings__c=cartSetting.id, Slick__type__c='Home Page Item', Slick__URL__c='/ShopByDepartment?category=Flavors+Of+Fall',Slick__Image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectimages/corp/products/homepageimages/SC_HomePage_PromoFall.jpg'),
            new Slick__Merge_item__c(name='Right Advert', Slick__cart_Settings__c=cartSetting.id,Slick__type__c='Home Page Item', Slick__URL__c='/ShopByDepartment?category=Supplements',Slick__Image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectimages/corp/products/homepageimages/SC_HomePage_RightFallPromo.jpg'),
            new Slick__Merge_item__c(name='Compare Recipies',Slick__cart_Settings__c=cartSetting.id, Slick__type__c='Home Page Item', Slick__URL__c='https://naturesselectpetfood.com/products', Slick__Image_Url__c='https://s3-us-west-1.amazonaws.com/naturesselectnsd/nsd/chicago/wp-content/uploads/2014/01/Screenshot-2014-06-24-09.08.31.png'),
            new Slick__Merge_item__c(name='ROP One Click Success',Slick__cart_Settings__c=cartSetting.id, Slick__type__c='Markup', Slick__body__c='<div class="fs_contentarea"> <div style="padding:30px 20px 30px 120px"> <div> <a href="mailto:alex@nschicago.com?subject=One Click Done Question"><img width="700" src="https://s3-us-west-1.amazonaws.com/naturesselectimages/nschicago/OneClick.jpg"/></a> </div> </div> </div>'),
            new Slick__Merge_item__c(name='ROP Your Request Success',Slick__cart_Settings__c=cartSetting.id, Slick__type__c='Markup', Slick__body__c='<div class="fs_contentarea"> <div style="padding:30px 20px 30px 120px"> <div> <a href="mailto:alex@nschicago.com?subject=One Click Done Question"><img width="700" src="https://s3-us-west-1.amazonaws.com/naturesselectimages/nschicago/ROP_YourRequestNSCHICAGO.jpg"/></a> </div> </div> </div>'),

            new Slick__Merge_item__c(name='Icon Link 1',Slick__cart_Settings__c=cartSetting.id, Slick__type__c='Home Page Item', Slick__URL__c='https://naturesselectpetfood.com/products'),
            new Slick__Merge_item__c(name='Icon Link 2',Slick__cart_Settings__c=cartSetting.id, Slick__type__c='Home Page Item', Slick__URL__c='https://naturesselectpetfood.com/services'),
            new Slick__Merge_item__c(name='Icon Link 3',Slick__cart_Settings__c=cartSetting.id, Slick__type__c='Home Page Item', Slick__URL__c='https://secure.livechatinc.com/licence/4152091/open_chat.cgi'),

            new Slick__Merge_item__c(name='Slide1',Slick__cart_Settings__c=cartSetting.id, Slick__type__c='Home Page Slider', Slick__Image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectnsd/nsd/chicago/wp-content/uploads/2014/09/SChomeSlide_1.jpg'),
            new Slick__Merge_item__c(name='Slide2',Slick__cart_Settings__c=cartSetting.id, Slick__type__c='Home Page Slider', Slick__Image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectnsd/nsd/chicago/wp-content/uploads/2014/09/SChomeSlide_2.jpg'),
            new Slick__Merge_item__c(name='Slide3',Slick__cart_Settings__c=cartSetting.id, Slick__type__c='Home Page Slider', Slick__Image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectnsd/nsd/chicago/wp-content/uploads/2014/09/SChomeSlide_3.jpg'),
            new Slick__Merge_item__c(name='Main Logo',Slick__cart_Settings__c=cartSetting.id, Slick__type__c='Sitewide', Slick__Image_URL__c='https://s3-us-west-1.amazonaws.com/naturesselectimages/corp/cart/logo-300x97.png'),
            new Slick__Merge_item__c(name='PromoText',Slick__cart_Settings__c=cartSetting.id, Slick__type__c='Sitewide', Slick__Body__c='Order Bag of Food, Get FREE SHIPPING')


        };
        insert mergeItems;
    }
}