public without sharing class wrapperClasses {
    //minified for cookie storage
    public virtual class Item{
        public string productcode {get;set;}
        public string pid {get;set;}
        public integer qty {get;set;}
        public Item(Item cItem){
            this.productcode=cItem.productcode;
            this.pid=cItem.pid;
            this.qty=cItem.qty;
        }
        public Item(){}
    }
    public class cartItem extends Item{
        //public string pid {get;set;}
        //public integer qty {get;set;}
        public string pname {get;set;}
        public decimal price {get;set;}
        public decimal salePrice {get;set;}
        public decimal litot {get;set;}
        public string thumbnail {get;set;}
        public string fullSizeImage{get;set;}
        public string subcategories{get;set;}
        public string categories{get;set;}
        public boolean taxExempt{get;set;}//Tax exempt

        public cartItem(Product2 p, string q, decimal pr, decimal spr){
            pid = p.id;
            pname = p.name;
            qty=((q!=null)?(integer.valueof(q)):1);
            price=pr;
            saleprice=spr;
            litot=qty*((saleprice!=null)?saleprice:price);
            thumbnail = p.Slick__thumbnail_image__c;
            fullSizeImage=p.Slick__Full_size_Image__c;
            categories=p.Slick__Cart_Category__c;
            subcategories=p.Slick__Cart_Subcategory__c;
        }
        public cartItem(){}
    }

    public class opptyCloneWrapper{
        public Opportunity theOrder {get;set;}
        public list<OpportunityLineItem> lineItems {get;set;}
        public opptyCloneWrapper(Opportunity inorder, list<OpportunityLineItem> inlines){
            theOrder = inorder;
            lineItems = inlines;
        }
    }

    public class productWrapper {
        @AuraEnabled public OpportunityLineItem oli {get;set;}
        @AuraEnabled public Product2 product {get;set;}
        @AuraEnabled public PricebookEntry priceBookEntry {get;set;}
        @AuraEnabled public Decimal lineTotal {get;set;}
        @AuraEnabled public Integer qty {get;set;}
        @AuraEnabled public Boolean isSelect {get;set;}
        
    }

    public class AuthNetCredsSetting {
        public string tk {get;set;}
        public string api {get;set;}
    }

    public class orderCloneWrapper {
        public Opportunity order {get; set;}
        public List<productWrapper> lineItems {get; set;}
        public Contact contact {get; set;}
    }

    public class couponWebserviceWrapper{
        public String couponCode {get;set;}
        public Item[] products {get;set;}
        public String custEmail {get;set;}
    }

    public class createProductWrapper{
        public String id {get;set;}
        public String productCode {get;set;}
        public Fields fields {get;set;}
    }

    public class Fields {
        public String name {get;set;}
        public Decimal price {get;set;}
        public Boolean active {get;set;}
        public Boolean activeInCart {get;set;}

        public Boolean taxExempt {get; set;}
        public Boolean featured {get; set;}
        public Boolean onSale {get; set;}

    }

    public class leadWrapper{
        public String firstname {get;set;}
        public String lastname {get;set;}
        public String email {get;set;}
        public String refemail {get;set;}
        public String refphone {get;set;}
    }

    public class customerWrapper{
        public String firstname {get;set;}
        public String lastname {get;set;}
        public String email {get;set;}
        public String phone {get;set;}
        public String mailingstreet {get;set;}
        public String mailingcity {get;set;}
        public String mailingstate {get;set;}
        public String mailingpostalcode {get;set;}
        public creditCardWrapper creditcard {get;set;}
    }

    public class creditCardWrapper {
        public String Name{get;set;}
        public String billingFirstName{get;set;}
        public String billingLastName{get;set;}
        public String billingCountry{get;set;}
        public String billingStreet{get;set;}
        public String billingCity{get;set;}
        public String billingState{get;set;}
        public String billingZip{get;set;}

        public String CreditCardNumber{get;set;}
        public String CreditCardSecurityCode{get;set;}
        public String CreditCardExpMonth{get;set;}
        public String CreditCardExpYear{get;set;}
        public String CreditCardType{get;set;}
        public String NameOnCard{get;set;}
    }


    public class checkoutWrapper{
        public String billingFirstName{get;set;}
        public String billingLastName{get;set;}
        public String billingCompany{get;set;}
        public String billingCountry{get;set;}
        public String billingAddress{get;set;}
        public String billingAddress2{get;set;}
        public String billingCity{get;set;}
        public String billingState{get;set;}
        public String billingZip{get;set;}

        public String shippingFirstName{get;set;}
        public String shippingLastName{get;set;}
        public String shippingCompany{get;set;}
        public String shippingCountry{get;set;}
        public String shippingAddress{get;set;}
        public String shippingAddress2{get;set;}
        public String shippingCity{get;set;}
        public String shippingState{get;set;}
        public String shippingZip{get;set;}
        public String shippingPhone{get;set;}

        public boolean useShipToAddress{get;set;}

        public String email{get;set;}
        public String phone{get;set;}
        public String ordernotes{get;set;}

        public String PaymentType{get;set;}

        public String CreditCardNumber{get;set;}
        public String CreditCardSecurityCode{get;set;}
        public String CreditCardExpMonth{get;set;}
        public String CreditCardExpYear{get;set;}
        public String CreditCardType{get;set;}
        public String NameOnCard{get;set;}

        public String DeliveryDate{get;set;}

        public String couponCode{get;set;}

        public String returningEmail{get;set;}
        public String returningBillingZip{get;set;}
        public String returningSecurityCode{get;set;}
        public boolean isReturningCustomer{get;set;}
        public boolean isNewShippingAddress{get;set;}

        public id contactId{get;set;}

        public Decimal amount {get;set;}
        public Decimal taxRate {get;set;}
        public Decimal tax {get;set;}
        public String taxDescription {get; set;}
        public Decimal shippingTotal {get;set;}
        public Decimal orderTotal{get;set;}
        public Decimal couponDiscount {get;set;}
        public transactionWrapper authtransaction{get;set;}
        public Item[] products {get;set;}
    }

    public class transactionWrapper {
        public Decimal Amount{get;set;}
        public String AuthorizationCode{get;set;}
        public String TransactionId{get;set;}
        public String TransactionStatus{get;set;}
        public String TransactionTime{get;set;}
        public String TransactionTimezone{get;set;}
        public String TransactionType{get;set;}
        public String CreditCardNumber{get;set;}
        public String CreditCardSecurityCode{get;set;}
        public String CreditCardExp{get;set;}
        public String FirstNameOnCard{get;set;}
        public String LastNameOnCard{get;set;}
    }


}