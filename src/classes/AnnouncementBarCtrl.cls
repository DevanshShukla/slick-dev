public with sharing class AnnouncementBarCtrl {
    public Slick__AnnouncementBar__c ab {get;set;}

    public AnnouncementBarCtrl(){        
        ab = new Slick__AnnouncementBar__c();
        ab = [ SELECT Id, Slick__Active__c, Slick__Announcement__c, Slick__background_color__c, Slick__Font_color__c FROM Slick__AnnouncementBar__c WHERE Slick__Active__c = TRUE LIMIT 1 ];
    }
}