/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class cart_mainsiteTemplateControllerTest {

    static testMethod void testMainSiteTemplateController() {
        // TO DO: implement unit test
        test.startTest();
        product2 prod=new product2(name='test',Slick__active_in_cart__c=true,isactive=true);
        insert prod;
        PriceBookEntry pbe=new PriceBookEntry(product2Id=prod.id,PriceBook2id=Test.getStandardPriceBookId(),unitprice=1,isActive=true);
        insert pbe;


        cart_mainsiteTemplateController cart= new cart_mainsiteTemplateController();
        cart.getCartTotal();
        cart.getCartItems();
        cart.getCartEmailAddress();
        pageReference p=new pageReference('/testcookies');
        Test.setCurrentPage(p);
        util.saveCookie('cartCookie','[{"pid":"'+prod.id+'","qty":1}]',2592000,false);
        cart.getCartItems();
        cart.getCartTotal();
        cart.getSelectShopsMenu();
        cart.getHomePageCompareRecipies();
        cart.getHomePageIconLink1();
        cart.getHomePageIconLink2();
        cart.getHomePageIconLink3();
        cart.getHomePageSlide1();
        cart.getHomePageSlide2();
        cart.getHomePageSlide3();
        cart.getMainLogo();
        cart.getLiveChatLicense();
        cart.getHomePageLeftAd();
        cart.getHomePageRightAd();
        cart_mainsiteTemplateController.SelectShopsMenuRow menrow=new cart_mainsiteTemplateController.SelectShopsMenuRow();

        cart.dummy();

        test.stopTest();


    }
}