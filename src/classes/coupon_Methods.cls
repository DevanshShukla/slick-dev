public with sharing class coupon_Methods {

	private static string CustomerEmail;
	private static map<id,Product2> productMap;
	public static map<id, string> couponRecordTypes = getCouponRecordTypes();
	private static Contact customer;
	private static Opportunity firstOrder;
	private static map<id, Slick__coupon_user__c> couponMap;

	public class ApplyCouponResult{
		public boolean couponsApplied{get;set;}
		public decimal overallDiscount{get;set;}
		public String appliedCouponCodes{get;set;}
		public set<id> couponIds{get;set;}
		public list<CouponRow> coupons{get;set;}
		public list<String> messages{get;set;}

		public ApplyCouponResult(){
			this.overallDiscount=0;
			coupons=new list<CouponRow>();
			messages=new list<String>();
			couponIds=new set<id>();
		}
		
		public void postProcess(){
			this.generateAppliedCouponCodes();
			this.generateDiscountStrings();
			this.calculateOverallDiscount();
		}
		
		public void generateAppliedCouponCodes(){
			this.appliedCouponCodes='';
			for(CouponRow cRow: coupons)
				this.appliedCouponCodes+=cRow.Coupon.name+';';
			this.appliedCouponCodes=this.appliedCouponCodes.removeEnd(';');
		}
		
		public void generateDiscountStrings(){
			for(couponRow cRow:coupons)
				cRow.generateDiscountString();
		}
		
		public void calculateOverallDiscount(){
			this.overallDiscount=0;
			for(couponRow cRow:coupons){
				if(cRow.discount!=null)
					overallDiscount+=cRow.discount;
			}
		}

	}
	public class CouponRow{
		//Dollar discount amount for this one coupon related to order.
		public decimal discount{get;set;}
		//Coupon applied.
		public Slick__Coupon__c coupon{get;set;}
		//Discount visual representation
		public String discountString{get;set;}
		public CouponRow(Slick__Coupon__c coupon){
			this.coupon=coupon;
			this.discount=0;
		}
		public void generateDiscountString(){
			if(this.coupon!=null){
				if(coupon.Slick__Coupon_Type__c=='Entire Order' || coupon.Slick__Coupon_Type__c=='Product Family'|| coupon.Slick__Coupon_Type__c=='Product Group' || coupon.Slick__Coupon_Type__c=='Specific Product' && coupon.Slick__Discount_Amount__c!=null){
					
					if(coupon.Slick__Discount_Type__c=='Dollar Off') discountString='$'+coupon.Slick__Discount_Amount__c+' Off';
					
					else if(coupon.Slick__Discount_Type__c=='Percent Off') discountString=(coupon.Slick__Discount_Amount__c.setScale(0))+'% Off';

					if(coupon.Slick__Coupon_Type__c=='Product Family' && String.isnotblank(coupon.Slick__Product_Family__c)){
						String pTrim=coupon.Slick__Product_Family__c;
						if(String.isnotblank(pTrim) && pTrim.length()>40)
							pTrim=pTrim.subString(0,40)+'...';
						discountString+=' Product Family:'+pTrim;

					}
					if(coupon.Slick__Coupon_Type__c=='Product Group' && String.isnotblank(coupon.Slick__Product_Group__c)){
						String pTrim=coupon.Slick__Product_Group__c;
						if(String.isnotblank(pTrim) && pTrim.length()>40)
							pTrim=pTrim.subString(0,40)+'...';
						discountString+=' Product Group:'+pTrim;
					}
					if(coupon.Slick__Coupon_Type__c=='Specific Product' && coupon.Slick__Product__c!=null){
						String pTrim=coupon.Product__r.name;
						if(String.isnotblank(pTrim) && pTrim.length()>40)
							pTrim=pTrim.subString(0,40)+'...';
						discountString+=' Product:'+pTrim;
					}
				}
				else if(coupon.Slick__Coupon_Type__c=='BOGO' && coupon.Slick__Product__c!=null){
					String pTrim=coupon.Product__r.name;
						if(String.isnotblank(pTrim) && pTrim.length()>40)
							pTrim=pTrim.subString(0,40)+'...';
					discountString='BOGO '+pTrim+' $'+discount+' Off';
				}

				String couponRecordType=couponRecordTypes.get(coupon.recordtypeid);
				if(couponRecordType=='New Customer' || couponRecordType=='Referral Reward')
					discountString=couponRecordType+' '+discountString;
				if(String.isnotblank(discountString) &&  coupon.Slick__apply_Automatically__c==true)
					discountString='Automatic '+discountString;
				else if(string.isnotblank(discountString))
					discountString=coupon.name+' '+discountString;
			}

		}

	}


	public static void beforeUpsert (list<Slick__Coupon__c> newlist, list<Slick__Coupon__c> oldlist) {
		for (integer i=0; i<newlist.size(); i++) {
			if (newlist[i].Slick__Coupon_Type__c == 'BOGO') {
				newlist[i].Slick__Discount_Type__c = 'BOGO';
			}
		}
	}

	public static void afterInsert (list<Slick__Coupon__c> newlist){
		list<Slick__Coupon__c> updateList = new list<Slick__Coupon__c>();
		for (integer i=0; i < newlist.size(); i++){
			if (couponRecordTypes.get(newlist[i].recordtypeid) == 'Credit Coupon' || couponRecordTypes.get(newlist[i].recordtypeid) == 'Referral Reward'){
				Slick__Coupon__c coup = new Slick__Coupon__c(id=newlist[i].id);
				coup.name = newlist[i].name.replace('{auto#}','')+newlist[i].Slick__Auto_Number__c;
				updatelist.add(coup);
			}
		}
		if (updatelist.size() > 0) update updatelist;
	}
	//1/16/2015
	public static ApplyCouponResult couponDiscount (string couponCode, list<wrapperclasses.cartItem> cartContents, string custEmail){
		return couponDiscount(couponCode, cartContents, custEmail, null);
	}

	public static ApplyCouponResult couponDiscount (string couponcode, list<wrapperClasses.cartItem> cartContents, string custEmail, Id cid){
		ApplyCouponResult couponResult=new ApplyCouponResult();
		couponMap = new Map<Id, Slick__coupon_user__c>();
		system.debug('Coupon Methods: Code: '+couponcode+' , CustEmail: '+custemail+' , CartContents: '+cartContents + ' , customerId: ' + cid);
		customerEmail = custEmail;
		List<Contact> clist = new List<Contact>();
		if( cid != null ){
			clist = [select id, email, Slick__alternate_email__c,Slick__Alternate_Email_2__c, (select id, Slick__Times_Used__c, Slick__coupon__c from coupon_users__r)  from Contact where Id =: cid limit 1];
			//clist = [select id, email, Slick__alternate_email__c,Slick__Alternate_Email_2__c,Slick__Alternate_Email_3__c, (select id, Slick__Times_Used__c, Slick__coupon__c from coupon_users__r)  from Contact where Id =: cid limit 1];
		}else{
			//clist = [select id, email, Slick__alternate_email__c,Slick__Alternate_Email_2__c,Slick__Alternate_Email_3__c, (select id, Slick__Times_Used__c, Slick__coupon__c from coupon_users__r)  from Contact where email = :customerEmail OR Slick__alternate_email__c = :customerEmail OR Slick__Alternate_Email_2__c= :customerEmail];
			clist = [select id, email, Slick__alternate_email__c,Slick__Alternate_Email_2__c,(select id, Slick__Times_Used__c, Slick__coupon__c from coupon_users__r)  from Contact where email = :customerEmail OR Slick__alternate_email__c = :customerEmail OR Slick__Alternate_Email_2__c= :customerEmail limit 1];
		}
		if(cList.size() > 0){
			customer = clist[0];
			for(Slick__coupon_User__c c : customer.coupon_users__r){
				couponMap.put(c.Slick__coupon__c, c);
			}
			//List<Opportunity> colist = [select id, contact__r.email, contact__r.Slick__alternate_email__c from Opportunity where contact__r.email = :customerEmail OR contact__r.Slick__alternate_email__c = :customerEmail OR contact__r.Slick__Alternate_Email_2__c= :customerEmail OR contact__r.Slick__Alternate_Email_3__c=:customerEmail limit 1];
			List<Opportunity> colist = [select id, contact__r.email, contact__r.Slick__alternate_email__c from Opportunity where contact__r.email = :customerEmail OR contact__r.Slick__alternate_email__c = :customerEmail OR contact__r.Slick__Alternate_Email_2__c= :customerEmail limit 1];
			if(colist.size() > 0){
				firstOrder = coList[0];
			}
		}


		productMap=new map<id,Product2>();
		for (wrapperClasses.cartItem ci : cartContents) productMap.put(ci.pid,null);
		set<id> prodSet = productMap.keyset();

		list<id> prodIdList=new list<String>();
		prodIdList.addAll(prodSet);
		String productSetString='ID IN (';
		for(String prodId:prodIdList){
			productSetString+=+'\''+prodId+'\',';
		}

		productSetString=ProductSetString.RemoveEnd(',');
		productSetString+=')';

		if(prodIdList.size()==0)
			productMap=new map<id, Product2>();
		else
			productMap = new map<id, Product2>((list<Product2>)util.queryAllFields('Product2',productSetString));//'id IN :ProdSet'


		// run methods

		system.debug(productMap);

		Slick__Coupon__c coupon;
		decimal totaldiscount = 0;
		totaldiscount = autoapply(cartContents,CouponResult);
		system.debug('--after autoapply--');
		system.debug(totaldiscount);


		if (couponcode != null && couponcode.trim().length()>0 && couponcode != 'autoprocess'){
			list<Slick__Coupon__c> couponMatch = util.queryAllFields('Slick__Coupon__c',new list<String>{'Slick__Product__r.name'},'name = \''+couponcode+'\' limit 1');
			if (couponMatch.size()==0){
					CouponResult.Messages.add('Invalid Coupon Code.');
					//apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.info, 'Invalid Coupon Code.'));
				//return totaldiscount;
				return CouponResult;
			}
			else {
				coupon = couponmatch[0];
			}
			system.debug('coupon: '+coupon);
			if (couponcode != 'autoProcess' && totaldiscount > 0 && coupon.Slick__not_useable_with_auto_discount__c==true) {
				apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.info, 'This coupon cannot be used with the auto applied discount given.'));
			}else if (couponcode != 'autoProcess') {
				coupon = couponMatch[0];
				boolean couponApplied = false;

				// verify coupon applies
				if (couponApplicable(coupon, cartContents)) {
					totaldiscount += calculateDiscount(coupon, cartContents,CouponResult);
		    		couponApplied = true;
				}
				else {
		    		couponApplied = false;
		    	}
		    	CouponResult.CouponsApplied=couponApplied;
				if (couponApplied == false) {
					CouponResult.Messages.add('Coupon cannot be applied to this order.');
					//apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.info, 'Coupon cannot be applied to this order.'));
				}
			}
		}

		//couponResult.overallDiscount = totaldiscount;

		system.debug(couponResult.Messages);
		system.debug('------TEST HELLO-----');
		//return totalDiscount;

		return couponResult;
	}

	/*public static decimal couponDiscount (string couponcode, list<wrapperClasses.cartItem> cartContents, string custEmail){

		system.debug('Coupon Methods: Code: '+couponcode+' , CustEmail: '+custemail+' , CartContents: '+cartContents);
		customerEmail = custEmail;
		productMap=new map<id,Product2>();
		for (wrapperClasses.cartItem ci : cartContents) productMap.put(ci.pid,null);
		set<id> prodSet = productMap.keyset();

		list<id> prodIdList=new list<String>();
		prodIdList.addAll(prodSet);
		String productSetString='ID IN (';
		for(String prodId:prodIdList)
			productSetString+=+'\''+prodId+'\',';
		productSetString=ProductSetString.RemoveEnd(',');
		productSetString+=')';
		productMap = new map<id, Product2>((list<Product2>)util.queryAllFields('Product2',productSetString));//'id IN :ProdSet'

		// run methods
		Slick__Coupon__c coupon;
		decimal totaldiscount = 0;
		totaldiscount = autoapply(cartContents);

		if (couponcode != null && couponcode.trim().length()>0 && couponcode != 'autoprocess'){
			list<Slick__Coupon__c> couponMatch = util.queryAllFields('Slick__Coupon__c','name = \''+couponcode+'\' limit 1');
			if (couponMatch.size()==0){
					apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.info, 'Invalid Coupon Code.'));
				return totaldiscount;
			}
			else {
				coupon = couponmatch[0];
			}
			system.debug('coupon: '+coupon);
			if (couponcode != 'autoProcess' && totaldiscount > 0 && coupon.Slick__not_useable_with_auto_discount__c==true) {apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.info, 'This coupon cannot be used with the auto applied discount given.'));}
			else if (couponcode != 'autoProcess') {
				coupon = couponMatch[0];
				boolean couponApplied = false;

				// verify coupon applies
				if (couponApplicable(coupon, cartContents)) {
					totaldiscount += calculateDiscount(coupon, cartContents);
		    		couponApplied = true;
				}
				else {
		    		couponApplied = false;
		    	}

				if (couponApplied == false) {
					apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.info, 'Coupon cannot be applied to this order.'));
				}
			}
		}

		return totalDiscount;
	}*/

	public static boolean couponApplicable(Slick__Coupon__c coupon, list<wrapperClasses.cartItem> cartContents) {

		system.debug('--checking if coupon applicable--');
		system.debug('--coupon--');
		system.debug(coupon);
		system.debug('--cartContents--');
		system.debug(cartContents);


		set<string> groupSet = new set<string>();
		set<string> familySet = new set<string>();
		if (coupon.Slick__Product_Group__c != null){
			for (string s : string.valueof(coupon.get('Slick__product_group__c')).split(';')) {
				groupSet.add(s);
			}
		}
		if (coupon.Slick__Product_Family__c != null) {
			for (string s : string.valueof(coupon.get('Slick__product_family__c')).split(';')) {
				familySet.add(s);
			}
		}

		//specific product
		if (coupon.Slick__coupon_type__c == 'Specific Product') {
    		boolean productmatch = false;
    		integer quantityCount = 0;

    		for (wrapperClasses.cartItem ci : cartContents) {
    			if (coupon.Slick__Product__c != null && ci.pid == coupon.Slick__Product__c) {
    				productmatch = true;
    				quantityCount += ci.qty;
    			}
    		}

    			//specific quantity
    		if (coupon.Slick__Quantity_Required__c != null && coupon.Slick__Quantity_Required__c >1 && quantityCount >= coupon.Slick__Quantity_Required__c && productMatch && couponValid(coupon, cartContents) ) {return true;}
    		else if ((coupon.Slick__Quantity_Required__c == null || coupon.Slick__Quantity_Required__c <=1) && productMatch && couponValid(coupon, cartContents)) {
    			//any quantity
    			return true;
    		}
		}

		//if coupon_Type == 'Product group'
		else if (coupon.Slick__coupon_type__c == 'Product Group') {
			boolean groupMatch = false;
			integer quantityCount = 0;
			//product group logic

			for (wrapperClasses.cartItem ci : cartContents) {
    			if (groupSet.contains(productMap.get(ci.pid).Slick__group__c)) {
    				groupMatch = true;
    				quantityCount+= ci.qty;
    			}
    		}

			if (coupon.Slick__Quantity_Required__c != null && coupon.Slick__Quantity_Required__c >1 && quantityCount >= coupon.Slick__Quantity_Required__c && groupMatch && couponValid(coupon, cartContents)){return true;}
			else if ((coupon.Slick__Quantity_Required__c == null || coupon.Slick__Quantity_Required__c <=1) && groupMatch && couponValid(coupon, cartContents)) {return true;}
		}


		//if coupon_Type == 'Product family'
    	else if (coupon.Slick__coupon_type__c == 'Product Family') {
			boolean familyMatch = false;
			integer quantityCount = 0;
			//product family logic
			for (wrapperClasses.cartItem ci : cartContents) {

    			if (productMap.containskey(ci.pid) && familySet.contains(productMap.get(ci.pid).family)) {
    				familyMatch = true;
    				quantityCount+= ci.qty;
    			}
    		}

			if (coupon.Slick__Quantity_Required__c != null && coupon.Slick__Quantity_Required__c >1 && quantityCount >= coupon.Slick__Quantity_Required__c && familyMatch && couponValid(coupon, cartContents)) {
				return true;
			}
			else if ((coupon.Slick__Quantity_Required__c == null || coupon.Slick__Quantity_Required__c <=1) && familyMatch && couponValid(coupon, cartContents)) {
    			return true;
    		}
		}


		//if coupon_Type == 'Entire order'
		else if (coupon.Slick__coupon_type__c == 'Entire Order') {
			//logic = minimum met?
			//entire order logic
			if (couponValid(coupon, cartContents)) {
    			return true;
    		}
		}

		//if coupon_Type == 'BOGO'
		else if (coupon.Slick__coupon_type__c == 'BOGO') {
			//if both items in cart; and quantities agree, else find max paired count to apply discount
			//bogo logic
			boolean getOneMatch = false;
			boolean buyOneMatch = false;

			for (wrapperClasses.cartItem ci : cartContents) {
    			if (coupon.Slick__Free_Product__c != null && ci.pid == coupon.Slick__Free_Product__c) {
    				getOneMatch = true;
    			}
    			if (coupon.Slick__Product__c != null && ci.pid == coupon.Slick__product__c) {
    				buyOneMatch = true;
    			}
    		}

			if (buyOneMatch && getOneMatch && couponValid(coupon, cartContents)) {
    			return true;
    		}
		}

		//not applicable
		return false;
	}

	public static boolean couponValid (Slick__Coupon__c Coupon, list<wrapperclasses.cartItem> cartContents) {
		boolean couponValid = true;
		decimal cartsubtotal = 0;
		for (wrapperclasses.cartItem ci : cartContents){
			cartsubtotal += ci.litot;
		}

		if ((couponRecordTypes.get(coupon.recordtypeid) == 'Individual Coupon' || couponRecordTypes.get(coupon.recordtypeid) == 'Credit Coupon' || couponRecordTypes.get(coupon.recordtypeid) == 'Referral Reward')){
		   if( customerEmail != null && customerEmail.trim().length()>0){
				/*List<Contact> clist = new List<Contact>();
				if(customer != null){
					clist.add(customer);
				}
				else{
					clist = [select id, email, Slick__alternate_email__c,Slick__Alternate_Email_2__c,Slick__Alternate_Email_3__c, (select id, Slick__Times_Used__c from coupon_users__r where Slick__coupon__c = :coupon.id)  from Contact where email = :customerEmail OR Slick__alternate_email__c = :customerEmail OR Slick__Alternate_Email_2__c= :customerEmail OR Slick__Alternate_Email_3__c=:customerEmail limit 1];
				}*/

				if (customer != null) {
					//customer = clist[0];
					if (coupon.Slick__Customer__c != customer.id) {
						couponValid = false;
					}
					else if(customer.coupon_users__r.size()>0){
						//Slick__coupon_user__c cUser=customer.coupon_users__r.get(0);
						Slick__coupon_user__c cUser = couponMap.get(coupon.Id);
						/*if(cUser == null){
							couponValid=false;
						}*/
						if(cUser != null && coupon.Slick__Max_Individual_Uses__c!=null && coupon.Slick__max_Individual_uses__c>0
							&& cUser.Slick__times_used__c!=null && cUser.Slick__times_used__c>0
						    && cUser.Slick__times_used__c>=coupon.Slick__Max_Individual_Uses__c)
								couponValid=false;
					}
					/*else if(coupon.Slick__Max_Individual_Uses__c != null && coupon.Slick__Max_Individual_uses__c > 0
						&& coupon.Slick__times_used__c != null && coupon.Slick__times_used__c > 0 && coupon.Slick__times_used__c >= coupon.Slick__Max_Individual_uses__c){
						couponValid = false;
					}*/

				}
				else{
					couponValid = false;
				}
			}
			//1/15/2015
			else
				couponValid=false;
		}
		if (couponRecordTypes.get(coupon.recordtypeid) == 'New Customer'){
		    if(customerEmail != null && customerEmail.trim().length()>0 && customerEmail.trim().toLowerCase().startsWith('none@ns')==false) {

		    	List<Opportunity> colist = new List<Opportunity>();
		    	if(firstOrder != null){
		    		colist.add(firstOrder);
		    	}

				if (colist.size() > 0) couponValid = false;
			}
			else
				couponValid=false;
		}


		//not available yet
		if (coupon.Slick__Start_Date__c != null && system.today() < coupon.Slick__start_date__c) couponValid = false;

		//expired
	    if (coupon.Slick__expiration_date__c != null && system.today() > coupon.Slick__expiration_date__c) couponValid = false;

	    //max uses exceeded
	    if ((coupon.Slick__Max_Uses__c != null && coupon.Slick__Max_Uses__c > 0 ) && (coupon.Slick__Times_Used__c != null  && coupon.Slick__Times_Used__c >= coupon.Slick__Max_Uses__c)) couponValid = false;

		//minimum order amount not met
		if ((coupon.Slick__Min_Order_Amnt_for_Target__c != null && coupon.Slick__Min_Order_Amnt_for_Target__c > 0) && coupon.Slick__Min_Order_Amnt_for_Target__c > cartsubtotal) couponValid = false;

		return couponValid;
	}

	public static decimal calculateDiscount(Slick__Coupon__c coupon, list<wrapperclasses.cartItem> cartContents,ApplyCouponResult couponResult){
		decimal couponDiscount = 0;
		decimal cartsubtotal = 0;
		for (wrapperClasses.cartItem ci : cartContents){
			cartsubtotal += ci.litot;
		}

		set<string> groupSet = new set<string>();
		set<string> familySet = new set<string>();
		if (coupon.Slick__Product_Group__c != null){
			for (string s : string.valueof(coupon.get('Slick__product_group__c')).split(';')) {
				groupSet.add(s);
			}
		}
		if (coupon.Slick__Product_Family__c != null) {
			for (string s : string.valueof(coupon.get('Slick__Product_Family__c')).split(';')) {
				familySet.add(s);
			}
		}

		if (coupon.Slick__discount_type__c == 'Dollar Off') {
			couponDiscount = coupon.Slick__Discount_Amount__c;
		}
		else if (coupon.Slick__discount_type__c == 'Percent Off') {
			if (coupon.Slick__Apply_to_Entire_Order__c == true) couponDiscount = (cartsubtotal*(coupon.Slick__Discount_Amount__c/100));
			else {
				for (wrapperclasses.cartItem ci : cartContents) {
					if (groupSet.contains(productMap.get(ci.pid).Slick__group__c) || familySet.contains(productMap.get(ci.pid).family)){
						couponDiscount += ci.litot*(coupon.Slick__Discount_Amount__c/100);
					}
				}
			}
		}
		else if (coupon.Slick__discount_type__c == 'BOGO') {
			for (wrapperClasses.cartItem ci : cartContents) {
    			if (coupon.Slick__Free_Product__c != null && ci.pid == coupon.Slick__Free_Product__c) {
					couponDiscount = (ci.saleprice!=null)?ci.saleprice:ci.price;
    			}
    		}
		}
		//else {
		//	return 0;
		//}

		/****MOVE THIS TIMES USED BLOCK TO AFTER ORDER PLACED.****/

		try {
			//coupon.Slick__Times_Used__c = coupon.Slick__Times_Used__c+1;
			//update coupon;

			/*if (couponRecordTypes.get(coupon.recordtypeid) == 'New Customer') {
				list<Slick__Coupon_User__c> cuList = [select id, Slick__times_used__c from Slick__Coupon_User__c where Slick__coupon__c = :coupon.id AND Slick__customer__c = :customer.id limit 1];
				Slick__Coupon_User__c cu;
				if (cuList.size()==0){
					cu = new Slick__Coupon_User__c();
					cu.Slick__customer__c = customer.id;
					cu.Slick__coupon__c = coupon.id;
				}
				else {
					cu = culist[0];
				}
				cu.Slick__times_used__c = (cu.Slick__times_used__c!=null)?cu.Slick__times_used__c+1:1;
				upsert cu;
			}*/
			if(!couponResult.couponIds.contains(coupon.id)){
				CouponRow cRow=new CouponRow(coupon);
				cRow.Discount=couponDiscount;
				couponResult.coupons.add(cRow);
				couponResult.couponIds.add(coupon.id);
			}

			return couponDiscount;
		}
		catch(exception e) {
			apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.info, 'There was an error applying this coupon.'));
			system.debug('TRACE: ERROR ON UPDATE COUPON TIMES USED: '+coupon.name+' - '+e.getmessage());
			return 0;
		}
	}

	public static decimal calculateDiscount(Slick__Coupon__c coupon, list<wrapperclasses.cartItem> cartContents){
		decimal couponDiscount = 0;
		decimal cartsubtotal = 0;
		for (wrapperClasses.cartItem ci : cartContents){
			cartsubtotal += ci.litot;
		}

		set<string> groupSet = new set<string>();
		set<string> familySet = new set<string>();
		if (coupon.Slick__Product_Group__c != null){
			for (string s : string.valueof(coupon.get('Slick__product_group__c')).split(';')) {
				groupSet.add(s);
			}
		}
		if (coupon.Slick__Product_Family__c != null) {
			for (string s : string.valueof(coupon.get('Slick__Product_Family__c')).split(';')) {
				familySet.add(s);
			}
		}

		if (coupon.Slick__discount_type__c == 'Dollar Off') {
			couponDiscount = coupon.Slick__Discount_Amount__c;
		}
		else if (coupon.Slick__discount_type__c == 'Percent Off') {
			if (coupon.Slick__Apply_to_Entire_Order__c == true) couponDiscount = (cartsubtotal*(coupon.Slick__Discount_Amount__c/100));
			else {
				for (wrapperclasses.cartItem ci : cartContents) {
					if (groupSet.contains(productMap.get(ci.pid).Slick__group__c) || familySet.contains(productMap.get(ci.pid).family)){
						couponDiscount += ci.litot*(coupon.Slick__Discount_Amount__c/100);
					}
				}
			}
		}
		else if (coupon.Slick__discount_type__c == 'BOGO') {
			for (wrapperClasses.cartItem ci : cartContents) {
    			if (coupon.Slick__Free_Product__c != null && ci.pid == coupon.Slick__Free_Product__c) {
					couponDiscount = (ci.saleprice!=null)?ci.saleprice:ci.price;
    			}
    		}
		}
		else {
			return 0;
		}

		/****MOVE THIS TIMES USED BLOCK TO AFTER ORDER PLACED.****/

		try {
			//coupon.Slick__Times_Used__c = coupon.Slick__Times_Used__c+1;
			//update coupon;

			/*if (couponRecordTypes.get(coupon.recordtypeid) == 'New Customer') {
				list<Slick__Coupon_User__c> cuList = [select id, Slick__times_used__c from Slick__Coupon_User__c where Slick__coupon__c = :coupon.id AND Slick__customer__c = :customer.id limit 1];
				Slick__Coupon_User__c cu;
				if (cuList.size()==0){
					cu = new Slick__Coupon_User__c();
					cu.Slick__customer__c = customer.id;
					cu.Slick__coupon__c = coupon.id;
				}
				else {
					cu = culist[0];
				}
				cu.Slick__times_used__c = (cu.Slick__times_used__c!=null)?cu.Slick__times_used__c+1:1;
				upsert cu;
			}*/
			return couponDiscount;
		}
		catch(exception e) {
			apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.info, 'There was an error applying this coupon.'));
			system.debug('TRACE: ERROR ON UPDATE COUPON TIMES USED: '+coupon.name+' - '+e.getmessage());
			return 0;
		}
	}
	public static void updateCouponUsage(String appliedCouponCodes,Id customerId){
		String couponClause;
		//; Delimited Coupon codes
		if(String.isnotblank(appliedCouponCodes)){
			couponClause='Name IN (';
			for(String couponCode:appliedCouponCodes.split(';'))
				couponClause+='\''+couponCode+'\',';
			couponClause=couponClause.RemoveEnd(',');
			couponClause+=')';
		}
		else
			return;

		list<Slick__Coupon__c> couponMatch = util.queryAllFields('Slick__Coupon__c',couponClause);
		if(couponMatch.size()>0){
			try{
				list<Slick__coupon__c> couponUpdates=new list<Slick__coupon__c>();
				set<id> newCouponIds=new set<id>();
				list<Slick__Coupon_User__c> couponUserUpserts=new list<Slick__Coupon_User__c>();
				for(Slick__coupon__c coupon:couponMatch){
					//Slick__coupon__c coupon=couponMatch.get(0);
					coupon.Slick__Times_Used__c = coupon.Slick__Times_Used__c+1;
					couponUpdates.add(coupon);
					//update coupon;

					String couponRecordType=couponRecordTypes.get(coupon.recordtypeid);
					if (couponRecordType== 'New Customer' || couponRecordType=='Individual Coupon') {
						newCouponIds.add(coupon.id);
					}
				}

				//Look for coupon user records related to this contact, if the couponcode was used before by this contact

				list<Slick__Coupon_User__c> cuList = [select id, Slick__times_used__c,Slick__coupon__c from Slick__Coupon_User__c where Slick__coupon__c in:newCouponIds AND Slick__customer__c = :customerid and Slick__customer__c!=null limit 1];
				set<id> couponsUsedByCustomer=new set<id>();
				for(Slick__Coupon_User__c cu:cuList)
					couponsUsedByCustomer.add(cu.Slick__coupon__c);

				//set of coupons that have never been used by user
				newCouponIds.removeAll(couponsUsedByCustomer);

				for(Slick__coupon_User__c cu:cuList){
					cu.Slick__times_used__c = (cu.Slick__times_used__c!=null)?cu.Slick__times_used__c+1:1;
					couponUserUpserts.add(cu);
				}
				for(id couponId: newCouponIds){
					Slick__Coupon_user__c cu=new Slick__Coupon_user__c(Slick__coupon__c=couponId,Slick__customer__c=customerId,Slick__times_used__c=1);
					couponUserUpserts.add(cu);
				}
					//Slick__Coupon_User__c cu;
					//if (cuList.size()==0){
					//	cu = new Slick__Coupon_User__c();
					//	cu.Slick__customer__c = customer.id;
					//	cu.Slick__coupon__c = coupon.id;
					//}
					//else {
					//	cu = culist[0];
					//}
					//cu.Slick__times_used__c = (cu.Slick__times_used__c!=null)?cu.Slick__times_used__c+1:1;
				update couponUpdates;
				upsert couponUserUpserts;
					//return couponDiscount;
			}
			catch(exception e) {
				apexpages.addmessage(new ApexPages.Message(ApexPages.Severity.info, 'There was an error applying updating this coupon.'));
				system.debug('TRACE: ERROR ON UPDATE COUPON TIMES USED: '+appliedCouponCodes+' - '+e.getmessage());
				ErrorLogger.logError(e,'TRACE: ERROR ON UPDATE COUPON TIMES USED: '+appliedCouponCodes+ ' '+CouponMatch);
				//return 0;
			}
		}
	}

	public static decimal autoApply (list<wrapperClasses.cartItem> cartContents,ApplyCouponResult couponResult){
		//Doesnt add applied coupon codes to the order, just the discount.
		decimal autodiscount = 0;

		list<Slick__Coupon__c> autoCoupons = util.queryAllFields('Slick__Coupon__c',new list<String>{'Slick__product__r.name'},'Slick__apply_Automatically__c = true order by Slick__discount_type__c, Slick__discount_amount__c desc');

		for (Slick__Coupon__c c : autoCoupons) {
			decimal tempdiscount = 0;
			if (couponApplicable (c, cartContents)) {
				tempdiscount = calculateDiscount(c, cartContents,couponResult);

				if (tempdiscount > autodiscount){
					autodiscount = tempdiscount;
				}
			}
		}

		return autodiscount;
	}

	/*public static decimal autoApply (list<wrapperClasses.cartItem> cartContents){
		//Doesnt add applied coupon codes to the order, just the discount.
		decimal autodiscount = 0;

		list<Slick__Coupon__c> autoCoupons = util.queryAllFields('Slick__Coupon__c','Slick__apply_Automatically__c = true order by Slick__discount_type__c, Slick__discount_amount__c desc');

		for (Slick__Coupon__c c : autoCoupons) {
			decimal tempdiscount = 0;
			if (couponApplicable (c, cartContents)) {
				tempdiscount = calculateDiscount(c, cartContents);

				if (tempdiscount > autodiscount){autodiscount = tempdiscount;}
			}
		}

		return autodiscount;
	}*/

	public static map<id, string> getCouponRecordTypes(){
		map<id, string> output = new map<id, string>();
		for (RecordType r : [select id, name from RecordType where sobjectType ='Slick__Coupon__c']){
			output.put(r.id,r.name);
		}
		return output;
	}
	
	public void demo(){
	    Decimal i = 0;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    i++;
	    
	    
	}
}