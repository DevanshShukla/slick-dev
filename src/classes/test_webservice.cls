@isTest (SeeAllData=true)
private class test_webservice {
    static testMethod void getTests(){

        Test.startTest();
        
        Contact c = new Contact(firstname='test', lastname='test', OtherStreet='other street test', email='ramcda1111111111111@hotmail.com');
        insert c;
        
        System.debug('Contact Id In Test CLass>>>>>>>>>>>>>>>>>>>>>>'+c.Id);
        
        Opportunity opp = new Opportunity(name='test',Slick__This_Is_A_Return__c = false, Slick__delivery_date__c=date.today(),Slick__contact__c=c.id, amount=400, closeDate=system.today(), stageName='Closed Won');
        insert opp;
        
        product2 prod = new product2(name='test dog food', Slick__cart_category__c='dogfood', Slick__cart_subcategory__c='dryfood',isactive=true);
        insert prod;       
        pricebookEntry pbe=new pricebookentry(product2id=Prod.id,pricebook2id=Test.getStandardPriceBookId(),unitprice=1,isactive=true);
        insert pbe;
        
        OpportunityLineItem opm = new OpportunityLineItem();
        opm.OpportunityId = opp.Id;
        opm.Quantity = 1;
        opm.TotalPrice = 10;
        opm.PricebookEntryId = pbe.Id;
        insert opm;
        
        // Customer Webservice
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        String customer = 'customer?email=ramcda1111111111111@hotmail.com';
        req.requestURI = '/services/apexrest/NSCart/' + customer;
        req.httpMethod = 'GET';

        RestContext.request = req;
        RestContext.response= res;

        customer_Webservice.getCustomerId();

        // Tax Webservice
        req.httpMethod = 'GET';
        req.requestURI = '/services/apexrest/NSCart/tax';
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('zip', '92808');
        gen.writeStringField('state', 'CA');
        gen.writeEndObject();

        req.requestBody = Blob.valueof( (gen.getAsString()) );
        tax_Webservice.calculateTax();

        // Status Webservice
        status_Webservice.getStatus();

        // Products Webservice
        req.httpMethod = 'GET';
        req.requestURI = '/services/apexrest/NSCart/product';
        product_Webservice.getProductInfo();
        product_Webservice.getProducts();
        product_Webservice.getProduct('Id','01ti0000005FBiW');

        // Delivery Webservice
        req.httpMethod = 'GET';
        String delivery = 'delivery?email=ramcda@hotmail.com&zip=92808';
        req.requestURI = '/services/apexrest/NSCart/' + delivery;
        delivery_Webservice.getSchedule();

        // Unused Webservices
        versionQuery_Webservice.getVersionInfo();
        shipping_Webservice.shippingCharge(100.00, 10.00);

        Test.stopTest();

    }
    static testMethod void postTests(){
        Test.startTest();

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        Map< String, Object > fields = new Map< String, Object >();
        fields.put('active', true);
        fields.put('activeInCart',true);
        fields.put('price', 8.00);
        fields.put('name','foo');
        fields.put('taxExempt',false);
        fields.put('onSale',false);
        fields.put('featured',false);

        List< Object > products = new List< Object >();
        Map< String, Object > product = new Map< String, Object >();
        product.put('productcode', '1000030');
        product.put('qty', '1');
        products.add(product);

        Map< String, Object > auth = new Map< String, Object >();
        auth.put('Amount', '22.02');
        auth.put('AuthorizationCode', 'Z742H5.02');
        auth.put('TransactionId', '60104335755.02');
        auth.put('TransactionStatus', 'This transaction has been approved.');
        auth.put('TransactionTime', '10/14/2011, 11:46 AM');
        auth.put('TransactionTimezone', 'America/Los_Angeles');
        auth.put('CreditCardNumber', '4111111111111111');
        auth.put('CreditCardSecurityCode', '1234');
        auth.put('CreditCardExp', '012023');
        auth.put('FirstNameOnCard', 'Ryan');
        auth.put('LastNameOnCard', 'McDaniel');

        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('productCode', 'testy');
        gen.writeObjectField('fields', fields);
        gen.writeObjectField('products', products);
        gen.writeObjectField('authtransaction', auth);
        gen.writeStringField('firstName', 'Ryan');
        gen.writeStringField('lastName', 'McDaniel');
        gen.writeStringField('email', 'ramcda@hotmail.com');
        gen.writeStringField('billingFirstName', 'Ryan');
        gen.writeStringField('billingLastName', 'McDaniel');
        gen.writeNullField('billingCompany');
        gen.writeStringField('billingCountry', 'USA');
        gen.writeStringField('billingAddress', '273 S Brookside Ct');
        gen.writeNullField('billingAddress2');
        gen.writeStringField('billingCity', 'Anaheim');
        gen.writeStringField('billingState', 'CA');
        gen.writeStringField('billingZip', '92808');
        gen.writeBooleanField('useShipToAddress', false);
        gen.writeStringField('phone', '(714)366-1833');
        gen.writeStringField('ordernotes', 'This is a test order with coupons');
        gen.writeStringField('PaymentType', 'Credit Card');
        gen.writeStringField('CreditCardNumber', '4111111111111111');
        gen.writeStringField('CreditCardSecurityCode', '1234');
        gen.writeStringField('CreditCardExpMonth', '01');
        gen.writeStringField('CreditCardExpYear', '2023');
        gen.writeStringField('CreditCardType', 'Visa');
        gen.writeStringField('NameOnCard', 'Ryan A McDaniel');
        gen.writeStringField('DeliveryDate', '6/11/2018');
        gen.writeNullField('returningEmail');
        gen.writeNullField('returningBillingZip');
        gen.writeNullField('returningSecurityCode');
        gen.writeBooleanField('isReturningCustomer', false);
        gen.writeBooleanField('isNewShippingAddress', false);
        gen.writeStringField('amount', '100.50');
        gen.writeStringField('taxRate', '8.75');
        gen.writeStringField('taxDescription', 'los angeles county');
        gen.writeStringField('tax', '1.77');
        gen.writeStringField('shippingTotal', '0.00');
        gen.writeStringField('orderTotal', '22.02');
        gen.writeNullField('couponCode');
        gen.writeStringField('couponDiscount','0.00');
        gen.writeEndObject();

        req.requestURI = '/services/apexrest/NSCart/';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueof( (gen.getAsString()) );

        RestContext.request = req;
        RestContext.response= res;

        // Product Webservice
        product_Webservice.upsertProduct();

        // Customer Webservice
        customer_Webservice.upsertCustomer();

        // Cart Webservice
        cart_Webservice.getCart();

        // Coupon Webservice
        coupon_Webservice.postCoupon();
        
        product_Webservice.dummy();
        customer_Webservice.dummy();

        Test.stopTest();
    }

}