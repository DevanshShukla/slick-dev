@isTest
public class MockHttpResponse implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req){
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
		
        return res;
    }
}