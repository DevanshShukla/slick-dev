public class UserLoginControl {
    

    // For ForgotPassword Page
    @AuraEnabled
    public static string resetmail(string email){
        list<Slick__Site_User__c> con = new list<Slick__Site_User__c>();
        System.debug(email);
        con = [SELECT Id,Slick__UserName__c,Slick__Contact__c FROM Slick__Site_User__c WHERE Slick__UserName__c =: email];
        System.debug(con);
        if(con.isEmpty()){
            string val = 'false';
            return val;
        }else{
            for(Slick__Site_User__c c : con){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[] {c.Slick__UserName__c}; 
                    mail.setToAddresses(toAddresses);
                //mail.setTargetObjectId(c.Slick__Contact__c);
                mail.setSenderDisplayName('Dharmik Shah');
                mail.setSubject('Reset Password');
                mail.setPlainTextBody('Hello, <br/><br/> Please reset your password from click on below url <br/><br/> Click here');
                mail.setHtmlBody('Hello, <br/><br/> Please reset your password from click on below url <br/><br/> <a href=https://slickdevelopment-dev-ed.lightning.force.com/Slick/ForgotPasswordConfirmApp.app?id='+c.Id+'>click here.</a>');
                // Send the email you have created.
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                string suc= 'true';
                return suc;
            }
            return null;
        }
        
    }
    // For ForgotPasswordConfirm Page
    @AuraEnabled
    public static string reset(string password,string urlid){
        Slick__Site_User__c pass = new Slick__Site_User__c();
        list<Slick__Site_User__c> var = new list<Slick__Site_User__c>();
        var = [SELECT Id,Slick__Password__c FROM Slick__Site_User__c WHERE Id=: urlid ];
        System.debug(var[0].Slick__Password__c);
        if(var.size() == 0){
            return 'false';
        }
        if(var.size() > 0){
            if(var[0].Slick__Password__c == password){
                return 'same';
            }else{
                pass.Id = urlid;
                pass.Slick__Password__c = password;
                update pass;
                return 'true';
            }
            
        }
        return null;
    }
    
    // For Signup page
    @AuraEnabled
    public static String register(string fn,string ln,string em,string pw){
        
        
        List<Site_User__c> siteUserList = [SELECT Id,Slick__UserName__c FROM Site_User__c WHERE Slick__UserName__c =: em];
        List<Contact> conList = [SELECT Id,Email FROM Contact WHERE Email =: em];
        System.debug(siteUserList);
        System.debug(conlist);
        if(conList.size()>0 && siteUserList.size() == 0){
            Site_User__c stUser = new Site_User__c();
            stUser.Slick__UserName__c	= em;
            stUser.Slick__Password__c  = pw;
            stuser.Slick__Contact__c   = conList[0].Id;
            insert stUser;
            return 'true';
        } else if(siteUserList.size() > 0 && conList.size() > 0)       {
            // Email is already registered

            return 'false';
        }else if(siteUserList.size() == 0 && conList.size() == 0){
            Contact con = new Contact();
            con.LastName = ln;
            con.FirstName = fn;
            con.Email = em;
            insert con;
            
            
            Site_User__c stUser = new Site_User__c();
            stUser.Slick__UserName__c	= em;
            stUser.Slick__Password__c  = pw;
            stuser.Slick__Contact__c   = con.Id;
            insert stUser;
            return 'true1';
        }
        return null;
    }




    // For Login Page
    public Static string LoginController(string username,string password) {
            boolean authenticated;
            List<Slick__Site_User__c> siteUsr = [Select Id, Slick__UserName__c, Slick__Contact__r.Slick__uid__c, Slick__Password__c from Slick__Site_User__c Where Slick__UserName__c =: username LIMIT 1];
                if(!siteUsr.isEmpty() && password.equals(siteUsr[0].Slick__Password__c)){
                    Pagereference pr = new Pagereference('/apex/LoginPage');
                    Cookie cook;
                    if(siteUsr[0].Slick__Contact__r.Slick__uid__c == null){
                        cook = new Cookie('valueLogin', siteUsr[0].Id, null, 1800, false);
                    }else{
                        cook = new Cookie('valueLogin', siteUsr[0].Slick__Contact__r.Slick__uid__c, null, 1800, false);   
                    }
                    pr.setCookies(new Cookie[] {cook});
                    authenticated = true;
                    pr.setRedirect(false);
                    return 'pr';
                }else if(siteUsr.size() == 0){
                
                    return 'false';
                }
                return null;
        }
        
        public Pagereference doLogout(){
            boolean authenticated;
            Pagereference pr = new Pagereference('/apex/LoginPage');
            Cookie cook = new Cookie('valueLogin', 'true', null, 0, false);
            pr.setCookies(new Cookie[] {cook});
            authenticated = false;
            pr.setRedirect(true);
            return pr;
        }
        
        public static string showChangePassword(){
            return 'passchange';
        }
        
        public string doInit(){
            boolean authenticated;
            String data = '';
            Cookie counter = ApexPages.currentPage().getCookies().get('valueLogin');
            if(counter != null){
                data = apexpages.currentPage().getCookies().get('valueLogin').getValue();
            }
            Authenticated = data != '' ? true : false;
            
            String currentPageStatus = ApexPages.currentPage().getParameters().get('passwordUpdate');
            
            if(currentPageStatus == 'true'){
                showChangePassword();
            }
            return null;
        }


}