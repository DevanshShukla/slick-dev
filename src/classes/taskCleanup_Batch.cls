/**
 * taskCleanup_Batch Class
 * @author: M. Wicherski, MK Partners 11/2012
 * @Description: Clears out open tasks by setting status to food ordered
 */

/*
	To Run:
			taskCleanup_Batch job = new taskCleanup_Batch();
			Id processId = Database.executeBatch(job);
	
	To Check Status:
			AsyncApexJob j = [select ApexClassId, CompletedDate, CreatedById, CreatedDate, 
								Id, JobItemsProcessed, JobType, MethodName, NumberOfErrors, Status, 
								TotalJobItems from AsyncApexJob where Id =:processId];
	
	Info about this Class
			ApexClass ac = [select ApiVersion, Body, BodyCrc, CreatedById, CreatedDate, Id, 
								IsValid, LastModifiedById, LastModifiedDate, LengthWithoutComments, Name, 
								NamespacePrefix, Status, SystemModstamp from ApexClass where Id = :j.ApexClassId];
*/
global with sharing class taskCleanup_Batch implements Database.Batchable<SObject>, Database.AllowsCallouts {
	// Queries to be used
	public static string getQueryString(){
		String x = 'Select id, Slick__Last_Delivery_Date__c From contact c where Slick__Last_Delivery_Date__c = TODAY OR Slick__Last_Delivery_Date__c = YESTERDAY';
		system.debug('QUERY STRING IS: ' +x);
		return x;
	}

	public static final String DATA_SET_QUERY = getQueryString();

	// Query to fetch the data required by the job
	private String jquery; 

	global taskCleanup_Batch() {
		this(DATA_SET_QUERY);
	}

	public class BatchApexJobException extends Exception {}
	public class InvalidObjectInstanceException extends Exception {}

	global taskCleanup_Batch(String query) {
		try {
	    	// If no query set, throw exception
	    	if (query == null) {
	    		throw new BatchApexJobException('Query string is null.');
	    	}
	    	jquery = query;
	    	system.debug('QUERY RESULT: '+Database.query(jquery));
	    	
	    	//Check if the job is running already
	        ApexClass ac = [select Name from ApexClass where Name = 'taskCleanup_Batch' and NamespacePrefix='Slick'];
	        
	        List<AsyncApexJob> aajs = [select Status from AsyncApexJob where ApexClassId = :ac.Id and 
	        							JobType = 'batchApex' and Status in ('Queued', 'Processing')];
	        if (!aajs.isEmpty()) {
	        	throw new BatchApexJobException(aajs.size() + ' taskCleanup_Batch job(s) already running or processing.');
			}
    	}
    	catch (Exception e) {
            throw e;
        }
	}

    //Method required by the framework to start the job execution.
    global Database.QueryLocator start(Database.BatchableContext ctx) {
        Database.QueryLocator qLocator = null;
		try {
            qLocator = Database.getQueryLocator(jquery);
        }
        catch (Exception e) {
            throw e;
        }
        return qLocator;
    }

	public static void exec (list<Contact> customers) {
		try {
			set<id> contactIds = new set<id>();
			for (Contact c : customers){
				contactIds.add(c.id);
			}
			
			list<Task> openTasks = [select id from Task where whoid IN :contactIds AND  subject = 'ROP F/Up' AND status != 'Ordered Food']; 
			for (Task t: openTasks){
				t.status = 'Ordered Food';
			}
			
			if (openTasks.size() > 0){
				database.update(openTasks);
			}
		}
		catch (Exception e) {
			throw e; 
		}
	}

    //Method required by the framework for the actual job execution. 
	global void execute(Database.BatchableContext ctx, List<SObject> sobjects) {
		list<Contact> thisList = toCollection(sobjects);
		exec(thislist);
	}

	//Method required and invoked by the framework when a batch job is done.
	global void finish(Database.BatchableContext ctx) {

	}
	
	//Method to convert list of SObjects to a list of opportunities
	public List<Contact> toCollection(SObject[] sobjects) {
		List<Contact> thisList = new List<Contact>();
		for (SObject obj : sobjects) {
			if (obj instanceof Contact) {
				thisList.add((Contact) obj);
			} else {
				throw new InvalidObjectInstanceException('Invalid object instance in cursor.');
			}
		}
		return thisList;
    }
}