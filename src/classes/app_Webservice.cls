@RestResource(urlMapping='/app_Webservice/*')
global with sharing class app_Webservice {
    static List<Organization> ORG = [SELECT Id, Phone FROM Organization LIMIT 1];

/******* NEW METHODS ******
    @HttpPost
    global static void Response(){
        RestRequest req = RestContext.request;
        RestContext.response.addHeader('Content-Type', 'application/json');

    }

    public class request_Resource {
        public String operation {get;set;}
        public String error {get;set;}
    }
    public class customer_Resource {
        public String id {get;set;}
        public String firstName {get;set;}
        public String lastName {get;set;}
        public String phone {get;set;}
    }
    public class order_Resource {
        public String customerId {get;set;}
    }
    public class line_Resource {
        public String id {get;set;}
        public String name {get;set;}
        public Integer qty {get;set;}
        public Decimal price {get;set;}
        public Decimal litotal {get;set;}
        public Decimal tax {get;set;}
        public String imageurl {get;set;}
        public String discount {get;set;}
    }
*******/



    public virtual class AppWebService_Response{
        String status{get;set;}
        String stackTrace{get;set;}
        String phone {get;set;}
    }
    public class AppWebservice_Register extends AppWebService_Response{
        String firstname{get;set;}
        String lastname{get;set;}
    }
    public class AppWebservice_Order extends AppWebService_Response{
        String lodate{get;set;}
        String lodel{get;set;}
        String cc4{get;set;}
        decimal amt{get;set;}
        String oid{get;set;}
        List<AppWebservice_Line> lines{get;set;}
        List<AppWebservice_Line> upsell{get;set;}
    }
    public class AppWebservice_Line{
        String name{get;set;}
        integer qty{get;set;}
        decimal litotal{get;set;}
        decimal price{get;set;}
        decimal tax{get;set;}
        String id{get;set;}
        String imageurl {get;set;}
    }

    @HttpGet
    global static void response(){
        RestRequest req = RestContext.request;
        Map<String,String> params = req.params;
        system.debug( JSON.serialize(params) );
        system.debug( req.requestURI );
        system.debug( JSON.serialize(req.headers) );
        if ( req.requestBody != null ){
            system.debug( JSON.serialize(req.requestBody.toString()) );
        }
        string action;
        string regkey;

        action = req.params.get('act');
        regkey = req.params.get('r');
        if (regkey !=null) regkey = regkey.toLowerCase();

        // RestContext.response.addHeader('Content-Type', 'application/json');

        //string responsevalue = fetchResponseValue(action,regkey);
        String responsevalue='';
        if(action=='NSInstances'){
            System.debug('got inside here');
            responseValue=fetchInstanceList();
        } else {
            AppWebService_Response responseObject;
            try{
                if ( ORG.size() > 0 && responseObject != null ){
                    responseObject.phone = ORG[0].Phone;
                }
                if(action=='reg'){
                    responseObject=fetchRegistrationResponse(regkey);
                } else
                if(action=='od') {
                    responseObject=fetchLatestOrderDetail(regkey);
                } else
                if(action=='reprep') {
                    responseObject=fetchReorderPrepResponse(regkey);
                } else
                if(action=='reorder'){
                    // reorderId is the last order's id
                    // pricebook entry + quantity
                    String reorderId = RestContext.request.params.get('oid');
                    Map<String,String> paramMap = RestContext.request.params;
                    responseObject=fetchReorderResponse(reorderId, paramMap);
                }
                responsevalue = JSON.serialize(responseObject);
            } catch (exception e) {
                responseObject = new AppWebService_Response();
                responseObject.status='Error Encountered';
                responseObject.stackTrace = e.getLineNumber()+' '+e.getMessage()+' '+e.getStackTraceString().replace('\n','');
                responsevalue = JSON.serialize(responseObject);
                system.debug(e.getmessage());
                System.debug(e.getStackTraceString());
            }
        }
        System.debug(responsevalue);
        RestContext.response.responseBody = Blob.valueOf(responsevalue);
        RestContext.response.statusCode = 200;
        // RestContext.response.addHeader('Access-Control-Allow-Origin',req.headers.get('Origin'));

        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.addHeader('Access-Control-Allow-Origin', '*');

        // below was for development
        // RestContext.response.addHeader('Access-Control-Allow-Origin', 'http://0.0.0.0:8100');
        // RestContext.response.addHeader('Access-Control-Allow-Credentials','true');
        RestContext.response.addHeader('Access-Control-Allow-Methods','GET, POST, DELETE, PUT');
        RestContext.response.addHeader('Status', 'HTTP/1.1 200 OK');
    }
    public static string fetchInstanceList(){
        Map<string,string> instanceList = new Map<string,string>();
        instancelist.put('Tennessee','https://nstennessee.secure.force.com/');
        instancelist.put('South Colorado','https://nssouthcolorado.secure.force.com/');
        instancelist.put('South Texas','https://nssanantonio.secure.force.com/');
        instancelist.put('Southeast Louisiana','https://nsselouisiana.secure.force.com/');
        instancelist.put('San Diego','https://nssandiego.secure.force.com/');
        instancelist.put('Richmond','https://nsrichmond.secure.force.com/');
        instancelist.put('Orange County','https://nsorangecounty.secure.force.com/');
        instancelist.put('Northeast Ohio','https://nsneohio.secure.force.com/');
        instancelist.put('North Arizona','https://nsnortharizona.secure.force.com/');
        instancelist.put('Norcal','https://nsnorcal.secure.force.com/');
        instancelist.put('Kansas City','https://nskansascity.secure.force.com/');
        instancelist.put('Inland Empire','https://nsinlandvalley.secure.force.com/');
        instancelist.put('Inland Valley','https://naturesselectinlandvalley.secure.force.com/');
        instancelist.put('Houston','https://nshouston.secure.force.com/');
        instancelist.put('Fort Worth','https://txpetfood.secure.force.com/');
        instancelist.put('DC Metro','https://nsdcmetro.secure.force.com/');
        instancelist.put('Dayton','https://nsdayton.secure.force.com/');
        instancelist.put('Dallas','https://nstxdallas.secure.force.com/');
        instancelist.put('Chicago','https://nschicago.secure.force.com/');
        instancelist.put('Carolinas','https://nscarolinas.secure.force.com/');
        instancelist.put('Atlanta','https://nsatlanta.secure.force.com/');
        instancelist.put('Arklatex','https://nsarklatex.secure.force.com/');
        instancelist.put('testuser','https://nsnewcartdev-developer-edition.na46.force.com/');
        // instancelist.put('Dev Instance','https://nsnewcartdev-developer-edition.na46.force.com/');
        system.debug(instancelist);
        return JSON.serialize(instanceList);
    }
    public static AppWebService_Response fetchRegistrationResponse(string regkey){
        // query for regkey
        AppWebservice_Register register=new AppWebservice_Register();
        if (regkey != null){
            //list<Contact> clist = [select id, firstname, lastname from Contact where Slick__Nature_s_Mobile_App_Registration_Key__c = :regkey limit 1];
             list<Contact> clist = [select id, firstname, lastname from Contact limit 1];
             if (!clist.isEmpty()){
                Contact c = clist[0];
                register.firstname=c.firstname;
                register.lastname=c.lastname;
                register.status='ok';
             }
             else register.status='Not Found';
        }
        else {
            register.status='Not Found';
        }
        //responsevalue=register.serialize();
        return register;
    }

    public static AppWebService_Response fetchLatestOrderDetail(string regkey){
        System.debug('regkey>>>>>>>>>>>>>>>>'+regkey);
        // last order details
        AppWebservice_Order orderResponse=new AppWebservice_Order();
        if (regkey != null){

            list<Opportunity> olist = [
                SELECT id, Slick__order_date__c, Slick__delivery_date__c, Slick__charge_amount__c,credit_Card_used__r.name,
                    (
                        SELECT id, pricebookentry.product2.Slick__full_size_image__c, quantity, unitprice, totalprice, pricebookentry.name
                        FROM OpportunityLineItems
                    )
                FROM Opportunity
                WHERE  Slick__order_date__c != null
                AND Slick__credit_card_Used__c!=null
                ORDER BY createdDate desc
                LIMIT 1
            ];


            // list<Opportunity> olist = [
            //     SELECT id, Slick__order_date__c, Slick__delivery_date__c, Slick__charge_amount__c,credit_Card_used__r.name,
            //         (
            //             SELECT id, pricebookentry.product2.Slick__full_size_image__c, quantity, unitprice, totalprice, pricebookentry.name
            //             FROM OpportunityLineItems
            //         )
            //     FROM Opportunity
            //     WHERE contact__r.Slick__Nature_s_Mobile_App_Registration_Key__c = :regkey
            //     AND Slick__order_date__c != null
            //     AND Slick__credit_card_Used__c!=null
            //     ORDER BY createdDate desc
            //     LIMIT 1
            // ];
            //System.debug('olist>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'+[SELECT id, contact__r.Slick__Nature_s_Mobile_App_Registration_Key__c, Slick__order_date__c, Slick__credit_card_Used__c FROM Opportunity]);
            System.debug('olist>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'+[SELECT id, Slick__order_date__c, Slick__credit_card_Used__c FROM Opportunity]);
            if (!olist.isEmpty()){
                Opportunity o = olist[0];
                orderResponse.lodate=o.Slick__order_date__c.format();
                orderResponse.lodel=o.Slick__delivery_date__c.format();
                orderResponse.cc4=o.credit_card_used__r.name;
                orderResponse.amt=o.Slick__charge_Amount__c.setScale(2);
                orderResponse.status='ok';
                orderResponse.lines=new list<AppWebservice_Line>();

                for (OpportunityLineItem oli : o.OpportunityLineItems){
                    AppWebservice_Line responseLine=new AppWebservice_Line();
                    responseLine.name=oli.pricebookentry.name;
                    responseLine.qty=Integer.valueof(oli.quantity);
                    responseLine.litotal=oli.totalprice;
                    responseLine.price=oli.unitprice;
                    responseLine.id =oli.pricebookentry.Id;
                    responseLine.imageurl = oli.pricebookentry.product2.Slick__full_size_image__c;
                    orderResponse.lines.add(responseLine);
                }
            } else {
                orderResponse.status='No recent orders found';
            }
        } else {
            orderResponse.status='Invalid Registration Key';
        }

        return orderResponse;
    }

    public static AppWebService_Response fetchReorderPrepResponse(string regkey){
        
        System.debug('fetchReorderPrepResponse regkey>>>>>>>>>>>>>>>>>>'+regkey);
        
        AppWebservice_Order orderResponse=new AppWebservice_Order();
         // reorder prep; returns last order products with new prices and upsell options
        if ( String.isNotBlank(regkey) ){
            
         /*   list<Opportunity> olist = [
                SELECT id, Slick__order_date__c, Slick__delivery_date__c, Slick__charge_amount__c, Slick__Shipping_Zipcode__c,Slick__Shipping_State__c, Slick__subtotal_with_tax__c,Credit_card_used__r.name,
                    (
                        SELECT id, quantity, unitprice, totalprice, pricebookentry.name, pricebookentry.product2id
                        FROM OpportunityLineItems
                    )
                FROM Opportunity
                WHERE contact__r.Slick__Nature_s_Mobile_App_Registration_Key__c = :regkey
                AND Slick__order_date__c != null
                AND Slick__credit_card_used__c!=null
                ORDER BY createdDate desc limit 1
            ];

         */   list<Opportunity> olist = [
                SELECT id, Slick__order_date__c, Slick__delivery_date__c, Slick__charge_amount__c, Slick__Shipping_Zipcode__c,Slick__Shipping_State__c, Slick__subtotal_with_tax__c,Credit_card_used__r.name,
                    (
                        SELECT id, quantity, unitprice, totalprice, pricebookentry.name, pricebookentry.product2id
                        FROM OpportunityLineItems
                    )
                FROM Opportunity
                WHERE Slick__order_date__c != null
                AND Slick__credit_card_used__c!=null
                ORDER BY createdDate desc limit 1
            ];
            Map<id,decimal> productPriceMap = new Map<id, decimal>();
            if (!olist.isEmpty()){
                Opportunity o = olist[0];
                for ( OpportunityLineItem oli : o.OpportunityLineItems ){
                    productPriceMap.put(oli.pricebookentry.product2id, null);
                }
                for (Product2 thisProduct : [
                    SELECT id, Slick__on_sale__c,
                        (
                            SELECT id, pricebook2.name, pricebook2.isstandard, unitprice, product2id
                            FROM PricebookEntries
                        )
                    FROM Product2
                    WHERE IsActive = true
                    AND (id IN :productPriceMap.keyset() OR Slick__Mobile_Featured_Upsell__c = true)
                ]){
                    for ( PricebookEntry pbe : thisProduct.pricebookentries ){
                        if (pbe.pricebook2.isstandard) productPriceMap.put(thisProduct.id,pbe.unitprice);
                        else if (thisProduct.Slick__on_Sale__c && pbe.pricebook2.name == 'On Sale') productPriceMap.put(thisProduct.id,pbe.unitprice);
                    }
                }
                
                if(!Test.isRunningTest()) wrapperClasses.opptyCloneWrapper owrap = reOrder_Methods.reconstructOrder(o.id, false);

                opportunity_Extension reOrder = new Opportunity_Extension();
                reorder.constructReorder(o.id);

                system.debug(productpricemap);

                orderResponse.oid = olist[0].id;
                orderResponse.cc4 = olist[0].credit_card_used__r.name;
                orderResponse.amt = reorder.oppty.Slick__Charge_amount__c.setScale(2);
                orderResponse.status = 'ok';
                orderResponse.lines = new list<AppWebservice_Line>();

                Decimal taxrate = opportunity_Methods.getTaxByZipthenState( o.Slick__Shipping_Zipcode__c,o.Slick__Shipping_state__c ).Slick__Tax_Percentage__c/100;
                for( wrapperClasses.productWrapper pw:reorder.opptyProducts ){
                    AppWebservice_Line responseLine = new AppWebservice_Line();
                    responseLine.name = pw.pricebookentry.name;
                    responseLine.qty = Integer.valueof(pw.oli.quantity);
                    responseLine.price = pw.oli.unitprice;
                    responseLine.litotal = responseLine.qty*responseLine.price;
                    responseLine.tax = ( pw.product.Slick__tax_exempt__c == false && taxrate != null ) ? (responseLine.price*taxrate) : 0;
                    responseLine.id = pw.pricebookentry.id;
                    responseLine.imageurl = pw.oli.priceBookEntry.product2.Slick__full_size_image__c;

                    orderResponse.lines.add(responseLine);
                }
                orderResponse.upsell=new list<AppWebservice_Line>();

                //tax rate is wrong.. need to apply on taxable items only.. and by state.

                for (Product2 p : reorder_Methods.query3UpsellProducts()){
                    AppWebservice_Line responseLine=new AppWebservice_Line();
                    responseLine.name = p.name;
                    responseLine.price = productpricemap.get(p.id);
                    responseLine.tax=(
                        ( p.Slick__tax_exempt__c==false && responseLine.price != null && taxrate != null ) ? (responseLine.price*taxrate) : 0
                    );
                    responseLine.id = p.PricebookEntries[0].Id;
                    responseLine.imageurl = p.Slick__full_size_image__c;
                    orderResponse.upsell.add(responseLine);
                }
            } else {
                orderResponse.status='Not Found';
            }
        } else {
            orderResponse.Status = 'Invalid Registration Key';
        }
        return orderResponse;
    }

    public static AppWebservice_Response fetchReorderResponse(String reOrderId, Map<String,String> paramMap){
        AppWebservice_Response reorderResponse=new AppWebservice_Response();
        System.debug('this is the reorder id inside fetchReorderResponse');
        System.debug(reOrderId);
        System.debug('this is paramMap inside fetchReorderResponse');
        System.debug(paramMap);
        // oid + last order Id
        // pricebook entry id + quantity

        Map<Id,integer> productMap = new Map<Id,integer>();
        for ( String p : paramMap.keySet() ){
            if ( p instanceof id && String.isNotBlank(paramMap.get(p)) && paramMap.get(p).isNumeric() ){
                productMap.put( p, Integer.valueOf(paramMap.get(p)) );
            }
        }
        opportunity_Extension reOrder = new opportunity_Extension();

        //Determine whether to Authorize or Charge depending on custom setting
        Slick__Instance_Setting__c authorizeOnly_Setting =
            (test.isRunningTest())
            ?(new Slick__Instance_Setting__c(name='Authorize Only Mobile App', Slick__text_value__c = 'false'))
            :((Slick__Instance_Setting__c.getInstance('Authorize Only Mobile App')));
        Boolean authorizeOnly=false;
        if( authorizeOnly_Setting!=null && String.isnotblank(authorizeOnly_Setting.Slick__text_value__c) ){
            authorizeOnly=boolean.valueof(authorizeOnly_setting.Slick__text_value__c);
        }
        reorder.constructReorder(reorderId);
        reorder.oppty.Slick__Order_Placed_Method__c = 'Mobile App';
        system.debug(productMap);
        
        if ( productMap.size() > 0 ){
            reOrder.opptyProducts = new List<wrapperClasses.productWrapper>();
            for ( PricebookEntry pbe : [
                SELECT Id, Name, Pricebook2Id, Product2Id, UnitPrice, IsActive, UseStandardPrice, ProductCode, Product2.Name, Product2.IsActive,
                    Product2.Description, Product2.Family, Product2.Slick__Family_Code__c, Product2.Slick__Family_Code2__c, Product2.Slick__Group__c, Product2.Slick__Tax_Exempt__c
                FROM PricebookEntry
                WHERE Product2.IsActive = true AND Id IN :productMap.keySet()
                ORDER BY Product2Id
            ]){
                wrapperClasses.productWrapper pw = opportunity_Methods.toProductWrapper(pbe);
                if ( productMap.containsKey(pw.priceBookEntry.Id) ){
                    pw.oli.Quantity = productMap.get(pw.priceBookEntry.Id);
                }
                reOrder.opptyProducts.add(pw);
            }
        }
        system.debug(reOrder.opptyProducts);
        if( authorizeOnly == TRUE ){
            reOrder.submitAndAuthorize();
        } else {
            reOrder.submitAndCharge();
        }
        /*
        if ( additionalProducts1 != null && string.isblank(additionalProducts1) && additionalProducts2!=null && string.isblank(additionalProducts2) && additionalProducts3!=null && string.isblank(additionalProducts3) ){
            //reorder_Methods.reconstructOrder(reorderid, true);
            if( authorizeOnly == TRUE ){
                reOrder.submitAndAuthorize();
            } else {
                reOrder.submitAndCharge();
            }
        } else {
            map<id,integer> upsellProductMap=new map<id,integer>();
            if (!string.isblank(additionalProducts1) && !string.isblank(additionalProducts1qty)) upsellProductmap.put(additionalProducts1,Integer.valueof(additionalProducts1qty));
            if (!string.isblank(additionalProducts2) && !string.isblank(additionalProducts2qty)) upsellProductmap.put(additionalProducts2,Integer.valueof(additionalProducts2qty));
            if (!string.isblank(additionalProducts3) && !string.isblank(additionalProducts3qty)) upsellProductmap.put(additionalProducts3,Integer.valueof(additionalProducts3qty));
            reOrder.addProducts(upsellProductMap);
            // recalculate totals
            // insert order
            if( authorizeOnly==true ){
                reOrder.submitAndAuthorize();
            } else{
                reOrder.submitAndCharge();
            }
        }
        */
        if( reOrder.hasError==true ){
            reorderResponse.status = 'Error Encountered '+ reOrder.errorMessage;
        } else {
            reorderResponse.status = 'Reorder Placed';
        }
        return reorderResponse;
    }

    private static string escapeDoubleQuotes(String str){
        if(str!=null)
            return str.replace('"','\\"');
        return str;
    }

    public static void addUpsellProduct(wrapperClasses.opptyCloneWrapper wrap, string productid, string prodqty){
        OpportunityLineItem oli = new OpportunityLineItem();

        oli.Quantity = double.valueof(prodqty);

        Product2 tempProd = [
            SELECT id, Slick__on_sale__c,
                (
                    SELECT id, pricebook2.name, pricebook2.isstandard, unitprice
                    FROM pricebookentries
                    WHERE isactive=true
                    AND pricebook2.isactive=true
                )
            FROM Product2
            WHERE id = :productid
        ];

        for (PricebookEntry pbe : tempProd.pricebookentries){
            if (pbe.pricebook2.isstandard) {
                oli.pricebookentryid = pbe.id;
                oli.unitprice = pbe.unitprice;
            }
            else if (tempProd.Slick__on_Sale__c && pbe.pricebook2.name == 'On Sale'){
                oli.pricebookentryid = pbe.id;
                oli.unitprice = pbe.unitprice;
                break;
            }
        }

        wrap.lineitems.add(oli);
    }

    //public static string fetchResponseValue(string action, string regkey){
    //  string responsevalue;
    //      try{
    //      if (action=='NSInstances'){
    //          map<string,string> instanceList = new map<string,string>();
    //          /*instancelist.put('Chicago','https://nschicago.force.com/');
    //          instancelist.put('Tampa Bay','https://nstampabay.force.com/');
    //          instancelist.put('Central Coast','https://nscentralcoast.force.com/');
    //          instancelist.put('South Colorado','https://nssouthcolorado.force.com/');
    //          instancelist.put('Carolinas','https://nscarolinas.force.com/');
    //          instancelist.put('Orange County','https://nsorangecounty.force.com/');
    //          instancelist.put('Norcal','https://nsnorcal.force.com/');
    //          instancelist.put('San Antonio','https://nssanantonio.force.com/');
    //          instancelist.put('SE Louisiana','https://nsselouisiana.force.com/');
    //          instancelist.put('Dev Instance','https://nsnewcartdev-developer-edition.na15.force.com/');*/

    //          instancelist.put('Chicago','https://nschicago.secure.force.com/');
    //          instancelist.put('Portland','https://nsportland.secure.force.com/');
    //          instancelist.put('Central Coast','https://nscentralcoast.secure.force.com/');
    //          instancelist.put('Orange County','https://nsorangecounty.secure.force.com/');
    //          instancelist.put('Dayton','https://nsdayton.secure.force.com/');
    //          instancelist.put('Louisiana','https://nsselouisiana.secure.force.com/');
    //          instancelist.put('Inland Empire - Shelly','https://nsinlandvalley.secure.force.com/');
    //          instancelist.put('Dallas','https://nstxdallas.secure.force.com/');
    //          instancelist.put('Northeast Ohio','https://nsneohio.secure.force.com/');
    //          instancelist.put('Arklatex','https://nsarklatex.secure.force.com/');
    //          instancelist.put('San Antonio','https://nssanantonio.secure.force.com/');
    //          instancelist.put('South Colorado','https://nssouthcolorado.secure.force.com/');
    //          instancelist.put('Norcal','https://nsnorcal.secure.force.com/');
    //          instancelist.put('Carolinas','https://nscarolinas.secure.force.com/');
    //          instancelist.put('Tennessee','https://nstennessee.secure.force.com/');
    //          instancelist.put('Dev Instance','https://nsnewcartdev-developer-edition.na15.force.com/');
    //          system.debug(instancelist);

    //          responseValue = JSON.serialize(instanceList);
    //      }
    //      else if(action=='reg'){
    //          // query for regkey
    //          if (regkey != null){
    //               list<Contact> clist = [select id, firstname, lastname from Contact where Slick__Nature_s_Mobile_App_Registration_Key__c = :regkey limit 1];
    //               if (!clist.isEmpty()){
    //                  Contact c = clist[0];
    //                  responseValue = '{"firstname":"'+c.firstname+'","lastname":"'+c.lastname+'","status":"ok"}';
    //               }
    //               else responseValue = '{"status":"Not Found"}';
    //          }
    //          else {
    //              responseValue = '{"status":"Not Found"}';
    //          }
    //      }
    //      else if (action=='od'){ // last order details
    //          if (regkey != null){
    //              list<Opportunity> olist = [select id, Slick__order_date__c, Slick__delivery_date__c, Slick__charge_amount__c, (select id, quantity, unitprice, totalprice, pricebookentry.name from OpportunityLineItems) from Opportunity where contact__r.Slick__Nature_s_Mobile_App_Registration_Key__c = :regkey and Slick__order_date__c != null order by createdDate desc limit 1];
    //              if (!olist.isEmpty()){
    //                  Opportunity o = olist[0];
    //                  responseValue = '{"lodate":"'+o.Slick__order_date__c.format()+'","lodel":"'+o.Slick__delivery_date__c.format()+'","cc4":"6545","amt":"'+o.Slick__charge_amount__c.setScale(2)+'","status":"ok", "lines":[';
    //                      for (OpportunityLineItem oli : o.OpportunityLineItems){
    //                          responseValue += '{"name":"'+escapeDoubleQuotes(oli.pricebookentry.name)+'","qty":'+oli.quantity+',"litotal":'+oli.totalprice+',"price":'+oli.unitprice+'},';
    //                      }
    //                      if (responseValue.substring(responseValue.length()-1,responseValue.length())==',') responseValue = responseValue.substring(0,responseValue.length()-1);
    //                  responseValue += ']}';
    //              }
    //              else responseValue = '{"status":"Not Found"}';
    //          }
    //          else responseValue = '{"status":"Not Found"}';
    //      }
    //      else if (action=='reprep'){ // reorder prep; returns last order products with new prices and upsell options
    //          if (regkey != null){
    //              list<Opportunity> olist = [select id, Slick__order_date__c, Slick__delivery_date__c, Slick__charge_amount__c, Slick__Shipping_Zipcode__c, Slick__subtotal_with_tax__c, (select id, quantity, unitprice, totalprice, pricebookentry.name, pricebookentry.product2id from OpportunityLineItems) from Opportunity where contact__r.Slick__Nature_s_Mobile_App_Registration_Key__c = :regkey and Slick__order_date__c != null order by createdDate desc limit 1];
    //              map<id,decimal> productPriceMap = new map<id, decimal>();
    //              if (!olist.isEmpty()){
    //                  Opportunity o = olist[0];
    //                  for (OpportunityLineItem oli : o.OpportunityLineItems){
    //                      productPriceMap.put(oli.pricebookentry.product2id, null);
    //                  }

    //                  for (Product2 thisProduct : [select id, Slick__on_sale__c, (select id, pricebook2.name, pricebook2.isstandard, unitprice, product2id from PricebookEntries) from Product2 where isactive = true AND (id IN :productPriceMap.keyset() OR Slick__Mobile_Featured_Upsell__c = true)]){
    //                      for (PricebookEntry pbe : thisProduct.pricebookentries){
    //                          if (pbe.pricebook2.isstandard) productPriceMap.put(thisProduct.id,pbe.unitprice);
    //                          else if (thisProduct.Slick__on_Sale__c && pbe.pricebook2.name == 'On Sale') productPriceMap.put(thisProduct.id,pbe.unitprice);
    //                      }
    //                  }

    //                  wrapperClasses.opptyCloneWrapper owrap = reOrder_Methods.reconstructOrder(o.id, false);

    //                  system.debug(productpricemap);
    //                  map<id,PricebookEntry> pbemap = new map<id,PricebookEntry>();
    //                  for (OpportunityLineItem oli : owrap.lineitems){
    //                      pbemap.put(oli.pricebookentryid,null);
    //                  }

    //                  pbemap = new map<id,PricebookEntry>([select id, name, product2id from PricebookEntry where id IN :pbemap.keyset()]);

    //                  responseValue = '{"oid":"'+olist[0].id+'","cc4":"6545","amt":"'+owrap.theorder.Slick__charge_amount__c.setScale(2)+'","status":"ok", "lines":[';
    //                  for (OpportunityLineItem oli : owrap.lineitems){
    //                      responseValue += '{"name":"'+escapeDoubleQuotes(pbemap.get(oli.pricebookentryid).name)+'","qty":'+oli.quantity+',"litotal":'+(oli.quantity*productpricemap.get(pbemap.get(oli.pricebookentryid).product2id))+',"price":'+productpricemap.get(pbemap.get(oli.pricebookentryid).product2id)+'},';
    //                  }
    //                  if (responseValue.substring(responseValue.length()-1,responseValue.length())==',') responseValue = responseValue.substring(0,responseValue.length()-1);
    //                  responseValue += '], "upsell":[';

    //                  decimal taxrate = opportunity_Methods.getTaxZip(o.Slick__Shipping_Zipcode__c).Slick__Tax_Percentage__c/100;

    //                  for (Product2 p : reorder_Methods.query3UpsellProducts()){
    //                      responseValue += '{"name":"'+escapeDoubleQuotes(p.name)+'","price":'+productPriceMap.get(p.id)+',"tax":'+((p.Slick__taxable__c)?(productPriceMap.get(p.id)*taxrate):0)+',"id":"'+p.id+'"},';
    //                  }
    //                  if (responseValue.substring(responseValue.length()-1,responseValue.length())==',') responseValue = responseValue.substring(0,responseValue.length()-1);
    //                  responseValue += ']}';
    //              }
    //              else responseValue = '{"status":"Not Found"}';
    //          }
    //          else responseValue = '{"status":"Not Found"}';
    //      }
    //      else if (action=='reorder'){

    //          string reorderId = RestContext.request.params.get('oid');

    //          string additionalProducts1 = RestContext.request.params.get('up1');
    //          string additionalProducts2 = RestContext.request.params.get('up2');
    //          string additionalProducts3 = RestContext.request.params.get('up3');

    //          string additionalProducts1qty = RestContext.request.params.get('up1q');
    //          string additionalProducts2qty = RestContext.request.params.get('up2q');
    //          string additionalProducts3qty = RestContext.request.params.get('up3q');
    //          opportunity_Extension reOrder=new Opportunity_Extension();

    //          Slick__Instance_Setting__c authorizeOnly_Setting=(test.isRunningTest())?(new Slick__Instance_Setting__c(name='Authorize Only Mobile App', Slick__text_value__c = 'false')):
    //          ((Slick__Instance_Setting__c.getInstance('Authorize Only Mobile App')));
    //          boolean authorizeOnly=false;
    //          if(authorizeOnly_Setting!=null && String.isnotblank(authorizeOnly_Setting.Slick__text_value__c))
    //              authorizeOnly=boolean.valueof(authorizeOnly_setting.Slick__text_value__c);

    //          reorder.constructReorder(reorderId);
    //          reorder.oppty.Slick__Order_Placed_Method__c='Mobile App';

    //          if (additionalProducts1!=null && string.isblank(additionalProducts1) && additionalProducts2!=null && string.isblank(additionalProducts2) && additionalProducts3!=null && string.isblank(additionalProducts3) ){
    //              //reorder_Methods.reconstructOrder(reorderid, true);
    //              if(authorizeOnly==true)
    //                  reOrder.submitAndAuthorize();
    //              else
    //                  reOrder.submitAndCharge();
    //          }
    //          else {
    //              //wrapperClasses.opptyCloneWrapper newOrderWrap = reorder_Methods.reconstructOrder(reorderid, false);

    //              // add in upsell products
    //              /*
    //              if (!string.isblank(additionalProducts1)) addUpsellProduct(newOrderWrap, additionalProducts1, additionalProducts1qty);
    //              if (!string.isblank(additionalProducts2)) addUpsellProduct(newOrderWrap, additionalProducts2, additionalProducts2qty);
    //              if (!string.isblank(additionalProducts3)) addUpsellProduct(newOrderWrap, additionalProducts3, additionalProducts3qty);
    //              */
    //              map<id,integer> upsellProductMap=new map<id,integer>();
    //              if (!string.isblank(additionalProducts1) && !string.isblank(additionalProducts1qty)) upsellProductmap.put(additionalProducts1,Integer.valueof(additionalProducts1qty));
    //              if (!string.isblank(additionalProducts2) && !string.isblank(additionalProducts2qty)) upsellProductmap.put(additionalProducts2,Integer.valueof(additionalProducts2qty));
    //              if (!string.isblank(additionalProducts3) && !string.isblank(additionalProducts3qty)) upsellProductmap.put(additionalProducts3,Integer.valueof(additionalProducts3qty));
    //              reOrder.addProducts(upsellProductMap);


    //              // recalculate totals

    //              //newOrderWrap = reorder_Methods.calculateTotals(newOrderWrap);

    //              // insert order
    //              if(authorizeOnly==true)
    //                  reOrder.submitAndAuthorize();
    //              else
    //                  reOrder.submitAndCharge();

    //              //insert newOrderWrap.theOrder;
    //              //for (OpportunityLineItem oli : newOrderWrap.lineItems){
    //              //  oli.opportunityid = newOrderWrap.theOrder.id;
    //              //}
    //              //insert newOrderWrap.lineITems;
    //          }
    //          if(reOrder.hasError==true)
    //              responsevalue = '{"status":"Error Encountered"}';
    //          else responsevalue = '{"status":"Reorder Placed"}';
    //      }
    //  }
    //  catch(exception e){
    //      responsevalue = '{"status":"Error Encountered",';
    //      responseValue+='"stackTrace":"'+e.getStackTraceString()+'"}';
    //      system.debug(e.getmessage());
    //      System.debug(e.getStackTraceString());
    //  }
    //  system.debug(responsevalue);
    //  return responseValue;
    //}
}