public with sharing class ErrorLogger {


	public static Slick__Logger__c LogError(String error, string body){
		Slick__Logger__c log=new Slick__Logger__c(Slick__error__c=error,Slick__Body__c=body);
     	insert log; 
     	return log; 	 
	}
	public static Slick__Logger__c LogError(Exception e){
		return logError(e.getTypeName(),'Message:'+ e.getMessage()+' Stack Trace: '+e.getStackTraceString());   	
	}
	public static Slick__Logger__c LogError(Exception e,String additionalBody){
		return logError(e.getTypeName(),'Message:'+ e.getMessage()+' Stack Trace: '+e.getStackTraceString()+' Additional: '+additionalBody);   	
	}
}