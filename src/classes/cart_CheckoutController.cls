public without sharing class cart_CheckoutController {

	public list<wrapperClasses.cartItem> cartContents{get;set;}
	public decimal cart_Subtotal{get;set;}
	public decimal cart_Tax{get;set;}
	public decimal cart_TaxRate{get;set;}
	public decimal cart_Coupon_Discount{get;set;}

	public String errorMessage{get;set;}
	public boolean hasError{get;set;}
	public boolean orderComplete{get;set;}
	public string verifiedEmail{get;set;}
	public boolean hasValidCCNumLen{get;set;}
	public boolean hasValidSecCode{get;set;}

	public wrapperClasses.checkoutWrapper checkoutData{get;set;}

	public string orderNumber {get;set;}
	public boolean couponApplied {get;set;}

	public Contact currentContact;
	public Id returningContactId{get;set;}
	public id selectedReturningCCId{get;set;}
	Slick__credit_card__c returningCreditCard{get;set;}

	public boolean checkPaymentEnabled{get;private set;}

	public coupon_Methods.ApplyCouponResult couponResult{get;set;}

	private Slick__sales_tax__c salesTaxRecord{get;set;}

	public string authorizeNetSealId {
		get{
			if(authorizeNetSealId==null)
				authorizeNetSealId=cart_mainSiteTemplateController.getauthorizeNetSeal();
				return authorizeNetSealId;
			}
		set;
	}
	private boolean authorizeOnly {
		get{
			if(authorizeOnly==null){
				Slick__Instance_Setting__c authorizeOnly_Setting = (test.isRunningTest())?(new Slick__Instance_Setting__c(name='Authorize Only Front End', Slick__text_value__c = 'false')):((Slick__Instance_Setting__c.getInstance('Authorize Only Front End')));
				if(authorizeOnly_Setting!=null && string.isnotblank(authorizeOnly_Setting.Slick__text_value__c)){
					authorizeOnly=boolean.valueof(authorizeOnly_Setting.Slick__text_value__c);
				} else {
					authorizeOnly=false;
				}
			}
			return authorizeOnly;
		}
		set;
	}

	public decimal cart_OrderTotal{
		get{
			return cart_SubTotal+cart_tax+cart_ShippingTotal-cart_Coupon_Discount;
		}
		set;
	}

	public decimal cart_ShippingTotal{get;set;}
	public decimal cart_TaxableSubtotal{get;set;}

	public string shippingOrDelivery{
		get{
			if(String.isblank(shippingOrDelivery))
				shippingOrDelivery=opportunity_methods.shippingOrDelivery;
			return shippingOrDelivery;
		}
		set;}

	public String shippingDeliveryMergeField{
		get{
			String s = 'Choose Shipping Date Options – require email and zip be filled in above';
			for(Slick__Merge_Item__c text : [SELECT Slick__Body__c FROM Slick__Merge_Item__c WHERE Name = 'Checkout Delivery/Shipping Text']){
				s = text.Slick__body__c;
			}
			return s;
		}
		set;
	}
	
	public decimal shippingMinimum{
		get{
			if(shippingMinimum==null)
				shippingMinimum=opportunity_methods.shippingMinimum;
			return shippingMinimum;
		}
		set;}
	public decimal shippingCost{
		get{
			if(shippingCost==null)
				shippingCost=opportunity_methods.shippingCost;
			return shippingCost;
		}
		set;}

	public cart_CheckoutController(){
		couponapplied=false;
		checkoutData=new wrapperClasses.checkoutWrapper();
		if ( checkoutData.isReturningCustomer == null ){
			checkoutData.isReturningCustomer = false;
		}
		cart_subTotal=0;
		cart_TaxableSubtotal=0;
		cart_OrderTotal=0;
		cart_Tax=0;
		cart_ShippingTotal=0;
		cart_Coupon_Discount=0;
		hasValidCCNumLen = false;
		hasValidSecCode = false;
		if ( orderComplete == null ){
			orderComplete = false;
		}

	}

	public PageReference loadCartContents(){
		cart_CartController cartCon=new cart_CartController();
		cart_SubTotal=cartCon.getCartTotal();
		cart_TaxableSubTotal=cartCon.getTaxableCartTotal();
		cartContents=cartCon.cartContents;
		if(cartContents.size()==0) return Page.Cart;

		cleanCartContents(cartContents);

		applyCoupon();
		calculateTotals();

		//TO-DO: add to Coupon List and discount.
		//coupon_Methods.couponDiscount('autoProcess',cartContents,null);//for Automatically applied coupons.
		return null;
	}
	public static void cleanCartContents(list<wrapperClasses.cartItem> cartCont){
		//make sure quantities >0
		//Prices / active check will be queried on submit.
		for(wrapperClasses.cartItem ci:cartCont ){
			if(ci.qty==null || ci.qty<0)
				ci.qty=0;
		}

	}

	public pagereference applyCoupon(){
		clearErrors();
		couponapplied=false;
		this.couponResult=new coupon_Methods.ApplyCouponResult();
		String couponEmail=this.checkoutData.email;
		if(checkoutData.isReturningCustomer==true)
			couponEmail=this.checkoutData.returningEmail;
		if(String.isnotblank(this.checkoutData.couponCode)){
	        //decimal discount= coupon_Methods.couponDiscount(this.checkoutData.couponCode,  this.cartContents, this.checkoutData.Email);
			this.couponResult=coupon_Methods.couponDiscount(this.checkoutData.couponCode,  this.cartContents, couponEmail);
		}
		else{
			this.couponResult=coupon_Methods.couponDiscount('autoProcess',  this.cartContents, couponEmail);
		}

		couponResult.postProcess();

		if(couponResult.messages.size()>0){
			hasError=true;
			errorMessage='';
			for(String mess:couponResult.messages){
				errorMessage+=mess+'\n';
			}
		}
		if(couponResult.overallDiscount>0 && couponResult.coupons.size()>0){
			cart_Coupon_Discount=couponResult.overallDiscount.setScale(2);
			couponApplied = true;
			calculateTotals();
		}
			//List<apexPages.message> messages=ApexPages.getMessages();
			//if(messages!=null && messages.size()>0){
			//	hasError=true;
			//	this.checkoutData.couponCode='Invalid';
			//	for(ApexPages.message mess:messages){
			//		errorMessage=mess.getSummary();
			//	}

			//}
		System.debug(couponResult);
		System.debug(cart_Coupon_Discount);


		//import coupon methods
		return null;
	}
	public pagereference clearErrors(){
		hasError=false;
		errorMessage=null;
		return null;
	}

	public PageReference placeOrder(){
		//verify fields; js or apex
		System.debug('placing order');
		clearErrors();

		validateForm();


		//hasError=true;
		//errorMessage+='Just Testing';


		if(hasError==true){

			return null;
		}
		//store tax info, coupon info, all other information
		opportunity order_oppty=opportunity_Methods.checkOutWrapperToOpportunity(checkoutData);

		order_oppty.Slick__Subtotal__c=this.cart_Subtotal;
		order_oppty.Slick__Subtotal_with_Tax__c=	this.cart_SubTotal+this.cart_Tax;
		order_oppty.Slick__Tax_Percentage__c=this.cart_TaxRate;
		order_oppty.Slick__Tax__c=this.cart_Tax;
		order_oppty.Slick__Shipping_Cost__c=cart_ShippingTotal;
		order_oppty.Slick__Charge_Amount__c= this.cart_OrderTotal;
		order_oppty.CloseDate=date.today();
		order_oppty.stagename='Closed Won';
		order_oppty.name='WebOrder';
		order_oppty.Slick__Coupon_Amount__c=cart_Coupon_Discount;
		order_oppty.Slick__Order_Placed_Method__c='NS Storefront';
		order_oppty.Slick__Applied_Coupon_Codes__c=couponResult.appliedCouponCodes;
		if(salesTaxRecord!=null)
			order_oppty.Slick__Sales_Tax_Region__c=salesTaxRecord.Slick__Tax_Description__c;


		Savepoint sp;

		api_AuthorizeDotNet.authnetReq_Wrapper chargeRequest;
		api_AuthorizeDotNet.authNetResp_Wrapper chargeResponse;
		Slick__Authorize_net_Transaction__c authTransaction;
		try{
			boolean isCheckPayment=false;
			decimal payment_Number;
			if(checkPaymentEnabled==true && checkoutData.paymentType=='Check (Paper)')
				isCheckPayment=true;

			if(isCheckPayment==false)
				order_Oppty.Slick__payment_Method__c = 'Credit Card';
			else{
				order_Oppty.Slick__payment_Method__c='Check (Paper)';
				order_Oppty.Slick__payment_Status__c='$/Check Open';
			}

			if(isCheckPayment==true){
				sp=Database.setSavepoint();
				attachOrderContact(order_oppty);
			}
			else{
				// process authorize.net transaction - notify of errors
				 chargeRequest=opportunity_Methods.opportunityToAuthorizeDotNetRequest_CreditCard(order_Oppty);
				 //chargeResponse =opportunity_Methods.chargeOpportunity(order_Oppty);
				 if(this.authorizeOnly==true){
					 chargeResponse=api_AuthorizeDotNet.authdotnetAuthorize(chargeRequest);
					 payment_Number=1.2;
				 }
				 else{
				 	chargeResponse=api_AuthorizeDotNet.authdotnetChargeDetailed(chargeRequest);
				 	payment_Number=1.1;
				 }
				 sp = Database.setSavepoint();
				/*if transaction successful*/

			    authTransaction=Opportunity_methods.AuthorizeDotNetToTransaction(chargeResponse,chargeRequest);
				if(chargeResponse.responseCode=='1'){
					attachOrderContact(order_oppty);
					if(returningCreditCard==null){
						//9-23 credit cards
						Slick__Credit_Card__c cc = new Slick__Credit_Card__c();
						cc.Slick__card_number__c = checkoutData.CreditCardNumber;
						cc.Slick__last_4__c = cc.Slick__card_number__c.substring(cc.Slick__card_number__c.length()-4,cc.Slick__card_number__c.length());
						//11.6 only match existing customer's creditcard.
						list<Slick__Credit_Card__c> matchingCards = [select id, Slick__cvv__c, Slick__card_number__c,Slick__expiration_year__c from Slick__credit_Card__c where Slick__last_4__c = :cc.Slick__last_4__c AND Slick__expiration_year__c > :string.valueof(system.today().year()) and Slick__Contact__c=:order_oppty.Slick__contact__c];
						if (matchingCards.isEmpty() || (matchingCards[0].Slick__card_number__c != cc.Slick__card_number__c) || matchingcards[0].Slick__cvv__c != checkoutdata.CreditCardSecurityCode || matchingcards[0].Slick__expiration_year__c!=checkoutdata.CreditCardExpYear){
							cc.Slick__name_on_card__c = checkoutData.NameOnCard;
							cc.Slick__cvv__c = checkoutdata.CreditCardSecurityCode;
							cc.Slick__expiration_month__c = checkoutData.CreditCardExpMonth;
							cc.Slick__expiration_year__c = checkoutdata.CreditCardExpYear;
							cc.Slick__contact__c = order_Oppty.Slick__contact__c;
							cc.Slick__billing_street__c = checkoutdata.billingAddress+((string.isnotblank(checkoutdata.billingaddress2))?('\n'+checkoutdata.billingaddress2):'');
							cc.Slick__billing_city__c = checkoutdata.billingcity;
							cc.Slick__billing_state__c = checkoutdata.billingstate;
							cc.Slick__billing_postal_code__c = checkoutdata.billingzip;
							cc.Slick__card_type__c = checkoutdata.CreditCardType; //add card type based on # scheme\
							//cc.name = cc.Slick__card_type__c+' ending in '+cc.Slick__last_4__c;
							cc.name=Credit_Card_Extension.getCreditCardName(cc);
							insert cc;
						}
						else cc = matchingCards[0];

						order_Oppty.Slick__credit_card_used__c = cc.id;
					}
					else
						order_oppty.Slick__credit_card_used__c=returningCreditCard.id;
				}
				else{
					hasError=true;
					errorMessage=chargeResponse.ResponseReasonText;
					 wrapperClasses.checkoutWrapper maskedCheckoutData=maskCheckoutData(checkoutData);
					Slick__Logger__c errorLog= ErrorLogger.LogError('Authorize.net Checkout Error',' Error in Submit Checkout. Customer:'+checkoutData.billingFirstName+' '+checkoutData.billingLastName+' Checkout Data: '+maskedCheckoutData+' '+' Line items: '+cartContents+' Error Displayed to User:'+errorMessage);

					if(checkoutData.isReturningCustomer==true)
						clearReturningCustomerData();
					return null;
				}
			}
			insert order_Oppty;

			orderNumber = [select id, Slick__order_number__c from Opportunity where id = :order_oppty.id].Slick__order_number__c;

			//store line items for order
			list<opportunitylineItem> olis=attachOpportunityLineItems(order_Oppty,cartContents);
			insert olis;

			if(isCheckPayment==false){
				authTransaction.Slick__order__c=order_Oppty.id;
				authTransaction.Slick__credit_card__c = order_oppty.Slick__credit_card_used__c;
				authTransaction.Slick__Payment_Number__c=payment_number;
				insert authTransaction;
			}
			//decrement inventory of products
			updateInventory(olis);


			//update coupon usage
			if(order_oppty.Slick__Applied_Coupon_Codes__c!=null && order_oppty.Slick__Coupon_Amount__c!=null && order_oppty.Slick__Coupon_Amount__c>0){
				Coupon_Methods.updateCouponUsage(order_oppty.Slick__Applied_Coupon_Codes__c,order_oppty.Slick__Contact__c);
			}
			Opportunity confirmationOppty=new Opportunity(id=order_Oppty.id,Slick__confirmation_sent__c=dateTime.now());
			update confirmationOppty;


			//Clear Cart Cookie
			if(order_oppty.id!=null)
				util.clearCookie('cartCookie');
		}
		catch (exception e){
			System.debug(e);
			System.debug(e.getStackTraceString());
			hasError=true;
			errorMessage=String.valueof(e);
			//errorMessage+=' '+e.getStackTraceString();
			if(checkoutData.isReturningCustomer==true)
					clearReturningCustomerData();
			if(sp!=null)
				Database.rollback(sp);
			//void transaction if it was successful
			if(chargeResponse!=null && chargeResponse.responseCode=='1' && String.isnotblank(chargeResponse.TransactionId)){
				//api_AuthorizeDotNet.authnetReq_Wrapper voidreq=new api_AuthorizeDotNet.authnetReq_Wrapper();
				//voidreq.transid=chargeResponse.TransactionId;
				//api_AuthorizeDotNet.authNetResp_Wrapper voidresponse=api_AuthorizeDotNet.authDotNetVoid(voidreq);
				voidCheckoutTransaction(chargeResponse.TransactionID);
			}
			//insert logger
			//maskCheckoutData
			 wrapperClasses.checkoutWrapper maskedCheckoutData=maskCheckoutData(checkoutData);
			 Slick__Logger__c errorLog= ErrorLogger.LogError(e,' Error in Submit Checkout. Customer:'+checkoutData.billingFirstName+' '+checkoutData.billingLastName+' Checkout Data: '+maskedCheckoutData+' '+' Line items: '+cartContents+ ' Error Displayed to User: '+errorMessage);

		}
			/*else*/
			//return error message regarding payment.
		if(hasError==false)
			orderComplete=true;
		return null;
	}
	
	public static wrapperClasses.checkoutWrapper maskCheckoutData(wrapperClasses.checkoutWrapper checkoutWrap){
		wrapperClasses.checkoutWrapper cloneCheckoutWrap=checkoutWrap.clone();
		cloneCheckoutWrap.CreditCardNumber=maskString(cloneCheckoutWrap.CreditCardNumber,4);
		cloneCheckoutWrap.CreditCardSecurityCode=maskString(cloneCheckoutWrap.CreditCardSecurityCode,0);
		return cloneCheckoutWrap;
	}
	//masks leaving last n chars
	public static string maskString(String val,integer chars){
		String masked;
		if(String.isnotblank(val)){
			if(val.length()>=chars){
				String mask='';
				for(integer i=0;i<(val.length()-chars);i++){
					mask+='*';
				}
				String lastn=val.subString((val.length()-chars),val.length());
				masked=mask+lastn;
			}
			else masked=val;
		}
		return masked;
	}
	@Future(callout=true)
	public static void voidCheckoutTransaction(String transactionId){
		api_AuthorizeDotNet.authnetReq_Wrapper voidreq=new api_AuthorizeDotNet.authnetReq_Wrapper();
		voidreq.transid=TransactionId;
		api_AuthorizeDotNet.authNetResp_Wrapper voidresponse=api_AuthorizeDotNet.authDotNetVoid(voidreq);
	}

	//Attach existing contact to order. Create new if doesn't exist.
	public void attachOrderContact(opportunity order_oppty){
		if(order_oppty.Slick__Contact__c==null){
			List<Contact> contacts = [
				SELECT id, accountid, Slick__route__c
				FROM Contact
				WHERE (
					email != null AND email=:order_oppty.Slick__Billing_Email__c) or
					(Slick__Alternate_Email__c != null AND Slick__Alternate_Email__c=:order_oppty.Slick__billing_Email__c) or
					(Slick__Alternate_Email_2__c!=null AND Slick__Alternate_Email_2__c=:order_oppty.Slick__billing_Email__c) 
					//or(Slick__Alternate_Email_3__c!=null AND Slick__Alternate_Email_3__c=:order_oppty.Slick__billing_Email__c
				//)
				LIMIT 1
			];
			if( contacts.size() == 0 ){
				List<Account> acc = [
					SELECT Id
					FROM Account
					WHERE name = 'Individual Customer'
					LIMIT 1
				];
				if( acc.size() > 0 ){
					Contact newCustomer = opportunity_Methods.checkoutWrapperToContact(checkoutData);
					newCustomer.accountId = acc[0].id;
					insert newCustomer;
					order_oppty.Slick__Contact__c = newCustomer.id;
					order_oppty.accountid = acc[0].id;
				}
			}
			else{
				order_oppty.Slick__Contact__c = contacts[0].id;
				order_oppty.Accountid = contacts[0].accountid;
			}
		}

	}

	public static void updateInventory(list<opportunityLineItem> olis){
		//Decrement product's Units_in_Inventory by opportunitylineitem quantity;
		//take account for items added twice in list
		set<id> pricebookentryIds =new set<id>();
		for(opportunityLineItem oli:olis){
			pricebookEntryIds.add(oli.priceBookEntryId);
		}
		map<id,pricebookentry> pricebookProductMap=new map<id,pricebookentry>([
			select id,product2Id,product2.Slick__Units_In_Inventory__c
			from PricebookEntry
			where id in :pricebookentryIds
		]);
		list<product2> productUpdates=new list<product2>();
		map<id,product2> productUpdateMap=new map<id,product2>();
		for(opportunityLineItem oli:olis){
			if(priceBookproductMap.containskey(oli.priceBookEntryId)){
				pricebookentry pbe=priceBookproductMap.get(oli.priceBookEntryId);
				decimal newQuantity;
				if(pbe.product2.Slick__units_in_Inventory__c!=null && oli.quantity!=null)
					newQuantity=pbe.product2.Slick__units_in_Inventory__c-oli.quantity;
				else if(oli.quantity!=null){
					newQuantity=0-oli.quantity;
				}
				if(productUpdateMap.containsKey(pbe.product2id)){
					product2 prod=productUpdateMap.get(pbe.product2Id);
					prod.Slick__Units_in_inventory__c=oli.quantity!=null?prod.Slick__Units_In_Inventory__c-oli.quantity:prod.Slick__Units_In_Inventory__c;
				}
				else{
					Product2 prod=new product2(id=pbe.product2id, Slick__units_in_inventory__c=newQuantity);
					productUpdateMap.put(prod.id,prod);
				}
			}

		}
		update productUpdateMap.values();
	}
	public list<opportunityLineItem> attachOpportunityLineItems(Opportunity oppty,list<wrapperClasses.cartItem> cItems){
		list<opportunityLineItem> lines=new list<opportunityLineItem>();
		set<id> productIds=new set<id>();
		for(wrapperClasses.cartItem cItem: cItems){
			productIds.add(cItem.pid);
		}
		map<id,pricebookEntry> productToPricebookMap=new map<id,PricebookEntry>();
		map<id, pricebookEntry> productToSalePricebookMap = new Map<Id, PricebookEntry>();

		for(pricebookEntry pbe:[select id,product2id,unitprice, pricebook2.name from pricebookEntry where product2Id in:productIds and isActive=true]){
			if(pbe.pricebook2.name == 'On Sale'){
				productToSalePricebookMap.put(pbe.product2id, pbe);
			}
			else{
				productToPricebookMap.put(pbe.product2id,pbe);
			}

		}
		//should handle no longer existing items or pricebookentries
		for(wrapperClasses.cartItem cItem: cItems){
			if(!productToPricebookMap.containskey(cItem.pid)){
				hasError=true;
				errorMessage='Error Adding Products to Order';
				return null;
			}
			else{


				//PriceBookEntry pbe=productToPricebookMap.get(cItem.pid);
				PriceBookEntry pbe = cItem.saleprice != null ? productToSalePricebookMap.get(cItem.pid) : productToPricebookMap.get(cItem.pid);
				opportunityLineItem line=new OpportunityLineItem();

				line.OpportunityId=oppty.id;
				line.pricebookEntryId=pbe.id;
				line.UnitPrice=pbe.unitprice;
				//IF ON SALE, use On sale price book list price.

				line.quantity=cItem.qty;
				lines.add(line);
			}
		}
		return lines;
	}
	public pageReference calculateTotals(){
		calculateShipping();
		calculateTax();
		return null;
	}

	public void calculateShipping(){
		//apply shipping cost if subtotal after discount less than minimum
		decimal subtotalAfterCoupon=cart_subTotal-cart_Coupon_Discount;
		if(subtotalAfterCoupon<this.shippingMinimum){
			cart_ShippingTotal=this.shippingCost;
		}
		else
			cart_ShippingTotal=0;
		//shipping calculation
	}

	public void calculateTax(){
		String zip = '';
		if ( String.isNotBlank(checkoutData.billingZip) ){
			zip = checkoutData.billingZip;
		}
		String state = '';
		if ( String.isNotBlank(checkoutData.billingState) ){
			state = checkoutData.billingState;
		}
		if(checkoutData.useShipToAddress == null || checkoutData.useShipToAddress==true){
			if ( String.isNotBlank(checkoutData.shippingZip) ){
				zip=checkoutData.shippingZip;
			}
			if ( String.isNotBlank(checkoutData.shippingZip) ){
				state=checkoutData.shippingZip;
			}
		}
		//tax estimate
		//if(checkoutData.isReturningCustomer==true){
		//	zip=checkoutData.returningBillingZip;
		//}

		this.cart_TaxRate=0;
		this.salesTaxRecord=null;
		if(currentContact!=null){
			if( checkoutData.isReturningCustomer != null && checkoutData.isReturningCustomer==true ){
				if( String.isnotblank(currentContact.mailingPostalCode) ){
					zip=currentContact.mailingPostalCode;
				} else
				if(String.isnotblank(currentContact.otherPostalCode)){
					zip=currentContact.otherPostalCode;
				} else{
					zip=checkoutData.ReturningBillingZip;
				}
				if( String.isnotblank(currentContact.mailingState) ){
					state=currentContact.mailingState;
				} else
				if(String.isnotblank(currentContact.otherState)){
					state=currentContact.otherState;
				} else {
					state=null;
				}
			}

			if(currentContact.Slick__Sales_Tax__c!=null){
				this.salesTaxRecord=opportunity_Methods.getTaxById(currentContact.Slick__Sales_Tax__c);
				this.cart_TaxRate=salesTaxRecord.Slick__Tax_Percentage__c;
			}
			else{
				this.salesTaxRecord=opportunity_Methods.getTaxByZipThenState(zip,state);
				this.cart_TaxRate=salesTaxRecord.Slick__Tax_Percentage__c;
			}
		}
		else{
			this.salesTaxRecord=opportunity_Methods.getTaxByZipThenState(zip,state);
			this.cart_TaxRate=salesTaxRecord.Slick__Tax_Percentage__c;
		}
		//decimal t = opportunity_Methods.calculateTax(zip,cart_Subtotal);
		//this.cart_tax=this.cart_TaxRate/100*cart_SubTotal;
		//Tax AFTER coupon discounts
		this.cart_tax=this.cart_TaxRate/100*(cart_TaxableSubtotal-cart_Coupon_Discount);
		this.cart_tax=this.cart_tax.setScale(2,RoundingMode.HALF_UP);
		if(this.cart_Tax<0) this.cart_Tax=0;

	}

	public PageReference validateForm(){
		hasError=false;
		applyCoupon();

		//if(this.checkPaymentEnabled==true){
		//	//Check if contact is enabled

		//}

		if(checkoutData.isReturningCustomer==true){
			if(String.isblank(checkoutData.returningEmail)||
			String.isblank(checkoutData.returningBillingZip)||
			String.isblank(checkoutData.returningSecurityCode)||
			String.isblank(checkoutData.DeliveryDate)){
				hasError=true;
				errorMessage='Returning Customer: Please fill out all required fields.';
			}
			else{
				validateReturningCustomer();
			}
		}
		else if(String.isblank(checkoutData.billingFirstName)||
		String.isblank(checkoutData.billingLastName)||
		String.isblank(checkoutData.billingAddress)||
		String.isblank(checkoutData.billingCity)||
		String.isblank(checkoutData.billingState)||
		String.isblank(checkoutData.billingZip)||
		String.isblank(checkoutData.email)||
		String.isblank(checkoutData.phone)||
		(checkoutData.useShipToAddress==true &&(
			String.isblank(checkoutData.shippingFirstName)||
			String.isblank(checkoutData.shippingLastName)||
			String.isblank(checkoutData.shippingAddress)||
			String.isblank(checkoutData.shippingCity)||
			String.isblank(checkoutData.shippingState)||
			String.isblank(checkoutData.shippingZip)
		))||
		String.isblank(checkoutData.DeliveryDate)||
		String.isblank(verifiedEmail)
		){
			hasError=true;
			errorMessage='Please fill out all required fields.';
		   //use javascript to mark specific fields class='woocommerce-invalid-required-field'
		}
		else if(checkPaymentEnabled==false || checkoutData.PaymentType=='Credit Card'){
			//Validate credit card if not check payment
			if(String.isblank(checkoutData.CreditCardExpMonth)||
			String.isblank(checkoutData.CreditCardExpYear)||
			String.isblank(checkoutData.CreditCardNumber)||
			String.isblank(checkoutData.CreditCardSecurityCode)||
			String.isblank(checkoutData.NameOnCard)
			){
				hasError=true;
				errorMessage='Please fill out all required fields.';
			}
			else if(!hasValidCCNumLen){
				hasError=true;
				errorMessage='Credit card number entered is too long';
			}
			//else if(!hasValidSecCode){
			//	hasError=true;
			//	errorMessage='Security code entered is too long';
			//}
			else if(String.isblank(checkoutData.CreditCardType)){
				hasError=true;
				errorMessage='Credit card not recognized';
			}
		}
		else if(verifiedEmail!=checkoutData.email){
			hasError=true;
			errorMessage='Please make sure your emails match.';
		}
    	return null;
	}
	public pageReference checkReturningCustomer(){
		hasError=false;
		returningContactId=null;
		if(checkoutData.isReturningCustomer==true && String.isnotblank(checkoutData.returningEmail)){
			//list<contact> contacts=[select id,accountid,(select id from credit_cards__r) from contact where email=:checkoutData.returningEmail or Slick__Alternate_Email__c=:checkoutData.returningEmail or Slick__Alternate_Email_2__c=:checkoutData.returningEmail or Slick__Alternate_Email_3__c=:checkoutData.returningEmail limit 1];
			list<contact> contacts=[select id,accountid,(select id from credit_cards__r) from contact where email=:checkoutData.returningEmail or Slick__Alternate_Email__c=:checkoutData.returningEmail or Slick__Alternate_Email_2__c=:checkoutData.returningEmail limit 1];
			if(contacts.size()>0){
				if(contacts.get(0).credit_cards__r.size()>0)
					returningContactId=contacts.get(0).id;
				else{
					hasError=true;
					errorMessage='Apologies! Looks like we don\'t have payment information on your customer file. Try another email or <a href="javascript:void(0)" class="returningCustomerLinkRefresh NSTextGreen">click here to check out and add payment information.</a>';
				}
			}
			else{
				hasError=true;
				//errorMessage='No existing customer found';
				errorMessage='Apologies! Looks like we don\'t have this email on your customer file. Try another email or <a href="javascript:void(0)" class="returningCustomerLinkRefresh NSTextGreen">click here to check out and add an additional email.</a>';

			}
		}
		else{
			hasError=true;
			errorMessage='Please enter returning customer email';
		}
		if(hasError==false){
			queryContactFromEmail();
		}
		return null;
	}
	public list<SelectOption> getReturningCardOptions(){
		//display masked cards
		list<SelectOption> options=new list<SelectOption>();
		if(checkoutData.isReturningCustomer==true && returningContactId!=null){
				//checkagainst query to make sure id wasn't manually changed
				set<id> cardIds=new set<id>();
				for(Slick__credit_card__c card:[select id, name,Slick__cvv__c,Slick__billing_postal_code__c from Slick__credit_Card__c where Slick__contact__c=:returningContactId]){
					cardIds.add(card.id);
						options.add(new selectOption(card.id,card.name));
				}
				if(selectedReturningCCid!=null && !cardIds.contains(selectedReturningCCid))
					selectedReturningCCId=null;
				if(options.size()>0){
					if(selectedReturningCCid==null)
						selectedReturningCCId=options.get(0).getValue();
				}
			}
		return options;
	}
	public void validateReturningCustomer(){
		System.debug('selected cc '+selectedReturningCCid);
		System.debug('returning customer '+ returningcontactId);

		list<Slick__credit_card__c> cards=	[select id,Slick__Name_on_Card__c, Slick__card_number__c,Slick__card_type__c,Slick__contact__c,Slick__expiration_year__c,Slick__expiration_month__c,Slick__last_4__c,name, Slick__Billing_postal_code__c, Slick__CVV__c,
		Slick__billing_street__c,Slick__billing_city__c,Slick__billing_state__c, contact__r.email,contact__r.firstName,contact__r.lastName,contact__r.phone,
		contact__r.mailingStreet,contact__r.mailingcity,contact__r.mailingPostalCode,contact__r.mailingState
		from Slick__credit_Card__c
		where Slick__billing_postal_code__c=:checkoutData.returningBillingZip
		//and (contact__r.email=:checkoutdata.returningEmail or contact__r.Slick__alternate_email__c=:checkoutdata.returningEmail  or contact__r.Slick__alternate_email_2__c=:checkoutdata.returningEmail or contact__r.Slick__alternate_email_3__c=:checkoutdata.returningEmail)
		and (contact__r.email=:checkoutdata.returningEmail or contact__r.Slick__alternate_email__c=:checkoutdata.returningEmail  or contact__r.Slick__alternate_email_2__c=:checkoutdata.returningEmail)
		and id=:selectedReturningCCId and Slick__contact__c=:returningContactId];
		if(cards.size()>0){
			System.debug(cards);
			if(cards.get(0).Slick__CVV__c == checkoutData.returningSecurityCode){
				returningCreditCard=cards.get(0);
				checkoutData.billingFirstName=returningCreditCard.contact__r.firstName;
				checkoutData.billingLastName=returningCreditCard.contact__r.lastName;
				checkoutData.billingAddress=returningCreditCard.Slick__billing_street__c;
				checkoutData.billingCity=returningCreditCard.Slick__billing_city__c;
				checkoutData.billingZip=returningCreditCard.Slick__billing_postal_code__c;
				checkoutData.billingState=returningCreditCard.Slick__billing_State__c;
				checkoutData.email=checkoutData.returningEmail;
				checkoutData.phone=returningCreditCard.contact__r.phone;
				//use contact mailing address?
				checkoutData.shippingFirstName=returningCreditCard.contact__r.firstName;
				checkoutData.shippingLastName=returningCreditCard.contact__r.lastName;
				checkoutData.shippingAddress=returningCreditCard.contact__r.mailingStreet;
				checkoutData.shippingCity=returningCreditCard.contact__r.mailingCity;
				checkoutData.shippingZip=returningCreditCard.contact__r.mailingPostalCode;
				checkoutData.shippingState=returningCreditCard.contact__r.mailingState;
				checkoutData.useShipToAddress=true;
				if(String.isblank(checkoutData.shippingAddress) &&
				    String.isblank(checkoutData.shippingCity) &&
					String.isblank(checkoutData.shippingZip) &&
					String.isblank(checkoutData.shippingState)){
					checkoutData.useShipToAddress=false;
				}
				checkoutData.CreditCardNumber=returningCreditCard.Slick__card_number__c;
				checkoutData.CreditCardType= returningCreditCard.Slick__card_type__c;
				checkoutData.CreditCardExpMonth= returningCreditCard.Slick__expiration_month__c;
				checkoutData.CreditCardExpYear=	returningCreditCard.Slick__expiration_year__c;
				checkoutData.CreditCardSecurityCode=returningCreditCard.Slick__CVV__c;
				checkoutData.NameOnCard=returningCreditCard.Slick__Name_on_Card__c;
			}
		}
		if(returningCreditCard==null){
			hasError=true;
			//errorMessage='No matching customer found';
			errorMessage='Apologies! This does not match your information on file. Try again or <a href="javascript:void(0)" class="returningCustomerLinkRefresh NSTextGreen">Click here to check out and we will update your information.</a>';
		}

	}
	public void clearReturningCustomerData(){
		checkoutData=new wrapperClasses.checkoutWrapper();
		returningCreditCard=null;

	}

	//public pageReference queryContactFromEmailOLD(){
	//	currentContact=null;
	//	//only if user verifies email.
	//	if(String.isnotblank(checkoutData.Email) && checkoutData.Email==verifiedEmail){
	//		list<Contact> contacts=[select id,Slick__route__c,Slick__Checks_Enabled__c,mailingpostalcode,otherpostalcode,Slick__Tax_Exempt__c from contact where email=:checkoutData.Email or Slick__Alternate_Email__c=:checkoutData.Email or Slick__Alternate_Email_2__c=:checkoutData.Email or Slick__Alternate_Email_3__c=:checkoutData.Email limit 1];
	//		if(contacts.size()>0){
	//			currentContact=contacts.get(0);

	//		}
	//	}
	//	validateCheckOption();
	//	return null;
	//}

	////We need the returning contact's route before order submition, but can't expose the record.
	//public contact queryQuickCheckoutContactFromEmailOLD(){

	//	if(String.isnotblank(checkoutData.returningEmail)){
	//		list<Contact> contacts=[select id,Slick__route__c,mailingpostalcode,otherpostalcode,Slick__Tax_Exempt__c from contact where email=:checkoutData.returningEmail or Slick__Alternate_Email__c=:checkoutData.returningEmail or Slick__Alternate_Email_2__c=:checkoutData.returningEmail or Slick__Alternate_Email_3__c=:checkoutData.returningEmail limit 1];
	//		if(contacts.size()>0){
	//			return contacts.get(0);
	//		}
	//	}
	//	return null;
	//}

	public pageReference queryContactFromEmail(){
		currentContact=null;
		//quick checkout
		if(String.isnotblank(checkoutData.returningEmail) && checkoutData.isReturningCustomer==true){
			//list<Contact> contacts=[select id,Slick__route__c,mailingState,otherState,mailingpostalcode,otherpostalcode,Slick__Tax_Exempt__c,Slick__Sales_Tax__c from contact where email=:checkoutData.returningEmail or Slick__Alternate_Email__c=:checkoutData.returningEmail or Slick__Alternate_Email_2__c=:checkoutData.returningEmail or Slick__Alternate_Email_3__c=:checkoutData.returningEmail limit 1];
			list<Contact> contacts=[select id,Slick__route__c,mailingState,otherState,mailingpostalcode,otherpostalcode,Slick__Tax_Exempt__c,Slick__Sales_Tax__c from contact where email=:checkoutData.returningEmail or Slick__Alternate_Email__c=:checkoutData.returningEmail or Slick__Alternate_Email_2__c=:checkoutData.returningEmail];
			if(contacts.size()>0)
				currentContact=contacts.get(0);
		}
		//only if user verifies email.
		else if(checkoutData.isReturningCustomer!=true && String.isnotblank(checkoutData.Email) && checkoutData.Email==verifiedEmail){
			//list<Contact> contacts=[select id,Slick__route__c,Slick__Checks_Enabled__c,mailingState,OtherState,mailingpostalcode,otherpostalcode,Slick__Tax_Exempt__c,Slick__Sales_Tax__c from contact where email=:checkoutData.Email or Slick__Alternate_Email__c=:checkoutData.Email or Slick__Alternate_Email_2__c=:checkoutData.Email or Slick__Alternate_Email_3__c=:checkoutData.Email limit 1];
			list<Contact> contacts=[select id,Slick__route__c,mailingState,OtherState,mailingpostalcode,otherpostalcode,Slick__Tax_Exempt__c,Slick__Sales_Tax__c from contact where email=:checkoutData.Email or Slick__Alternate_Email__c=:checkoutData.Email or Slick__Alternate_Email_2__c=:checkoutData.Email];
			if(contacts.size()>0)
				currentContact=contacts.get(0);
			validateCheckOption();
		}
		calculateTotals();
		return null;
	}



	public void validateCheckOption(){
		checkPaymentEnabled=false;
		Slick__Instance_Setting__c checkSetting=(test.isRunningTest())?(new Slick__Instance_Setting__c(name='Enable Check Payment', Slick__text_value__c = 'true')): ((Slick__Instance_Setting__c.getInstance('Enable Check Payment')));
		
		//If both the instance setting and individual contact has checks enabled
		if(checkSetting!=null && String.isnotblank(checkSetting.Slick__text_value__c) && currentContact!=null){
			//if(boolean.valueOf(checkSetting.Slick__text_value__c)==true && currentContact.Slick__Checks_Enabled__c==true)
			if(boolean.valueOf(checkSetting.Slick__text_value__c)==true){
				checkPaymentEnabled=true;
				if(String.isblank(checkoutData.PaymentType))
					checkoutData.PaymentType='Credit Card';
			}
		}
	}

	public list<selectOption> getShippingDateOptions(){
		list<SelectOption> options=new list<SelectOption>{new selectoption('','Enter email and zip into address areas above')};
		Contact existingContact=currentContact;
		String zip=checkoutData.billingZip;

		if(checkoutData.useShipToAddress==true)	zip=checkoutData.shippingZip;

		if(checkoutData.isReturningCustomer==true){
			//existingContact=queryQuickCheckoutContactFromEmail();
			if(existingContact!=null){
				if(String.isnotblank(existingContact.mailingPostalCode))
					zip=existingContact.mailingPostalCode;
				else if(String.isnotblank(existingContact.otherPostalCode))
					zip=existingContact.otherPostalCode;
				else
					zip=checkoutData.ReturningBillingZip;
			}
		}

		//If Email filled and contact exists. And Contact Slick__Route__c exists. Get ship dates from Slick__route__c.
		if(existingContact!=null && existingContact.Slick__route__c!=null && String.isnotblank(existingContact.mailingpostalcode) && zip==existingContact.mailingpostalCode){
			options= opportunity_Methods.calculateDeliveryDates(new Slick__route__c(id=existingContact.Slick__route__c));
		}
		else if (string.isnotblank(zip)) options = opportunity_Methods.calculateDeliveryDates(zip);
		/*if(checkoutData.isReturningCustomer==false && currentContact!=null && currentContact.Slick__route__c!=null && String.isnotblank(currentContact.mailingpostalcode) && zip==currentContact.mailingpostalCode){
			options= opportunity_Methods.calculateDeliveryDates(new Slick__route__c(id=currentContact.Slick__route__c));
		}*/
		return options;
	}
	public list<SelectOption> getMonthOptions(){
		return util.getExpirationMonths();
	}
	public list<SelectOption> getYearOptions(){
		return util.getExpirationYears();
	}
	public list<SelectOption> getStateOptions(){
		return util.getUSStates();
	}

	public list<SelectOption> getCountryOptions(){
		return util.getCountries();
	}
	public list<SelectOption> getCreditCardOptions(){
		return util.getCreditCards();
	}
	public list<SelectOption> getPaymentOptions(){
		list<SelectOption> options=new list<SelectOption>();
		options.add(new SelectOption('Credit Card','Credit Payment'));
		options.add(new SelectOption('Check (Paper)','Check Payment'));
		return options;
	}

	public pageReference checkCCType(){
		if(String.isnotblank(checkoutData.CreditCardNumber)){
			checkoutData.CreditCardType=determineCCType(checkoutData.CreditCardNumber);
			hasValidCCNumLen = isValidCCNumLength(checkoutData.CreditCardNumber);
		}
		return null;
	}
	public pageReference checkSecCode(){
		if(String.isnotblank(checkoutData.CreditCardSecurityCode)){
			hasValidSecCode = isValidSecCode(checkoutData.CreditCardSecurityCode);
		}
		return null;
	}

	public static map<String,boolean> cardTypes{
		get{
			if(cardTypes==null){
				cardTypes=new map<String,boolean>();
				cardTypes.put('Visa',false);
				cardTypes.put('Mastercard',false);
				cardTypes.put('American Express',false);
				cardTypes.put('AMEX',false);
				cardTypes.put('Discover',false);
				for(Schema.PicklistEntry ples:Opportunity.Slick__card_Type__c.getDescribe().getPicklistValues())
					cardTypes.put(ples.getValue(),true);
			}
			return cardTypes;
		}set;}

	public static boolean isValidCCNumLength(String cardNumber){
		if(cardNumber.length() > 16){
			return false;
		}
		else{
			return true;
		}
	}
	public static boolean isValidSecCode(String code){
		if(code.length() > 4){
			return false;
		}
		else{
			return true;
		}
	}

	public static string determineCCtype (String cardNumber){
		String cctypeimage;
		//dynamically picklist values from types from Slick__Card_Type__c
		if(cardNumber!=null){
			if (cardTypes.get('Visa')==true && cardNumber.length()>=1 && cardNumber.substring(0,1)=='4') {
				cctypeimage = 'Visa'; //visa
			}
			else if (cardTypes.get('Mastercard')==true && cardNumber.length()>=2 &&(cardNumber.substring(0,2)=='51' || cardNumber.substring(0,2)=='52' || cardNumber.substring(0,2)=='53' || cardNumber.substring(0,2)=='54'  || cardNumber.substring(0,2)=='55')){
				cctypeimage = 'Mastercard';//mastercard
			}
			else if (cardTypes.get('Discover')==true && cardNumber.length()>=4 && cardNumber.substring(0,4)=='6011') {
				cctypeimage = 'Discover';//discover
			}
			else if ((cardTypes.get('American Express')==true||cardTypes.get('AMEX')==true) && cardNumber.length()>= 2 && (cardNumber.substring(0,2)=='34' || cardNumber.substring(0,2)=='37')) {
				cctypeimage = 'American Express';//american express
			}
		}
		return cctypeimage;
	}

}