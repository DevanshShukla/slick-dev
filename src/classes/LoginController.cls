public class LoginController {
    public String recordId {get;set;}
    public String username {get;set;}
    public String password {get;set;}
    public Boolean authenticated {get;set;}
    
    // Page messages
    public String PageMsg {get;set;}
   
    public LoginController(){
        String data = '';
        Cookie counter = ApexPages.currentPage().getCookies().get('valueLogin');
        if(counter != null){
            data = apexpages.currentPage().getCookies().get('valueLogin').getValue();
        }
        Authenticated = data != '' ? true : false;
        
        String currentPageStatus = ApexPages.currentPage().getParameters().get('passwordUpdate');
        
        if(currentPageStatus == 'true'){
            showChangePassword();
        }
    }
    
    public pagereference doLogin(){ 
        
        if(username == null || username == ''){
            PageMsg = 'Please Enter Email Id !!';
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter username'));
            return null;
        }else if(password == null || password == ''){
            PageMsg = 'Please Enter Password !!';
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter password'));
            return null;
        }else{
            List<Slick__Site_User__c> siteUsr = [Select Id, Slick__UserName__c, Slick__Contact__r.Slick__uid__c, Slick__Password__c from Slick__Site_User__c Where Slick__UserName__c =: username LIMIT 1];
            if(!siteUsr.isEmpty() && password.equals(siteUsr[0].Slick__Password__c)){
                Pagereference pr = new Pagereference('/apex/naturesselect');
                Cookie cook;
                if(siteUsr[0].Slick__Contact__r.Slick__uid__c == null){
                    cook = new Cookie('valueLogin', siteUsr[0].Id, null, 1800, false);
                }else{
                    cook = new Cookie('valueLogin', siteUsr[0].Slick__Contact__r.Slick__uid__c, null, 1800, false);   
                }
                pr.setCookies(new Cookie[] {cook});
                authenticated = true;
                pr.setRedirect(false);
                return pr;
            }else{
                PageMsg = 'Incorrect Email Or Password !!';
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Username or Password is incorrect'));
                return null;
            }
        }  
    }
    
    public Pagereference doLogout(){
        Pagereference pr = new Pagereference('/apex/LoginPage');
        Cookie cook = new Cookie('valueLogin', 'true', null, 0, false);
        pr.setCookies(new Cookie[] {cook});
        authenticated = false;
        pr.setRedirect(true);
        return pr;
    }
    
    public void showChangePassword(){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Password has been changed'));
    }
    
    public pagereference forgotpassword(){
        Pagereference pr = new Pagereference('/apex/ForgotPass');
        
        return pr;
    }

    public pagereference SignUp(){
        Pagereference pr = new Pagereference('/apex/SignUp');
        return pr;
    }
}