({
	doinit : function(component, event, helper) {
		helper.fetchOpportunityData(component, event, helper);
		helper.fetchPast12MonthsProducts(component, event, helper);
		helper.fetchlastOrderItems(component, event, helper);
	},
	
	addNewProductPopup : function(component, event, helper) {
		component.set("v.isAddProduct", true);
	},
	
	closeModel : function(component, event, helper) {
		component.set("v.isAddProduct", false);
	},
	
	updateOrder : function(component, event, helper) {
		helper.updateOrder(component, event, helper);
	},
	
	addProduct : function(component, event, helper) {
		helper.updateOrder(component, event, helper);
	},
	
	SearchProduct : function(component, event, helper) {
		helper.SearchProduct(component, event, helper);
	},
	
	selectProduct : function(component, event, helper) {
		var ProductList = component.get("v.opptyProducts");

		var searchKey = event.target.title;
		
		var selectedList = component.get("v.SelectedProductList");
		
		for(var i=0; i<ProductList.length; i++){
		    if(ProductList[i].product.Id == searchKey && ProductList[i].isSelect == true){
		        ProductList[i].qty = 1;
		        selectedList.push(ProductList[i]);
		    }else if(ProductList[i].isSelect == false && ProductList[i].product.Id == searchKey){
		        for(var j=0; j<selectedList.length; j++){
		            ProductList[i].qty = 0;
		            if(selectedList[j].product.Id == searchKey){
		                selectedList.splice(j, 1);
		            }
		        }
		    }
		}
		
		component.set("v.opptyProducts", ProductList);
		console.log(selectedList);
	},
})