({
	fetchOpportunityData : function(component, event, helper) {
	    
		var action = component.get("c.GetOpportunityData");
		
		action.setParams({
            "SobjectApiName" : component.get('v.RecordId') 
        });
        
        action.setCallback(this, function(a){
            var result = a.getReturnValue();
            component.set("v.OpportunityData", result[0]);
            console.log(a.getReturnValue());
        });
        $A.enqueueAction(action);
	},
	
	fetchPast12MonthsProducts : function(component, event, helper) {
	    
		var action = component.get("c.getPast12MonthsProducts");
		
		action.setParams({
            "RecordId" : component.get('v.RecordId') ,
            "type"  : "Past12Months"
        });
        
        action.setCallback(this, function(a){
            var result = a.getReturnValue();
            component.set("v.Past12MonthsProduct", result);
        });
        $A.enqueueAction(action); 
	},
	
	fetchlastOrderItems : function(component, event, helper) {
	    
		var action = component.get("c.getPast12MonthsProducts");
		
		action.setParams({
            "RecordId" : component.get('v.RecordId') ,
            "type"  : "LastOrderItems"
        });
        
        action.setCallback(this, function(a){
            var result = a.getReturnValue();
            component.set("v.lastOrderItems", result);
        });
        $A.enqueueAction(action); 
	},
	
	SearchProduct : function(component, event, helper) {
	    
	    var selectedList = component.get("v.SelectedProductList");
	    
	    var searchKey = event.getSource().get("v.value");
	    
	    if(searchKey != null && searchKey != ''){
	        component.set("v.opptyProducts", []);
	        
    		var action = component.get("c.getSearchProduct");
    		
    		action.setParams({
                "SearchKey" : searchKey
            });
            
            action.setCallback(this, function(a){
                var result = a.getReturnValue();
                component.set("v.opptyProducts", result);
                
                for(var i=0; i<result.length; i++){
    		        for(var j=0; j<selectedList.length; j++){
    		            if(selectedList[j].product.Id == result[i].product.Id){
    		                result[i].isSelect = true;
    		                result[i].qty = selectedList[j].qty;
    		            }
    		        }
    		    }
                console.log(a.getReturnValue());
            });
            
            $A.enqueueAction(action);
	    }
	},
})