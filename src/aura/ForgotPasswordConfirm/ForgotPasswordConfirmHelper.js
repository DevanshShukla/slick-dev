({
    doInit: function(component, event, helper) {
        var x = window.location.search;
        var y = x.split('=');
        component.set("v.test", y[1]);
        console.log(component.get("v.test"));
    },


    resetpass: function(component, event, helper) {
        var action = component.get("c.reset");
        action.setParams({
            'password': component.get("v.newpassword"),
            'urlid': component.get("v.test")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(response.getState());
            console.log(response.getReturnValue());
            if (state === "SUCCESS") {
                if (response.getReturnValue() == 'true') {
                    document.getElementById("success").style.display = "block";
                    document.getElementById("link").style.display = "block";
                    document.getElementById("same").style.display = "none";
                    document.getElementById("form").style.display = "none";
                } else if (response.getReturnValue() == 'same') {
                    document.getElementById("same").style.display = "block";
                } else if (response.getReturnValue() == 'false') {
                    document.getElementById("error").style.display = "block";
                    document.getElementById("success").style.display = "none";
                    document.getElementById("same").style.display = "none";
                }
            } else {
                document.getElementById("error").style.display = "block";
            }
        });
        $A.enqueueAction(action);
    },


    matchpass: function(component, event, helper) {
        var pass = component.get("v.newpassword");
        var conpass = component.get("v.confirmpassword");

        if (component.get("v.password") == null || component.get("v.password") == '' || component.get("v.confirmpassword") == null || component.get("v.confirmpassword") == '') {
            component.set("v.disabled", 'true');
        }
        if (pass == conpass) {
            component.set("v.text", "Confirm Password (Match)");
            component.set("v.disabled", 'false');
        }
        if (pass != conpass) {
            component.set("v.text", "Confirm Password (MisMatch)");
            component.set("v.disabled", 'true');
        }
    }
})