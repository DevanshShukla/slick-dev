({
    
    doInit: function(component, event, helper) {
        
    },
    save: function(component, event, helper) {
        var action = component.get("c.register");
        console.log('helper');
        action.setParams({
            'fn': component.get("v.firstname"),
            'ln': component.get("v.lastname"),
            'em': component.get("v.email"),
            'pw': component.get("v.password")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                if(response.getReturnValue() == "true"){
                    document.getElementById("success").style.display = "block";
                    document.getElementById("link").style.display = "block";
                    document.getElementById("form").style.display = "none";
                    document.getElementById("error").style.display = "none";                 
                }
                if(response.getReturnValue() =="false") {
                    document.getElementById("error").style.display = "block";
                }
                if(response.getReturnValue() == "true1"){
                    document.getElementById("success1").style.display = "block";
                    document.getElementById("link").style.display = "block";
                    document.getElementById("form").style.display = "none";
                    document.getElementById("error").style.display = "none";
                }
            }else{
                window.alert('Something went worng');
                document.getElementById("success1").style.display = "none";
                document.getElementById("link").style.display = "none";
                document.getElementById("error").style.display = "none";
            }
        });
        $A.enqueueAction(action);
    },
    matchpass: function(component, event, helper) {
        var pass = component.get("v.password");
        var conpass = component.get("v.confirmpassword");
        
        if (component.get("v.password") == null || component.get("v.password") == '' || component.get("v.confirmpassword") == null || component.get("v.confirmpassword") == '') {
            component.set("v.disabled", 'true');
        }
        if (pass == conpass) {
            component.set("v.text", "Confirm Password (Match)");
            component.set("v.disabled", 'false');
        }
        if (pass != conpass) {
            component.set("v.text", "Confirm Password (MisMatch)");
            component.set("v.disabled", 'true');
        }
        if (pass == "") {
            component.set("v.text", "Confirm Password");
            component.set("v.disabled", 'true');
        }
        
    }
    
})