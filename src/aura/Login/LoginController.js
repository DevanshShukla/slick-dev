({
    doInit: function(component, event, helper) {
        helper.doInit(component, event, helper);
    },
    login: function(component, event, helper) {
        helper.login(component, event, helper);
    },
    validate: function(component, event, helper) {
        helper.validate(component, event, helper);
    },
    doLogout: function(component, event, helper) {
        helper.doLogout(component, event, helper);
    }
})