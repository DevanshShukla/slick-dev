({
    doInit: function(component, event, helper) {

    },
    login: function(component, event, helper) {

    },
    validate: function(component, event, helper) {
        var user = component.get("v.username");
        var pass = component.get("v.password");

        var x = component.find("password").get("v.validity");
        var y = component.find("username").get("v.validity");
        console.log(x.valid);

        if (user == null || user == '' || pass == null || pass == '') {
            component.set("v.disabled", "true");
        } else if (x.valid == true && y.valid == true) {
            component.set("v.disabled", "false");
            console.log("true");
        }
    },
    doLogout: function(component, event, helper) {

    }
})