({
    doInit : function(component, event, helper) {
        var action = component.get('c.getData');
        action.setParams({
            ProductId : component.get('v.ProductId')
        });
        action.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
                // alert('Success');
                component.set('v.Product',response.getReturnValue());
                console.log(response.getReturnValue());
            }else{
                alert('Fail');
            }
        });
        $A.enqueueAction(action);
    }
})