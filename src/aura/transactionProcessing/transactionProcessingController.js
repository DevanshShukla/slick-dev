({
    init: function(component, event, helper) {
        
        helper.getPickListValues(component, event, helper, 'Opportunity', 'Slick__Payment_Method__c', 'picklist1');
        helper.getPickListValues(component, event, helper, 'Opportunity', 'Slick__Payment_Status__c', 'picklist2');
        helper.fetchPositions(component, event, helper);
    },
    fetch: function(component, event, helper) {
        helper.fetchPositions(component, event, helper);
    },
    handleSelect: function(component, event, helper) {
        var selectedRows = event.getParam('selectedRows');
        var setRows = [];
        for (var i = 0; i < selectedRows.length; i++) {
            
            setRows.push(selectedRows[i]);
            
        }
        component.set("v.selectedpstns", setRows);
        var records = component.get("v.selectedpstns");
        component.set("v.totalselected", records.length);
    }
    
})