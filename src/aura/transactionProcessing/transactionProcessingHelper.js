({
    fetchPositions: function(component, event, helper) {
        var action = component.get("c.fetchPostns");
        //var v1 = component.find("a3").get("v.value");
        console.log(component.get("v.select"));
        console.log(component.get("v.select1"));
        console.log(component.get("v.select2"));
        console.log(component.get("v.select3"));
        console.log(component.get("v.select4"));
        action.setParams({
            'OS': component.get("v.select"),
            DD: component.get("v.select1"),
            OD: component.get("v.select2"),
            'PM': component.get("v.select3"),
            'PS': component.get("v.select4")
        });
        component.set('v.columns', [
            { label: 'Order', fieldName: 'linkName', type: 'url', typeAttributes: { label: { fieldName: 'Name' }, target: '_blank', tooltip: 'Click to open Record' } },
            { label: 'Order Source', fieldName: 'Slick__OrderSource__c', Type: 'Picklist' },
            { label: 'Customer Name', fieldName: 'Slick__Contact__c', Type: 'Text' },
            { label: 'Charge Amount', fieldName: 'Slick__Charge_Amount__c', Type: 'Currency' },
            { label: 'Total Amount', fieldName: 'Slick__TotalDiscountTaxOther__c', Type: 'Currency' },
            { label: 'Delivery Date', fieldName: 'Slick__Delivery_Date__c', Type: 'Date' },
            { label: 'Order Date', fieldName: 'Slick__Order_Date__c', Type: 'Date' },
            { label: 'Payment Method', fieldName: 'Slick__Payment_Method__c', Type: 'Picklist' },
            { label: 'Credit Card USed', fieldName: 'Slick__Credit_Card_Used__c', Type: 'Text' },
            { label: 'Has Critical Note', fieldName: 'Slick__Has_Critical_Note__c', Type: 'CheckBox' },
            { label: 'Payment Status', fieldName: 'Slick__Payment_Status__c', Type: 'Picklist' },
            { label: 'Route', fieldName: 'Slick__Route__c', Type: 'text' }



        ]);
        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {

                console.log(response.getReturnValue());
                var records = response.getReturnValue();
                records.forEach(function(record) {


                    record.linkName = '/' + record.Id;

                });

                component.set("v.positionlist", records);
                component.set("v.total", records.length);


            }

        });
        $A.enqueueAction(action);
    },

    getPickListValues: function(component, event, helper, objectName, FieldName, AttributName) {
        var status = component.get("c.getPickListValues");
        status.setParams({
            'ObjName': objectName,
            'FieldName': FieldName
        });
        status.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            console.log(response.getReturnValue());
            component.set("v." + AttributName, allValues);
        });
        $A.enqueueAction(status);
    },

})