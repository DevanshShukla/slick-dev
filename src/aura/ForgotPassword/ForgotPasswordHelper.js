({
    forgotpass: function(component, event, helper) {
        var action = component.get("c.resetmail");
        console.log(component.get("v.email"));

        action.setParams({
            'email': component.get("v.email")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                if (response.getReturnValue() == 'true') {
                    document.getElementById("success").style.display = "block";
                    document.getElementById("error").style.display = "none";
                    document.getElementById("form").style.display = "none";
                } else if (response.getReturnValue() == 'false') {
                    document.getElementById("error").style.display = "block";
                }
            }
        });
        $A.enqueueAction(action);
    },
    emailvalid: function(component, event, helper) {
        if (component.get("v.email") == null || component.get("v.email") == '') {
            component.set("v.disabled", 'true');
        } else {
            component.set("v.disabled", 'false');
        }
    }

})