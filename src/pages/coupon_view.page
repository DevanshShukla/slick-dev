<apex:page standardController="Coupon__c" extensions="couponEdit_Extension">
    <apex:sectionheader title="Coupon " subtitle="{!Coupon__c.name}" rendered="{!ISBLANK(Coupon__c.name)==false}"/>
    <apex:sectionheader title="Coupon " subtitle="New Coupon" rendered="{!ISBLANK(Coupon__c.name)==true}"/>
    <apex:form >
        <apex:pageblock title="General Information">
            <apex:pageblockbuttons location="top">
                <apex:commandbutton value="Edit" action="{!URLFOR($Action.Slick__Coupon__c.edit, Slick__coupon__c.id)}"/>
                <apex:commandbutton value="Delete" action="{!delete}"/>
            </apex:pageblockbuttons>
            <apex:pageblocksection >
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="RecordType Name"/>
                    <apex:outputfield value="{!Coupon__c.RecordType.Name}"/>
                </apex:pageBlockSectionItem>
                 <apex:outputtext value=""/>
                <apex:outputfield value="{!Coupon__c.Customer__c}" rendered="{!r.name != 'Standard Coupon'}"/>
                <apex:outputtext rendered="{!r.name != 'Standard Coupon'}"/>
                <apex:outputfield value="{!Coupon__c.name}"/>
                <apex:outputtext value=""/>
                <apex:outputfield value="{!Coupon__c.Description__c}"/>
                <apex:outputtext value=""/>
                <apex:outputfield value="{!Coupon__c.Coupon_Type__c}">
                    <apex:actionsupport event="onchange" rerender="specifics"/>
                </apex:outputfield>
                <apex:outputtext value=""/>
                <apex:outputfield value="{!Coupon__c.Start_Date__c}"/>
                <apex:outputtext value=""/>
                <apex:outputfield value="{!Coupon__c.Expiration_Date__c}"/>
                <apex:outputtext value=""/>
                <apex:outputfield value="{!Coupon__c.Apply_Automatically__c}"/>
                <apex:outputtext value=""/>
                <apex:outputfield value="{!Coupon__c.Not_useable_with_auto_discount__c}"/>
                <apex:outputtext value=""/>
                <apex:outputfield value="{!Coupon__c.Max_Uses__c}"/>
                <apex:outputtext value=""/>
                <apex:outputfield value="{!Coupon__c.Max_Individual_Uses__c}" rendered="{!r.name != 'New Customer'}"/>
                <apex:outputtext value=""/>
                 <apex:outputfield value="{!Coupon__c.Times_Used__c}" rendered="{!r.name != 'New Customer'}"/>
                 <apex:outputtext value=""/>
                <apex:outputtext value="" rendered="{!r.name != 'New Customer'}"/>
                <apex:pageblocksectionitem >
                    <apex:outputtext value=""/>
                    <apex:outputtext value="Leave max uses blank for automatically applied coupons unless there really should be a limit."/>
                </apex:pageblocksectionitem>
            </apex:pageblocksection>
            <br/>
            <apex:outputpanel id="specifics">
                <apex:pageblock title="BOGO" id="BOGO_opts" rendered="{!Coupon__c.Coupon_Type__c == 'BOGO'}">   
                    <apex:pageblocksection >
                        <apex:outputfield value="{!Coupon__c.Product__c}"/>
                        <apex:outputtext value=""/>
                        <apex:outputfield value="{!Coupon__c.Free_Product__c}"/>
                        <apex:outputtext value=""/>
                        <apex:pageblocksectionitem >
                            <apex:outputtext value=""/>
                            <apex:outputtext escape="false" value="The cost of the free product will be deducted from the order total <br/>if both products are present in cart."/>
                        </apex:pageblocksectionitem>
                    </apex:pageblocksection>
                </apex:pageblock>
                    
                <apex:pageblock title="Entire Order" rendered="{!Coupon__c.Coupon_Type__c == 'Entire Order'}">
                    <apex:pageblocksection >
                        <apex:outputfield value="{!Coupon__c.Discount_Type__c}">
                            <apex:actionsupport event="onchange" rerender="selectWhole"/>
                        </apex:outputfield>
                        <apex:outputpanel id="selectWhole">
                            <apex:pageblocksectionitem >
                                <apex:outputlabel value="{!$ObjectType.Coupon__c.fields.Apply_to_Entire_Order__c.label}" rendered="{!Coupon__c.Discount_Type__c == 'Percent Off'}"/>
                                <apex:outputfield value="{!Coupon__c.Apply_to_Entire_Order__c}" rendered="{!Coupon__c.Discount_Type__c == 'Percent Off'}"/>
                            </apex:pageblocksectionitem>
                        </apex:outputpanel>
                        <apex:outputfield value="{!Coupon__c.Discount_Amount__c}"/>
                        <apex:outputtext value=""/>
                        <apex:outputfield value="{!Coupon__c.Min_Order_Amnt_for_Target__c}"/>
                    </apex:pageblocksection>
                </apex:pageblock>   
                    
                <apex:pageblock title="Product Family" rendered="{!Coupon__c.Coupon_Type__c == 'Product Family'}">
                    <apex:pageblocksection >
                        <apex:outputfield value="{!Coupon__c.Discount_Type__c}">
                            <apex:actionsupport event="onchange" rerender="selectWhole"/>
                        </apex:outputfield>
                        <apex:outputpanel id="selectWhole">
                            <apex:pageblocksectionitem >
                                <apex:outputlabel value="{!$ObjectType.Coupon__c.fields.Apply_to_Entire_Order__c.label}" rendered="{!Coupon__c.Discount_Type__c == 'Percent Off'}"/>
                                <apex:outputfield value="{!Coupon__c.Apply_to_Entire_Order__c}" rendered="{!Coupon__c.Discount_Type__c == 'Percent Off'}"/>
                            </apex:pageblocksectionitem>
                        </apex:outputpanel>
                        <apex:outputfield value="{!Coupon__c.Discount_Amount__c}"/>
                        <apex:outputtext value=""/>
                        <apex:outputfield value="{!Coupon__c.Product_Family__c}"/>
                        <apex:outputtext value=""/>
                        <apex:outputfield value="{!Coupon__c.Quantity_Required__c}"/>   
                        <apex:outputtext value=""/>
                        <apex:outputfield value="{!Coupon__c.Min_Order_Amnt_for_Target__c}"/>
                    </apex:pageblocksection>
                </apex:pageblock>   
                    
                <apex:pageblock title="Product Group" rendered="{!Coupon__c.Coupon_Type__c == 'Product Group'}">
                    <apex:pageblocksection >
                        <apex:outputfield value="{!Coupon__c.Discount_Type__c}">
                            <apex:actionsupport event="onchange" rerender="selectWhole"/>
                        </apex:outputfield>
                        <apex:outputpanel id="selectWhole">
                            <apex:pageblocksectionitem >
                                <apex:outputlabel value="{!$ObjectType.Coupon__c.fields.Apply_to_Entire_Order__c.label}" rendered="{!Coupon__c.Discount_Type__c == 'Percent Off'}"/>
                                <apex:outputfield value="{!Coupon__c.Apply_to_Entire_Order__c}" rendered="{!Coupon__c.Discount_Type__c == 'Percent Off'}"/>
                            </apex:pageblocksectionitem>
                        </apex:outputpanel>
                        <apex:outputfield value="{!Coupon__c.Discount_Amount__c}"/>
                        <apex:outputtext value=""/>
                        <apex:outputfield value="{!Coupon__c.Product_Group__c}"/>
                        <apex:outputtext value=""/>                     
                        <apex:outputfield value="{!Coupon__c.Quantity_Required__c}"/>   
                        <apex:outputtext value=""/>
                        <apex:outputfield value="{!Coupon__c.Min_Order_Amnt_for_Target__c}"/>
                    </apex:pageblocksection>
                </apex:pageblock>   
                    
                <apex:pageblock title="Specific Product" rendered="{!Coupon__c.Coupon_Type__c == 'Specific Product'}">
                    <apex:pageblocksection >
                        <apex:outputfield value="{!Coupon__c.Discount_Type__c}">
                            <apex:actionsupport event="onchange" rerender="selectWhole"/>
                        </apex:outputfield>
                        <apex:outputpanel id="selectWhole">
                            <apex:pageblocksectionitem >
                                <apex:outputlabel value="{!$ObjectType.Coupon__c.fields.Apply_to_Entire_Order__c.label}" rendered="{!Coupon__c.Discount_Type__c == 'Percent Off'}"/>
                                <apex:outputfield value="{!Coupon__c.Apply_to_Entire_Order__c}" rendered="{!Coupon__c.Discount_Type__c == 'Percent Off'}"/>
                            </apex:pageblocksectionitem>
                        </apex:outputpanel>
                        <apex:outputfield value="{!Coupon__c.Discount_Amount__c}"/>
                        <apex:outputtext value=""/>
                        <apex:outputfield value="{!Coupon__c.Product__c}"/>
                        <apex:outputtext value=""/>
                        <apex:outputfield value="{!Coupon__c.Quantity_Required__c}"/>   
                        <apex:outputtext value=""/>
                        <apex:outputfield value="{!Coupon__c.Min_Order_Amnt_for_Target__c}"/>
                    </apex:pageblocksection>
                </apex:pageblock>
                
                    
                <apex:pageblocksection rendered="false">
                    <apex:outputfield value="{!Coupon__c.Used__c}"/>
                    <apex:outputfield value="{!Coupon__c.Times_Used__c}"/>
                </apex:pageblocksection>    
            </apex:outputpanel>
        </apex:pageblock>
    </apex:form>
    <apex:relatedList list="Slick__Coupon_Users__r">   </apex:relatedList>
</apex:page>