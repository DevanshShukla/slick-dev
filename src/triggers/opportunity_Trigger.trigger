trigger opportunity_Trigger on Opportunity (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    Instance_Setting__c disable_Triggers_Setting=(test.isRunningTest())?(new Instance_Setting__c(name='Disable Triggers', text_value__c = 'false')):
    ((Instance_Setting__c.getInstance('Disable Triggers')));
    boolean disableTriggers=false;
    if(disable_Triggers_Setting!=null)
        disableTriggers=disable_Triggers_Setting.text_value__c=='true'?true:false;
        
    if (trigger.isBefore && trigger.isInsert && disableTriggers==false){
        opportunity_Methods.beforeInsert(trigger.new);  
    }
    if (trigger.isAfter && trigger.isUpdate && disableTriggers==false){
        opportunity_Methods.afterupdate(trigger.old,trigger.new);   
    }
    
    if (trigger.isAfter && trigger.isInsert && disableTriggers==false){
        opportunity_Methods.afterInsert(trigger.new);
    }
    if (trigger.isAfter && trigger.isDelete && disableTriggers==false){
        opportunity_Methods.afterDelete(trigger.old);
    }
    if (trigger.isAfter && trigger.isUnDelete && disableTriggers==false){
        opportunity_Methods.afterDelete(trigger.new);
    }
}