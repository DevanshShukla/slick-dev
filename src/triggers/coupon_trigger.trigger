trigger coupon_trigger on Coupon__c (before insert,after insert) {
	if (trigger.isbefore && (trigger.isinsert || trigger.isUpdate)) {
		coupon_Methods.beforeUpsert(trigger.new,trigger.old);
	}
	
	if (trigger.isafter && trigger.isinsert){
		coupon_Methods.afterInsert(trigger.new);
	}
}