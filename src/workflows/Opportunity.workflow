<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Regular_Order_Confirmation_Email_Alert</fullName>
        <description>Regular Order Confirmation Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>dharmik@mvclouds.com.nschicago</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Customer_Emails/VF_Updated_Order_Confirmation</template>
    </alerts>
    <alerts>
        <fullName>Send_Customer_Email</fullName>
        <description>Send Customer Email</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Denied_CC_Alert_Emails/Credit_Card_Declined_Customer_Alert</template>
    </alerts>
    <alerts>
        <fullName>Update_Internal_Users</fullName>
        <description>Update Internal Users</description>
        <protected>false</protected>
        <recipients>
            <recipient>All_Internal_Users</recipient>
            <type>group</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Denied_CC_Alert_Emails/Credit_Card_Declined_Distributor_Update</template>
    </alerts>
    <fieldUpdates>
        <fullName>First_Order_for_this_Customer</fullName>
        <description>updates field First Order this Customer to true if new customer</description>
        <field>First_order_for_this_Contact__c</field>
        <literalValue>1</literalValue>
        <name>First Order for this Customer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Delivery_Date</fullName>
        <field>Delivery_Date__c</field>
        <formula>Alternate_Delivery_Date__c</formula>
        <name>Update Delivery Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Fulfillment_Status_FE</fullName>
        <field>Fulfillment_Status__c</field>
        <literalValue>Open</literalValue>
        <name>Update Fulfillment Status FE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Order Confirmation Email</fullName>
        <actions>
            <name>Regular_Order_Confirmation_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Confirmation_Sent__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Customer %26 Owner to Denied CC</fullName>
        <actions>
            <name>Send_Customer_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Internal_Users</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Charge_Status__c</field>
            <operation>equals</operation>
            <value>5.1</value>
        </criteriaItems>
        <description>Sends emails to the Order Owner &amp; related contact. Informing each to &apos;Void&apos; credit cards.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Delivery Date</fullName>
        <actions>
            <name>Update_Delivery_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Alternate_Delivery_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update First Order this Contact</fullName>
        <actions>
            <name>First_Order_for_this_Customer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>updates the field First Order this Contact to true if new customer, duplicate customer records, not having Account name field filled out, and using alternate delivery date can cause issues</description>
        <formula>Contact__r.First_Delivery_Date__c = Delivery_Date__c</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Fulfillment Status Front End</fullName>
        <actions>
            <name>Update_Fulfillment_Status_FE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Name</field>
            <operation>contains</operation>
            <value>Cart</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Fulfillment_Status__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
