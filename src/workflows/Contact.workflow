<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Customer_Email_Welcome</fullName>
        <description>New Customer Email Welcome</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ROP_Templates/New_Customer_1_Day_Welcome</template>
    </alerts>
    <alerts>
        <fullName>One_Click_Done_EM_Alert</fullName>
        <description>One Click Done EM Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>dharmik@mvclouds.com.nschicago</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ROP_Templates/One_Click_Done_Request_Alert</template>
    </alerts>
    <alerts>
        <fullName>ROP_Call_Request_Alert</fullName>
        <description>ROP Call Request Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>dharmik@mvclouds.com.nschicago</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ROP_Templates/ROP_Request_a_Call_Alert</template>
    </alerts>
    <alerts>
        <fullName>ROP_Email</fullName>
        <description>ROP Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>ROP_Templates/ROP_1</template>
    </alerts>
    <alerts>
        <fullName>Welcome_Email_For_User</fullName>
        <description>Welcome Email For User</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Welcome_User</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_NextOrderDate</fullName>
        <field>Next_Order_Date__c</field>
        <formula>Last_Delivery_Date__c +(7* Frequency__c )</formula>
        <name>Update NextOrderDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Autoclone_OCD</fullName>
        <description>make the autoclone ocd field checked</description>
        <field>Autoclone_OCD__c</field>
        <literalValue>1</literalValue>
        <name>update Autoclone OCD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Autoclone OCD Checked</fullName>
        <actions>
            <name>update_Autoclone_OCD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Checks the Autoclone OCD field if OCD field is changed</description>
        <formula>ISCHANGED(One_Click_Done_Order_Request__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Call Requested</fullName>
        <actions>
            <name>ROP_Call_Request_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>ROP_Call_Request</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Call_Requested__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Customer Follow Up</fullName>
        <actions>
            <name>NC_Check_Up_Call_2</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>NC_Welcome_Call_1</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.First_Delivery_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>New_Customer_Email_Welcome</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Contact.First_Delivery_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>OCD Error</fullName>
        <actions>
            <name>Check_Customer_CC</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Autoclone_OCD__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Last_Date_Ordered__c</field>
            <operation>notEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.One_Click_Done_Order_Request__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>checks to see if an order was made</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>One Click Done Order</fullName>
        <actions>
            <name>One_Click_Done_EM_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>One_Click_Done_Order_Create</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.One_Click_Done_Order_Request__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>When customer clicks the One Click Done link in ROP email this sets up a task to create an order.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ROP Call - 3 Day</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Last_Delivery_Date__c</field>
            <operation>greaterOrEqual</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Creates a task to call customer at 3 days before order. Can change time to 2, etc.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>ROP_3_Day</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Contact.Next_Order_Date__c</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>ROP Email</fullName>
        <actions>
            <name>ROP_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Low_on_pet_food_Easy_Reorder_Links_from_Nature_s_Select</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( ROP_Trigger_Field__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set NextOrderDate</fullName>
        <actions>
            <name>Update_NextOrderDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Frequency_Trigger__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>When the Freq Weeks or Delivery Date are changed and Freq Trigger is set to Today, will recalculate the Next Order Date field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Check_Customer_CC</fullName>
        <assignedTo>dharmik@mvclouds.com.nschicago</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Check the customer&apos;s cc because no order was created.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Contact.One_Click_Done_Order_Request__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Check Customer CC</subject>
    </tasks>
    <tasks>
        <fullName>Low_on_pet_food_Easy_Reorder_Links_from_Nature_s_Select</fullName>
        <assignedTo>dharmik@mvclouds.com.nschicago</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Sent ROP Email</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Low on pet food - Easy Reorder Links from Nature’s Select</subject>
    </tasks>
    <tasks>
        <fullName>NC_Check_Up_Call_2</fullName>
        <assignedTo>dharmik@mvclouds.com.nschicago</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>15</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Contact.First_Delivery_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>NC Check Up Call #2</subject>
    </tasks>
    <tasks>
        <fullName>NC_Welcome_Call_1</fullName>
        <assignedTo>dharmik@mvclouds.com.nschicago</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Contact.First_Delivery_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>NC Welcome Call #1</subject>
    </tasks>
    <tasks>
        <fullName>One_Click_Done_Order_Create</fullName>
        <assignedTo>dharmik@mvclouds.com.nschicago</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Contact.One_Click_Done_Order_Request__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>One Click Done Order Create</subject>
    </tasks>
    <tasks>
        <fullName>ROP_3_Day</fullName>
        <assignedTo>dharmik@mvclouds.com.nschicago</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Contact.Next_Order_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>ROP 3 Day</subject>
    </tasks>
    <tasks>
        <fullName>ROP_Call_Request</fullName>
        <assignedTo>dharmik@mvclouds.com.nschicago</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>ROP Call Request</subject>
    </tasks>
</Workflow>
